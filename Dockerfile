FROM python:3.13-alpine

RUN apk update && apk add gcc musl-dev git

WORKDIR /workspace/
COPY requirements.txt .
RUN pip install -r requirements.txt

# We use git to determine the edit history fo pages to provide a "last updated" timestamp. Work
# around a git feature which guards against certain types of attacks from untrusted users.
#
# More information: https://www.kenmuse.com/blog/avoiding-dubious-ownership-in-dev-containers/
RUN git config --global --add safe.directory /workspace

CMD ["mkdocs",  "serve", "-a", "0.0.0.0:8000"]
