"""
Macros for diagram / figure generation.
"""

from mkdocs_macros.plugin import MacrosPlugin

def define_env(env: MacrosPlugin):
    env.macro(downloadable_figure)

def downloadable_figure(path: str, caption: str):
    file_name = path.split("/")[-1]
    return f'''
<figure markdown>
![]({path})
<figcaption markdown>
{caption}
([download]({path}){{:download="{file_name}"}})
</figcaption>
</figure>
        '''
