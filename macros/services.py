"""
Macros for service information.
"""

from mkdocs_macros.plugin import MacrosPlugin
import re
from . import render
from .config import service_config
from .utils import variables

_ENV = None

def define_env(env: MacrosPlugin):
    global _ENV
    _ENV = env
    env.macro(service_title)
    env.macro(service_management)
    env.macro(all_teams_services)
    env.macro(tabbed_services_view)

def _service_config(
    _pattern=re.compile(r"/?services/(?P<service_key>[^/\.]+)"),
) -> dict:
    # Get info about the current .md page being rendered
    page = variables(_ENV).get("page", {})
    # The page url here is a URI like one of:
    # - '/services/my-service/'
    # - 'services/web-recruitment-system.html'
    page_name = _pattern.match(page.url).group("service_key")
    # For page name like 'my-service(.md)'
    # retrieve services.yml > services > my-service
    service = page_name
    return service_config.services[service]

def service_title() -> str:
    """
    Returns:
        str:
            Rendered service page title and sub-title pills.
    """
    return render.service_title(_ENV, _service_config())

def service_management() -> str:
    """
    Returns:
        str:
            Rendered service management section for service page.
    """
    return render.service_management(_ENV, _service_config())

def all_teams_services() -> str:
    """
    Returns:
        str:
            Rendered team sections inc. their services.
    """
    return render.all_teams_services(_ENV)

def tabbed_services_view() -> str:
    """
    Returns:
        str:
            Rendered DevOps Division Structure table.
    """
    return render.tabbed_services_view(_ENV)
