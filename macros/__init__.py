"""
Definition of macros used within the Guidebook via MkDocs Macros:

    https://mkdocs-macros-plugin.readthedocs.io/

Each macro in a Markdown (`.md`) file is invoked using `{{ macro_name(..args..) }}`
"""

import os
import re
import yaml

from mkdocs_macros.plugin import MacrosPlugin
from . import (diagram, gitlab, services, table, utils)
from .constants import TEMPLATE_VARIABLES

def define_env(env: MacrosPlugin):
    """
    Define custom macros, filters, variables in here using MkDocs Macros:
        https://mkdocs-macros-plugin.readthedocs.io/
    """
    # Add custom template variables to `constants.TEMPLATE_VARIABLES`
    # then they're all automatically included in the template here.
    env.variables.update(TEMPLATE_VARIABLES)
    # Load all context-specific macros.
    diagram.define_env(env)
    gitlab.define_env(env)
    services.define_env(env)
    table.define_env(env)
    utils.define_env(env)
