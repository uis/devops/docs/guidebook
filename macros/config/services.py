from __future__ import annotations
import os
from functools import cached_property as cached
from pydantic import BaseModel, Field, field_validator
from pydantic_settings import (
    BaseSettings,
    EnvSettingsSource,
    SettingsConfigDict,
    PydanticBaseSettingsSource,
    YamlConfigSettingsSource,
)
from urllib.parse import quote as urlquote, urljoin
from ..constants import DATA_DIR, LOOKUP_PERSON_BASE_URL, LOOKUP_TEAM_BASE_URL


class Model(BaseModel):

    @property
    def config(self) -> ServiceConfig:
        global service_config
        return service_config

class Person(Model):

    crsid: str

    @property
    def lookup_url(self) -> str:
        return urljoin(LOOKUP_PERSON_BASE_URL, urlquote(self.crsid))

    @classmethod
    def from_crsid(cls, crsid: str) -> Person:
        return cls(crsid=crsid)


class Portfolio(Model):
    title: str

    @classmethod
    def from_raw(cls, key: str) -> Portfolio:
        global service_config
        return service_config.portfolios[key]


class ServiceCategory(Model):
    title: str

    @classmethod
    def from_raw(cls, key: str) -> Portfolio:
        global service_config
        return service_config.service_categories[key]


class Team(Model):

    title: str
    portfolio_name: str|None = Field(alias="portfolio", default=None)
    shorthand: str|None = None
    email: str
    lead: Person|None = Field(default=None)
    lookup_id: str|None = Field(default=None)

    @field_validator("lead", mode="before")
    def str_to_person(cls, v):
        return Person.from_crsid(v)

    @property
    def portfolio(self) -> Portfolio:
        return Portfolio.from_raw(self.portfolio_name)

    @property
    def short_name(self) -> str:
        return self.shorthand or self.title.split(" ")[0]

    @cached
    def key(self) -> str|None:
        return next((k for k,v in self.config.teams.items() if v.title == self.title), None)

    @cached
    def services(self) -> dict[str, "Service"]:
        """
        Returns:
            dict[str, Service]:
                Subset of `ServiceConfig.services` whose `team_keys` include `self.key`,
                ordered by `Service.title`.
        """
        services = {k:v for k,v in self.config.services.items() if self.key in v.team_keys}
        return {k:v for k,v in sorted(services.items(), key=lambda kv: kv[1].title)}

    @property
    def lookup_url(self) -> str:
        return LOOKUP_TEAM_BASE_URL.format(lookup_group_id=self.lookup_id or f"uis-devops-{self.key}")


class Service(Model):

    title: str
    category_name: str|None = Field(alias="category", default=None)
    team_keys: list[str] = Field(alias="teams", default_factory=list)
    product_manager: Person|None = Field(alias="product-manager", default=None)
    service_manager: Person|None = Field(alias="service-manager", default=None)
    service_owner: Person|None = Field(alias="service-owner", default=None)
    tech_lead: Person|None = Field(alias="tech-lead", default=None)
    engineers: list[Person] = Field(alias="engineers", default=[])

    @field_validator("product_manager", "service_manager", "service_owner", "tech_lead",
                     mode="before")
    @classmethod
    def str_to_person(cls, v):
        return Person.from_crsid(v)

    @property
    def category(self) -> Portfolio:
        return ServiceCategory.from_raw(self.category_name)

    @field_validator("engineers", mode="before")
    @classmethod
    def strs_to_people(cls, vs):
        return list(map(Person.from_crsid, vs))

    @cached
    def teams(self) -> dict[str, Team]:
        """
        Returns:
            dict[str, Team]:
                Subset of `ServiceConfig.teams` whose `services` include `self`,
                ordered by `Team.title`.
        """
        teams = {k:v for k,v in self.config.teams.items() if k in self.team_keys}
        return {k:v for k,v in sorted(teams.items(), key=lambda kv: kv[1].title)}

    @cached
    def service_management_individuals(self) -> dict[str, str]:
        """
        Returns:
            dict:
                Service management individuals in left-to-right, top-to-bottom render order.
        """
        return dict(
            tech_lead=self.tech_lead,
            service_owner=self.service_owner,
            service_manager=self.service_manager,
            product_manager=self.product_manager,
        )


class ServiceConfig(BaseSettings):

    portfolios: dict[str, Portfolio]
    service_categories: dict[str, ServiceCategory]
    services: dict[str, Service]
    teams: dict[str, Team]

    @classmethod
    def settings_customise_sources(
        cls, settings_cls: BaseSettings, **kwargs
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            EnvSettingsSource(
                settings_cls,
                env_prefix="guidebook_",
            ),
            YamlConfigSettingsSource(
                settings_cls, yaml_file=os.path.join(DATA_DIR, "services.yml")),
        )

service_config = ServiceConfig()
