"""
Macros for table generation.
"""

from mkdocs_macros.plugin import MacrosPlugin
import re
from identitylib.identifiers import IdentifierSchemes
from .constants import TDA_APPROVED

def define_env(env: MacrosPlugin):
    env.macro(identifier_table)

def identifier_table(*, deprecated: bool):
    rows = [
        "| Scope | Common name | TDA |",
        "|-|-|:-:|",
    ]

    schemes = [
        getattr(IdentifierSchemes, a)
        for a in dir(IdentifierSchemes)
        if re.match("^[A-Z][A-Z0-9_]+$", a)
    ]
    schemes.sort(key=lambda s: s.identifier)

    for scheme in schemes:
        if not deprecated:
            rows.append(
                "".join(
                    [
                        f"| `{scheme.identifier}` | {scheme.common_name} |",
                        '<span style="display: none">yes</span> :material-check: |'
                        if scheme.identifier in TDA_APPROVED
                        else '<span style="display: none">no</span> |',
                    ]
                )
            )
        for k, v in scheme.aliases.items():
            if not deprecated and "deprecated" in k:
                continue
            rows.append(
                "".join(
                    [
                        f"| `{v}` | {scheme.common_name} |",
                        '<span style="display: none">yes</span> :material-check: |'
                        if v in TDA_APPROVED
                        else '<span style="display: none">no</span> |',
                    ]
                )
            )

    return "\n".join(rows)
