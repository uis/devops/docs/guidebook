import os

# Root directory of this project.
ROOT_DIR = os.path.dirname(os.path.dirname(__file__))

# Root data/ directory of this project.
DATA_DIR = os.path.join(ROOT_DIR, "data")

# Path to file containing current GitLab labels.
GITLAB_LABELS_FILE = os.path.join(DATA_DIR, "labels.yml")

# Text to render in pills & auto-generated sections
# when no value exists for that attribute/field.
NO_VALUE_TEXT = "TBC"

# Base Lookup URL onto which a person's CRSID is appended
# to create that person's Lookup URL.
LOOKUP_PERSON_BASE_URL = "https://www.lookup.cam.ac.uk/person/crsid/"

# Base Lookup URL used to create a team members membership page URL.
LOOKUP_TEAM_BASE_URL = "https://lookup.cam.ac.uk/group/{lookup_group_id}/members"

# A set of TDA-approved identifier scopes.
# Eventually this should be moved into identitylib.
TDA_APPROVED = {
    "v1.person.identifiers.cam.ac.uk",
    "person.v1.student-records.university.identifiers.cam.ac.uk",
    "person.v1.human-resources.university.identifiers.cam.ac.uk",
}

# Variables to auto-include in template environment.
TEMPLATE_VARIABLES = dict(
    annotation_icon = (
        '<span class="md-annotation__index" style="cursor: inherit;">'
        '<span data-md-annotation-id="1"></span>'
        "</span>"
    )
)

# URI under which all service pages are located.
SERVICES_PAGE_URI = "/services"

# Relative URI for teams summary page.
TEAMS_PAGE_URI = "/home/teams"
