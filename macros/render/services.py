"""
Render utilities for services.
"""

from mkdocs_macros.plugin import MacrosPlugin
from .base import badge, hyperlink, mailto
from ..config import service_config, Team, Service
from ..constants import (
    NO_VALUE_TEXT,
    TEAMS_PAGE_URI,
    SERVICES_PAGE_URI,
)

def _person_label_link(person: dict) -> str:
    """
    Returns:
        tuple:
            (link label, link to lookup page)
    """
    return person.crsid, person.lookup_url

def _team_label_link(env: MacrosPlugin, team: Team, use_email: bool=False) -> str:
    """
    Returns:
        tuple:
            (link label, link to Teams page or 'mailto:' team email address)
    """
    if use_email:
        return team.email, mailto(team.email)
    else:
        rel_url = f"site:{TEAMS_PAGE_URI}"
        destination = rel_url + "/#" + team.title.lower().replace(" ", "-")
        return team.title, destination

def _service_label_link(env: MacrosPlugin, service_key: str, service: Service):
    """
    Returns:
        str:
            HTML hyperlink to Guidebook service page.
    """
    return service.title, f"site:{SERVICES_PAGE_URI}/{service_key}"

def _person_key_label(service_key: str) -> str:
    """
    Returns:
        str:
            Human-readable badge label for the given person key on config model,
            e.g. "tech_lead" becomes "Tech Lead".
    """
    return " ".join([w.capitalize() for w in service_key.split("_")])

def _badge_class(person_key: str) -> str:
    return person_key.replace("_", "-")

def service_badges(env: MacrosPlugin, service: Service) -> str:
    """
    Macro to render service page sub-title pills.
    """
    output = ""

    # Process team(s)
    label = "Team"

    if teams := service.teams:
        for team_key, team in teams.items():
            value, link = _team_label_link(env, team)
            output += badge(label, value, link=link) + "\n"
    else:
        value = NO_VALUE_TEXT
        output += badge(label, value) + "\n"

    # Process Tech Lead, Service Owner, Service Manager, Product Manager
    for person_key, person in service.service_management_individuals.items():
        label = _person_key_label(person_key)
        badge_class = _badge_class(person_key)

        if person:
            value, link = _person_label_link(person)
            output += badge(label, value, link=link, extra_classes=[badge_class]) + "\n"
        else:
            value = NO_VALUE_TEXT
            output += badge(label, value, extra_classes=[badge_class]) + "\n"

    return output

def service_title(env: MacrosPlugin, service: Service) -> str:
    """
    Returns:
        str:
            Rendered service page title & sub-title pills.
    """
    return "\n".join([f"# {service.title}", service_badges(env, service)])

def service_management(env: MacrosPlugin, service: Service) -> str:
    """
    Returns:
        str:
            Rendered "Service Management" section
    """
    lines = ["## Service Management"]

    # Process team(s)
    team_values = []
    if teams := service.teams:
        for team_key, team in teams.items():
            #colours = team.get("colours", {})
            value, link = _team_label_link(env, team)
            team_values.append(hyperlink(value, link))
    else:
        team_values.append(NO_VALUE_TEXT)

    if len(team_values) > 1:
        lines.append(f"The **Teams** responsible for this service are:")
        lines.append("\n".join([f"- {v}" for v in team_values]))
    else:
        lines.append(f"The **Team** responsible for this service is {team_values[0]}.")

    # Process Tech Lead, Service Owner, Service Manager, Product Manager
    for person_key, person in service.service_management_individuals.items():
        label = _person_key_label(person_key)

        if person:
            #colours = person.get("colours", {})
            value, link = _person_label_link(person)
            value = hyperlink(value, link)
        else:
            value = NO_VALUE_TEXT

        lines.append(f"The **{label}** for this service is {value}.")

    if engineers := service.engineers:
        lines.append(" ".join([
            "The following engineers have operational experience with this service",
            "and are able to respond to support requests or incidents:",
        ]))

        engineer_values = []
        for person in engineers:
            value, link = _person_label_link(person)
            engineer_values.append(hyperlink(value, link))
        engineer_bullets = "\n".join([f"- {v}" for v in engineer_values])

        lines.append(engineer_bullets)

    return "\n\n".join(lines)

def all_teams_services(env: MacrosPlugin) -> str:
    """
    Returns:
        str:
            Rendered team sections inc. their services.
    """
    sections = []
    for team_key, team in service_config.teams.items():

        lines = []
        lines.append(f"## {team.title}\n")

        label = "Email"
        badge_class = "email"
        value, link = _team_label_link(env, team, use_email=True)
        lines.append(badge(label, value, link=link, extra_classes=[badge_class]))

        label = "Team Lead"
        badge_class = "team-lead"
        if team_lead := team.lead:
            value, link = _person_label_link(team_lead)
            lines.append(badge(label, team_lead.crsid, link=link, extra_classes=[badge_class]))
        else:
            lines.append(badge(label, NO_VALUE_TEXT, extra_classes=[badge_class]))

        label = "Team Members"
        badge_class = "team-members"
        lines.append(badge(label, "View", link=team.lookup_url, extra_classes=[badge_class]))

        if team.services:
            lines.append(f"{team.title} is responsible for the following services:\n\n")
            for service_key, service in team.services.items():
                value, link = _service_label_link(env, service_key, service)
                value = hyperlink(value, link)
                lines.append(f"- {value}")

        sections.append("\n".join(lines))

    return "\n\n".join(sections)

def tabbed_services_view(env: MacrosPlugin) -> str:
    sections = []
    indent = " " * 4

    info_by_service = {}
    links_by_service = {}
    services_by_title = {}
    #teams_by_title = {}

    for team_key, team in service_config.teams.items():
        lines = []
        #teams_by_title[team.title] = team

        title, dest = _team_label_link(env, team)
        team_link = f"[{title}]({dest})"

        service_links = []

        lines += [f"{team_link} is responsible for {len(team.services)} services.", ""]

        if team.services:
            lines += ["|IT Portfolio|Service Category|Service|", "|---|---|---|"]
            for service_key, service in team.services.items():
                value, link = _service_label_link(env, service_key, service)
                service_link = hyperlink(value, link)
                service_links.append(service_link)

                services_by_title[service.title] = service
                links_by_service[service.title] = service_link
                info_by_service[service.title] = dict(team=team)

                portfolio = team.portfolio.title
                category = service.category.title
                lines.append(f"|{portfolio}|{category}|{service_link}|{team_link}|")

        lines = [f'=== "{team.short_name}"'] + [indent + line for line in lines]
        section = "\n".join(lines)
        sections.append(section)

    lines = ["|IT Portfolio|Service Category|Service|Team|", "|---|---|---|---|"]
    for service_title in sorted(info_by_service.keys()):
        service = services_by_title[service_title]
        service_info = info_by_service[service_title]
        service_link = links_by_service[service_title]

        team = service_info["team"]
        portfolio = team.portfolio.title
        category = service.category.title

        title, dest = _team_label_link(env, team)
        team_link = f"[{title}]({dest})"
        lines.append(f"|{portfolio}|{category}|{service_link}|{team_link}|")

    lines = [f'=== "All (A-Z)"'] + [indent + line for line in lines]
    section = "\n".join(lines)
    sections.insert(0, section)

    return "\n".join(sections)
