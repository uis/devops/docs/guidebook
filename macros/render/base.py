"""
Generic render utilities.
"""

def mailto(email: str) -> str:
    return f"mailto:{email}"

def style(styles: dict=None) -> str:
    """
    Args:
        styles (dict):
            HTML "style" attribute values as <style>:<value> pairs.
    Returns:
        str:
            Style string for use in "style" HTML tag attribute.
    """
    if styles is None:
        return ""
    return " ".join(f'{k}: {v};' for k, v in styles.items())

def hyperlink(text: str, url: str, styles: dict=None) -> str:
    """
    NOTE: Does no escaping of url or styles.

    Args:
        text (str):
            Text to display in link.
        url (str):
            URL to link to.
        styles (dict):
            HTML "style" attribute values as <style>:<value> pairs.
    Returns:
        str:
            HTML hyperlink.
    """
    attrs = dict(
        href = url,
        style = style(styles),
        target = "_blank",
        rel="noopener noreferrer",
    )
    attrs = " ".join(f'{k}="{v}"' for k, v in attrs.items())
    return f'<a {attrs}>{text}</a>'

def gitlab_label(name, bg_colour="#444", text_colour="#fff", scope=None):
    """
    Renders a Gitlab-style label.

    Args:
        name (str):
            Name to display in pill.
        bg_colour (str, optional):
            Background colour of pill.
        text_colour (str, optional):
            Text colour of pill.
        scope (str):
            Optional scope for pill.
    """
    styles = {
        "--label-background-colour": bg_colour,
        "--label-text-colour": text_colour,
    }
    _style = style(styles)

    if scope:
        return (
            f'<span class="gl-label gl-label-scoped" style="{_style}">'
            f'<span class="gl-label-text">{scope}</span>'
            f'<span class="gl-label-text-scoped">{name}</span>'
            "</span>"
        )
    else:
        return (
            f'<span class="gl-label" style="{_style}">'
            f'<span class="gl-label-text">{name}</span>'
            "</span>"
        )

def badge(label, value, *, link=None, extra_classes=None):
    """
    Renders a badge.

    See https://mkdocs-badges.six-two.dev/badges/
    """
    components = [label, value]
    if link:
        components.append(f"link:{link}")
    for css_class in ["default"] + (extra_classes or []):
        components.append(f"class:{css_class}")
    return f"|{'|'.join(components)}|"
