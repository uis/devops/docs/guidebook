"""
Macros for Gitlab content.
"""

from mkdocs_macros.plugin import MacrosPlugin
import yaml
from . import render
from .constants import GITLAB_LABELS_FILE

def define_env(env: MacrosPlugin):
    env.macro(gitlab_label)

def _gitlab_labels() -> str|None:
    # Load a static list of current GitLab labels.
    # See data/README.md for how this file is generated.
    with open(GITLAB_LABELS_FILE) as fobj:
        return yaml.safe_load(fobj)
    return None

def gitlab_label(name: str) -> str:
    """Renders a GitLab-style label."""

    bg_colour = None
    text_colour = None

    try:
        # Try to find a matching label.
        label_dict = [label for label in _gitlab_labels() if label["name"] == name][0]
        bg_colour = label_dict["color"]
        text_colour = label_dict["text_color"]
    except IndexError:
        # We have no label, use default colours.
        pass

    scope = None
    if "::" in name:
        scope, name = name.split("::")

    return render.gitlab_label(
        name=name, bg_colour=bg_colour, text_colour=text_colour, scope=scope)
