"""Common macro utilities."""

from mkdocs_macros.plugin import MacrosPlugin
from urllib.parse import urljoin

def environment(env: MacrosPlugin) -> dict:
    """
    Useful for debugging & development.

    Returns:
        dict:
            The template environment include "variables".
    """
    return { name:getattr(env, name) for name in dir(env) if not name.startswith('_') }

def variables(env: MacrosPlugin) -> dict:
    """
    Useful for debugging & development.

    Returns:
        dict:
            Value of "variables" in `environment(env)`.
    """
    return environment(env).get("variables", {})

def rel_url(env: MacrosPlugin, rel_path: str) -> str:
    site_url = environment(env)["conf"]["site_url"]
    return urljoin(site_url, rel_path)
