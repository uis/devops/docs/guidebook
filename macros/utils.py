"""Common macro utilities."""

from mkdocs_macros.plugin import MacrosPlugin
from urllib.parse import urljoin

# Utilities:

def environment(env: MacrosPlugin) -> dict:
    """
    Useful for debugging & development.

    Returns:
        dict:
            The template environment include "variables".
    """
    return { name:getattr(env, name) for name in dir(env) if not name.startswith('_') }

def variables(env: MacrosPlugin) -> dict:
    """
    Useful for debugging & development.

    Returns:
        dict:
            Value of "variables" in `environment(env)`.
    """
    return environment(env).get("variables", {})

# Generic Macros:

_ENV = None

def define_env(env: MacrosPlugin):
    global _ENV
    _ENV = env
    env.macro(site_url)

def site_url() -> str:
    env = environment(_ENV)
    return env["conf"]["site_url"]
    #return environment(_ENV)["conf"]["site_url"]
