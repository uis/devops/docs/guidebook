# DevOps Guidebook

This repository contains the source for the UIS
[DevOps Guidebook](https://guidebook.devops.uis.cam.ac.uk/)

## Developing locally

The guidebook uses mkdocs to convert Markdown source into a static website.
You can run it from either the dockerised setup or your own local environment.

To begin development, [install pre-commit](https://pre-commit.com/index.html#install)
and clone the repo locally:

```console
git clone git@gitlab.developers.cam.ac.uk:uis/devops/docs/guidebook.git
pre-commit install
```

Start the guidebook via:

```console
docker-compose up
```

The local documentation is now available at <http://localhost:8000/>.

### Adding a page

1. Create a Markdown file in the relevant directory under `docs`.
2. Add this file's path to the `nav` configuration in `mkdocs.yml`.

## Hosting

The guidebook is hosted via GitLab pages. readthedocs.org (RTD) and is automatically deployed on
each commit to master. The GitLab pages-managed URL is at
[https://uis.uniofcam.dev/devops/docs/guidebook/](https://uis.uniofcam.dev/devops/docs/guidebook/).

To support a custom domain, the ``guidebook.devops.uis.cam.ac.uk`` DNS record is a CNAME pointing to
``uis.uniofcam.dev.io`` following the [custom domain
documentation](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/).

## Macros

The guidebook uses a set of macros to provide a consistent look and feel to the documentation.

Also to auto-generate certain information based on static configuration (`.yml`) files.

The [MkDocs-Macros plugin](https://mkdocs-macros-plugin.readthedocs.io/)
is used to simplify this process.

* It loads [main.py](./main.py) to access & apply all custom macros.
* Those are stored within [macros/](./macros/).
* To add a new macro, either add to or create a file in [macros/<context>.py](./macros/).

### Create a New Template Variable

If you want to add a new template variable,
simply define it in `TEMPLATE_VARIABLES` within [macros/constants.py](./macros/constants.py).

It will then be available within all `.md` files as `{{ my_variable }}`.

### Create a Macro Mixin

If adding macros for a new context, create a new file `macros/{my-context}.py`:

```python
from ..render import hyperlink # etc

def define_env(env):
  env.macro(my_macro)
  # ..wrap all other macros too..

def my_macro(...):
  pass # return render result here e.g. hyperlink(...)
```

Then import and invoke your module's `define_env` in `macros/__init__.py`:

```python
from . import (..., my_context, ...)

def define_env(env):
    # ...
    my_context.define_env(env)
    # ...
```

In similar fashion, add to [macros/render](./macros/render)
any needed context-specific HTML rendering _(see existing examples there)_.

Your macro(s) will then be available within all `.md` files as `{{ my_macro(..args..) }}`.

## Service Information

Service & team information is encoded in [services.yml](./data/services.yml).

When adding or updating a Service, you must update that file to define such information.

Comments in the file itself describe each section's format.

The file is exposed at endpoint `/api/services.yml` for consumption by other apps/services,
via a script in the [Gitlab CI config](./.gitlab-ci.yml).

**NOTE:**

* The canonical source of DevOps teams and team-service mappings is
  [this Google Doc][devops-team-structure].

* This YAML configuration simply encodes the information in a machine-parsable format,
  and must be manually updated to remain in-sync with that document.

<!-- markdownlint-disable MD013 -->
[devops-team-structure]: https://docs.google.com/document/d/1fNkYa5z0vY44aStN_67BjoAn8VJN8mvHhX-iip1y25g/edit#heading=h.gb6gzxjsa2r2
