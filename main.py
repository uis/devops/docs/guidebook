import os
import sys

# Add the current script's directory to sys.path
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_dir)

# Now we can import from ./macros
from macros import define_env
