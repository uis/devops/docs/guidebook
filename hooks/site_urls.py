"""
This plugin replaces all instances of `site:` in the `href`, `src`, and `data`
attributes of HTML tags with the site URL. This is useful when you want to
reference a file in the site's root directory, or relative to there,
but you don't know the site URL ahead of time.

Based on:
  - https://foosel.net/til/how-to-make-mkdocs-support-siteurl-relative-urls/
  - https://github.com/OctoPrint/mkdocs-site-urls/blob/main/mkdocs_site_urls/__init__.py
"""
import logging
import urllib.parse
import re
import mkdocs.plugins
from mkdocs.config import config_options as c
from mkdocs.config.defaults import MkDocsConfig

log = logging.getLogger("mkdocs")

_site_pattern = None

def site_pattern(
    config: MkDocsConfig,
    default_attrs: list[str] = ["href", "src", "data"],
) -> None:
    global _site_pattern
    if _site_pattern is None:
        attrs = config.get("attributes", default_attrs)
        _site_pattern = re.compile(
            r"(" + "|".join(attrs) + r')="site:([^"]+)"',
            re.IGNORECASE,
        )
    return _site_pattern

_regex = None
def on_pre_build(config: MkDocsConfig) -> None:
    global _site_pattern
    site_pattern(config)

@mkdocs.plugins.event_priority(50)
def on_page_content(html, page, config, files):

    global _site_pattern
    site_pattern(config)

    site_url = config["site_url"]
    use_directory_urls = config["use_directory_urls"]

    path = urllib.parse.urlparse(site_url).path

    if not path:
        path = "/"

    if not path.endswith("/"):
        path += "/"

    def _replace(match, use_directory_urls: bool = use_directory_urls):
        param = match.group(1)
        url = match.group(2)
        if url.startswith("/"):
            url = url[1:]

        if not use_directory_urls:
            if url.endswith("/"):
                url += "index.html"
            else:
                if url.split("/")[-1].find(".") == -1:
                    url += ".html"

        #log.info(f"REPLACING site:{match.group(2)} with {path}{url}")
        return f'{param}="{path}{url}"'

    return _site_pattern.sub(_replace, html)
