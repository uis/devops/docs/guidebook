---
title: Our workflow
---

!!! danger
    This section is now a little out of date as we have moved into a multi-team
    arrangement. This section will be updated after some consultation with the
    different teams.

# How we structure our work

This section provides an overview of the day-to-day workflow for a member of the
DevOps division.

## Sprints

We schedule our work in terms of fortnightly *sprints* which run Wednesday to
Wednesday. The middle Wednesday is reserved for backlog refinement and the final
Wednesday is reserved for scheduling the next sprint and for demoing work done
during the sprint.

## Demos

Sprint demos happen on the last Wednesday of the sprint and are public events.
They provide an opportunity for all team members to demo the work that had been
done that sprint. This can, and does, include non-technical work such as design
or documentation.

!!!important
    Only issues which have been closed should be demo-ed. In particular, work
    which has not yet been reviewed should not be demo-ed unless it is
    **required** as context for potential reviewers.

    Occasionally we may discuss issues which haven't been closed to provide
    context for people present at the demo and to flag blockers which have
    resulted in issues not being closed in a timely manner.

## Daily stand up meetings

Each morning the team has a stand up. The usual team stand up is 9:45 but some
product teams may also have their own stand ups around that time.

At the stand up each team member briefly states what they're currently working
on, what they're going to be working on and outlines any blockers which are
stopping them making progress.

!!!note
    On demo days there is no stand up meeting. This is partially to allow people
    to prepare for their demo and partially to avoid Wednesday's stand up being
    "...and today I will demo" and Thursday's being "yesterday, I demoed..." for
    all participants.

## Learning resources

If some of the technologies and tools referenced in this document are
unfamiliar, the following resources may be useful:

* [GitLab documentation](https://docs.gitlab.com/ee/README.html).
* [GitLab](https://www.linkedin.com/learning/continuous-delivery-with-gitlab)
    course on LinkedIn Learning.
* [Git for teams](https://www.linkedin.com/learning/git-for-teams) course on
    LinkedIn Learning.
* [Git-related courses](https://www.linkedin.com/learning/topics/git) on
    LinkedIn Learning.
