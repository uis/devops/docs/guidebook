---
title: Onboarding new starters
---

The following checklist is to be used as a reminder of the admin tasks needed to onboard a new
member to the DevOps team. These should be done after the University's onboarding tasks for any new
member of staff.

To track the completion of these tasks the manager or a peer should raise a [new starter issue in
the administration
project](https://gitlab.developers.cam.ac.uk/uis/devops/admin/-/issues/new?issuable_template=new_starter).
Assign both the line manager and new starter to this issue once created.

### Performed by manager or admin staff before start date

* Add to the corresponding [DevOps team
  (subgroup)](https://www.lookup.cam.ac.uk/group/uis-devops-division) within DevOps Division lookup
  group.
* Add to the [University Information Services](https://teams.microsoft.com/l/team/19%3ace6d44bf2ed34388b2d39db1bb39b294%40thread.tacv2/conversations?groupId=8b9ab893-3917-42bb-ba20-6cbd4bd2d304&tenantId=49a50445-bdfa-4b79-ade3-547b4f3986e9),
  [UIS DevOps](https://teams.microsoft.com/l/team/19%3ace6d44bf2ed34388b2d39db1bb39b294%40thread.tacv2/conversations?groupId=8b9ab893-3917-42bb-ba20-6cbd4bd2d304&tenantId=49a50445-bdfa-4b79-ade3-547b4f3986e9),
  and any other relevant teams on MS Teams.
* Add to the regular meetings they are expected to attend.

### Performed by manager or peer before start date

#### Gitlab

* Add to the [DevOps GitLab
  group](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/group_members) and other
  corresponding GitLab groups.
* Add as a viewer, or deployer as appropriate in [Team
  Data](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json)
  for [Google Cloud
  resources](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/-/blob/master/products.tf)

#### Google

When the user is added to the corresponding DevOps team in lookup, they should get access to:

* [Google Shared drive for DevOps](https://drive.google.com/drive/folders/0AJboYXmrsklAUk9PVA)
    and possibly other Shared drives relevant to the project(s) they will be working on.

#### Other services

* Invite to 1password and add to appropriate vaults.

### Performed by the new starter

Follow the instructions for the first time setup of [Raven](https://raven.cam.ac.uk/).

<!-- markdownlint-disable-next-line MD024 -->
#### Google

* Login to your Google acccount (use <your_crsid@cam.ac.uk> in google.com) and accept the T&Cs.
* Enable 2FA in your Google Account.

#### Microsoft

* Login to your Microsoft account (use <your_crsid@cam.ac.uk> in office.com) and accept the T&Cs. It
  may take some hours before it is ready.
* Enable 2FA in your Microsoft Account.
* Validate that your calendar (Outlook) and email (Exchange Online) is working correctly.

#### Mailing lists

Subscribe to appropriate lists by searching for them in [sympa
search](https://lists.cam.ac.uk/sympa/search_list_request). Note that with some you will need to
wait for approval.

* uis-service-automation
* uis-staff
* uis-individual-staff
* social (various, optional)

You will automatically be added to some mailing lists corresponding to your DevOps team (lookup subgroup).

#### SSH key

* Create a 4096 bit RSA key if you don't already have a preferred one
* Add your SSH public key to gitlab to allow for git over SSH
* Open Merge Requests as appropriate to add your key to the various resources:
    * [ansible-roles](https://gitlab.developers.cam.ac.uk/uis/devops/infra/ansible-roles/tree/master/roles/devops-ssh-users)
    which will only be needed if you are dealing with legacy systems

#### GPG

You may want to at least:

* Re-encrypt ansible vault passwords, see [setting up GPG to decrypt
  secrets](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/docs/secrets.md#setting-up-gpg-to-decrypt-secrets)
  for guidance
* Add public GPG key to [team public keyring](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/files/teampubkeys.gpg)
* Update the guidebook - [copy the updated file](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/-/blob/master/docs/secrets.md#updating-the-publicly-available-keyring)

There are more detailed instructions in the [general
docs](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/docs/secrets.md) to
add your GPG key.

#### Documentation

* [guidebook](https://guidebook.devops.uis.cam.ac.uk/en/latest/)
* [general search](https://search.cam.ac.uk/)
* [lookup people](https://www.lookup.cam.ac.uk/)
