# Our Tools

The DevOps division have created tools to speed up workflow for day-to-day tasks and other
operations having wider scope.

The canonical source of these tools is on [gitlab > Information Services > DevOps >
Tools](https://gitlab.developers.cam.ac.uk/uis/devops/tools)

## Ansible Role Fetcher

When an ansible playbook relies on shared ansible roles held elsewhere the ARF
can be used to set up your Ansible environment to automatically refer to a local
copy of this(-ese) role(s)

See the [Ansible Role Fetcher
project](https://gitlab.developers.cam.ac.uk/uis/devops/tools/ansibleroles) on
the Developers' Hub for instructions on use.

## Google Cloud SQL Backup

We host many of our databases in Google's Cloud SQL product. We have written a
[service](https://gitlab.developers.cam.ac.uk/uis/devops/infra/data-backup) to
scan all of our Google Cloud Projects and back any Cloud SQL databases found to
a Google Cloud Storage bucket in a central backup project.

See [the tool's service page](site:services/data-backup) page for more information.

## Mailtrap

For testing application which deliver email, we use
[Mailtrap.io](https://mailtrap.io).

Most users will be using Mailtrap as a 'sandbox' SMTP server, however an API is
available. Mailtrap also allows users to separate test mails in to project
reducing clutter and making is easier to identify your test mails.

If you require an account for mailtrap.io, please contact
<mailto:cloud@uis.cam.ac.uk>.

You can get more information about mailtrap.io from [their help pages](https://help.mailtrap.io/)
or [their API documentation](https://api-docs.mailtrap.io/docs/mailtrap-api-docs/5tjdeg9545058-mailtrap-api).

## Mailhog

!!! warning
    This service is being decommissioned. Please use [Mailtrap](#mailtrap).
    Mailhog may still be used as part of a docker compose environment.
    More information on this is available in the [tour of our development environment](site:notes/webapp-dev-environment#a-tour-of-our-setup).

For testing applications which deliver email, we have a
[Mailhog](https://github.com/mailhog/MailHog) instance
at `mailhog.devops.uis.cam.ac.uk`.

Mailhog is a mail server that only receives mail sent to it no matter who it is
sent to or from and makes the mail available to view through via a web fronted.
The mail is never delivered to it's supposed recipient. This is very useful for
testing.

The web frontend is protected by raven to only members of the
[UIS lookup group (ID: 101611)](https://www.lookup.cam.ac.uk/group/uis-members)

Mail can be sent to this from any app for testing by using

- Host: `mailhog.devops.uis.cam.ac.uk`
- Port: `9025`

The web frontend is available at [https://mailhog.devops.uis.cam.ac.uk](https://mailhog.devops.uis.cam.ac.uk).
