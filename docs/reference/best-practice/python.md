# Python

The DevOps division are heavy users of the Python programming language
for backend work.

## Code style

We have a number of [templates](site:reference/deployment/templates) which configures
automated code style checks using [flake8](https://flake8.pycqa.org/en/latest/) and
[black](https://black.readthedocs.io/en/stable/). The checks are enforced locally
via pre-commit hooks and in CI via a pre-commit job.

A step-by-step explanation of how to configure the checks added by our standard templates can be
found in the [the Python package
tutorial](site:tutorials/creating-a-python-package#catching-and-fixing-style-issues). A deeper
explanation of other concepts like Tests, Logging, Packaging, etc, can also be found on the [same
page](site:tutorials/creating-a-python-package).
