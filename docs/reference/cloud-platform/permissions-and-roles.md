# Permissions and Roles

This page documents how we manage personal and service account permissions.

## Permissions granted by gcp-product-factory

As part of the [product provisioning process](./gcp-folders.md), we have a
product folder and product meta project created automatically for each product.

A *product admin* service account is created in the meta project and is configured with
a specific set of required "admin" permissions over all projects in the product folder.
Additionally, the product admin can create new projects within the folder and associate
them with the University Billing Account.

!!! note

    To view the full list of roles assigned to a *product admin* service account see the
    `product_admin_sa_roles` local in the
    [gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/locals.tf#L73)
    project.

Access is granted to individual team members with one or more of the three levels. The roles
aren't mutually exclusive, so a user can have more than one of the roles if necessary. The
three levels are:

* **Viewer** These users have "viewer" rights over all projects in the product
    folder. They can inspect the deployment in the Google Cloud console but
    cannot see sensitive resources such as secrets. Giving someone "viewer"
    access is appropriate if they're involved in initial troubleshooting, need
    to see logs or are being shown around the deployment.
* **Deployer** These users have the ability to impersonate the environment-specific
    `terraform-deploy` service accounts. As such, they can run the infrastructure terraform configuration.
    Since they can impersonate these accounts, they can perform any actions
    the service accounts can but only via terraform or the `gcloud` utility; they
    cannot modify things in the Google Cloud Console and can only *see* things
    in the console if they are also "Viewer" users.
* **Editor** These users have the "editor" rights over all projects in the product
    folder. They can create, modify and delete resources for most Google Cloud
    services, but doesn't contain permissions to perform all actions for all services.
    This gives all the view permissions, so it is not necessary to have `Viewer` together
    with `Editor`.

!!! question "Why does 'Deployer' exist?"

    Ideally we'd be able to add a big "elevate your permissions" button to the
    Google Cloud console so that most of the time you're a viewer but you have
    to take decisive action to make changes. The Google Cloud console is very
    powerful and it is easy to "fat-finger" a change when you just meant to look
    around.

    Until users get comfortable with what _is_ and _isn't_ safe to do in the
    console, we make them viewers and deployers. The viewer rights mean that
    they can inspect the current configuration and the deployer rights mean that
    they can make changes but _only_ through terraform or the `gcloud`
    command-line utility. This at least makes one have to actively _decide_ to
    do something which could change something.

    As such, our concept of "deploy user" is more of a "prevent accidental
    change" feature than a security-related feature. Much like passwordless sudo
    on a traditional Unix box it is designed to make it more obvious to an admin
    when they're doing something potentially dangerous.

The association between individual accounts and these roles is specified in the
gcp-product-factory project. For DevOps we have also made this information available via a
[team data
module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/)
(University members only).

!!! note "Future improvements"

    Since this system was developed, Google Cloud gained knowledge of [Lookup
    groups](https://www.lookup.cam.ac.uk/). As such we can also start assigning
    these roles to members of Lookup groups by granting permissions to
    the `[GROUPID]@groups.lookup.cam.ac.uk` Google Group.

    This improvement is being tracked in [an
    issue](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/-/issues/55)
    on the Developers' Hub (DevOps only).

## Per-environment permissions

Some service accounts and permissions are set at an environment level rather
than at a product level.

### Terraform deploy service account

The gcp-product-factory creates a `terraform-deploy` service account in each environment.
This account is given a specific set of roles to enable it to deploy the terraform configuration.
We then use service account impersonation to authenticate as this service account and associate it
with the default `google` and `google-beta` providers in our terraform configurations.

This service account is created [within the gcp-product-factory terraform](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/modules/workspace/iam.tf).

### Service identities

All processes within Google Cloud have an identity associated with them. There
is usually a default identity for each service. For example, unless configured
otherwise, applications running via Cloud Run have the [Compute Engine default
service account
identity](https://cloud.google.com/run/docs/securing/service-identity).

To allow granular control we try to configure all processes to have dedicated
service accounts. In particular you should try to create custom service accounts
for:

* Cloud Run services,
* Cloud Function functions, and
* Kubernetes workloads via [GKE workload
    identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity).

!!! example

    Within the [Raven
    infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/blob/master/raven_stats.tf)
    (DevOps only) we create a Cloud Function which fetches device statistics via
    the Google Analytics API and stores a summary report in a Cloud Storage
    bucket. This function runs as a custom service account which is given rights
    to update the summary in the bucket and is added to our Google Analytics
    property as a read-only user.

## Summary

In summary,

* Each *product* has a *product admin* service account which is only used by the gcp-product-factory
    and is granted a specific set of admin roles over all projects in the product folder.
* For real users, as opposed to service accounts, per *product* permissions are
    "Owner", "Deployer" and "Viewer". "Owners" can
    do everything, "Deployers" can run terraform and use the `gcloud` CLI to do
    anything and "Viewers" can see how infrastructure is configured but not some
    sensitive resources such as secrets.
* There is one Google project per environment. For example, one project for
    "production", one for "staging", etc.
* Each environment-specific Google project has a `terraform-deploy` service account
    which has a specific set of admin roles granted on that project.
* The default terraform provider uses the `terraform-deploy` service account via impersonation.
* We create custom Cloud IAM roles to allow continuous deployment jobs to deploy
    new versions but not create new services or delete old ones.
* We try to explicitly create custom service account identities for all processes.
