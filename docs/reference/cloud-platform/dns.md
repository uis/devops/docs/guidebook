# DNS

This page documents how we configure DNS zones for our deployments. It also
discusses how we verify ownership of domains under `cam.ac.uk` with Google.

## Zone delegation

The DNS zone `gcp.uis.cam.ac.uk` is delegated to a Google Cloud DNS hosted zone
managed by
[gcp-infra](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/)
(University members only).

When a new product is added to the
[gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory), a
*product zone* is created. This will be of the form `[product-slug].gcp.uis.cam.ac.uk`.
Per-environment zones are also created by the gcp-product-factory and the workspace specific
`terraform-deploy` service accounts are granted the ability to add records to these zones. For
example, the "development" environment for the "example" product would have the zone
`devel.example.gcp.uis.cam.ac.uk` created. DNSSEC records are added as appropriate.

At this point, the `terraform-deploy` service accounts can freely add records to the
environment-specific zones.

!!! example "Example: Load Balancer IPs"

    When configuring the [Cloud Load
    Balancer](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/blob/master/template/webapp_load_balancer.tf)
    in our boilerplate, we create an "A" record pointing to the load balancer's
    IP with the following config:

    ```tf
    # DNS records for webapp. For load-balanced applications, these are created
    # irrespective of the any custom DNS name. For custom DNS name-hosted webapps,
    # you will probably need a further CNAME record pointing to this record.
    resource "google_dns_record_set" "load_balancer_webapp" {
      count = local.webapp_use_cloud_load_balancer ? 1 : 0

      managed_zone = google_dns_managed_zone.project.name

      ttl  = 300
      name = local.webapp_dns_name
      type = "A"
      rrdatas = [
        module.webapp_http_load_balancer[0].external_ip
      ]
    }
    ```

## Adding records under `.cam.ac.uk`

The existing [IP register database](https://www.dns.cam.ac.uk/) is used to
provision records under `.cam.ac.uk`. At the moment this cannot easily be
automated.

!!! tip "The IP-register service"

    New team members will almost certainly want to read the [IP register
    documentation](https://www.dns.cam.ac.uk/ipreg/docs.html) since the system
    is bespoke to Cambridge. Unfortunately there's not much substitute for
    seeing someone do this the first time and so, if you've not done this
    before, it's well worth shadowing another team member when the following
    procedure is next performed.

To add records you will need to liaise with
[ip-register@uis.cam.ac.uk](mailto:ip-register@uis.cam.ac.uk) to ensure the
following:

* The zone containing the record you wish to register is in the `UIS-DEVOPS`
    *mzone*. (For example, if you want to register `foo.raven.cam.ac.uk` then
    `raven.cam.ac.uk` needs to be in the mzone.)
* You have the ability to administer the UIS-DEVOPS mzone in the IP register
    database. The list of administrators for the mzone can be listed via the
    [single_ops page](https://jackdaw.cam.ac.uk/ipreg/single_ops).

Our general approach is to add whatever records we need for the service within
the product environment specific zone under `gcp.uis.cam.ac.uk` and add CNAME
records for the `.cam.ac.uk` "friendly" names.

Adding a CNAME is a bit of a fiddle:

* In [vbox_ops](https://jackdaw.cam.ac.uk/ipreg/vbox_ops) add a vbox which
    corresponds to the `.gcp.uis.cam.ac.uk` host name.
* In [cname_ops](https://jackdaw.cam.ac.uk/ipreg/cname_ops) add a CNAME which
    points to the `.gcp.uis.cam.ac.uk` host name.

## TLS Certificates

Google Cloud services which can host web content generally have two options for
TLS certificates:

* **Self-managed certificates** require that one generate private keys and
  corresponding TLS certificates signed by some appropriate trust root "out of
  band" and provide the key and certificate to the service. They can be a
  management burden since one needs to make sure that appropriate procedures are
  in place to renew and replace certificates before expiry. As such we are
  incentivised to have long-lived certificates.

* **Google managed certificates** are issued and managed as part of the Google
  Cloud platform. They are automatically renewed and replaced as necessary. Like
  many automated certificate provisioning solutions, Google managed certificates
  tend to be short-lived, typically 90 days. Renewing and rotating TLS
  certificates often is desirable from a security hygiene perspective. This,
  coupled with the lower maintenance burden, means we generally use Google
  managed certificates where possible.

## Domain verification

!!! warning

    Domain verification is not normally required for new projects. Google-managed certificates
    associated with a Load Balancer now use
    [ACME](https://en.wikipedia.org/wiki/Automatic_Certificate_Management_Environment) to verify
    ownership automatically.

    It is likely that Google will eventually remove the ability to verify domains.

In order for Google to issue certificates, you must verify ownership of the
domain.

### Domains under `.gcp.uis.cam.ac.uk`

The gcp-product-factory includes semi-automated verification for domains under
`.gcp.uis.cam.ac.uk` by means of verification records. For more information see
the [README.md](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory#domain-verification).

### Domains under `.cam.ac.uk`

Unfortunately, we cannot yet semi-automate the verification of domains outside
of `gcp.uis.cam.ac.uk`. For all other `.cam.ac.uk` domains we need to perform
manual verification.

The G Suite admins can manually verify ownership of domains under `.cam.ac.uk`
with Google. They will need:

* The email address of the *project admin* service account.
* The domain to verify.

One can contact the G Suite admins via
[gapps-admin@uis.cam.ac.uk](mailto:gapps-admin@uis.cam.ac.uk). Since all G Suite
admins are also in DevOps, you might find it quicker to ask in the divisional
Google Chat.

!!! info

    [A
    recording](https://drive.google.com/file/d/1TAPkz971dVXLma4jUebcxFoikCP9dHE9/view)
    (DevOpd only) of a meeting where a G Suite admin demonstrated how this
    is done is available in the DevOps shared drive.

## Example - Lab Allocator

The [Lab Allocator](site:services/lab-allocator) product required a custom domain. A tutorial
walkthrough of the process is available:

* [Tutorial: Custom DNS for Cloud Platform Service](site:tutorials/gcp-custom-dns)

## Summary

In summary:

* Domains under `.gcp.uis.cam.ac.uk` are managed via Google's Cloud DNS service.
* Each product receives a dedicated zone named `[PRODUCT].gcp.uis.cam.ac.uk`
    which can be administered by the `terraform-deploy` service account.
* Each environment is provisioned with a dedicated zone named
    `[ENVIRONMENT].[PRODUCT].gcp.uis.cam.ac.uk` which is administered by the
    gcp-product-factory.
* All zones have appropriate DNSSEC records created.
