# GCP Budget Alerts

This page documents how we configure Google Cloud Platform (GCP) budgets and related alerts for
projects, initialized with
[gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory). It
also discusses how we react on such alerts.

## Why do We Need Budget Alerts

GCP budget alerts are essentials for several reasons:

- **Cost Control**: Prevent overspending and cost overruns.
- **Financial Planning**: Helps with budget allocation and tracking.
- **Proactive Monitoring**: Real-time spending alerts.
- **Resource Optimization**: Encourages efficient resource use.
- **Cost Allocation**: Allocates expenses to projects/teams.
- **Resource Usage Patterns**: Identifies consumption patterns.
- **Avoiding Surprises**: Prevents unexpected, large bills.
- **Compliance & Security**: Ensures financial and security compliance.

Budget alerts are crucial for maintaining control and financial stability in a cloud computing
environment.

## How Budget Alerts are Configured

Every new product added to the
[gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory), has
a corresponding `.tfvars` file in the product folder. In this file we set a number of
project-related variables, including:

```tf
budget       = ""
cost_centre  = ""
team_name    = ""
```

where:

<!-- markdownlint-disable MD013 -->
- `budget` is an approximate budget value in £ (GBP). If set, it will be used as value for
  `amount.specified_amout.units` in `google_billing_budget` terraform resource. The billing account
  (and budget alerts as well) is in € (EUR), so the value of `budget` is converted to that currency.
- `cost_centre` is a cost centre code. Usually, contains information about
  [Cost Centre Owners](https://universityofcambridgecloud.sharepoint.com/sites/UIS_Staff_Intranet/Lists/SoF%20List/AllItems.aspx?origin=createList)
  and [Source of Fund](https://universityofcambridgecloud.sharepoint.com/sites/UIS_Staff_Intranet/Lists/SoF%20List/AllItems.aspx?origin=createList).
  Used as the GCP project label.
- `team_name` is a name of team associated with the product. Used as the GCP project label.
<!-- markdownlint-enable MD013 -->

The intial estimate for a budget is discussed on the phase of adding services to the factory. GCP
budgets are centralised in a "Cloud" cost centre that belongs to DevOps. Some of these costs are
recharged to other cost centres but not all of them.

While the corresponding product team is responsible to make sure the budgets remain below 100%
(including cloud team), any increase in budget needs to be authorised by [Abraham
Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203), because it needs to be included in our
budget planning for every year. This includes spikes, as our budgets are annual.

## How to Investigate Billing Spikes in a GCP Project

Currently we have two triggers, **90%** and **100%**. The first one indicates we are close to the
limit, the second one should be investigated immediately.

Billing spikes in a Google Cloud Platform (GCP) project can be concerning, but investigating them
can help you understand and mitigate unexpected charges. Here's a short guide on how to investigate
billing spikes in a GCP project:

- **Access the Billing Section**: Sign in to the Google Cloud Console.
  Go to the Billing section to access a billing data in a linked billing account.
- **Check Billing Reports**: In the Billing section, you can view detailed billing reports and
  insights into the project's spending.
  Review the Billing Overview to see any recent spikes in spending. This provides a quick overview
  of a billing trends.
- **Examine Billing Breakdown**: Access the billing breakdown by service to identify which resources
  or services are responsible for the spike.
  Adjust the date range to narrow down the time period when the spike occurred. This can help
  pinpoint the cause.
- **Check Resource Usage**: Examine the usage of specific resources during the spike. Look for
  increased usage of virtual machines, storage, or other services.
  Check if there were any recent changes to the project's resource configuration. Upgrades, scaling,
  or new deployments could lead to increased costs.
  If everything looks normal and no recent changes took place, investigate any unusual or
  unauthorized access, unexpected traffic, or other anomalies in the project.
- **Take Action**: Based on your findings, optimize resources, such as resizing VMs, scaling down
  services, or utilizing more cost-effective options.
  If there are no place for futher optimizations and the increase in expenses is caused by natural
  growth, it is needed to update the budget in
  [gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
  after approval from the budget owners.
