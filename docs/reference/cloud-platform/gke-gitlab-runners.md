---
title: GKE GitLab Runners
---

# GKE GitLab Runner Platform

This page provides reference information relating to the DevOps team's Google Kubernetes Engine
(GKE) GitLab Runner platform.

## Platform Description

The GKE GitLab runner platform provides GKE-hosted GitLab Runner resources for DevOps product's to
run CI jobs on.

## Platform Status

The GKE GitLab runner platform is live and in use by multiple DevOps product teams.

## Contact

Technical queries and support should be directed to the [Cloud Team MS Teams
channel](https://teams.microsoft.com/l/channel/19%3afd77aa792d2243d4ae3818ee4b17d55e%40thread.tacv2/Cloud%2520Team?groupId=8b9ab893-3917-42bb-ba20-6cbd4bd2d304&tenantId=49a50445-bdfa-4b79-ade3-547b4f3986e9)
where members of the Cloud Team will be able to assist.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure/-/issues).

## Environments

The GKE GitLab Runner platform is currently deployed to both `development` and `production`
environments. The GCP console landing page for the environment projects are as follows:

| Name        | Project landing page |
| ----------- | ------------------------ |
| Production | [gitlab-runner-prod-22257483](https://console.cloud.google.com/welcome?project=gitlab-runner-prod-22257483) |
| Development | [gitlab-runner-devel-72a2b0bc](https://console.cloud.google.com/welcome?project=gitlab-runner-devel-72a2b0bc) |

## Source code

Source code for the GKE GitLab Runner platform infrastructure is in the
[gitlab-runner-infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
repository.

## Technologies used

The GKE GitLab Runner platform uses the following technologies:

- [Google Kubernetes
  Engine](https://cloud.google.com/kubernetes-engine/docs/concepts/kubernetes-engine-overview) (GKE)
- [Google IAM](https://cloud.google.com/iam/docs)
- [GKE Workload
  Identity](https://cloud.google.com/kubernetes-engine/docs/concepts/workload-identity)
- [GitLab Runner](https://docs.gitlab.com/runner/) with the [Kubernetes
  Executor](https://docs.gitlab.com/runner/executors/kubernetes.html)

## Architecture

### GKE cluster configuration

The platform requires a GKE cluster. Unfortunately, due to the GitLab Auto-DevOps [Build stage's
dependency on Docker in
Docker](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-build), it's not possible to
use GKE AutoPilot as it [doesn't support privileged
containers](https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-security#built-in-security).
Instead, the cluster is provisioned as a Standard GKE cluster with the following notable
configuration:

- The cluster is deployed to the `gitlab-runner-prod-22257483` Google project.
- It is a zonal cluster configured in the `europe-west2-a` zone.
- A single, auto-scaling node pool is configured to deploy nodes of machine type `n1-standard-2`
  running the recommended `Container-optimised OS with containerd (cos_containerd)` image type.
- Workload Identity is enabled.

### Per-product configuration

Each product has the following resources created by the Terraform configuration (see the
[how-to](site:howtos/gke-gitlab-runners/register-a-gke-gitlab-runner) for steps on adding a
product to the Terraform deployment).

- A unique Kubernetes namespace for the product's runners.
- A Kubernetes service account (named `gke-ci-run`) in the product's namespace.
    - This includes IAM bindings for this service account to impersonate other service accounts
      using Workload Identity.
- A GitLab runner pod deployed using the [GitLab runner Helm
  chart](https://docs.gitlab.com/runner/install/kubernetes.html).
- A Kubernetes network policy which blocks ingress for all pods within the namespace and allows
  egress to any IP on port tcp/443 only. This ensures isolation between different product CI/CD
  jobs.
- The following CI/CD variables populated with the relevant values.
    - `GKE_RUNNER_TAG`
    - `ARTIFACT_REGISTRY_DOCKER_REPOSITORY`
    - `ARTIFACT_REGISTRY_SERVICE_ACCOUNT`

## Service Management and tech lead

The **service owner** for the GKE GitLab Runner platform is
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203).

The **service manager** and **tech lead** for the GKE GitLab Runner platform is
[Adam Deacon](https://www.lookup.cam.ac.uk/person/crsid/ad2139).

The following engineers have operational experience with the GKE GitLab Runner platform and are able
to respond to support requests or incidents:

- [Adam Deacon](https://www.lookup.cam.ac.uk/person/crsid/ad2139)
- [Roy Harrington](https://www.lookup.cam.ac.uk/person/crsid/rh841)
- [Ryan Kowalewski](https://www.lookup.cam.ac.uk/person/crsid/rk725)

## See also

- [How to register GKE-hosted GitLab
  runners](site:howtos/gke-gitlab-runners/register-a-gke-gitlab-runner)
- [How to run CI/CD jobs on a GKE-hosted GitLab
  runner](site:howtos/gke-gitlab-runners/run-ci-jobs-on-a-gke-gitlab-runner)
- [How to access a Google Secret Manager secret using service account
  impersonation](site:howtos/gke-gitlab-runners/access-secrets-in-ci-jobs-using-impersonation)
