---
title: Local app development
---

# Local application development environment

!!! warning

    This document describes our "new-style" developer environment. If your project has a
    `compose.sh` file present in the root, look at the [original
    documentation](site:notes/webapp-dev-environment).

This page lists common tasks which a developer may want to perform when working with our standard
webapp developer environment. Read more about the philosophy behind the design of this environment
in our [explainer guide](site:explanations/development/webapp-boilerplate).

Unless otherwise stated, commands in this guide should be run from the project's root directory.

If you have `poe` installed to your `PATH` then commands of the form `poetry poe ...` can be
shortened to `poe ...`.

## Bootstrapping

Make sure that you have `docker`, `docker compose`, `poetry` and `poe` installed. Instructions for
this can be found in the [guide on how-to prepare your system](site:howtos/development/prepare-your-system).

Install application dependencies via:

```sh
poetry install
```

All `poe` tasks and a brief description can be shown via:

```sh
poetry poe
```

## Standard endpoints

When running the application in production or development mode, the following endpoints will usually
be present:

- The application itself at [http://localhost:8000/](http://localhost:8000/).
- A PostgreSQL instance at `localhost:5432`.
- For applications which host an API behind the API Gateway:
    - A UI for the API Gateway emulator at [http://localhost:8001/](http://localhost:8001/) for
      changing details on signed in user, etc.
    - The "raw" application without being proxied by the API Gateway emulator at
      [http://localhost:9000/](http://localhost:9000/).

## Running the application

### Foreground

To spin up a local database, run migrations and start the application in _debug
mode_:

```sh
poetry poe up
```

To spin up a local database, run migrations and start the application _from the
production container_:

```sh
poetry poe up:production
```

Press Ctrl-C to stop the application.

### Background

To run the application in the background, waiting for application startup before returning control:

```sh
poetry poe up --wait
```

Similarly, for the production application:

```sh
poetry poe up:production --wait
```

When running in the background, logs can be streamed from the webapp via:

```sh
docker compose logs --follow webapp
```

Similarly, for the production application:

```sh
docker compose logs --follow webapp-production
```

### Stopping

To make sure that the application is stopped **but keeping the local database's
contents**:

```sh
poetry poe down
```

To make sure that the application is stopped, **delete any local database contents**
and remove any containers which may still be running:

```sh
poetry poe down:hard
```

## Pulling and  rebuilding containers

To build or rebuild all container images used by the application:

```sh
poetry poe compose:build
```

To build or rebuild all container images used by the application and pull any
new base images:

```sh
poetry poe compose:build --pull
```

To pull any images used by the docker compose configuration:

```sh
poetry poe compose:pull
```

## Django management commands

To run Django management commands:

```sh
poetry poe manage {command}
```

To run a database migration:

```sh
poetry poe manage migrate
```

To create a new super-user which can sign in to the admin UI:

```sh
poetry poe manage createsuperuser
```

To create new migrations after a change of model:

```sh
poetry poe manage makemigrations {app} -n "{human_friendly_name_of_migration}"
```

??? warning "On Linux machines the generated files are owned by `root`."

    The resulting migrations will be owned by "root" on Linux systems. You may want to change
    ownership after creation by running `sudo chown -R "$USER" .` in the repository root.

To launch an interactive Python shell pre-configured to be able to use database models:

```sh
poetry poe manage shell
```

??? warning "This shell runs _inside_ the container and so files you create will be owned by "root"."

    You can change ownership of any files after creation by running `sudo chown -R "$USER" .` in the
    repository root.

## Test suites and code linting

To run pre-commit checks in order to fix up code style and layout to match our
common convention:

```sh
poetry poe fix
```

To run the Python test suite via tox:

```sh
poetry poe tox
```

!!! important

    The `tox` poe task runs `tox` in a container which is close to the production container. On
    Apple Silicon machines, this means that the tests may run in x86 emulation and will probably be
    slower than running them "natively". On the flip side, this means that the tests are running in
    an environment which better matches the one the code will ultimately run in.

To run the Python test suite via tox _using the locally installed version of
Python_:

```sh
poetry poe tox:local
```

To run the Python test suite directly via `pytest`:

```sh
poetry poe pytest:local
```

Some IDEs, such as VSCode, will run `pytest` directly like this rather than running `tox` and so
it's important to check that doing so still works.

!!! tip

    The `tox:local` and `pytest:local` tasks are very similar. Running tests in `tox` arranges for
    code coverage and test reports to be generated which are used in CI. The HTML code coverage
    report is also useful for tracking down which lines are not being covered by tests.

Both `tox` and `tox:local` can take additional arguments which are passed to `tox`. Use this to, for
example, only run tests within a single file:

```sh
poetry poe tox -e py3 -- path/to/test_module.py
```

## Dependencies

!!! important

    If you add a new dependency to the application as described below you will need to stop and
    re-start the application via `poetry poe up` for the dependencies to be installed within the
    application container.

    If you are running the application via `docker compose up`, you'll need to either add the
    `--build` option or use `docker compose build` to re-build the containers.

To add a new dependency _for the application itself_:

```sh
poetry add {dependency}
```

To add a new development-time dependency _used only when the application is running locally in
development or in testing_:

```sh
poetry add -G dev {dependency}
```

To remove a dependency which is no longer needed:

```sh
poetry remove {dependency}
```

For example:

- Django 4.2 is required to run the application at all. It was added via
  `poetry add Django~=4.2.0`.
- The Django debug toolbar is required only when running locally in development.
  It was added via `poetry add -G dev django-debug-toolbar`.
- The pytest library is only used when running tests. It was added via
  `poetry poe add -G dev testing pytest`.

## Poetry-managed virtualenv

To run a command within the Python virtualenv managed by poetry, use `poetry run`. For example, the
`tox:local` poe task could also be done via:

```sh
poetry run tox
```

To launch a command line shell inside the virtual environment:

```sh
poetry shell
```

To delete the automatically created Python virtual environment in order to save disk space:

```sh
poetry env remove python3
```
