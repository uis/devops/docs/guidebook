---
title: The Tech Lead Forum
---

# Terms of Reference for the Technical Lead Forum

This page lists the purpose, the composition and the processes for the Tech Lead Forum. An
explanation of why the Tech Lead Forum exists and how it helps us is available on [a separate
page](site:standards/tech-lead-forum).

## Purpose

The purpose of the Tech Lead forum is to provide a space where cross-team technical procedures,
standards and practices can be discussed, refined, documented and agreed upon. The aim being to
discover and promote good practice where it arises, to standardise it where it should be shared and
to enable movement between teams through a shared library of technical practice.

## Membership

Membership of the Tech Lead forum shall be the technical leads in DevOps. An [up-to-date
list](https://www.lookup.cam.ac.uk/group/uis-devops-tech-leads/members) can be found on Lookup.

It is co-chaired by the Head of DevOps, [Abraham
Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203) and [Rich
Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

Tech Leads may co-opt members as necessary for particular meetings where specific technical
expertise is required.

## Responsibilities

It shall be the duties of the Tech Lead Forum to:

* meet regularly and often,
* approve technical papers presented to the UIS
  [Technical Design Authority](https://techdesign.uis.cam.ac.uk/),
* provide a forum to discuss technical matters which are of interest to the wider DevOps division,
* identify and document technical problems which are common to multiple teams,
* discover good practice in technology design, architecture, implementation and delivery,
* recommend that good practice where it exists,
* document that good practice when it should be followed by the wider DevOps
  division, and
* standardise and establish that good practice.

These standards will be sent to the UIS [Technical Design
Authority](https://techdesign.uis.cam.ac.uk/) for ratification.

## Operation

The operation of the Tech Lead Forum shall be as follows:

* The Tech Forum shall maintain a GitLab project. The project shall be visible to DevOps members.
* Items for discussion at the next meeting shall be recorded as issues within the project with
  sensitive items marked as confidential issues.
* Each meeting shall be represented by an "iteration" in GitLab and issues for discussion shall be
  assigned to that iteration.
* Where possible issues should exist for
  at least a week before the meeting in which they shall be discussed.
* Before the meeting, the Tech Lead Forum membership shall read comments and discussions on the
  issues for the forthcoming meeting.
* During the meeting, the issue shall be discussed and a member of the Tech Lead Forum shall be
  nominated to record the discussion.
* After the meeting, a summary of the discussion shall be added to the issue along with any
  decisions made. Decisions must be accompanied by a rationale.

The chair and deputy chair have a responsibility to codify and document practice as it evolves on
this page.
