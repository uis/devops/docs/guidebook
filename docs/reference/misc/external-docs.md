# Other sources of documentation

This page lists some other sources of documentation outside of the guidebook.

The guidebook is intended to be the repository for all documentation
surrounding workflow, best practice and common technical standards. We may also
add specific "application notes" on technologies or techniques within the [notes
section](site:reference/notes).

It is not intended for deep operational documentation on individual services.
Generally that documentation is maintained by the individual service teams and
is found in other locations.

## Service specific documentation

Some services or groups of services have documentation located in specific
places. This section provides links to those places. The majority of these links
will only be accessible to members of DevOps.

### "Former Dev Group Services"

We have inherited some services which were developed and maintained by a
previous development group in UIS. They are:

* [Lookup](https://www.lookup.cam.ac.uk/)
* [The Raven Password Application](https://password.raven.cam.ac.uk/)
* [The Training Booking System](https://training.cam.ac.uk)
* [The Human Tissue Tracker](https://tissue.apps.cam.ac.uk/)
* [Eduroam tokens](https://tokens.uis.cam.ac.uk)
* [The Streaming Media Service](https://sms.cam.ac.uk/)

Other inherited services are in the process of being decommissioned or replaced.

We have not yet developed a per-service operational documentation strategy. For
the moment, operational documentation exists in the following areas:

* Service-specific GitLab projects.
* Our general [admin
    wiki](https://gitlab.developers.cam.ac.uk/uis/devops/admin/-/wikis/home).
* A set of [handover
    notes](https://docs.google.com/document/d/19fb9893JZY9v0o_YjwMXBnZ_oW7eqad8THwJsH9G-HE/edit)

If you don't know where to document something, use the general wiki as a general
rule of thumb.

### Digital Admissions

An index of documentation relating to Digital Admissions is maintained in a
[separate Google
doc](https://docs.google.com/document/d/19j0wYtijlYHv3AYGKnSFSNASCSJT4saFEWFhQc3chgs/edit)
within the Digital Admissions Shared Drive. The document is open to the entire
Digital Admissions team and not just DevOps.

### Raven

There is a dedicated [operational
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/tree/master/doc)
project for Raven.

### Identity

There is a dedicated [operational
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation)
project for Identity.

## General documentation

Wider technical standards documentation is in the process of being published on
the [Technical Design Authority site](https://techdesign.uis.cam.ac.uk/). It is
likely that some [application notes](site:reference/notes) may be migrated there in
due course.

Information on internal UIS policies and procedures are available on the [UIS
Intranet site](https://intranet.uis.cam.ac.uk/).
