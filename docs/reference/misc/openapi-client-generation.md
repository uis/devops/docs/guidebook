---
title: API client generation from OpenAPI specifications
---
# API client generation from OpenAPI specifications

This page provides more in-depth reference on customising the OpenAPI-based client generation
described in the [how to automatically generate API clients
guide](site:howtos/development/generate-api-clients).

## Customising the generated clients

The OpenAPI generator templates will generate source code, package artefacts and documentation for a
number of clients. Currently these are:

* A TypeScript-based client using axios for HTTP transport.
* A Python-based client using urllib3 for HTTP transport.

[OpenAPI generator](https://openapi-generator.tech/) is used to generate the clients. If you need to
customise generation, additional arguments to the `generate` CLI command can be added to
`OPENAPI_GENERATOR_GENERATOR_EXTRA_ARGS`. If you need to specialise the behaviour of an individual
client, set the variable on the job. For example:

```yaml
# Use a dedicated configuration file for the TypeScript + Axios generator.
openapi:generate:typescript-axios:
  variables:
    OPENAPI_GENERATOR_GENERATOR_EXTRA_ARGS: "--config clients/typescript-config.yaml"
```

### Disabling generation

Individual clients can be disabled by setting a CI variable. For example, set the
`OPENAPI_GENERATOR_PYTHON_URLLIB3_DISABLED` CI variable to some non-empty value to disable
generating the Python client.

### CI job artefact locations

Source code for the clients is written to `$OPENAPI_GENERATOR_SOURCE_ARTIFACT_DIR`, package
artefacts are written to `$OPENAPI_GENERATOR_PACKAGE_ARTIFACT_DIR` and documentation is written to
`$OPENAPI_GENERATOR_DOCS_ARTIFACT_DIR`. You can override these directories if you want to, for
example, place package files and generated documentation in a known location for publication.

### Package names for generated clients

The package name for API clients is taken from the name of the GitLab project. If this is
inappropriate, the name can be overridden by setting the `OPENAPI_GENERATOR_PACKAGE_NAME` variable.
Package names can additionally by overridden per-generator. For example, to change the name of the
Python package, set the `OPENAPI_GENERATOR_PYTHON_URLLIB3_PACKAGE_NAME` variable.

## Publication of packages

By default publication jobs don't run unless there is a commit in the default branch which changes
the static schema file. This is because there's not yet a well established convention as to when
packages should be built for projects; some teams prefer to release packages on each commit to the
default branch while others prefer a tag-based approach.

To enable publication you will need to add rules to the `.openapi:publish:base` job which controls
when publication happens. For example, to publish packages from commits tagged with a version-like
tag, you can use the following in your project's CI configuration:

```yaml
# Set rules for when we publish OpenAPI client packages.
.openapi:publish:base:
  rules:
    # We publish packages for all commits tagged with what looks like a version number.
    - if: $CI_COMMIT_TAG =~ /v?[0-9]+\.[0-9]+\.+[0-9]+.*/
```

API client packages are published to the GitLab package registry. If you want to disable *all*
publication, set the `OPENAPI_GENERATOR_PUBLISH_DISABLED` variable to some value. If you want to
disable publication of clients to GitLab but are open to auto publication to public registries like
PyPI or NPM in future, set the `OPENAPI_GENERATOR_PUBLISH_GITLAB_DISABLED` variable. To disable
publication of a specific client, override the CI job ans set the
`OPENAPI_GENERATOR_PUBLISH_DISABLED` variable on it. For example:

```yaml title=".gitlab-ci.yml"
openapi:publish:typescript-axios:gitlab:
  variables:
    OPENAPI_GENERATOR_PUBLISH_DISABLED: "1"
```

## CI variables

The following variables can be set to change the behaviour of the CI template. The default values of
variables marked as *private* should not be relied on. If you add CI jobs which rely on them taking
some value, set it yourself.

<!-- markdownlint-disable MD013 -->
|Variable|Default|Description|
|-|-|-|
|`OPENAPI_GENERATOR_ARTIFACT_DIR`|*private*|Directory in which all generated artifacts are placed unless overridden.|
|`OPENAPI_GENERATOR_DISABLED`|*unset*|Set to non-empty value to *disable* OpenAPI client generation if automatically triggered when it shouldn't be.|
|`OPENAPI_GENERATOR_DOCS_ARTIFACT_DIR`|*private*|Directory in which generated documentation artifacts are placed.|
|`OPENAPI_GENERATOR_ENABLED`|*unset*|Set to non-empty value to *enable* OpenAPI client generation if not otherwise automatically triggered.|
|`OPENAPI_GENERATOR_PACKAGE_ARTIFACT_DIR`|*private*|Directory in which generated package artifacts are placed.|
|`OPENAPI_GENERATOR_PACKAGE_NAME`|`$CI_PROJECT_NAME`|Name of generated API client packages.|
|`OPENAPI_GENERATOR_PACKAGE_VERSION_SUFFIX`|`+$CI_JOB_ID.$CI_COMMIT_SHORT_SHA`|Value appended to all version numbers when packaging clients.|
|`OPENAPI_GENERATOR_SCHEMA_ARTIFACT_PATH`|*private*|Location of OpenAPI schema used by all generators.|
|`OPENAPI_GENERATOR_SCHEMA_PATH`|`openapi.yaml`|Location of static OpenAPI schema file within the repository.|
|`OPENAPI_GENERATOR_SOURCE_ARTIFACT_DIR`|*private*|Directory in which generated source code artifacts are placed.|
<!-- markdownlint-enable MD013 -->
