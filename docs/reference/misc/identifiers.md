# Well-known Identifier Scopes

!!! warning

    This page is provisional pending UIS-wide agreement.

The following is a list of well-known identifier scopes. Within DevOps-developed systems, any of the
scopes may be used. Those which have been approved by the UIS Technical Design Authority (TDA) for
interoperability with systems outside of DevOps are so indicated.

There is a separate page providing an [explanation of how we use identifiers in
DevOps](site:explanations/misc/identifiers).

This table is generate automatically from our [identity
library](https://gitlab.developers.cam.ac.uk/uis/devops/iam/identity-lib).

<!-- The list of TDA approved identifiers is maintained in main.py -->
{{ identifier_table(deprecated=False) }}

The following identifier scopes are deprecated and should not be used in new products.

{{ identifier_table(deprecated=True) }}
