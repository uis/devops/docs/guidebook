# Entra ID and Azure Application Authentication URLs and IDs

The following URLs and IDs are useful when configuring sign in with Entra ID and authenticating
Azure Applications to make use of APIs hosted using Azure API Management.

<!-- markdownlint-disable MD013 -->
|Description|URL or ID|
|-|-|
|OpenID Connect (OIDC) Discovery Document|`https://login.microsoftonline.com/49a50445-bdfa-4b79-ade3-547b4f3986e9/.well-known/openid-configuration`|
|OAuth 2.0 Token Endpoint|`https://login.microsoftonline.com/49a50445-bdfa-4b79-ade3-547b4f3986e9/oauth2/token`|
|OAuth 2.0 Authorization Endpoint|`https://login.microsoftonline.com/49a50445-bdfa-4b79-ade3-547b4f3986e9/oauth2/authorize`|
|OAuth 2.0 End Session Endpoint|`https://login.microsoftonline.com/49a50445-bdfa-4b79-ade3-547b4f3986e9/oauth2/logout`|
|Directory ID / Tenant ID|`49a50445-bdfa-4b79-ade3-547b4f3986e9`|
<!-- markdownlint-enable MD013 -->

UIS also maintain [a list of the Object IDs for some Entra
Groups](https://help.uis.cam.ac.uk/service/accounts-passwords/it-staff/university-central-directory/understanding-users-and-groups)
in case you need to grant access to, e.g., "all staff" or "all students".
