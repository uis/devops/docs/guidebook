# Traffic ingress

Our boilerplate prefers the use of [Cloud Run](https://cloud.google.com/run) to
host web applications. Applications hosted via Cloud run can use two different
services to connect traffic from the outside world to them: Cloud Load Balancers
and Domain Mappings. This page documents when and how to use both.

## Our Cloud Run module

We have a [standard
module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
which we use to configure applications hosted in Cloud Run. This module can be
used to configure both domain mapping and load balancer ingress. See [the
module's documentation for more
details](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/-/tree/master#ingress-style).

## Which ingress to use

This section helps you select which ingress style to use.

### No ingress

If no ingress style is specified and the application is marked as being public,
a URL will be generated for the application under the `.run.app` domain. This is
unlikely to be a human-friendly name but may suffice for test or development
instances.

### Domain mapping

!!! warning "Deprecation notice"
    Domain Mapping in Cloud Run has been [in Pre-GA for multiple
    years](https://cloud.google.com/run/docs/mapping-custom-domains#run). Due to
    [latency
    issues](https://cloud.google.com/run/docs/mapping-custom-domains#limitations),
    Google states that this feature is not fit for production use. Therefore,
    since version `9.0.0` of our [Google Cloud Run terraform
    module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/-/releases/9.0.0),
    we only support custom domains when using a Cloud Load balancer.

The Cloud Run domain mapping ingress is simple to configure:

1. Verify [a DNS domain](site:reference/cloud-platform/dns) for the application.
2. Specify the DNS domain via our standard module's `dns_names` variable.
3. Add DNS records for that domain according to the `dns_resource_records`
   output from our standard module.

The application will then be served from the DNS domain provided. There are some
restrictions surrounding domain mapping of which the most pressing is that
**applications hosted in europe-west2 (London) cannot use domain mapping**. _De
facto_ this restricts our more modern applications to use load balancing or the
"no ingress" configuration above.

### Load balancer

A Cloud Load Balancer is appropriate when there are complex needs. Such needs
include:

* Using non-managed TLS certificates. This is often the case if one is
  transitioning a service fom on-premises to Cloud.
* Using [Cloud Identity Aware Proxy](https://cloud.google.com/iap) to restrict
  resources to particular identities.
* The need to shape or otherwise filter traffic via Cloud Armour rules or
    backend weighting.

Our standard module comes with a basic Load Balancer configuration. For advanced
use you may find that you need to configure it yourself. Examples of manually
configured load balancers are:

* The [Raven Core IdP
  configuration](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/blob/master/ravencore_load_balancer.tf)
  (DevOps only) configures a basic load balancer. This was done ahead of support in our
  standard module and so provides a "minimal" configuration example with no
  fancy features.
* The [Raven Admin
  API configuration](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/infrastructure/-/blob/master/admin_scripts.tf)
  includes an example of configuring Cloud Identity Aware Proxy to restrict
  certain resources to individual service accounts.

Load Balancers incur additional costs when used and so domain mapping should be
used in preference if possible.

## When **not** to use an ingress

Sometimes you will not need to use either a load balancer nor domain mapping
ingress. This is usually for services which are only ever called by resources
within the parent Google project. A typical example of this is a service which
is used in combination with [Cloud
Scheduler](https://cloud.google.com/scheduler) to perform actions at regular
intervals.

In these cases our standard module may not prove sufficient as it assumes the
application you are hosting is public.

## Summary

In summary:

* Ingresses should be used for externally available production applications or
  internal applications which need to make use of Identity Aware Proxy
  policies.
* Our standard Cloud Run module can configure domain mapping or load balancer
  ingresses.
* Domain mapping ingresses are easy to use but are inflexible.
* Load balancer ingresses are flexible but may require additional configuration.
* Load balancer ingresses should be used when:
    * the application is hosted in a region not supported by domain mappings,
    * custom TLS certificates need to be used, or
    * custom access and routing policies are required.
* Custom access policies for Load balancers can be implemented via Cloud Armour
    policies.
* There are cost implications with using load balancers which means their use
  should be considered carefully.
