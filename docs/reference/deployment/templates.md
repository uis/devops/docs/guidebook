# Templates

This page documents the various templates and boilerplate deployment
configurations we use.

## Copier

[Copier](https://copier.readthedocs.io/en/stable/) is a powerful and user-friendly tool designed
to simplify the process of duplicating and customizing project templates. Copier is a Python
library and CLI tool. Below are some key advantages of using Copier:

1. **Template Support:**
   Copier allows us to employ templates during the copying process, facilitating customisation
   based on variables and configurations.
2. **Built-in updating ability:**
   Copier includes a command to update the project with a new revision of parent template.
3. **Interactive Configuration:**
   The tool features an interactive command-line interface for configuring copying operations.
4. **Project Initialization:**
   It can be utilised to initialise projects based on predefined configuration, termed "answers",
   saving time and promoting consistency across projects.
5. **Git Integration:**
   Copier seamlessly integrates with Git, simplifying the cloning and customisation of projects.
6. **Cross-Platform Compatibility:**
   Copier is designed to work seamlessly across different operating systems, providing a consistent
   experience on Windows, macOS, and Linux.

In the UIS DevOps Division we use Copier for two boilerplate projects:

- [webapp-boilerplate](
   https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate)
- [gcp-deploy-boilerplate](
   https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate)

## More Information

More information on how to use Copier to perform specific tasks can be found in the following
how-to guides:

- [Creating a new project from Copier template](site:howtos/development/copier/create)
- [Updating a project created with Copier to the newer version](site:howtos/development/copier/update)
- [Migrating a project created with Cookiecutter to Copier](site:howtos/development/copier/migrate)
