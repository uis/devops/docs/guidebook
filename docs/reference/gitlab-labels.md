# Labels in GitLab

We use [scoped
labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels) in
GitLab to manage the issue life cycle and to aid discovery. Scoped labels use
`::` to separate the scope from the label text. For example the label
`~team::DevOps` becomes {{ gitlab_label("team::DevOps") }} in GitLab.

We also use un-scoped labels to categories issues into broad work areas. For
example, the label `~Documentation` is used to tag all documentation-related
issues and becomes {{ gitlab_label("Documentation") }} in GitLab.

A good rule of thumb for deciding if something should be a label is "would I
want to search for issues with it". For example, it is desirable to search for
issues which relate to a particular role within a product. It would not be
necessary to label issues with, for example, the exact epic they are part of
because one could just list all issues in that epic directly.

## Issue life cycle and workflow

The `workflow::...` scoped labels are used to manage the issue life cycle. It
is the responsibility of all team members to make sure that the labels reflect
reality but the daily stand up provides a good opportunity to re-visit issues
within the current sprint to check they are labelled correctly.

### Labels used before the sprint

The following labels are usually applied to issues *before* they enter the
current sprint.

{{ gitlab_label("workflow::Needs Refinement") }}
: The issue requires refining to meet the Definition of Ready. This refinement
usually happens as part of the sprint refinement ceremony.

{{ gitlab_label("workflow::Needs Sizing") }}
: The issue has been refined but needs an estimate on how long it will take in
order to meet the Definition of Ready. This sizing is usually done as part of
the sprint planning ceremony.

{{ gitlab_label("workflow::Blocked") }}
: Work on this issue cannot proceed because of external restrictions. Ideally
blockers will be identified *before* issues enter a sprint.

{{ gitlab_label("workflow::Sprint Ready") }}
: This issue meets the Definition of Ready and can become part of a sprint.

{{ gitlab_label("workflow::BAU Blocked") }}
: This issue represents ongoing business-as-usual work which has stalled due to
an external restriction.

We try to be asynchronous where possible but we do have two important ceremony
meetings where the entire team is present: backlog refinement where issues in
need of refinement can be discussed and sprint planning where we schedule the
next sprint.

On the face of it, the {{ gitlab_label("workflow::Needs Refinement") }} and {{
gitlab_label("workflow::Sprint Ready") }} labels should suffice for those
meetings but we sometimes want to tag issues we particularly want to discuss at
the next meeting. For these we have `meeting::` scoped labels:

* {{ gitlab_label("meeting::backlog refinement") }}
* {{ gitlab_label("meeting::sprint planning") }}

**These labels should be temporary** since they explicitly refer to the *next*
meeting. At the end of the meeting, there should be no issues labelled with the
related `meeting::` label.

### Labels used during the sprint

{{ gitlab_label("workflow::In Progress") }}
: This issue is currently under development. It should be assigned to the
person currently performing the work. Ideally people should only be assigned to
one "in progress" issue at any one time; if they cannot proceed, it should be
moved to {{ gitlab_label("workflow::Blocked") }}.

{{ gitlab_label("workflow::Review Required") }}
: The work required for this issue has been completed and is ready for review.
For issues with associated code changes, this usually means there is a Merge
Request open which references the issue.

{{ gitlab_label("workflow::Needs Testing") }}
: For teams which have separate testing roles, this issue is ready
to be picked up by QA.

{{ gitlab_label("workflow::In Testing") }}
: For teams which have separate testing roles, this issue is currently being
tested after a positive review.

{{ gitlab_label("workflow::Rework") }}
: The work completed as part of this issue has had some changes requested by
the reviewer or, for teams which have separate testing roles, the tester.

{{ gitlab_label("workflow::Needs Deployment") }}
: The work associated with this issue has been merged but a manual deployment
step is required for the work to be completed.

{{ gitlab_label("workflow::Done") }}
: Issue meets the Definition of Done. Usually the issue will have also been closed.

### Labels used after the sprint

Once closed in a sprint, issues are generally not looked at again but it is
useful to tag them with the ultimate resolution for the benefit of anyone
watching the issue via GitLab notifications.

An issue being closed usually means "the work in this issue has been done".
This is not always the case and it is useful to be able to see at a glance what
ultimately caused an issue to be closed. We use the `resolution::` scoped
labels to record this for issues which were closed for reasons out of the
ordinary.

{{ gitlab_label("resolution:: Can't Reproduce") }}
: The issue appears to report a bug but, either through lack of steps or
because it was fixed before being looked at, the bug cannot be triggered by a
developer so that they can find and fix it. The issue was closed but could be
re-opened if a way of triggering the bug is found.

{{ gitlab_label("resolution:: DevNotPlanned") }}
: The issue relates to a potential feature which we have decided not to
implement.

{{ gitlab_label("resolution:: Duplicate") }}
: This issue contains work which is already contained in an existing issue and
has been closed in favour of the existing one.

{{ gitlab_label("resolution::Fixed") }}
: This issue represented either a bug or misconfiguration which has been
addressed.

{{ gitlab_label("resolution:: Won't Fix") }}
: The issue appears to report a bug which is actually either an example of the
product working as intended or is functionality we can accept within the scope
of the product.

## Teams

We have multiple teams within DevOps. The `team::...` scoped labels can be used
to denote "ownership" of issues by a particular team. By taking ownership of an
issue, a team commits to refining and sizing it until it is sprint ready and to
schedule it as appropriate in upcoming sprints.

The following team labels are defined:

* {{ gitlab_label("team::Authentication") }}
* {{ gitlab_label("team::Cloud") }}
* {{ gitlab_label("team::Identity") }}

There is also {{ gitlab_label("team::DevOps") }} for issues which are, in some
way, the responsibility of the entire division.

## Prioritisation

Although teams can usually reflect priorities within a sprint by directly
ordering the issues in the {{ gitlab_label("workflow::Sprint Ready") }} column
of the sprint board, it is useful for the team to have a wider view of product
priorities and for those priorities to be assigned to issues before they enter
the sprint.

The `priority::...` scoped labels describe priorities and *indicate* in which
order issues should be done *within* the sprint. For example, priority 1 issues
should be picked up in a sprint before priority 2 ones and so on.

Priority labels should not be used to indicate strict ordering outside of the
sprint. For example, do not create a sprint consisting entirely of issues with
priority 0 when there are priority 1 issues open since this will cause resource
starvation for lower-priority issues.

{{ gitlab_label("priority::0 Highest") }}
: The issue has the highest priority and requires immediate attention. This
label should be used for issues tracking work which is necessary *now* to
prevent service disruption or some other severe consequence.

{{ gitlab_label("priority::1 High") }}
: The issue is urgent and needs to be addressed as soon as possible.

{{ gitlab_label("priority::2 Medium") }}
: The issue has "normal" priority. The issue can be scheduled for a sprint and
can be addressed at any point during the sprint. We do strongly desire that the
issue be addressed at *some point* within the sprint.

{{ gitlab_label("priority::3 Low") }}
: The issue reflects work we would like to complete within a given sprint but
it is not necessary that the work be completed. For example, the work has a
natural due date which is well beyond the end of the sprint and is not
currently blocking any other work.

{{ gitlab_label("priority::4 Lowest") }}
: The issue represents a "nice-to-have" but does not have any natural due date
and does not prevent further enhancement of the product.

## Epics

We structure epics as a tree grouped into three levels which are, from most
general to least: "features", "initiatives" and simple "epics". We use the
`EpicType::...` scoped labels to label epics so that we can tell at a glance
where they sit in this hierarchy.

{{ gitlab_label("EpicType::Feature") }}
: Used to group all issues and epics related to a single high-level feature.
Usually this will have initiatives or epics as children.

{{ gitlab_label("EpicType::Initiative") }}
: Used for grouping detailed epics for easier management.

{{ gitlab_label("EpicType::Epic") }}
: The most detailed type of epic. Issues should be attached to epics at this level.

{{ gitlab_label("EpicType::Placeholder") }}
: Used early in the life of a feature to capture anticipated work before it is
refined into epics and issues.

## Issue types

Often the same broad categories of issues come up within a sprint. The
`issueType::...` scoped labels are used to provide a broad category for an
issue so that the balance of a sprint may be seen at a glance.

{{ gitlab_label("issueType::Bug") }}
: The issue describes a way in which the functionality of the product is not as
designed or intended by the code author.

{{ gitlab_label("issuetype::User Story") }}
: The issue captures an end-to-end user-centric piece of work sized to fit
within a single sprint

{{ gitlab_label("issuetype::Feedback") }}
: The issue captures feedback on the product provided by users.

{{ gitlab_label("issuetype::Support") }}
: The issue captures a support request from a user of the product.

{{ gitlab_label("issuetype::Task") }}
: The issue represents a general task which needs to be performed but is not
directly related to a bug or a new feature.

Issues have a habit of growing in refinement. For these issues we have the
following *temporary* labels which indicate that they should be split into
epics or features.

{{ gitlab_label("issuetype::Theme") }}
: This issue has unpacked into  a large collection of work, comprised of one or
more epics. The issue should be split into features and thence into epics.

{{ gitlab_label("issuetype::Epic") }}
: This issue represents work which has grown beyond the size of a single issue
and needs splitting out into an epic.

### Support requests

While the use of issue type labels is a convenience for those building new
features or fixing bugs, it is also of great utility to those providing user
support for a product. As such the {{ gitlab_label("issuetype::Support") }}
label is one of the heavier used issue type labels and it is probably useful to
bookmark [the list of issues with this
tag](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/issues?label_name%5B%5D=issuetype%3A%3ASupport).

## Roles

We have some labels which describe which role(s) within the product team
especially relate to the issue.

!!!important
    Remember, a golden rule is "it is your responsibility to move things
    forward". Just because an issue is labelled with a particular role, it
    doesn't mean that your input on the issue is not valued.

{{ gitlab_label("Accessibility") }}
: Issues which relate to making the product accessible for users with
additional needs. Usually used by teams with dedicated accessibility testing.

{{ gitlab_label("Business Analysis") }}
: Issues which require analysing business practices of the University.

{{ gitlab_label("Development") }}
: Issues which require active software or infrastructure-as-code development
work.

{{ gitlab_label("Developer Experience") }}
: Issues which require technical work which does not *directly* improve the
product but is required to make future development easier or more pleasant.

{{ gitlab_label("Documentation") }}
: Issues which require writing documentation for the team or for end-users.

{{ gitlab_label("Operations") }}
: Issues which relate to deployment or packaging of products.

{{ gitlab_label("Testing") }}
: Issues relating to testing tools, procedures, technologies or practices.

{{ gitlab_label("UI Design") }}
: Issues which require user interface design work

{{ gitlab_label("UX Research") }}
: Issues relating to user experience research.

## Special-use labels

We have some labels which are used to indicate "special" issues which have
properties we care about. This section discusses these labels.

### Good first issues

Everyone will be new to a product at some point. Some issues may lend
themselves to a newcomer because they involve a small, well-defined task which
also illustrates an important feature of the product's architecture of
deployment. As a courtesy to people new to a product we use the {{
gitlab_label("good first issue") }} label to indicate these issues.

### Chores

The work required to address some issues is "obvious" or "well rehearsed". We
call these issues "chores" since they reflect repetitive work which many team
members could do. Examples of chores include: fixing typographic or grammatical
errors in documentation, sending an email requesting something from the rest of
the business or performing some manual configuration of a service.

We use the {{ gitlab_label("chore") }} label for such issues.

A "chore" may be sufficiently small that it can be addressed and closed without
progressing to review. Usually, however, it is good practice for even small
configuration changes to be put into {{ gitlab_label("workflow::Review Required") }}
as an additional check and to allow at least one other team member to see what
work had been done.

### Spikes

Some issues have the potential to be open-ended. These are usually issues which
require some form of research into a particular technology or business practice.
It can be hard to estimate such issues as they could expand to fill the
available time.

To make it possible to estimate work from a point of knowledge, an issue may be
labelled as with {{ gitlab_label("spike") }}. A spike is an issue which is
time-boxed, usually to one day. The team member picking up the spike does as
much work as they can within the spike's time window and reports back in the
issue.

The report can be used to more accurately gauge what work is required to close
an issue and how long it will take. It may also result in the team deciding to
close or de-sprint the issue if the work/reward ratio is not favourable.

Usually any code arising from a spike is considered "prototype" or "throwaway".
Occasionally the refinement of the code into a more polished initial version may
be added as a new issue.

### Placeholders

Inevitably, despite one's best efforts, issues will arise mid-sprint which need
to be addressed. In sprint planning we usually keep a small amount of time back
for these issues. When they arise we add the {{ gitlab_label("placeholder") }}
label to them.

!!! info
    Way back in the before times, we used to manage our sprint physically on
    boards with sticky notes. We would create dedicated "placeholder" notes in
    the sprint at planning time which would be exchanged for mid-sprint issues.
    By having a fixed number of "placeholder" notes, we ensured that we did not
    introduce too many additional issues.

    These sticky notes were indicated with a purple sticky dot for easy
    identification because we had a lot of purple dots. This is the reason for
    the name, and the colour, of the label.

### Quick to review

The {{ gitlab_label("quick to review") }} label can be added to issues in {{
gitlab_label("workflow::Review Required") }} to indicate to potential reviewers
that review should be a straightforward and quick process. If you find yourself
waiting for a long deployment or for a slow test suite to run, consider picking
up a quick to review issue to help move the sprint along.

### Comms required

The {{ gitlab_label("Comms Required") }} label is often used in combination
with {{ gitlab_label("workflow::Blocked") }}. It is used to indicate that the
work has been completed but that deploying the change cannot be done before
external communications have been sent out to users by the UIS Communications
team. They can be contacted at
[comms@uis.cam.ac.uk](mailto:comms@uis.cam.ac.uk).

## Modifying top-level labels

The labels discussed on this page are defined within the [uis/devops
group](https://gitlab.developers.cam.ac.uk/uis/devops) and are intended to be
shared as a common language between teams. As such it is considered courteous
to make changes to these labels in co-ordination with the Head of DevOps so
that consistency between teams can be maintained.
