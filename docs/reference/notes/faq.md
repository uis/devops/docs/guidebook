---
title: FAQ
---

# Frequently Asked Questions

This page features common questions that newcomers may have about the University,
UIS, or DevOps.

**What College code should I use for Murray Edwards?**
Prior to being renamed in 2008, Murray Edwards College was known as New Hall. As
such, different systems use different codes for the College. CamSIS refers to it
as [`NH`](https://www.camsis.cam.ac.uk/files/student-codes/a01.html), Lookup uses
both `NEWH` & `NH`, and new systems use `MUR`.
The consensus within DevOps is to keep using the New Hall codes in existing systems
and use `MUR` in new ones, taking care to translate codes consistently when
interfacing with systems that use a different College code.
