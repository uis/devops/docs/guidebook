---
title: Domain verification
---

# Domain verification for the Google Cloud platform

!!! warning

    Domain verification is no-longer required for Google-managed certificates.

When deploying services to Google Cloud platform (GCP), you will usually want
them accessible as "friendly" names under `cam.ac.uk`.

!!! important
    The contents of this note have moved to our [description of deployments to
    Google Cloud](site:reference/cloud-platform/dns).
