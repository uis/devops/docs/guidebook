# CI Pipelines

For every merge request there is an automated process, known as a CI
(continuous integration) pipeline, that runs a number of checks before the
merge request can be merged into the master branch. This pipeline is triggered
every time a new commit is pushed to the branch associated with the merge
request. The pipeline runs in stages, some of which are run in parallel while
others depend on the result of a previous stage (such as the build stage).

Common checks performed by the pipeline include verifying that:

* the project builds correctly
* our coding standards are adhered to
* the regression tests pass without a reduction in code coverage
* the licenses of dependencies are acceptable
* there are no known code vulnerabilities (security issues)

GitLab provides [Auto DevOps CI/CD templates](https://docs.gitlab.com/ee/topics/autodevops/)
that provide most of these checks. The templates that scan for licenses and
code vulnerabilities are particularly useful as they are regularly updated
with the latest common patterns.

CI pipelines help minimise the risk of merging code into the master branch that
breaks existing functionality or can't be deployed, as the checks performed by
all pipeline stages must pass before a merge can occur. The automated nature of
the pipelines ensures that the checks are always performed (as opposed to
relying on a developer to run such checks before pushing their commits).
