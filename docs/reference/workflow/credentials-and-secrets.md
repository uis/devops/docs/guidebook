# Shared credentials, secrets and external accounts

## Introduction

This page covers how DevOps manages shared credentials, secrets and external accounts.

IMPORTANT:
This page is incomplete

## Recovery email addresses

When there needs to be a real mailbox behind a user account email,
devops-account-recovery+{service}@uis.cam.ac.uk should be used as an email address
for service accounts/shared credentials.
