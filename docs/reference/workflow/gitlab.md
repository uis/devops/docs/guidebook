---
title: GitLab
---

We "[eat our own
dogfood](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)" and use the
[GitLab instance](https://gitlab.developers.cam.ac.uk) on the Developers' Hub to
manage our work. All team members should be part of the [DevOps
group](https://gitlab.developers.cam.ac.uk/uis/devops/) on GitLab.

For particular products there may be more members added to the product-specific
group.

## Golden rules

1. GitLab holds the entire state of the product development.
2. _Nothing_ should only be in your head.
3. External resources such as spreadsheets, documents or websites should always be linked to in
   either an issue or on a wiki page.
4. It is your responsibility to move things forward.

## What do I do day-to-day?

If you are at a loose end and want to move the product forward, look at the
sprint board. There are many ways to move forward:

* Pick up an issue which is "urgent" from the backlog, assign yourself and
* start work.
* Pick up an issue which is "review required" and review it.
* Pick up an issue which was assigned to you which is currently in "rework".
* Pick up a merge request which you have been set as a _Reviewer_.
* Pick up an issue which is "sprint ready" from the backlog, assign yourself and
    start work.

!!!note
    When scheduling a sprint we tend to order issues in rough order of priority.
    In general try to pick issues which are near the top or, for exceptional
    reasons, have been pre-assigned to you.

You should also keep an eye on issues which are in the "needs refinement" state
as you may be able to offer some insight in the discussion on the issue.

If none of these options are possible, contact the scrum master and they’ll try
to unblock you.

!!!note
    A more detailed description of day-to-day work is contained in the [common
    flows](flows.md) document.

## Team responsibilities

Although the scrummaster ensures that the process in this document is followed,
it is the responsibility of every team member to do their part.

Be proactive in monitoring issues and taking part in discussions.

Subscribe to notifications on the `uis/devops` group.

Do not wait until a refinement meeting to look at issues which "need refinement".

## GitLab concepts

There are a number of GitLab concepts which can have non-obvious names like
"issue", "epic", "milestone", "label", etc. This section provides an overview of
the terms.

### Issues

An issue is the smallest unit of work. It may form part of a larger epic. Each
issue belongs to zero or one sprint which is represented by its milestone.

An issue cannot be assigned to a sprint until it is "sprint ready". The bar for
being "sprint ready" is relatively low:

* It is sufficiently well specified that at least one team member can read the discussion on the
  issue, determine what needs to be done to make progress and can start work doing it and that
  another team member knows how to determine if the work has been done.
* It has a "size".
* It is not blocked by any issues which are not part of the same sprint.

### Merge Requests

A Merge Request represents a request to _merge_ some code changes into the
`master` branch. Merge requests are covered in more detail in [a dedicated
section of the guidebook](merge-requests.md).

### Milestones

Milestones are collections of issues with a "start" and "due" date. We use
milestones to document which sprint an issue is part of. Milestones are created
within the uis/devops/uga group.

Until an issue is in a sprint, it is "floating". It is a common pattern to
create an issue when a work item appears and "pencil it in" to the next sprint
but label it as "needs refinement" or "needs sizing" as appropriate. When we
come to plan the next sprint, this ensures these issues are visible and triaged
appropriately as being urgent enough to schedule in the upcoming sprint, move
the sprint after next or to keep "floating".

**All** work associated with a sprint should be added to the sprint milestone.
When addressing a GitLab issue make sure to add any related Merge Requests to
the milestone.

!!! warning
    Merge Requests are not listed on the board. If you want someone to pick up
    and review your Merge Request in good time, make sure it has a related issue
    which appears on the board.

### Sprint boards

A sprint board, for example
[https://gitlab.developers.cam.ac.uk/groups/uis/devops/uga/-/boards/630](https://gitlab.developers.cam.ac.uk/groups/uis/devops/uga/-/boards/630),
provides an overview of the current progress of a sprint. It contains all the
issues which form part of the sprint organised into columns showing where the
issue is in its lifecycle.

We create a new board for each sprint and set the "scope" of the board to be the
sprint's milestone. Each board will show the state of each issue in the sprint
and the progress through the issue lifecycle.

Issues can be re-ordered within columns and dragged from column to column. In
particular we find that it is useful to order the "sprint ready" column in rough
priority order so that issues toward the top are higher priority than issues
near the bottom.

## The issue lifecycle

Issues are born, are refined, are done and die. The progress of an issue is
called the "issue lifecycle". Diagrammatically, the issue lifecycle is
represented by the following graph:

![The issue lifecycle](issue-lifecycle.png)

### Start

An issue needs to be created. Many things can prompt an issue creation from an
_ad hoc_ comment in a conversation to a formal specification process. Creating
issues is supposed to be a lightweight, common process.

The minimum required to create an issue is a title and a description. Usually
one of the "workflow::needs refinement" or "workflow::needs sizing" labels will
be applied at creation time. Urgent issues are distinguished by applying the
"urgent" label.

Some issues will naturally have deadlines associated with them. In such cases a
deadline is added to the issue in GitLab which will make sure to email the
project in advance of the deadline if the issue is not closed.

### Needs refinement

An issue starts as a description and title. Sometimes this is enough in that the
issue is sufficiently small that it can be tackled immediately. It is more often
the case that the work required to move the issue forward is unclear.

In this case the "workflow::needs refinement" label is added to the issue.

Discussion takes place on the issue until a way forward becomes clear. It can be
that the discussion results in the original issue being closed and split into
smaller sub-issued.

Once the work required to move the issue forward is clearly specified either in
the description or in the discussion, the issue is re-labelled as
"workflow::need sizing". Anyone on the team can apply this label if they feel
the issue is refined.

### Needs sizing

There is a weekly opportunity for team members to help sizing issues. The process for sizing issues
is described in a dedicated [how-to guide](site:howtos/git-gitlab/size-issues).

Once sized and refined, an issue gets the "workflow::sprint ready" label.

!!! note
    Issues with the "spike" label are timeboxed. They are issues which could
    expand without bound and so we set a hard maximum of time that can be spent
    on them before the person doing the spike reports back in the issue.

### Sprint ready

Once "sprint ready", an issue is eligible for scheduling in a sprint. This is
usually done by the scrum master depending on priorities at the time.

An issue is scheduled into a sprint by adding it to the appropriate sprint
_milestone_.

### In Progress

When an issue is picked up by a team member, they:

* Assign themselves to the issue
* Add the "workflow::in progress" label

Ordinarily we do not "pre-assign" issues except where there is a clear need.
Pre-assignment is an early indicator of single points of failure. Reasons for
pre-assignment should be clear and it should be the exception rather than the
rule.

Reasons for pre-assignment can include selecting educational issues for junior
or mentee team members. In that case care should be taken when sizing to account
for the time that a senior or more experienced team member will spend helping
the assignee.

### Review required and Needs deployment

!!! info
    This section has moved to [a dedicated page](site:explanations/git-gitlab/reviewing-issues).

### Rework

You should keep an eye on the board for issues which are in rework which are
assigned to you. Go into the issue and review the comments.

If there is simply a question, answer it and add the "workflow::review required"
label.

If there is more work to do, add the "workflow::in progress" label and continue work
on the issue. Once the work is finished, move the issue forward as discussed
above in "In Progress".

### Closing an issue

Occasionally the work described by an issue may either become unnecessary, be
replaced by another issue or become merged with another issue. In those
circumstances is is fine to just close the issue with a note explaining why it
wasn’t necessary any more or where the work has moved to. For issues that are
complete apply the label "workflow::done". If the issue is abandoned with no
intention to complete, the "resolution::DevNotPlanned" label can also be applied.

## Labels

!!! note
    This section has moved to a [separate page](site:reference/git-gitlab/gitlab-labels).

## Groups and projects

Each product has its own group on GitLab. These are usually sub-groups of the
DevOps group.

Within each product's group, there are usually one or more projects. Projects
can be roughly grouped into the following categories:

* **Support and documentation.** Projects which have repository hosting disabled
    and exist purely as wiki and/or issue tracking projects. These can be the
    "public" side of private repos if we want to offer public developer focussed
    documentation or a public issue tracker.
* **Deployment.** Projects which contain code to deploy a particular product.
    These usually only contain deployment configuration and secrets. They are
    almost always private as they contain deep details of how a product is
    deployed.
* **Implementation.** Projects which contain the implementation of products. We
    try to structure our products with the idea that they may be deployed in
    multiple places or by multiple people. As such the implementation of a
    product should not assume it will be deployed in a particular place or at a
    particular URL.

When creating new projects in GitLab we try to follow the conventions below.

### Open by default

Our software projects are Open Source by default. As such the GitLab project
should be made public except where:

1. It contains internal details of deployments. (These are usually the "deploy"
   repos.)
2. It is a project entirely devoted to tracking operational issues with a
   particular deployment where tier 3 issues need to be escalated.
3. If contains grandfathered code which has not been audited.

We run our projects as Open Source projects and so the code should be general,
understandable and written with the assumption it may be deployed in multiple
places. Try to avoid Cambridge-specific hard-coded values although
Cambridge-specific defaults are fine.

### License

Our default license is the [MIT-style
license](https://opensource.org/licenses/MIT).

### Where

Usually a project will be created within a sub-group of the
[DevOps group](https://gitlab.developers.cam.ac.uk/uis/devops/).

### Naming

Naming is hard. When providing the "short name" for a project, consider that it
will appear in URLS. As such it is better to have projects named like
``uis/devops/iam/authentication/raven/infrastructure`` rather than
``uis/devops/iam/authentication/raven/raven-infrastructure``.

The "long name" should make sense in isolation so, for the example above, naming
the project "Raven Deployment Configuration" makes sense.

### Settings

New projects should have the following settings changed:

* Require 1 approver for all Merge Requests
* Default to "delete branch after merging" for Merge Requests

### Content

Create the project with a README. This provides an initial commit which forms
the ``master`` branch. The contents of the repository should be opened as the
first merge request for review.

### Default issue template

Under Settings > General > Default issue template, paste the following:

```md
**DO NOT POST PERSONAL OR CONFIDENTIAL DATA IN THIS PROJECT**

This project is hosting an Open Source project. By design the development
process is open to be inspected. Issues should be related to the code itself and
bugs which affect all deployments. Do not open issues relating only to a single
deployment.

It is useful if you follow [Simon Tatham's guide for reporting
bugs](https://www.chiark.greenend.org.uk/~sgtatham/bugs.html). At a minimum we
would need:

* Browser and Operating System vendor and version numbers.
* A description of what you were *trying* to achieve.
* A description of what you did in order to achieve it.
* A description of what you expected to happen.
* A description of what actually happened.

If we don't have enough information to reproduce your issue, we will probably
not be able to address it.
```

## GitLab "netiquette"

This section lists some "netiquette" rules for GitLab we've found useful over
time.

### Create issues liberally

If a task appears organically in a conversation, feel free to create a quick
issue and tag it as "needs refinement". A common convention is to "pencil an
issue in" to the next sprint so that it is visible for the next refinement
session.

### Consider people’s inboxes

Whenever you create an issue, it emails people
watching the project. Try to make sure that initial email will be useful. A
great issue will have the following when "Save" is pressed:

* A descriptive title
* A clear body
* Any labels it needs to

The comments section in an issue supports two forms of reply: creating a new
discussion thread or responding to an existing one. If responding to a
particular thread, please try to do so _within_ the thread and don’t start a new
discussion.

### Create group-wide labels

It is rare that a label applies _only_ to issues within a particular project. If
in doubt, create them within `uis/devops`.

### Be concise

Think of discussion threads as a chat client rather than email. As such,
cuddling your comments with "Hi, X" and "Thanks" are superfluous.

### Quote when necessary

When replying to one part of a long comment, quote the part you are replying to.

### Assume you're talking in a public forum

**Always** be mindful that GitLab supports open projects as well as closed ones.
**Never** assume that anything said in an issue is confidential. If in doubt,
make an issue confidential before replying.

### Don't trap words or screenshots in a Word document

Paste the contents of a document into a discussion thread or wiki page. Include
a screenshot inline.

### Use attachments lightly

Rather than attaching, for example, a spreadsheet to an issue, consider
uploading it to a Google Shared Drive for the product and pasting a link in.

### Use @-tags appropriately

If you @-tag someone, they will get an email drawing their attention to a
thread. This is useful if you want to bring someone into a discussion. Don’t
over use it!

### Don’t be precious

GitLab and the state within it should be seen as being owned by the team
collectively. Never be afraid to add a label, move an issue forward or comment
on a discussion. Respect the rights of others to do the same.

### Explain your actions

A GitLab issue should be readable from top-to bottom. If you change the state of
an issue, move it, re-label it, etc provide a one-line summary of what you're
doing and why to help make the issue readable.

### Don't assume everyone is familiar with the process

We have a fairly light process but a new team member may still not be familiar
with it. In particular, they may not have read this document from end-to-end. If
you spot an issue with the wrong label, etc. feel free to fix it but respect
that the original author may be unaware of convention.
