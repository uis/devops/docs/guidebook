<!-- markdownlint-disable MD041 -->
??? tip "Log an Incident"
    To **log/escalate an incident**, **report a bug** or **submit a feature request**,
    follow the instructions on [our contact page](/home/contact/index.md).
<!-- markdownlint-enable MD041 -->
