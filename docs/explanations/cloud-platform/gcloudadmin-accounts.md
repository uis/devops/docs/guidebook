# Google Cloud admin accounts

As noted in the [checklist for products](site:howtos/products/check-product-configuration),
development team members who need to administer, deploy or examine Google Cloud resources need
to be listed in our [team data file](
https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json)
and given a Google account of the form `{crsid}@gcloudadmin.g.apps.cam.ac.uk`.

This document contains specific guidance for team members about this account. We use the term
"Google Cloud admin" account in this document to refer to these accounts.

## Expectations on you as a developer

Your Google Cloud admin account is powerful. It is expected that you:

* Ensure that the account is configured with multi-factor authentication.
* Use a strong password.
* Limit the number of places you sign in with the account.
* Do not leave long-lived browser sessions signed in to the account.
* Only use the account in the browser to access the [Google Cloud
  console](https://console.cloud.google.com/).
* Use the account when authenticating to the `gcloud` SDK tool.

## Do's and don'ts

DO NOT:

* Use the Google Cloud admin account as the default Google session.
* Use the Google Cloud admin account to sign in to Chrome.
* Enable any additional Google Services for the Google Cloud admin account via the [Google Workspace
  preferences
  app](https://preferences.g.apps.cam.ac.uk/) *except* for the Google Cloud service.
* Use the Google Cloud admin account to configure a Google developer profile.
* Use the Google Cloud admin account to "sign-in with Google".

DO:

* Use the Google Cloud admin account within a private browsing tab when feasible.
* Use your `{crsid}@cam.ac.uk` account for day-to-day browsing.
* Consider using 1password's passkey support or hardware passkeys such as Touch ID to
  sign in to the account. The use of passkeys is [documented on Google's support
  site](https://support.google.com/accounts/answer/13548313).

## More information

* [How to create a Google Cloud admin account](site:howtos/cloud-platform/create-gcloudadmin-account).
