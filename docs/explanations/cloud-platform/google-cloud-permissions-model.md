---
title: Permissions in Google Cloud
---
# Permissions in Google Cloud

In the [service infrastructure guide](./service-infrastructure.md), we covered how the team data
terraform module is used to drive memberships of Google Groups.

This guide covers what groups we use and how they are granted permissions in Google Cloud.

## Team data, gcp-workspace-management and the Google Cloud Project Factory

As a brief recap of the [service infrastructure guide](./service-infrastructure.md):

- We group sets of users who can manage products into "teams". Each team has a set of role groups
  associated with it.
- Team and role group membership is controlled by a [JSON
  file](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json).
- Groups defined in the team data file are used by
  [gcp-workspace-management](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-workspace-management)
  to populate Google groups.
- Google groups are granted permissions in Google Cloud by
  [gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/).

!!! question "What is a team?"

    The granularity of teams can vary depending on the nature of the product. For example,
    particularly sensitive products may want their own "team" to tightly restrict powerful roles
    whereas other products can be grouped into generic teams which align with the line management
    structure in the division. The decision on granularity is ultimately one for the product team to
    decide.

    A reasonable default is to start with a team aligned with the line management team and become
    more granular if necessary.

    **DO NOT** add new users to the division-wide `devops` team since, for historical reasons, that
    team has broad permissions on all products.

## Roles

We generally assign three roles within a team:

- **Editors** can sign in to the Google console and have full edit rights on all resources. These
  should always be special-purpose [gcloudadmin accounts](./gcloudadmin-accounts.md).
- **Deployers** cannot directly modify Google Cloud resources in the Google console but they *can*
  impersonate the `terraform-deploy@...` service account and so run `terraform` via `logan`. At a
  practical level, deployer users are restricted to actions which `terraform` can perform when
  deploying. These permissions are set in the Google Project factory configuration.
- **Viewers** can sign in to the Google console and inspect all resources but cannot modify them.

An "ordinary" developer within a product team would likely be granted the deployer and viewer roles
allowing them to inspect the state of a deployment in the Google cloud console but restricting their
actions to those configured in the Google project factory.

## How roles get their permissions

Roles defined in the team data JSON file are mapped into Google groups by gcp-workspace-management.
For example, the `hr-editors@g.apps.cam.ac.uk` group consists of those users with the "editor" role
within the "hr" team.

Individual products within the Google product factory then specify which groups should have which
roles within the product. Taking the example of the [Ballots of the Regent House product
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/product-vars/regent-house-ballots/regent-house-ballots.tfvars?ref_type=heads):

```tf title="product-vars/regent-house-ballots/regent-house-ballots.tfvars"
additional_editor_users = [
  "group:jackson-admin@gcloudadmin.g.apps.cam.ac.uk",
]

additional_deploy_users = [
  "group:jackson-admin@gcloudadmin.g.apps.cam.ac.uk",
  "group:jackson-deploy@gcloudadmin.g.apps.cam.ac.uk"
]

additional_view_users = [
  "group:jackson-admin@gcloudadmin.g.apps.cam.ac.uk",
  "group:jackson-deploy@gcloudadmin.g.apps.cam.ac.uk",
  "group:jackson-view@gcloudadmin.g.apps.cam.ac.uk"
]
```

Notice how the configuration ensures that editors have at least the permissions of deployers and
that deployers have at least the permissions of viewers. This is not strictly necessary but ensures
that permissions always increase as one moves from viewer to editor.

Deployers can modify the same set of resources which the terraform deploy service account can. The
Google Project factory assigns a [default set of
permissions](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/b3ce173bfa6fcd64f41d20cf52c13e3362dd3309/locals.tf#L84)
to the terraform deploy service account but products can request additional ones. For example, from
the [gitlab bot deployment
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/product-vars/gitlab-bots/gitlab-bots.tfvars?ref_type=heads):

```tf title="product-vars/gitlab-bots/gitlab-bots.tfvars"
terraform_sa_additional_roles = [
  "roles/cloudscheduler.admin",
  "roles/file.editor",
  "roles/pubsub.admin",
]
```

Deployers in the GitLab bots projects will, by impersonating the terraform deploy service account,
additionally be able to modify Cloud Scheduler, Cloud Filestore and Pub/Sub resources.

## See also

- [How to start a new product](site:howtos/products/bootstrap-a-product)
- [The explainer guide on our service infrastructure](./service-infrastructure.md)
