---
title: Azure and Entra ID Applications
---

# Azure API Management and Entra ID Applications

This document provides a high-level overview of Azure API Management and Entra ID Applications. It
covers what they are, when you want to use them, the difference between Azure API Management and
Entra and provides pointers to how we configure them in DevOps.

The overlapping use and overloading of terminology in this area can be confusing. This explainer
guide aims to remove some confusing.

## Microsoft products and their naming

Although initially confusing, Microsoft do try to keep a degree of consistency in their product
naming with the use of prefixes:

* **Entra** usually denotes a product related to the management of identity within an organisation.
* **Azure** usually denotes a cloud product intended to be used to host a service or to be used as
  part of hosting that service.

The two products most relevant to this guide are:

* **Entra ID, formerly Azure Active Directory** which, for our purposes, is a product which provides:
    * an [OAuth 2.0](https://oauth.net/2/) and [OpenID
      Connect](https://openid.net/developers/how-connect-works/) compliant authentication server,
    * a directory of real-person user identities,
    * a directory of non-person identities,
    * a means to group those identities,
    * a directory of registered applications which users can sign in to,
    * a means to define custom roles on those applications driven by membership of groups, and
    * a set of permissions associated with applications which indicate which actions an
      application may perform.
* **Azure API Management** which, for our purposes, is a product which provides:
    * proxying of incoming HTTP requests to API backends, and
    * authentication and authorisation via Entra ID-issued access tokens.

**There is no such thing as "Azure API Gateway".** Using terms like "API Gateway" risks confusion
with the [API Gateway service](site:services/api-gateway).

## The three hats of an Application Registration

In Entra ID and Azure, application registrations are used for the following purposes:

* To record *sign in applications*. These are applications which a user will sign in to and will
  usually have an associated web interface. Examples might include: "CHRIS Frontend", "Punt Booking
  System", "UIS Intranet", etc.
* To record *API client applications*. These applications represent some non-person identity which
  calls APIs hosted by Azure API Management.
* To record *API server applications*. These applications represent an API hosted by Azure API
  Management.

The fact that Entra ID and Azure application registrations wear all three hats means that not only
can it be confusing when discussing with people which hat your application is wearing, it also makes
searching the Microsoft documentation difficult.

Application registrations each have an "application id". This id is *global*. That is to say that
the application id you register is unique between all Microsoft tenancies worldwide.

For example, the Microsoft Graph API is itself an "API server application". It's application id is
`00000003-0000-0000-c000-000000000000`. As application ids are global, this id is the same in each
tenancy.

## Application credentials

Application credentials allow an application to authenticate *itself* to Entra ID. Sign in apps will
usually ask Entra ID to authenticate the current user and provide its own credentials alongside so
that Entra ID can inform the user which application they are signing in to.

??? note "Single page web applications are a special case"

    Single page web applications run entirely in the user's web browser and so cannot include any
    secret application credentials. These are called "public" or "non-confidential" clients and
    use OAuth 2.0 [Authorization Code with PKCE](https://oauth.net/2/pkce/) flow to securely
    authenticate the user without needing application credentials.

    In this case Entra ID relies on the behaviour of web browsers in that it will redirect the user
    to a URL registered with the application. It is assumed that the application is in control of
    this URL and that users being redirected to it is sufficient to ensure that the appropriate
    application recieves the sign in token.

API client applications will generally provide their credentials directly to Entra ID and receive an
access token back which they can use to call other APIs.

### Client secrets

Sign in applications usually make use of client secrets to authenticate themselves as part of the
sign in flow.

### Federated credentials

API client applications may also make use of *federated credentials* to authenticate themselves.
A federated credential is configuration which delegates authentication to a third-party.

In our case we usually configure Google Cloud as the third party. This lets us directly use Service
Account id tokens to authenticate to APIs. Further Service Account id tokens can be created by
Google Cloud workloads in Cloud Run with no additional secrets.

This lack of secret management makes federated credentials a far preferable choice for calling APIs
from Google Cloud-hosted products.

## Authentication of users and applications

Sign in applications will usually use OAuth 2.0 to interactively authenticate the user in front of
them. The application will then access APIs *on behalf of* that user. Sign in apps generally perform
actions *as the user that signed in*.

API client applications will usually use client credentials to authenticate themselves. API client
applications generally perform actions *as themselves* as there is no concept of the "signed in
user".

## Permissions

An application can be granted permissions. Usually only API client or sign in applications are
granted permissions.

Permissions are always granted on another application, usually an API server application.

The permissions indicate which actions the API client or sign in application can perform using the
API associated with the API server application. Sign in applications may also have *delegated*
permissions which describe which actions they can perform *on behalf of* the user which signs in.

For example, the Microsoft Graph API has two permissions, a delegated permission called `User.Read`
and a non-delegated permission called `User.Read.All`.

A sign in application may be granted the delegated `User.Read` permission. That lets the application
read users from the Graph API as the signed in user. This will usually mean that it can read only
the signed in user's profile.

An API client application may be granted the `User.Read.All` permission on the Microsoft Graph
API. This allows the API client application to read details of *all* users in the directory.

!!! note

    Microsoft provide a [full list of Graph API
    permissions](https://learn.microsoft.com/en-us/graph/permissions-reference). Some permissions
    may be both delegated and non-delegated!

As application ids are global, an application from one tenancy may be granted permissions on an API
hosted in another. For example, the Microsoft Graph API is not *hosted* in our tenancy but we can
grant applications from our tenancy permissions on it.

## Roles

Usually only sign in apps will have roles configured on them. A role is a short name which is
associated with one or more Entra groups. When users sign in to the application, the OpenID connect
id token which is passed to the application will have a `roles` claim which lists all the roles
which the user has in the application.

This can be used for authorisation in your web applications. For example, you might have an `admin`
role in the application which you associate with membership of a particular group. When a user signs
in to your application, you can use the presence of `admin` in the `roles` claim to authorise
certain operations.

!!! example

    To see a live example, look at the [single page web application
    example](https://entra-app-factory-uis-devops-infra-946927de0488b373ac33fb3cd729.uniofcam.dev/spa.html)
    from the Entra ID Application Factory project. If you sign in to that application and look at
    the id token claims which are returned you'll see a `roles` claim. If you're a member of the UIS
    DevOps division then `devops-division` will appear there.

    The
    [configuration](https://gitlab.developers.cam.ac.uk/uis/devops/infra/entra-app-factory/-/blob/main/applications/development/example.yaml?ref_type=heads)
    for this application can be found in the Entra ID Application Factory project.

## Registration of applications

Microsoft provide a means to register applications via the Entra ID portal. For most users this
ability is disabled in our tenancy. Some of the functionality is exposed via the [UIS
Toolkit](https://help.uis.cam.ac.uk/service/accounts-passwords/it-staff/university-central-directory/toolkit).

Within the DevOps division we have automated the process of application registration by means of a
terraform configuration called the [Entra ID Application
Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/entra-app-factory/).

The [Application Factory
README](https://gitlab.developers.cam.ac.uk/uis/devops/infra/entra-app-factory/-/blob/main/README.md?ref_type=heads)
should be seen as the best reference for the exact syntax used to define applications.

In brief, each application is configured by a YAML file in `applications/production/`. Applications
can be one of two types: sign in applications and API client applications.

## Summary

In this explainer we covered:

* the three types of application: sign in, API client and API server,
* permissions which can be granted to sign in and API client applications,
* roles which may be configured on sign in applications, and
* the Entra ID Application Factory we use to register applications.

## See also

* [Useful URLs and IDs for dealing with Entra ID Applications](site:reference/misc/entra-id)
* [How to add sign in to a web application](site:howtos/entra-azure/add-sso-sign-in)
