---
title: Budgets and Alerts
---

# Budgets and Alerts

This document explains how billing budgets and alerts are configured for our
Google Cloud Platform (GCP) products. For information on how to react to
triggered alerts see [How to respond to billing
alerts](site:howtos/cloud-platform/respond-to-billing-alerts).

## Introduction

In the dynamic landscape of cloud computing, where flexibility and scalability
are key advantages, organisations face the challenge of efficiently managing
resources while keeping a close eye on costs. The pay-as-you-go model, inherent
to cloud services, allows businesses to scale their operations seamlessly;
however, without proper oversight, it can lead to unforeseen expenses. This is
where the strategic use of budgets becomes paramount.

Cloud computing budgets serve as financial guardrails, empowering organisations
to navigate the intricate cost structures of cloud platforms. By establishing
spending limits and financial plans, businesses can gain control over their
cloud expenditures, ensuring that the benefits of scalability do not translate
into budgetary overruns. This proactive approach not only aids in cost
containment but also facilitates resource optimization, financial planning, and
enhanced accountability.

## How we configure budgets and alerts

The [GCP product
factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
is responsible for configuring default billing budgets and alerts for our GCP
products. Each product deployed by the factory will have mandatory `budget`,
`cost_centre`, and `team` variables configured in the relevant `.tfvars` file.
These variables are then used to configure the following.

### Budgets

A monthly budget is configured using the `budget` variable. This budget is an
amount, specified in € (EUR)[^1], encompassing the total cost of _all_ projects
deployed for the specific product. For example, if a product is configured with
the standard `development`, `staging`, and `production` workspaces, the budget
would include the total cumulative spend of the three workspace projects _and_
the meta project for each monthly invoice period.

The budget amount should be approximately 20% above the normal usage for a
specific product. This gives us enough headroom to allow for acceptable spikes
during peak times without triggering unnecessary alerts. For new products, this
amount will initially be a best guess, however, once we have enough data the
amount should be reviewed and amended as required.

### Alerts

The product factory configures two alerts by default. The first alert triggers
when the budget is _forecasted_ to exceed 100%, the second triggers when the
budget _exceeds_ 100%.

See [How to respond to billing alerts](site:howtos/cloud-platform/respond-to-billing-alerts)
for information on how to handle these alerts when they're triggered.

### Tags/labels

The `cost_centre` and `team` variables are added to the meta project as labels.
This allows us to see at a glance who is responsible for the resources in a
given product. While the corresponding product team is responsible for ensuring
their budgets remain below 100%, any increase in budget needs to be authorised
by the Head of DevOps, [Abraham
Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203).

<figure markdown>
![cost centre labels](./meta-project-cost-centre-labels.png)
<figcaption>An image showing the "cost-centre" and "team" labels assigned to an
example product's meta project.</figcaption>
</figure>

## See also

- [How to respond to billing alerts](site:howtos/cloud-platform/respond-to-billing-alerts)

[^1]:
    Our reseller, TI Sparkle (or just Sparkle), is an Italian company which is
    why we specify our budget amounts in € (EUR).
