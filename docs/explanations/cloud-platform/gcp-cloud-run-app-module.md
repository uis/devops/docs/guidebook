---
title: Migrating to Google Cloud Run App Terraform module v9.0.0
---

# Migrating to Google Cloud Run App Terraform module v9.0.0

This document outlines the changes and considerations required when migrating from previous
versions of the [Google Cloud Run application terraform module](
https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/) to version
`9.0.0` and above. For step-by-step migration instructions please see the associated [How-to page](
site:howtos/development/migrate-a-gcp-cloud-run-app-terraform-deployment-to-v9.md).

Before the release of version [9.0.0](
https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/-/releases/9.0.0),
our in-house developed [Google Cloud Run application terraform module](
https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/)
used the Cloud Run Admin API V1. However, the Cloud Run Admin API V2 is now
[recommended](
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service)
as it offers a better developer experience and broader support of Cloud Run
features. Therefore, starting with the `9.0.0` release, the Cloud Run App module
now exclusively uses the Cloud Run Admin V2 API. This is a breaking change which
_will_ require teams to perform some refactoring to migrate to. This page
explains some of the key changes and considerations that should be taken before
migrating to version `>= 9.0.0`.

## Key changes in version 9.0.0

### Load balancer

Many of our ["gcp-deploy-boilerplate"](
https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate) template deployments are
configured to use a Cloud Load Balancer via the `webapp_use_cloud_load_balancer: true` Terraform
variable. Previously, this variable enabled load balancer resources which were declared at the
project's top level in `webapp_load_balancer.tf`. However, in the latest version of the
boilerplate this file has been removed as we now make use of the `enable_load_balancer` option
of the Cloud Run App module directly. Therefore, if your project currently sets
`webapp_use_cloud_load_balancer: true` it is necessary to add following `moved` blocks to the
project's Terraform code to avoid the existing load balancer being destroyed.

```tf
moved {
  from = google_compute_region_network_endpoint_group.webapp[0]
  to   = module.webapp.google_compute_region_network_endpoint_group.webapp[0]
}

moved {
  from = module.webapp_http_load_balancer[0]
  to   = module.webapp.module.webapp_http_load_balancer[0]
}
```

### Module variables

Since version `9.0.0` of the Cloud Run App Terraform module uses the Cloud Run
Admin V2 API exclusively, many of the module's input variables have changed.
Below you'll find details of which variables have changed and how existing
configurations must be updated when migrating. The full list of variables with
detailed explanations can be found in the module's [README file](
https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app/-/blob/master/README.md).

- `image_name`: this variable is not declared at the top level anymore. Instead, each container in
  `containers` map has its own `image` key:

  ```tf
  containers = {
    webapp = {
      image = "${local.container_images.webapp_base}:${local.container_images.webapp_tag}"
      env = [{
        name  = "EXTRA_SETTINGS_URLS",
        value = local.extra_settings_urls
      }]
    }
  }
  ```

- `cloud_run_region`: has been changed to `region`.
- `max_scale`: scaling config has been moved to `scaling` map:

  ```tf
  scaling = {
    max_instance_count = 10
  }
  ```

- `dns_name`: has been removed after long deprecation period. Now `dns_names` is used insted.
  Contains a map of dns names:

  ```tf
  dns_names = {
    webapp = (
      (!local.webapp_use_cloud_load_balancer && local.domain_verification.verified)
      ? coalesce(local.webapp_custom_dns_name, trimsuffix(local.webapp_dns_name, "."))
      : ""
    )
  }
  ```

- `sql_instance_connection_name`: has been changed to `mount_cloudsql_instance`.
- `disable_monitoring`: has been renamed to `enable_monitoring` and the new default behaviour is to
   disable monitoring.
- `environment_variables`: now this variable is not declared at the top level. Instead, each
  container in `containers` map has an `env` key with a list of maps (see example above).
- `allowed_ingress`: changed to `ingress`. The ingress setting for the Cloud Run service.
  Possible values are `INGRESS_TRAFFIC_ALL`, `INGRESS_TRAFFIC_INTERNAL_ONLY`, and
  `INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER`. If variable `use_load_balancer` is set to `true`,
  the provided `ingress` value will be ignored and the ingress will be set automatically to
  `INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER`.
- `alert_notification_channels`: has been changed to `alerting_notification_channels`.

### Deleted features

In version `9.0.0` the Domain mapping feature was dropped so the module now has no related outputs
`domain_mapping_present`, `domain_mapping_resource_record`, and `domain_mapping_dns_name`.
This GCP functionality is not recommended for production usage as this is ["Pre-GA feature"](
https://cloud.google.com/run/docs/mapping-custom-domains#run) and it's been in this stage for
a very long time. Besides that, it has a number of limitations, such as limited availability across
GCP regions, [latency issues](https://cloud.google.com/run/docs/issues#latency-domains) and more.

If you were using Domain mapping it is likely the CloudRun instance was located in a different
region to of the other cloud resources, hence the variable `cloud_run_region`. This migration will
move the CloudRun instance to the same region as the other cloud resources. Note, the region is
included in the automatically assign URLs, i.e. `https://webapp-xxxxxxxxxx.europe-west2.run.app`.
The change to these URLs can impact integrations with other services, for example the API Gateway.

### Other changes

The previous major release, `8.0.0`, introduced the `enable_pre_deploy_job` variable which,
when set to `true`, creates a Cloud Run job to execute a configurable command before new Cloud Run
service revisions are deployed. This is a useful way to run database migrations and other commands
which are tightly coupled to the release cadence of the main Cloud Run service. However, in version
`9.0.0` of the module, the pre-deploy job syntax has changed. Below you'll find examples of
the "pre-deploy" job implementations using both the old and new syntax.

For `gcp-cloud-run-app` module versions `>= 8.1.0` and `< 9.0.0`:

```tf
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 8.1"

  # ...

  enable_pre_deploy_job  = true
  pre_deploy_job_command = ["python3"]
  pre_deploy_job_args    = ["/usr/src/app/manage.py", "migrate"]
}
```

For `gcp-cloud-run-app` module versions `>= 9.0.0`:

```tf
module "webapp" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-cloud-run-app/devops"
  version = "~> 9.0"

  # ...

  enable_pre_deploy_job = true
  pre_deploy_job_container = {
    image   = "image:tag", # need to specify image
    command = ["python3"],
    args    = ["/usr/src/app/manage.py", "migrate"]
  }
}
```
