---
title: Renovatebot
---
# Keeping dependencies fresh with renovatebot

Our GitLab projects tend to hard-code a lot of dependency versions. Maven, poetry and npm all
support specifying which versions of dependencies need to be installed and terraform configurations
depend on particular provider versions.

Keeping on top of these dependencies is a challenge. In particular it is a challenge to keep on top
of dependencies which prove a security risk due to known vulnerabilities. We employ container and
dependency scanning in GitLab CI which helps with awareness of vulnerabilities but it is still
something of a chore to freshen dependencies when necessary. The exact scan schedule may change
as the service evolves but will always comprise multiple runs per week. If you require an exact
schedule, [trigger renovatebot from
a scheduled CI job](site:howtos/renovatebot/trigger-renovatebot-from-ci.md).

[Renovatebot](https://docs.renovatebot.com/) is a piece of software which regularly scans projects
in GitLab and maintains Merge Requests (MRs) which freshen dependencies.

<figure markdown>
![](./renovatebot-mr.png)
<figcaption markdown>
An [example
MR](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/-/merge_requests/24)
maintained by renovatebot.
</figcaption>
</figure>

The renovatebot documentation has a more information on [how renovatebot
works](https://docs.renovatebot.com/key-concepts/how-renovate-works/).

## More than just packages

While renovatebot can keep dependencies from maven, poetry and npm up-to-date that is not the limit
of its abilities. Some other features which are of use to DevOps:

* Automatically applying updates from our webapp and GCP deployment boilerplate template
  repositories. ([Example
  MR](https://gitlab.developers.cam.ac.uk/uis/devops/experiments/rjw57/misc/-/merge_requests/4))
* Keeping [pre-commit hooks](site:explanations/development/webapp-boilerplate#pre-commit) fresh. ([Example
  MR](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/-/merge_requests/24))
* Making sure that we're always using the most recent version of our [CI
  templates](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates).
  ([Example
  MR](https://gitlab.developers.cam.ac.uk/uis/devops/experiments/rjw57/gitlab-utils/-/merge_requests/14))
* Creating MRs for deployment projects when a new version of a docker image is
  available. ([Example MR](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/-/merge_requests/19))
* Freshening terraform providers. ([Example
  MR](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/-/merge_requests/8))

## The renovatebot workflow

### Onboarding

Renovatebot will create an "onboarding" Merge Request in each project
([example](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config/-/merge_requests/11)).

<figure markdown>
![](./renovatebot-onboarding-mr.png)
<figcaption markdown>
An example "on-boarding" Merge Request created by renovatebot.
</figcaption>
</figure>

This on-boarding MR will list the dependencies renovatebot has found in your project and a preview
of what it intends to do. If this all looks good, merge the MR to start getting renovatebot updates.

### Configuration

The on-boarding MR will create a `renovate.json` configuration file for renovatebot. The default
configuration references our [default
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config):

```json title="renovate.json"
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "local>uis/devops/renovate-config"
  ]
}
```

Renovatebot is *very configurable* and configuration can be set per-project in the `renovate.json`
file.

You may find that you end up adding the same configuration to multiple places. Renovatebot has a
mechanism called "presets" which can help with this.

!!! note "See also"

    Renovatebot's documentation has a longer [discussion of
    presets](https://docs.renovatebot.com/key-concepts/presets/).

For example, some teams may prefer to have renovatebot run on Sundays so that MRs are ready to
review on Mondays. The default configuration has renovatebot run on week days but the
[renovate-config project](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config) also
provides a "preset" configuration snippet called
[scheduleSundays](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config/-/blob/main/scheduleSundays.json?ref_type=heads).
Projects can "extend" their default renovatebot config to make use of that preset:

```json title="renovate.json" hl_lines="5"
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "local>uis/devops/renovate-config",
    "local>uis/devops/renovate-config:scheduleSundays"
  ]
}
```

You can [host your own presets in GitLab](https://docs.renovatebot.com/config-presets/#gitlab) and
share them with all your projects.

### The dependency dashboard

Renovatebot will maintain a "dependency dashboard" issue which you can bookmark
([example](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp/-/issues/287)).

The dashboard will give you an overview of which dependencies renovatebot has detected in your
project and any open MRs still needing attention.

Each MR has a tick box next to it. Ticking the box will cause renovatebot to attempt to update and
rebase that MR when it next runs.

!!! note "See also"

    The renovatebot documentation has a longer [discussion of the dependency
    dashboard](https://docs.renovatebot.com/key-concepts/dashboard/).

<figure markdown>
![](./renovatebot-dep-dashboard.png)
<figcaption markdown>
An example dependency dashboard issue.
</figcaption>
</figure>

### Freshening MRs

Renovatebot maintains a number of "freshening" MRs
([example](https://gitlab.developers.cam.ac.uk/uis/devops/experiments/rjw57/gitlab-utils/-/merge_requests/15))
which update dependencies. Our default configuration will group all the non-minor dependency updates
together into a single MR abut keep major dependency upgrade MRs separate.

<figure markdown>
![](./renovatebot-freshen-mr.png)
<figcaption markdown>
An example MR updating all the non-major dependencies in a project.
</figcaption>
</figure>

The MR will, when possible, include changelog summaries for each package freshened so that you can
see what's changed.

## Tips-and-tricks

This section covers some general tips-and-tricks for renovatebot.

### Assigning reviewers automatically

Renovatebot can auto-assign reviewers to the MRs it creates if you make use of a `CODEOWNERS` file.
There are some limitations in renovatebot's support for this. See the [how to
guide](site:howtos/renovatebot/auto-assign-renovatebot-mr-reviewers) for more information.

### Follow the dashboard issue

If you don't want to auto-assign reviewers to MRs, you can instead have the potential field of
reviewers [enable notifications for the dependency dashboard
issue](https://docs.gitlab.com/ee/user/profile/notifications.html#notifications-on-issues-merge-requests-and-epics).
Then they will be notified when renovatebot has created or updated freshening MRs.

### Per-manager configuration

Renovatebot has the concept of [managers](https://docs.renovatebot.com/modules/manager/) which cover
the different package managers renovatebot knows about. It is sometimes convenient to have
configuration be scoped to a particular manager. This is done by putting the configuration into a
section named after the manager.

For example, to customise the names of the `requirements.txt` files which the Python package
manager looks for, add the following to `renovate.json`:

```js title="renovate.json"
{
  // ... other config ...
  "pip_requirements": {
    "fileMatch": [
      "^specially-named-requirements\\.txt$"
    ]
  }
}
```

### Pre-commit

Renovatebot can freshen hooks from [pre-commit configurations](https://pre-commit.com/) but this
support is currently in beta and you must explicitly
[opt-in](site:howtos/renovatebot/use-pre-commit-hooks-with-renovatebot).

### Copier

Renovatebot can try to update webapp and boilerplate configurations which make use of our [copier
templates](site:howtos/development/copier/create). In order to do this the `_src_path` *must*
be of the form `https://gitlab.developers.cam.ac.uk/...`.

This can make it difficult when using private templates, such as the GCP deployment boilerplate, and
running copier locally. The following command will tell git to use SSH when cloning `https://...`
URLs for GitLab:

```sh
git config set --global \
  url."git@gitlab.developers.cam.ac.uk:".insteadOf https://gitlab.developers.cam.ac.uk/
```

## More documentation

The following how-to guides cover specifics of configuring renovatebot:

* [Configure renovatebot for your projects](site:howtos/renovatebot/start-using-renovatebot).
* [Automatically assigning reviewers to MRs](site:howtos/renovatebot/auto-assign-renovatebot-mr-reviewers).
* [Testing per-project changes to renovatebot's
  configuration](site:howtos/renovatebot/run-renovatebot-locally).
* [Temporarily disabling renovatebot](site:howtos/renovatebot/disable-renovatebot).
* [Keep docker images fresh in terraform
  deployments](site:howtos/renovatebot/keep-docker-image-references-fresh).
