# GitLab release automation

Creating a release of your code in GitLab can involve numerous manual tasks such
as updating the changelog, bumping the version in `package.json` or
`pyproject.toml`, tagging the correct commit with a git tag, creating the
"Release" in the GitLab UI etc. These tasks are very repetitive and prone to
human error so we've put together an automated workflow to make the process as
easy as possible.

## How does it work?

Including the
[release-it](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/release-it.yml)
template from our `ci-templates` repository, either directly or as part of the
common pipeline, enables a `release-it:release` pipeline job which runs on each
merge into the default branch of your project. The `release-it:release` job
calculates the next version of your project and performs a number of actions
such as updating the changelog and bumping `package.json` versions etc.

The `release-it` tool is very configurable and can handle many different
requirements (see [the configuration
doc](https://github.com/release-it/release-it/blob/main/docs/configuration.md)),
however, there's an example `.release-it.json` file in the [How to enable
automated GitLab releases](site:howtos/git-gitlab/enable-automated-gitlab-releases) page
which serves as a good starting point for most of our projects.

### Commit messages

As recommended in the [Git commits](site:explanations/git-gitlab/git#commits) section,
teams should ideally follow the [Conventional
Commits](https://www.conventionalcommits.org/) standard for projects using the
`release-it.yml` pipeline template. This is also a requirement for the
`conventional-changelog` plugin which is configured in the example
`.release-it.json` config file in [How to enable automated GitLab
releases](site:howtos/git-gitlab/enable-automated-gitlab-releases). The `release-it` tool
can be configured to work without Conventional Commits if required, however, it
should be noted that the `release-it.yml` pipeline template has only ever been
tested using Conventional Commits.

## Alternate workflow

Some of our projects prefer not to release a new version of the code each time
commits are merged to the default branch. Instead, unreleased changes are queued
up and a new release is only created once all features/bug fixes for the
targeted version have been merged to the default branch. To support this
workflow, the `release-it.yml` GitLab template has an alternate workflow mode
which is enabled by setting the `USE_MERGE_REQUEST_RELEASE_FLOW` variable to a
non-null value.

In this mode, each time commits are merged to the default branch an
`update-release-merge-request` job is triggered. This job assesses all commits
since the previous git tag and calculates what the next release version should
be. It then generates a merge request containing the proposed changes to the
`CHANGELOG.md` file and any other files configured in the `.release-it.json`
config file for your project. As more commits are merged into the default branch
the `update-release-merge-request` job keeps the merge request updated with the
new commits and the calculated release version. Once the team are ready to
release the proposed version they simply merge the release merge request. This
triggers a pipeline containing the main `release` job which performs the git tag
and generates the GitLab Release for the new version.

## Patching previous releases

Both the default and alternate workflows also enable the ability to patch
previously released versions. For example, if you have released versions `1.0.0`
and `2.0.0` on the default branch you can release a bug fix to the `1.0.0` code
on a hotfix branch.

Hotfix branches are simply branches with names that begin `release/fix-`. The
`release-it.yml` template is configured to also run the `release` and
`update-release-merge-request` jobs (depending on the workflow you're using) on
hotfix branches. This allows you to checkout hotfix branches from previous tags
and commit your bug fixes. You then follow the standard workflows above to
automatically release the hotfix version. See the [Creating a hotfix for a
previously released
version](site:tutorials/automating-gitlab-releases#creating-a-hotfix-for-a-previously-released-version)
section of the Automating GitLab Releases tutorial for steps on how to achieve
this.

## See also

- [How to enable automated GitLab releases](site:howtos/git-gitlab/enable-automated-gitlab-releases).
- [Automating GitLab releases](site:tutorials/automating-gitlab-releases)
  tutorial.
