# Testing Raven

It is useful to be able to test remote against against the various authentication
protocols that Raven offers. Below is a list of suggested test site.

| Test Protocol | Project |Database | Suggested Test Site |
|---------------|-----|-----|---------------------|
| SAML/Shib     | Raven IdP | shibboleth | [https://ft.com](https://ft.com) |
|               | || [https://gitlab.developers.cam.ac.uk](https://gitlab.developers.cam.ac.uk) |
|               | || [https://www.vlebooks.com](https://www.vlebooks.com) |
|               | || [https://www.brill.com](https://www.brill.com) |
|               | || [https://www.degruyter.com](https://www.degruyter.com) |
| OAuth         | Raven IdP |ravencore | Sign in to [https://google.com](https://google.com) |
| ucam_webauth  | Raven Legacy | raven | [https://www.uis.cam.ac.uk/service-info/incidents](https://www.uis.cam.ac.uk/service-info/incidents) |
