---
title: Workflows Webhooks
---

# Microsoft Teams Workflows Webhooks

Microsoft Teams via Workflows Webhooks allows you to send messages to a Teams channel. This is
useful for automating notifications from your application or service.

When setting up a new product in GCP, the [gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/)
has a configuration item called `msteams_channel_webhook_urls` which will automatically set up the
required plumbing in your GCP product to send alerts or notifications to the specified Teams
channel.

## Setting up a webhook

Prerequisites:

* You must be an admin of the team that owns the Teams channel you want to send messages to.

Steps:

* In the Teams channel, click on the three dots next to the channel name and select **Workflows**.
* Search for **Post to a channel** and click on it.
* Give it a meaningful name and click **Next**.
* Ensure the **Details** match your team and channel (they should).
* Click the **Add workflow** button.
* Copy the URL that is generated, which is the webhook URL.
* Add that to your GCP product configuration into the `msteams_channel_webhook_urls` map.
