---
title: Reporting
---

# How to receive email event feeds

This how-to guide covers how to add reporting on email send, delivery, bounce and complaint events
to Google Sheets for generating Sheets which can be shared with Service Management teams. This guide
does _not_ cover adding data sources to [Looker Studio](https://lookerstudio.google.com/) for
generating reports although Service Management teams may want to investigate using Looker Studio if
they want to generate more "dashboard"-style real time reporting.

<figure markdown>
![](./user-notify-connected-sheet.png)
<figcaption markdown>An example email event feed in a Connected Sheet.</figcaption>
</figure>

## Register your service

Your service must have been [registered with the User Notify
service](./register-with-user-notify.md). As part of that process you identify _reporting IAM
principals_ which correspond to Google users who will be able to add and query event feeds from a
Google sheet. Ideally those principals should be Lookup groups specified as
`group:{numeric-id}@groups.lookup.cam.ac.uk`. Members of those Lookup groups will be able to add
email event feeds to Sheets.

!!! tip "Alerting on email events"

    The reporting IAM principals will also be able to run BigQuery queries over your email logs.
    Advanced users may wish to have a scheduled task run to raise alerts if emails start bouncing.
    Suggestions for how to do this are covered in a [Stack Overflow
    question](https://stackoverflow.com/questions/67067220/).

## Create a data connection in your Sheet

1. In a Google Sheet, select **Data** > **Data connectors** > **Connect to BigQuery**.
2. Choose **User Notify - prod** from the list of projects.
3. Select the **email_events** data set.
4. Select the table corresponding to your service. It will be named
   `service-ses-events-{service-id}` where `{service-id}` is the id of the service as registered
   with the User Notify service.
5. Click **Connect**.
6. Select the filter icon next to **publish_time** and choose **Sort Z to A** to see the most recent
   events first in the preview.

You may now generate extracts, charts and pivot tables from your email event feed. Watch the
tutorial video below to learn more.

<figure>
<iframe width="560" height="315" src="https://www.youtube.com/embed/rkimIhnLKGI?si=YaF4ZrK7tCeFTQm1"
  title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write;
  encrypted-media; gyroscope; picture-in-picture; web-share"
  referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<figcaption markdown>A tutorial video showing how to use Connected Sheets.</figcaption>
</figure>

## Next steps

* Read the [Google documentation on connected sheets](https://cloud.google.com/bigquery/docs/connected-sheets).
* Watch a [tutorial video](https://www.youtube.com/results?search_query=looker+studio+bigquery) on
  connecting Looker Studio to BigQuery queries.
