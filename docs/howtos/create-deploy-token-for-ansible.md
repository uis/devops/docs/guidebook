---
title: Create GitLab deploy token for Ansible deployment
---

# How to create a deploy token for Ansible deployment

For several applications, Ansible is used to deploy them to the on-premise infrastructure.
For example, Ansible is used most heavily in [Grails application ansible deployment](
    https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment) project.

This document describes the steps needed to create new or update expired [deploy tokens](
    https://docs.gitlab.com/ee/user/project/deploy_tokens/).

Deploy tokens are used to enable authentication of deployment tasks, independent of a user
account. With a deploy token, Ansible deployment can:

- Clone Git repositories.
- Pull from a GitLab container registry.
- Pull from a GitLab package registry.

## Create a token in the the GitLab project

The Ansible deployments use GitLab deploy tokens, encrypted and stored in each project's
[Ansible Vault](https://docs.ansible.com/ansible/latest/vault_guide/vault.html).
To create a project's deploy token:

1. Open the project in GitLab UI.
2. Go to **Settings > Repository**.
3. Expand the "Deploy Tokens" section.
4. Click the "Add token" button.
5. Fill the following fields as shown below:

    - **Name**: Token should be named "Ansible Deployment".
    - **Expiration Date (optional)**: Could be omitted. By default, a deploy token does not expire.
    - **Username**: Set username to `ansible-<PROJECT-NAME>`, where `PROJECT-NAME` is a project's
        slug (gaobase, rgea, etc).

6. Give `read_repository`, `read_registry` and `read_package_registry` permissions.
7. Copy username and token, they will be used in the next step.

## Encrypt the token and udpate the Ansible project

1. Clone the git project locally.
2. Create a new branch:

    ```shell
    git checkout -b upgrade-deploy-token
    ```

3. Prepare environment. Filenames vary across project, but in common scenario the preparation
    process is as shown:

    ```shell
    eval $(op signin)
    source <APP>-<ENV>-setup
    ```

    where `APP` is an application's slug, `ENV` is the environment name (live, test, etc.).

4. Ecrypt the username and token.

    ```shell
    DEPLOY_USERNAME='PUT USERNAME HERE'
    DEPLOY_TOKEN='PUT TOKEN HERE'
    ansible-vault encrypt_string $DEPLOY_USERNAME --vault-password-file secrets/open-vault --name=gitlab_access_user
    ansible-vault encrypt_string $DEPLOY_TOKEN --vault-password-file secrets/open-vault --name=gitlab_access_token
    ```

5. Copy the output from `ansible-vault` to the `group_vars/<APP>-<ENV>-secrets.yml`, where `APP`
    is an application's slug, `ENV` is the environment name.

6. Add the file to the git and push:

    ```shell
    git add group_vars/<APP>-<ENV>-secrets.yml
    git commit -m "chore: Update deploy token for <APP>"
    git push --set-upstream origin upgrade-deploy-token
    ```

7. Submit a new merge request for review.
