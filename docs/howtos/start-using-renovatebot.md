---
title: Get started
---
# How to get started with renovatebot

!!! warning

    Our use of renovatebot is in an early stage. Feedback is welcome via the [GitLab
    project](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment).

Renovatebot is an automated process which will raise Merge Requests in your GitLab projects to keep
your dependencies fresh. It is *very* configurable but the [default
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config) has been designed to
fulfil the needs of a typical DevOps project.

This how-to guide covers how to opt in to having renovatebot scan your projects and what to expect.

## Optionally, configure `CODEOWNERS` for your project

Renovatebot can auto-assign Merge Requests to people in your project but you need to [add a
CODEOWNERS file first](./auto-assign-renovatebot-mr-reviewers.md).

## Merge the on-boarding Merge Request

All projects should have has an onboarding Merge Request from renovatebot
([example](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp/-/merge_requests/261))
which will configure your project to use the [default renovatebot
configuration](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config).

Merge this Merge Request to enable renovatebot for your project.

Alternatively, if you can't find the onboarding MR, add the following to `renovate.json` at the root
of your repository:

```json title="renovate.json"
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "local>uis/devops/renovate-config"
  ]
}
```

## Summary

In this guide you learned how to add your GitLab group to the renovatebot allowlist and to merge the
on-boarding Merge Request.

## Next steps

* [Enable auto-assignment of reviewers](./auto-assign-renovatebot-mr-reviewers.md)
