---
title: Create Google Cloud Admin accounts
---

# How to create Google Cloud Admin accounts

We prefer that team members keep the permissions of their ordinary `{crsid}@cam.ac.uk` Google
account relatively light. The permissions should be no more than "read only" or, ideally,
non-existent.

Instead we prefer team members to have a dedicated "gcloudadmin" account used for manipulating
resources in the Google Cloud console and for deploying services.

An explanation of the use of these accounts and expectations on developer's who have them is covered
in a [dedicated explainer guide](site:explanations/cloud-platform/gcloudadmin-accounts).

This page describes the process to create a "gcloudadmin" account.

## Create an issue

Create an issue in the project you are working on outlining the need for you to have an admin
account and which team you need to be in.

## Update `team_data.json`

The
[team_data.json](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json)
file contains a description of team members and their roles.

If you already have a `{crsid}@cam.ac.uk` account in that file, replace it with an account named
`{crsid}@gcloudadmin.g.apps.cam.ac.uk`.

If you do not already have a `{crsid}@cam.ac.uk` account in that file, add one to the appropriate
role in your team. Generally teams have three roles: "view" which allows read-only access in the
Google Cloud console, "deploy" which allows impersonation of the terraform deploy service account
but no elevated console access and "admin" which grants full rights in the Google Cloud Console.

You may grant your personal account (`{crsid}@cam.ac.uk`) the "view" role, which allows you to
browse the GCP console without switching accounts and free from the fear of accidentally making
changes via click-ops.

## Open a Merge Request

Open a Merge Request on the [team-data
project](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/) with your
changes. Add "Closes #{issue number}" to the description to link it to the issue you created above.

Add a {{ gitlab_label("team::Cloud") }} label **to the issue**, add the
{{ gitlab_label("workflow::Review Required") }} label **to the issue** and assign the issue to the
iteration corresponding to the Cloud Team's current sprint.

## Sign in and update 2FA settings

A member of the Cloud Team will contact you, securely share initial login information and request
that you enable two-factor authentication for the gcloudadmin account. Once you have done this,
your account will be added to the appropriate Google Groups which are, in turn, granted appropriate
permissions on the Google Cloud projects.

## Summary

In this how-to you saw how to create a Google Cloud admin account which is reserved for privileged
access to Google Cloud resources.

## Next steps

* Read more about how we [configure Google Cloud](site:reference/cloud-platform).
