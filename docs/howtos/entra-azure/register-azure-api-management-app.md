---
title: Register an Azure API Management client application
---

# How to register an Azure API Management client application

In this guide you will learn how to register an application so that you can authenticate one or more
Google Service Accounts against APIs hosted behind Azure API Management.

Some internal UIS APIs are hosted behind Azure API management. In order to call these APIs we need
to register an *Entra ID Application* and configure it so that a Google Service account can
authenticate.

## Find the unique ids for all the Google Service accounts

Due to the technicalities of cross authentication between Google and Azure you will need the
*unique id* for each Google service account. This can be found on the "details" page for the service
account:

<figure markdown>
![Google Service Account details page](./google-service-account-unique-id.png)
<figcaption>An image showing the unique id of a service account on its "details" page.</figcaption>
</figure>

## Our example

We'll use a case study of a fictional punt booking system which provides a frontend to an API
provided by another team hosted behind Azure API Management.

* Our production webapp runs with the Google Service account identity
  `webapp@punt-booker-prod.iam.gserviceaccount.com` with unique id `1111111111`.
* Our development webapp runs with the Google Service account identity
  `webapp@punt-booker-devel.iam.gserviceaccount.com` with unique id `2222222222`.
* Our local development environment uses local developer credentials to impersonate the service
  account `local-dev@punt-booker-devel.iam.gserviceaccount.com` with unique id `3333333333`.
* We want to register two API client applications:
    * One for the production instance which will be granted the ability to call the production punt
      booker API which is made available by Azure API Management under `https://api.punt-booker.invalid/`.
    * One for local development and the development instance which will be granted the ability to
      call the testing punt booker API which is made available by Azure API Management
      under `https://api.test.punt-booker.invalid/`.

## Open a Merge Request

Open a Merge Request (MR) in the [Entra ID Application
Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/entra-app-factory) project. The Merge
Request should contain:

<!-- markdownlint-disable MD031 -->
* A file under `applications/production/` called `punt-booker-client-production.yaml` with the
  following content:

    ```yaml title="applications/production/punt-booker-client-production.yaml"
    type: api-client
    display_name: Punt Booker

    federated_google_service_account_unique_ids:
      - "1111111111"
    ```

* A file under `applications/production/` called `punt-booker-client-development.yaml` with the
  following content:

    ```yaml title="applications/production/punt-booker-staging.yaml"
    type: api-client
    display_name: Punt Booker (Development)

    federated_google_service_account_unique_ids:
      - "2222222222"
      - "3333333333"
    ```

    **Note that this file is under `applications/production/`.** In this case, "production" means
    "the production deployment of Entra ID application factory".
<!-- markdownlint-enable MD031 -->

When you have opened the Merge Request, tag it {{ gitlab_label("team::Cloud") }} and alert the Cloud
Team in their Teams channel. You or a reviewer can trigger a manual plan for the MR from the CI
pipelines page of the MR. This can help verify that the format of your files is correct and that the
things you expect to be created will be created.

Once merged and deployed, your applications will be created.

## Get your application's id

Your application will now appear in the [list of registered
applications](https://storage.cloud.google.com/entra-applications-11f0f206/applications.json). That
document is only viewable to members of the `uis-devops-division` in Lookup who are signed in to
Google via their `@cam.ac.uk` account.

Your application's id is visible in this document and has the form
`01234567-89ab-cdef-0123-456789abcdef`.

!!! important

    Depending on how the team running the punt booker API are managing their API authorisation, they
    may need to add this application identifier to the allowlist to allow your application call the API.

## Test that you can obtain access tokens

The general approach for obtaining access tokens for the API is as follows:

1. Obtain a signed identity token from Google corresponding to the service account identity.
2. Exchange that token for an Azure API Management access token.
3. Pass that token in the `Authorization` header as a bearer token.

To authenticate for local development:

<!-- markdownlint-disable MD031 -->
1. Ensure you have the [gcloud](https://cloud.google.com/sdk/docs/install),
   [jq](https://jqlang.github.io/jq/) and [httpie](https://httpie.io/) tools installed.
2. Authenticate via `gcloud login` as a user who has rights to impersonate the
   `local-dev@punt-booker-devel.iam.gserviceaccount.com` service account.
3. Get an identity token for the service account with the appropriate audience claim set:

    ```sh
    GOOGLE_ID_TOKEN=$(gcloud auth print-identity-token \
     --impersonate-service-account=local-dev@punt-booker-devel.iam.gserviceaccount.com \
     --audiences=api://AzureADTokenExchange)
    ```

4. Set the `CLIENT_ID` variable to your application's id:

    ```sh
    CLIENT_ID="{paste your application id here}"
    ```

5. Exchange that token for an Azure API Management access token:

    ```sh
    API_ACCESS_TOKEN=$(http --form POST \
      https://login.microsoftonline.com/49a50445-bdfa-4b79-ade3-547b4f3986e9/oauth2/token \
      grant_type=client_credentials \
      client_id=${CLIENT_ID} \
      client_assertion_type=urn:ietf:params:oauth:client-assertion-type:jwt-bearer \
      client_assertion=${GOOGLE_ID_TOKEN} | jq -r .access_token)
    ```

6. Call the punt booker API with the access token:

    ```sh
    http GET --auth-type bearer --auth ${API_ACCESS_TOKEN} https://api.test.punt-booker.invalid/
    ```
<!-- markdownlint-enable MD031 -->

## Summary

In this how to guide, you learned how to register an API client application with Azure API
management and how to test that you can obtain an access token.

## Next steps

* [Learn more about Azure and Entra ID Applications](site:explanations/cloud-platform/azure-and-entra-id-apps).
