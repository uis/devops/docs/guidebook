---
title: Prepare macOS workstation for running Ansible deployment script
---

# How to install dependencies and configure macOS workstation for running Ansible deployments

For several projects such as [Grails application ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment)
or [HR Ansible Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure),
Ansible is being used to deploy apps to the on-premise infrastructure. The standard method for
running Ansible is through the script `./run-ansible-playbook.sh`, which is included in every
Ansible project. This script is a wrapper that helps to prepare environment, fetch secrets and
run `ansible-playbook` inside docker container.

This document outlines the steps required to **configure a macOS workstation** for running
the Ansible deployment script.

## Install dependencies on macOS

### Docker

To install Docker on macOS, follow these steps:

1. Download Docker Desktop for Mac:

    Go to the Docker [Desktop download page](https://www.docker.com/products/docker-desktop/).
    Click on **Download Docker Desktop**. Select your architecture.

2. Install Docker Desktop:

    Once the .dmg file is downloaded, double-click it to open the installer. Drag the Docker
    icon into the Applications folder.

3. Run Docker Desktop:

    Open the Applications folder and launch Docker. Docker may prompt you to enter your system
    password to allow the installation of system components (like network management tools).
    The Docker icon should appear in your menu bar once it has started.

4. Verify Installation
    Open a terminal and run the following command to ensure Docker is installed and running:

    ```sh
    docker --version
    ```

    This should return the installed version of Docker.

### 1Password CLI

The typical UIS DevOps Ansible project uses secrets for `ansible-vault` stored in 1Password.
The 1Password CLI tool `op` is one of the dependecies. To install 1Password CLI with [homebrew](https://brew.sh/):

1. Run `brew install 1password-cli`

2. Check that 1Password CLI installed successfully:

    ```sh
    op --version
    ```

### Coreutils

The Ansible deployment script uses `sha256sum` which is absent by default on macOS. It can be
installed as part of the package called `coreutils`. To install `coreutils` with [homebrew](https://brew.sh/):

1. Run `brew install coreutils`

2. Check that the package installed successfully:

    ```sh
    sha256sum --version
    ```

## Configure macOS host

### Configure access to University Developers' Hub docker registry

The Ansible deployment script uses docker images specially built for this purpose. Those images
are stored in the University [Developers' Hub docker registry](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages).
To let docker to access the registry you need to allow docker to login there. For this, perform
the following steps:

1. Login to the the University [Developers' Hub](https://gitlab.developers.cam.ac.uk/).

2. Go to your account's [Personal Access Tokens page](https://gitlab.developers.cam.ac.uk/-/user_settings/personal_access_tokens).

3. Click "Add new token".

4. Set the name for the new token and limit scope to `read_registry`.

5. Configure expiration date. By default personal tokens are valid for short amount of time.
    It makes sense to set it to maximum expiration date available.

6. Using new token, run

    ```sh
    docker login registry.gitlab.developers.cam.ac.uk -u $GITLAB_USER -p $GITLAB_TOKEN
    ```

    where `$GITLAB_USER` is your username and `$GITLAB_TOKEN` is a newly generated personal access token.

7. Verify the image is now can be pulled by running the following command:

    ```sh
    docker pull registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/ansible-playbook:7
    ```

### Configure ssh agent

The Ansible deployment script needs to have SSH access to the target hosts. For this it's
necessary to load your private SSH key into the SSH agent and store it in the macOS keychain,
enabling automatic SSH authentication when connecting to remote servers. This can be achieved
by running the following command:

```sh
ssh-add -K $PATH_TO_PRIVATE_KEY
```

where `$PATH_TO_PRIVATE_KEY` is a path to the private ssh key e.g.:

```sh
ssh-add -K ~/.ssh/id_rsa
```

Public part of the key must be added to the remote host.

## Summary

In this how to, you learned how to prepare macOS workstation for running Ansible deployment
script including instructions for dependencies installation and host configuration.
