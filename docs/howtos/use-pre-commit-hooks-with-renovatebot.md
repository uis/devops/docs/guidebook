---
title: Use pre-commit hooks
---
# How to use pre-commit hooks with renovatebot

Renovatebot supports pre-commit hooks in two ways:

* Keeping the hooks referenced in `.pre-commit-config.yaml` fresh, and
* Checking your `renovate.json` configuration is valid.

## Check `renovate.json` on commit

Add the following to `.pre-commit-config.yaml`:

```yaml title=".pre-commit-config.yaml"
repos:
  # ... other hooks ...
  - repo: https://github.com/renovatebot/pre-commit-hooks
    rev: 38.26.0
    hooks:
      - id: renovate-config-validator
        files: ^renovate\.json$
```

## Enable pre-commit hook freshening

!!! warning

    The `pre-commit` renovate manager is [currently in
    beta](https://docs.renovatebot.com/modules/manager/pre-commit/) and must be manually enabled.

To enable renovatebot auto-freshening of pre-commit hook versions, add the following to
`renovatebot.json`:

```js
{
  extends: [
    // ... other presets ...
    "local>uis/devops/renovate-config:enablePreCommit"
  ]
}
```

## Summary

In this guide you learned about the two ways renovatebot can support pre-commit and how to configure
them.
