---
title: Access the AWS console
---
# How to access the AWS console

Some of our products make use of AWS resources. If they have been configured according to the [AWS
guide](./get-started-with-aws.md) then they will have a file named `.aws-helper.yaml` in the root of
the repository. This guide covers how to access the AWS console for such products.

If you're interested in the rationale and background for how we integrate with AWS, there is a
dedicated [explainer guide](site:explanations/cloud-platform/aws).

## Requirements and deliverables

Before you start you will need:

- an existing terraform deployment using Google Cloud based on our
  [template](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate),
- AWS configuration for that deployment as per the [relevant guide](./get-started-with-aws.md), and
- the ability to access the Google Cloud console for your project with "editor" rights through a
  [gcloudadmin account](site:howtos/cloud-platform/create-gcloudadmin-account).

At the end of the process you will have the ability to access the AWS console for each environment.

## Install the `aws-helper` tool

AWS-enabled deployments will have an `.aws-helper.yaml` file in the root of the repository which
configures a related [aws-helper](https://gitlab.developers.cam.ac.uk/uis/devops/tools/aws-helper)
tool.

Install `pipx` as described [in the
guidebook](https://guidebook.devops.uis.cam.ac.site:howtos/development/prepare-your-system/).

Install the most recent version of the `aws-helper` tool:

```sh
pipx install \
    --index-url https://gitlab.developers.cam.ac.uk/api/v4/groups/5/-/packages/pypi/simple \
    --pip-args=--upgrade \
    aws-helper
```

## Authenticate to Google

Authenticate using the same `...@gcloudadmin.g.apps.cam.ac.uk` account which you use to sign in to
the Google Cloud console:

```sh
gcloud auth application-default login
```

## Access the AWS console

Within the repository you should now be able to open a new browser tab for the AWS console in the
"development" environment via:

```sh
aws-helper console
```

!!! tip

    If you are already signed in to the AWS console for a different product or different
    environment, you need to sign out first.

If you want to access the AWS console for a different environment, use the `-e` flag. For example,
to access the "staging" environment:

```sh
aws-helper -e staging console
```

Within the console you can check which environment you are accessing at the top right.

<figure markdown>
![](./aws-console-header.png)
<figcaption markdown>
The AWS console header showing that you are currently accessing the "staging" environment using the
"spqr2" account.
</figcaption>
</figure>

## Summary

In this guide we covered how to sign in to the AWS console in [appropriately
configured](./get-started-with-aws.md) deployments.

## Next steps

- [How to use the `aws` CLI tool](./use-aws-cli.md)
- [How to audit access to the AWS console](./audit-aws-console-access.md)
