---
title: Audit AWS console access
---
# How to audit access to the AWS console

In Google Cloud, changes made via the console or `gcloud` CLI tool are logged against the user's
`...@gcloudadmin.g.apps.cam.ac.uk` account in the [audit
log](https://cloud.google.com/logging/docs/audit). Although AWS also has an audit log, we [use a
shared Admin role](site:explanations/cloud-platform/aws) to access the console and so it is
not quite as straightforward to identify which individuals performed an action should that
knowledge be needed after the event.

This guide describes how you can work backwards from an action in AWS to determine the likely set of
users which performed the action.

As a reminder, all access to the AWS console and `aws` CLI tool is gated through impersonation of a
dedicated "AWS admin" service account. The general process is to determine when an action of
interest in AWS was performed and from which IP. This can then be correlated with which users
impersonated the "AWS admin" account around that time and from a matching IP.

Assuming you've [configured the CLI tool](./use-aws-cli.md), obtain timestamps for each time
the AWS Admin IAM Role was assumed:

<!-- markdownlint-disable MD013 -->
```console
$ aws --profile=development --region=eu-west-2 \
    cloudtrail lookup-events \
    --lookup-attributes AttributeKey=EventName,AttributeValue=AssumeRoleWithWebIdentity \
    --max-items 10 \
    --query "Events[*].{Time:EventTime,Resources:Resources}" --output table
-----------------------------------------------------------------------------------------------------------
|                                              LookupEvents                                               |
+---------------------------------------------------------------------------------------------------------+
|                                                  Time                                                   |
+---------------------------------------------------------------------------------------------------------+
|  2024-09-04T11:51:01+00:00                                                                              |
+---------------------------------------------------------------------------------------------------------+
||                                               Resources                                               ||
|+------------------------------------------------------------------------------+------------------------+|
||                                 ResourceName                                 |     ResourceType       ||
|+------------------------------------------------------------------------------+------------------------+|
||  ASIAQ3EGSW2FUSBWK6NC                                                        |  AWS::IAM::AccessKey   ||
||  1725447061200805883                                                         |  AWS::STS::AssumedRole ||
||  AROAQ3EGSW2FV23XO4CGR:1725447061200805883                                   |  AWS::STS::AssumedRole ||
||  arn:aws:sts::058264303243:assumed-role/TerraformDeploy/1725447061200805883  |  AWS::STS::AssumedRole ||
||  arn:aws:iam::058264303243:role/TerraformDeploy                              |  AWS::IAM::Role        ||
|+------------------------------------------------------------------------------+------------------------+|
```
<!-- markdownlint-enable MD013 -->

Note the time frame you are interested in. Use the `gcloud` tool to correlate that with the Google
Cloud audit log. The audit log will name the underlying `...@gcloudadmin.g.apps.cam.ac.uk` account
which initiated the sign in. For example, assuming the corresponding Google project is
`punt-booker-devel-123abc`:

```console
$ gcloud \
    --project=punt-booker-devel-123abc \
    logging read \
    --freshness=1h \
        "resource.type="service_account" \
        AND resource.labels.email_id="aws-admin@punt-booker-devel-123abc.iam.gserviceaccount.com" \
        AND protoPayload.methodName="GenerateIdToken" \
        AND protoPayload.authorizationInfo.granted=true" \
    --format="table(\
        timestamp, \
        protoPayload.requestMetadata.callerIp, \
        protoPayload.authenticationInfo.principalEmail \
    )[box]"
┌────────────────────────────────┬─────────────┬────────────────────────────────────┐
│           TIMESTAMP            │  CALLER_IP  │          PRINCIPAL_EMAIL           │
├────────────────────────────────┼─────────────┼────────────────────────────────────┤
│ 2024-09-04T11:51:00.026278129Z │ 11.22.33.44 │ spqr2@gcloudadmin.g.apps.cam.ac.uk │
│ 2024-09-04T10:44:34.536266084Z │ 11.22.33.44 │ spqr2@gcloudadmin.g.apps.cam.ac.uk │
└────────────────────────────────┴─────────────┴────────────────────────────────────┘
```

## Summary

In this guide we covered how to obtain timestamps from AWS for elevated access and how to correlate
those with service account impersonation events in Google.

## Next steps

- [How to use the `aws` CLI tool](./use-aws-cli.md)
