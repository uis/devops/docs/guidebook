---
title: Create new AWS accounts
---
# How to create new AWS accounts within our organisation

This guide covers how to manually provision AWS accounts for products. If you do not have access to
the AWS organisational management account you will need to ask {{ gitlab_label("team::Cloud") }} to
complete these steps for you.

If you're interested in the rationale and background for how we integrate with AWS, there is a
dedicated [explainer guide](site:explanations/cloud-platform/aws).

## Requirements and deliverables

Before you start you will need:

- the AWS organisation account access details stored in [a 1Password
  item](https://start.1password.com/open/i?a=D3ATZUD36RDHLDKSVJUQZWGORQ&v=jxr2cqzf6va6lgo3abcgwteeja&i=nmijsckj4p34qx2cgnxewksfey&h=uis-devops.1password.eu),
- the ability to read email for `devops-account-recovery@uis.cam.ac.uk`,
- the ability to either store the root user credential in the destination 1Password vault or a
  secure way to share root user credentials with the product team,
- the name of the product, and
- the set of environments to be deployed, usually "production", "staging" and "development".

At the end of this process you will get:

- one AWS *account* for each environment which mirrors the one Google *project* for each
  environment, and a
- set of "root user" credentials which product teams can use to sign in to the AWS console and
  complete setup as described in the [getting started with AWS guide](./get-started-with-aws.md).

Within the AWS console, the setup will look like the following:

<figure markdown>
![](./aws-org-account-layout.png)
<figcaption markdown>AWS accounts for the "postbox" product within the AWS Organizations
console page.</figcaption>
</figure>

## Create a new Organizational Unit (OU)

1. Using the AWS organisation account credentials, sign in to the AWS console.
2. Select "AWS Organizations" from either the landing page or via the search box at the top of the
   page.
3. Navigate the organisational structure until you find the "DevOps" OU under the "UIS" OU.
4. Select the tickbox next to the "DevOps" OU.
5. At the top of the page, click **Actions** > **Organizational unit: Create new**.
6. Set **Organizational unit name** to the name of the product.
7. Click **Create organizational unit**.

## Create AWS accounts for each environment

You should have a list of environments for the product. Usually these will be "production",
"staging" and "development" with the corresponding "short names" of "prod", "test" and "devel".

For each environment:

1. From the "AWS Organizations" page, click **Add an AWS account**.
2. For **AWS account name** use "{product} - {environment}". For example: "Punt Booking - Staging".
3. For **Email address of the account's owner** use "devops-account-recovery+{product}-{short
   environment name}@uis.cam.ac.uk". For example:
   "<devops-account-recovery+punt-booking-devel@uis.cam.ac.uk>".
4. Leave **IAM role name** as the default.
5. Click **Create AWS account**.

Once the AWS accounts have been created:

1. From the "AWS Organizations" page, select the tickbox next to all of the newly created accounts.
2. Click **Actions** > **AWS account: Move**.
3. Select the product-specific OU you created previously.
4. Click **Move AWS accounts**.

## Obtain credentials for the root user of each AWS account

AWS does not let you specify root user credentials at account creation time. Instead, follow the
[forgotten password
flow](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_accounts_access-as-root.html)
to reset the automatically generated password. You will need to be able to read email for
`devops-account-recovery@uis.cam.ac.uk` to do this.

Once the password is reset you should be able to sign in as the root user following the [AWS
documentation](https://docs.aws.amazon.com/signin/latest/userguide/introduction-to-root-user-sign-in-tutorial.html).

If you are storing the credentials directly in a 1Password item, enable TOTP-based multi-factor
authentication by following the [AWS
documentation](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa.html) and use
1Password's TOTP support. Otherwise, share the root user credentials with the product team.

## Summary

In this guide you learned how to create a new AWS account within our organisation and to ensure it
sits within the correct OU.

## Next steps

- [How to add AWS to our existing terraform deployments](./get-started-with-aws.md)
