---
title: Use the AWS CLI tool
---
# How to use the AWS CLI tool

Some of our products make use of AWS resources. If they have been configured according to the [AWS
guide](./get-started-with-aws.md) then they will have a file named `.aws-helper.yaml` in the root of
the repository. This guide covers how to configure the [AWS command line
tool](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) for such products.

If you're interested in the rationale and background for how we integrate with AWS, there is a
dedicated [explainer guide](site:explanations/cloud-platform/aws).

Just as only users with "editor" rights can directly manage resources in Google via the `gcloud`
command line tool, only users with "editor" rights can directly manage resources in AWS via the
`aws` command line tool.

## Requirements and deliverables

Before you start you will need:

- an existing terraform deployment using Google Cloud based on our
  [template](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate),
- AWS configuration for that deployment as per the [relevant guide](./get-started-with-aws.md), and
- the ability to access the Google Cloud console for your project with "editor" rights through a
  [gcloudadmin account](site:howtos/cloud-platform/create-gcloudadmin-account).

At the end of the process you will have the ability to access the AWS console for each environment.

## Install the `aws` CLI tool

Install the `aws` CLI tool as per the
[documentation](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

## Install the `aws-helper` tool

AWS-enabled deployments will have an `.aws-helper.yaml` file in the root of the repository which
configures a related [aws-helper](https://gitlab.developers.cam.ac.uk/uis/devops/tools/aws-helper)
tool.

Install `pipx` as described [in the
guidebook](https://guidebook.devops.uis.cam.ac.site:howtos/development/prepare-your-system/).

Install the most recent version of the `aws-helper` tool:

```sh
pipx install \
    --index-url https://gitlab.developers.cam.ac.uk/api/v4/groups/5/-/packages/pypi/simple \
    --pip-args=--upgrade \
    aws-helper
```

## Authenticate to Google

Authenticate using the same `...@gcloudadmin.g.apps.cam.ac.uk` account which you use to sign in to
the Google Cloud console:

```sh
gcloud auth application-default login
```

## Configure the `aws` CLI tool

Add the following to `~/.aws/config`:

```ini
[profile default]
# Required so that aws-helper doesn't get into an infinite loop trying to call itself

[profile development]
credential_process = aws-helper --environment=development --aws-profile=default cli-credentials

[profile staging]
credential_process = aws-helper --environment=staging --aws-profile=default cli-credentials

[profile production]
credential_process = aws-helper --environment=production --aws-profile=default cli-credentials
```

!!! tip

     This config assumes that the environments for your product are "production", "staging" and
     "development". The vast majority of our products use these environments but you may need to
     add additional environments is some special cases.

## Test using the `aws` CLI tool

You should now be able to use the `aws` command line tool to authenticate as the `Admin` role.

Use the `--profile` option to specify the profile for each call, e.g.:

```console
$ aws --profile development sts get-caller-identity
{
    "UserId": "AROA6GBMDAMTQ5GB7SOAU:development-spqr2",
    "Account": "975050048295",
    "Arn": "arn:aws:sts::975050048295:assumed-role/Admin/development-spqr2"
}
```

Alternatively, you can set the `AWS_PROFILE` environment variable to use a fixed profile for all
calls:

```console
$ export AWS_PROFILE=development
$ aws sts get-caller-identity
{
    "UserId": "AROA6GBMDAMTQ5GB7SOAU:development-spqr2",
    "Account": "975050048295",
    "Arn": "arn:aws:sts::975050048295:assumed-role/Admin/development-spqr2"
}
```

You may need to set the AWS region for some operations. For example:

```console
$ AWS_PROFILE=development AWS_REGION=eu-west-2 aws ses list-identities
{
    "Identities": [
        "example@example.com"
    ]
}
```

## Summary

In this guide we covered how to use the `aws` command line tool in [appropriately
configured](./get-started-with-aws.md) deployments.

## Next steps

- [How to access the AWS console](./access-aws-console.md)
