---
title: Publish a package
---

# How to publish a Python package

!!! warning

    This page assumes that you are working with an *existing* Python package
    which has been configured for auto-publishing via our common pipeline.
    See [how to add the common pipeline to a Python
    package](site:howtos/python/add-common-pipeline-to-python) if this has not been done.

    If you want to learn more about how we structure Python packages, follow
    [our Python package tutorial](site:tutorials/creating-a-python-package).

Python packages which make use of our common pipeline are set up to auto-publish
packages when a new git tag is pushed.

All packages publish to our [internal package
registry](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/packages).
Some will additionally publish to the public [PyPI registry](https://pypi.org/).

## Increment the version number

When making a new release, the version number defined in `pyproject.toml` or
`setup.py` needs to be incremented. This may have already been done by Merge
Requests adding new features.

If you need to increment the version number, follow the [semantic versioning
guidelines](https://semver.org/).

## Update the changelog

Packages should have a `CHANGELOG.md` file in the root of the repository
providing a summary of changes over time. If you have increased the version,
make sure that the changelog is up to date.

## Tag the release

Tag the `main` or `master` branch commit with your version number. For example,
if you are releasing version 1.2.3:

```console
git tag 1.2.3
```

!!! warning

    Tag names *must* match the version number set in the `setup.py` or
    `pyproject.toml` file.

Push tags to the repository:

```console
git push origin --tags
```

## Check the release progress

A CI pipeline will be created to build and publish the package. The pipeline
checks that the version number in your `setup.py` or `pyproject.toml` file
matches the tag name.

An example pipeline from the [FastAPI pagination
library](https://gitlab.developers.cam.ac.uk/uis/devops/lib/fastapi-pagination/)
is show below. This pipeline publishes to PyPI and the GitLab package registry.
Before a package is published to the public PyPI registry, it is pushed to the
test registry.

![A publish pipeline](./publish-python-ci.png)

## Summary

In this how-to you learned how to tag and publish a new release of a Python
project.

## Next steps

* See [how to add the common pipeline to an existing Python
  package](site:howtos/python/add-common-pipeline-to-python).
