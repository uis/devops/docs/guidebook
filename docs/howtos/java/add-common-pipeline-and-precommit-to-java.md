---
title: Add common CI jobs
---

# How to add the common CI pipelines and pre-commit configuration to an existing Java project

The
[common CI pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/)
and
[Java Maven pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/maven.gitlab-ci.yml?ref_type=heads)
templates provide a number of common jobs useful for Java based projects.

This page shows you how to add these jobs to a pipeline for an existing Java project.

## Add the CI configuration

Update your `.gitlab-ci.yml` file and add the following pipeline templates:

```yaml title=".gitlab-ci.yml"
include:
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "/auto-devops/common-pipeline.yml"
    ref: "{replace with the latest ci-templates repository tag}"
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "auto-devops-stages.yml"
    ref: "{replace with the latest ci-templates repository tag}"
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "/auto-devops/maven.gitlab-ci.yml"
    ref: "{replace with the latest ci-templates repository tag}"

variables:
  # Add this variable if you don't have a Dockerfile in the repository and/or
  # want to disable automatic image building.
  BUILD_DISABLED: "1"

  # Add this variable if your project does not need to run 'mvn deploy'
  # to publish build artifacts to a GitLab Maven Repository
  MAVEN_DEPLOY_DISABLED: "1"

  # Add this variable if your project does not need to run 'mvn verify'
  # to run tests in your pipeline
  MAVEN_VERIFY_DISABLED: "1"

  # Add this variable if you do not need to retrieve GitLab Access Tokens
  # used to download Maven dependencies from a configured Maven Repository in GitLab
  MAVEN_ACCESS_TOKENS_DISABLED: "1"
```

## How to disable optional features

The `maven.gitlab-ci.yml` template includes a number of common jobs that are **enabled by default**.

If your CI pipeline for your project does not need to run any of the included jobs, disable
them by including the specific variable for the job you need to disable as shown above.

## Retrieving Maven Dependencies from a shared GitLab Package Registry

If your project needs to retrieve Jar dependencies from a GitLab Package Registry configured in your
Maven pom.xml:

- this feature is enabled in `.maven.gitlab-ci.yml` by default, do not add the
`MAVEN_ACCESS_TOKENS_DISABLED` variable as shown above. If you do not need to access a GitLab
Package Registry, disable this feature by adding `MAVEN_ACCESS_TOKENS_DISABLED: "1"`

- Configure a Maven Repository url in the root `pom.xml` in your Java project by adding the
configuration shown in the [How to Configure Maven Package Registries](configure-access-to-gitlab-java-repositories.md#identifying-or-configuring-maven-remote-package-registries)
guide. Ignore the other sections on that page as they cover how to configure access for local
development only.

- Ensure your project was created using the
[GitLab Project Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory),
and you have configured your
[product-vars](<https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/-/tree/main/product-vars?ref_type=heads>)
to configure access to a `group_deploy_token`, where:
    - `[GROUP-ID]` is the GitLab group id that contains the project and it's Package Registry that your
    project needs to access to retrieve dependant Jars. Note that this group id must be the same
    as the id in the url that you configured in your project's `pom.xml` in the Maven Repository
    step above.

    - `[GCP-META-PROJECT-ID]` is the Project Id for the meta project for your system in GCP

```text
group_deploy_tokens = {
  deploy = {
    group_id     = "[GROUP-ID]"
    access_level = "maintainer"
    scopes       = ["read_package_registry", "write_package_registry"]
    iam_policy = {
      "roles/secretmanager.secretAccessor" : [
        "serviceAccount:gitlab-token-accessor@[GCP-META-PROJECT-ID].iam.gserviceaccount.com",
      ]
    }
  }
}
```

- add a `ci_settings.xml` file in the root of your project with the following content:

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Deploy-Token</name>
            <value>${GITLAB_TOKEN}</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

Note that the `Deploy-Token` name must be used, and `${GITLAB_TOKEN}` is replaced at runtime with the
value of the GitLab Access Token retrieved by a script in `maven.gitlab-ci.yml`.

- extend the build job in your .gitlab-ci.yml to use a provided script to retrieve GitLab Access Tokens:

```yaml
#add runner tag to maven hidden job
.maven:
  tags:
    - $GKE_RUNNER_TAG

build:
  before_script: !reference [.maven.deploy_tokens]
  tags:
    - $GKE_RUNNER_TAG

```

Note that tag value `$GKE_RUNNER_TAG` must be added to use a GitLab Runner on GKE which has access to
retrieve secrets from Secrets Manager.

## Add the google-java-format pre-commit plugin

The google-java-format pre-commit plugin should also be
[added to the project](site:howtos/development/setup-pre-commit-hooks#java).

## IDE formatter configuration

Plugins for the google-java-format code formattter are available for most IDEs - download and install
from the following links:

- [IntelliJ](https://plugins.jetbrains.com/plugin/8527-google-java-format)
- [Eclipse](https://github.com/google/google-java-format/releases)
