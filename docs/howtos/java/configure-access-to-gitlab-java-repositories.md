---
title: GitLab Java Package Registry Configuration
---

# How to configure local access to GitLab Package Registries for Java projects

Some UIS Java-based systems are built with package dependencies shared from
GitLab Package Registries. This guide shows how to configure local developer
access to GitLab Package Registries to build and run these systems locally.

## Identifying (or configuring) Maven remote Package Registries

Open the parent `pom.xml` for your Java project. This file is usually in the root of the project.

If you need to add configuration to use a GitLab Package Registry to a new or
existing project, retrieve the GitLab Group id from the GitLab group that contains your
published packages using the '...' link at the top right of your group page in GitLab,
replace `[group-id]` in the configuration below with your group id and add to your `pom.xml`:

```xml
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.developers.cam.ac.uk/api/v4/groups/[group-id]]/-/packages/maven/</url>
    </repository>
</repositories>
```

If this is an existing project that does not use a GitLab Package Registry (the above configuration
is not in your `pom.xml`), you do not need to follow the rest of this guide.

## Create a GitLab Access Token

In GitLab, access your Preferences (from your user icon), then click 'Access Tokens'. Create
a new Access Token with the 'api' scope selected. Save the created Access Token value
to a safe place (e.g. your Employee vault on 1Password).

## Configure a Maven settings.xml file

By default, Maven looks for your user settings.xml file in `~/.m2/`. If `~/.m2/settings.xml`
does not yet exist, create a new file, otherwise edit this file and add:

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Private-Token</name>
            <value>[your-access-token]</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

Replace `[your-access-token]` with the value of your GitLab access token.

If you already have an existing `settings.xml` file, ensure the `<server>` section is added within
an existing `<servers>` element, or within an exisitng `<settings>` parent element.

## Examples of successful package downloads

If dependant packages are being downloaded, the output of `mvn compile` will look like:

```text
Downloading from gitlab-maven: https://gitlab.developers.cam.ac.uk/api/v4/groups/10920/-/packages/maven/uk/ac/cam/irs/api/1.19.0/api-1.19.0.pom
```

## Identifying errors related to missing or incorrect Package Repository configuration

In Eclipse, errors from a missing Package Repository config in your settings.xml look like:

```text
Missing artifact uk.ac.cam.irs:api:jar:1.19.0
```

If you run a `mvn compile` from the command line, you will see more verbose errors relating to
failed attempts to retrieve dependent packages that are not available from other public sources,
like Maven Central:

```text
[ERROR] Failed to execute goal on project irs-core: Could not resolve dependencies for project
uk.ac.cam.irs:irs-core:jar:1.38.0-SNAPSHOT: The following artifacts could not be resolved:
uk.ac.cam.irs:api:jar:1.19.0 (absent), uk.ac.cam.uis.common:htmltopdfbox:jar:1.1-SNAPSHOT
(absent), uk.ac.cam.hr:virus-scanner:jar:1.0.42-cdi-SNAPSHOT (absent),
uk.ac.cam.hr:filestorage:jar:1.0.42-cdi-SNAPSHOT (absent),
uk.ac.cam.hr:api-gateway:jar:1.0.42-cdi-SNAPSHOT (absent): Could not find artifact
uk.ac.cam.irs:api:jar:1.19.0 in gitlab-maven
(https://gitlab.developers.cam.ac.uk/api/v4/groups/10920/-/packages/maven/)
```

Note that errors similar to the following are shown when you don't have a valid Access Token
configured in your settings.xml to access a GitLab Package repository:

```text
Could not find artifact uk.ac.cam.irs:api:jar:1.19.0 in gitlab-maven
(https://gitlab.developers.cam.ac.uk/api/v4/groups/10920/-/packages/maven/)
```

If you see this error `not found in ... in a previous attempt` this may indicate that
you did have access and now the token has expired, or you previously atttempted to
retrieve from the Repository and now it's failing and the error is cached locally in your
`~/.m2/repository`.

If your configuration in `settings.xml` is good and you are still seeing this
error, delete the folders below `~/.m2/repository` that match the packages shown in the error and
try a `mvn clean compile` again to force retrieving the packages again.

Full example error message:

```text
[ERROR] Failed to execute goal on project irs-core: Could not resolve dependencies for project
uk.ac.cam.irs:irs-core:jar:1.38.0-SNAPSHOT: The following artifacts could not be resolved:
uk.ac.cam.irs:api:jar:1.19.0 (absent), uk.ac.cam.hr:filestorage:jar:1.0.42-cdi-SNAPSHOT
(absent): uk.ac.cam.irs:api:jar:1.19.0 was not found in
https://gitlab.developers.cam.ac.uk/api/v4/groups/10920/-/packages/maven/ during a previous
attempt. This failure was cached in the local repository and resolution is not reattempted until
the update interval of gitlab-maven has elapsed or updates are forced
```
