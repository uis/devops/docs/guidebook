---
title: Run GitLab CI jobs on a GKE-hosted GitLab runner
---

# How to run CI/CD jobs on a GKE-hosted GitLab runner

This short guide shows you how to run CI jobs on product's GKE-hosted GitLab runner using
[tags](https://docs.gitlab.com/ee/ci/yaml/#tags). For an explanation of why you might want to do
this see the **explanation on GKE-hosted runners**.

## Prerequisites

- Ensure that the product in question has been configured with a GKE-hosted GitLab runner. See [GKE
  GitLab Runners](site:reference/cloud-platform/gke-gitlab-runners) for more information.

## Steps

1. Add the `GKE_RUNNER_TAG` CI/CD variable to a job definition. For example:

    ```yaml title=".gitlab-ci.yml"
    dummy-job:
      stage: test
      script: echo "I am a dummy job!"
      tags:
        - $GKE_RUNNER_TAG
    ```

## Summary

In this guide, you learnt how to run CI jobs on a product's GKE-hosted GitLab runner.
