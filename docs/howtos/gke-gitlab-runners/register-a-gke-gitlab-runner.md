---
title: Register GKE-hosted GitLab runners
---

# How to register GKE-hosted GitLab runners

This guide shows you how to deploy/register Google Kubernetes Engine (GKE) hosted GitLab runners
for products deployed by the
[gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
repository.

For more information on the GKE runner configuration, see
[gke-gitlab-runners](site:reference/cloud-platform/gke-gitlab-runners) in the
reference section.

## Prerequisites

The following information will be required to complete the steps in this guide.

- The name of the configuration bucket (created by the `gcp-product-factory`) in the target
  product's meta project. For example, the Gaobase product meta project (`gaobase-meta-5c82d605`)
  has a bucket named
  [`gaobase-config-b1410ccd`](https://console.cloud.google.com/storage/browser/gaobase-config-b1410ccd).
- The `id` of the GitLab sub-group to register the new GitLab runner against. For example, the
  [Gaobase product's sub-group](https://gitlab.developers.cam.ac.uk/uis/devops/gaobase) id is
  `7541`.

## Steps

1. Clone the
   [`gitlab-runner-infrastructure`](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
   repository to your local machine.
2. Create a [new feature branch](site:explanations/git-gitlab/git#feature-branches) for your changes.
3. In `locals.tf`, add your new product to the `product_runners.production` map using the
   information from the prerequisites section above. For example, if you are adding a new product
   called `test-product` you might do the following.

    ```tf hl_lines="7-10"
    product_runners = {
        production = {
            "admissions-portal" = {
                config_bucket   = "ap-config-738a8404"
                gitlab_group_id = 1883
            }
            "test-product" = {
                config_bucket = "test-product-config-012345"
                gitlab_group_id = 666
            }
            ...
            ...
        }
    }
    ```

4. [Commit](site:explanations/git-gitlab/git#commits) your changes,
   push your branch to the remote, and open a merge request.
5. The merge request should be reviewed, merged, and deployed by a member of the Cloud Team.
   Deployment steps can be found in the
   [`README.md`](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure/-/blob/master/README.md)
   file in the
   [gitlab-runner-infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
   repository. If you are not a member of the Cloud Team, ask for assistance in the [Cloud Team
   channel](https://teams.microsoft.com/l/channel/19%3afd77aa792d2243d4ae3818ee4b17d55e%40thread.tacv2/Cloud%2520Team?groupId=8b9ab893-3917-42bb-ba20-6cbd4bd2d304&tenantId=49a50445-bdfa-4b79-ade3-547b4f3986e9).

## Summary

In this guide, you learned how to configure and deploy a GKE hosted GitLab runner for a new product.

## Next steps

- Learn [how to run CI/CD jobs on a GKE-hosted GitLab
  runner](./run-ci-jobs-on-a-gke-gitlab-runner.md)
