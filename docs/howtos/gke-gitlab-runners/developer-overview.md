---
title: GitLab Runner Overview For Developers
---

# GitLab Runner Overview For Developers

This guide provides a quick overview of the GitLab runners used in UIS, any actions / decisions
developers need to take and links to further information.

## What is a GitLab runner?

A GitLab runner is a lightweight application used to execute CI/CD jobs defined in a GitLab
projects `.gitlab-ci.yml` file. It works as an agent that connects the UIS GitLab instance
with the systems where the CI/CD jobs are executed.

## Types of runners

There are two types of runners used in UIS:

- **Product-wide Shared runners**: These are runners shared across all projects under `uis/devops`.
  They are used for general purpose jobs that do not require any special permissions or
  configurations and currently run on `on-prem VMs`. They are selected by default for all jobs
  unless overridden.
- **Product-specific runners**: These are runners dedicated to a single product and are hosted on
  `GKE`. They are used for jobs that require special permissions or configurations. They are
  selected by adding the appropriate tag to the job definition in the `.gitlab-ci.yml` file.

## What do I need to do?

As long as you've configured GKE runners during your product setup and are using the standard
boilerplate templates for your code / infrastructure, you don't need to do anything. Everything
will automatically run on the correct runner.

## What should I read next?

<!-- markdownlint-disable MD013 -->
- [GitLab CI runners](site:explanations/cloud-platform/service-infrastructure#gitlab-ci-runners): Explains the
  architecture of the GitLab runners in UIS.
- [How to register GKE-hosted GitLab runners](./register-a-gke-gitlab-runner.md):
  A guide on how to deploy/register Google Kubernetes Engine (GKE) hosted GitLab runners.
- [How to run CI/CD jobs on a GKE-hosted GitLab runner](./run-ci-jobs-on-a-gke-gitlab-runner.md):
  A guide on how to run CI/CD jobs on a GKE-hosted GitLab runner.
<!-- markdownlint-enable MD013 -->
