---
title: Run an application locally
---

# How to run an application on your local system

This guide describes how to start from the GitLab page for a web application and get to the point
where you can access a local instance in your browser and run tests locally.

!!! warning

    This guide is appropriate for applications which do **not** have a `compose.sh` file in the root
    of the repository. See the [older reference guide](site:notes/webapp-dev-environment) if that
    file is present.

## Prerequisites

If you have not already done so, make sure you have followed our [how-to guide for preparing your
system](site:howtos/development/prepare-your-system) and installed the software which it recommends.
You should confirm that the `docker`, `poetry` and `poe` commands are present on your `PATH`
and that the `docker compose` plugin is installed.

## Downloading the application

Use `git clone` to clone the application repository to your machine. Change to the directory
containing the application using `cd`.

## Running the application

**Check the `README.md` file for any application-specific bootstrap steps.** Commonly these will
take the form of copying a `secrets.env.in` file provided in the repository to a file named
`secrets.env` and providing secret values. The `README` and/or `secrets.env.in` file should provide
more information.

!!! tip

    If the `README.md` file *doesn't* include something which you later find out is required for
    bootstrapping, [open an issue](site:howtos/git-gitlab/open-an-issue) and,
    if you feel comfortable, a merge request adding the missing information.

You may be used to manually creating a virtualenv for Python projects in a directory named `venv` or
`.venv`. **Do not do this.** Instead, let the `poetry` tool create a local Python virtualenv for the
application and install dependencies:

```sh
poetry install
```

??? question "What is a "virtualenv" and what does "poetry install" do?"

    We cover this in the [explainer document](site:explanations/development/webapp-boilerplate)
    about our webapp boilerplate.

Check that the test suite passes:

```sh
poetry poe pytest:local
```

The `poe` command to `poetry` runs one of a predefined set of "tasks" within the virtualenv. You can
see the list of tasks in the `pyproject.toml` file.

If you have installed `poe` into your `PATH`, you can also use `#!sh poe pytest:local` as a shortcut.

!!! tip

    You can see all the "poe" tasks by running `#!sh poetry poe` with no other arguments.

Start the application:

```sh
poetry poe up --wait
```

The `--wait` flag is passed to `docker compose` and tells docker to wait until all the containers
have started and return control back to you.

!!! info

    The `up` task is a convenient way of running the `docker compose --profile development up`
    command. You could have also started the application via `docker compose --profile development
    up --wait` if you wanted to exercise your fingers with extra typing.

Examine the logs from the application:

```sh
docker compose logs --follow webapp
```

Visit [http://localhost:8000/](http://localhost:8000/) in your browser to access the application.

## Summary

In this guide you learned how to clone one of our applications locally, run tests and start the
application.

## Next steps

* Read more about what you can do with our webapp developer environment in the [reference
  guide](site:reference/misc/webapp-developer-environment).
