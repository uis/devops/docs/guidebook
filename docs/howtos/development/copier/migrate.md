---
title: Migrate an existing cookiecutter project to Copier
---

# How to migrate an existing project created with cookiecutter to Copier

!!! Info
    This how-to guide assumes that the Copier tool has been installed as described in
    [the how-to guide for preparing your system](site:howtos/development/prepare-your-system).

!!! info
    For this operation it is necessary to know answers that were used initially.
    Over time, the list of options may change. Please see `README.md` of the corresponding
    boilerplate project for more information.

!!! warning
    In some cases it might be difficult to migrate project to Copier, as files and content were
    changed heavily over time.

In order to add Copier to the existing project (e.g. project created with Cookiecutter)
you need to perform the following steps:

1. Create a local branch in the project. Make sure there's no changes waiting to pushed.

    ```bash
    # Check everything is committed and there is no pending changes
    git status
    # pull latest changes from GitLab
    git pull
    # Create a new branch
    git checkout -b migrate-to-copier
    ```

2. In the root folder of your project create file `.copier-answers.yml`.

3. Put all answers, you gave when created the project from template along with `_commit`
    and `_src_path`. It is recommended that `_commit` be a git tag from one of the boilerplate
    project repositories."

    !!! info
         There are two copier-based boilerplate projects:

         - [webapp-boilerplate](
         https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate)
         - [gcp-deploy-boilerplate](
         https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate)

    - Available tags for [webapp-boilerplatet](
    https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate/-/tags)
    - Available tags for [gcp-deploy-boilerplate](
    https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/tags)

    Two examples are provided below:

    For **webapp-boilerplate**:

    ```yaml
    _commit: 2.0.1 # Boilerplate tag. It makes sense to use lates available at the moment.
    _src_path: git+https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate.git
    api_module: #put_answer_here
    api_name: #put_answer_here
    api_slug: #put_answer_here
    application_module: #put_answer_here
    application_name: #put_answer_here
    application_slug: #put_answer_here
    include_api: #put_answer_here
    include_ui: #put_answer_here
    max_line_length: #put_answer_here
    project_module: #put_answer_here
    project_name: #put_answer_here
    project_repo: #put_answer_here
    project_slug: #put_answer_here
    python_version: #put_answer_here
    respect_x_forward_header: #put_answer_here
    secret_key: #put_answer_here
    ui_module: #put_answer_here
    ui_name: #put_answer_here
    ui_slug: #put_answer_here
    ```

    For **gcp-deploy-boilerplate**:

    ```yaml
    _commit: 2.0.1 # Boilerplate tag. It makes sense to use lates available at the moment.
    _src_path: git+https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate.git
    database_type: #put_answer_here
    gitlab_webapp_project_id: #put_answer_here
    notification_channel: #put_answer_here
    product_configuration_bucket: #put_answer_here
    product_configuration_path: #put_answer_here
    product_name: #put_answer_here
    product_slug: #put_answer_here
    terraform_state_sa_email: #put_answer_here
    use_cloud_load_balancer: #put_answer_here
    ```

4. Run `copier recopy --trust` and answer a number of questions (`Overwrite <filename>? [Y/n]`).
    Answer `Y` to all.

    !!! question
         Why do I need to use the `--trust` flag?

         Our copier templates include some custom code which is used to pre-populate
         poetry lock files, etc. The `--trust` flag is required to allow `copier` to
         run this custom code.

5. Carefully review the output from `git diff`. You may need to manually
    fix up any changes which Copier missed or got wrong. The CHANGELOG for
    the boilerplate template or a `git log` for the tag you used should alert
    you to any significant changes.

6. Test the changes which were made. The testing procedure depend on project, for example
    for projects derives from `gcp-infra-boilerplate` it make sense to apply terraform code
    to the `development` workspace.

7. Make a commit to the project repository.
