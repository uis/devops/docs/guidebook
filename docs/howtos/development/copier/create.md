---
title: Create a new project from a template using the Copier tool
---

# How to create a new project from a project template using the Copier tool

!!! Info
    This how-to guide assumes that the Copier tool has been installed as described in
    [the how-to guide for preparing your system](site:howtos/development/prepare-your-system).

There are two copier-based boilerplate projects:

- [webapp-boilerplate](
   https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate)
- [gcp-deploy-boilerplate](
    https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate)

Details may vary, please check `README.md` of those projects. But in general, process of project
creation is the following:

1. Select the boilerplate git tag you want to create your project from. Tags are available in GitLab

      - Available tags for [webapp-boilerplatet](
      https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate/-/tags)
      - Available tags for [gcp-deploy-boilerplate](
      https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/tags)

2. Run `copier copy` command and provide answers.

    ```bash
    copier copy --trust --vcs-ref <TAG> <URL> <OUTPUT_PATH>
    ```

    Where:

    - <TAG> is a version of the template (e.g. 2.0.0).
    - <URL> is a url of the template project (one of two mentioned on this page).
    - <OUTPUT_PATH> is a path where generated content will be stored. Can be relative or absolute.

    !!! question
        Why do I need to use the `--trust` flag?

        Our copier templates include some custom code which is used to pre-populate
        poetry lock files, etc. The `--trust` flag is required to allow `copier` to
        run this custom code.

    Copier will ask you for a number of input values. Every boilerplate has its own set, please
    check the docummentation.

3. Navigate to the `<OUTPUT_PATH>` directory. This is a newly created project.
    It contains `.copier-answers.yml` where all data is store to be used later when this project
    will be updated with a new version of parent boilerpalate. The only thing left is to init git
    repository and add newly created project:

    ```bash
    git init .
    git add -A
    git commit -m "Initial commit"
    ```

4. Review the result and push to the GitLab.

!!! tip "Using https URLs for templates"

    The `_src_path` URL in `.copier-answers.yml` should be a `https://...` URL to work well with
    [renovatebot](site:howtos/renovatebot/start-using-renovatebot).
    This is fine for public templates but private templates, such as the GCP deploy boilerplate,
    will need the following command to be run to tell git to use SSH to clone those repositories:

    ```sh
    git config set --global \
      url."git@gitlab.developers.cam.ac.uk:".insteadOf https://gitlab.developers.cam.ac.uk/
    ```
