---
title: Update Projects Created from Boilerplate Templates
---

# How to update a project created with Copier to the newer version

!!! Info
    This how-to guide assumes that the Copier tool has been installed as described in
    [the how-to guide for preparing your system](site:howtos/development/prepare-your-system).

Over time, the boilerplate is updated and it is might becomes necessary to update the project as
well with new changes. With copier it can be done in few simple steps.

1. Check the `CHANGELOG` file for information about what has been changed between versions. The
    file is stores in the root of each boilerplate repository.

2. Create a local branch in the root of your project.

3. Select the git tag you want to update your project to.

    - Available tags for [webapp-boilerplatet](
    https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate/-/tags)
    - Available tags for [gcp-deploy-boilerplate](
    https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate/-/tags)

4. Go to your project's workdir and make sure there are no uncommitted files.

5. Run the update command inside you project's root directory.

    ```bash
    copier update --defaults --trust
    ```

    !!! question
         Why do I need to use the `--trust` flag?

         Our copier templates include some custom code which is used to pre-populate
         poetry lock files, etc. The `--trust` flag is required to allow `copier` to
         run this custom code.

    The option `--defaults` indicates the copier will use the previous answers to populate the
    templates before applying the diff on top of your project. In some cases, additional
    information might be required. In this case please check the CHANGELOG for more information.

6. Review changes with `git diff`, resolve conflicts, if any. Push to GitLab.

7. Open a new Merge Reguest to the default branch.
