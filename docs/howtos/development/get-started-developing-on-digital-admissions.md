---
title: Get started developing for Digital Admissions
---

# Developer Onboarding Guide

## Introduction

This how-to guide is intended for new developers working on Digital Admissions.
It aims to provide guidance on setting up the development environment for various components
of the project. This includes backend and frontend tools, testing commands, and guidelines
for linters, auto-formatters, and pre-commit hooks.

## Development Environment Setup

For guidance on preparing your system, please refer to the following guide:
[Prepare Your System guide](https://guidebook.devops.uis.cam.ac.uk/latesite:howtos/development/prepare-your-system/).
This DevOps-wide resource provides detailed instructions to ensure your environment
is correctly set up for development.

## Accessing Project Resources

Follow these steps to gain access to each product.
Make sure to follow up with the relevant contacts if you experience any issues.
The scope of this documentation is to set up a new developer for 3 particular environments
(development, staging, production) across the following systems.
They may have more environments however these are the key ones
 a new dev will encounter in their first months.

### Prod Account separation

As a general rule in DevOps, we split prod admin priveledge to our own `gcloudadmin` accounts.
This is an effort to ensure we don't accidently erase data / infrastructure when browsing GCP.
In summary, for int/staging/dev, our CRSid accounts can have full access.
In prod, use CRSid for view then swap to gcloudadmin account to make changes for prod.

### Step 1: GCloudAdmin setup

- **Instructions**:
  Below is a link to a guide on how to set up a gcloudadmin account.
 [Create Gcloudadmin](https://guidebook.devops.uis.cam.ac.site:howtos/cloud-platform/create-gcloudadmin-account/)
- **Team Data json Link**:
  [Team Data](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json)

### Step 2: General Access

- **External Permissions Needed**: DA GitLab access.
- **Instructions**: Check if access has already been set up for you.
  If not, consult your Tech Lead for assistance.
- **Link**: [DA GitLab Group](https://gitlab.developers.cam.ac.uk/groups/uis/devops/digital-admissions/)
- **GitLab Personal Token Access**: Create a GitLab personal access token on the following page:
 ["Access Tokens" page under "User Settings"](https://gitlab.developers.cam.ac.uk/-/profile/personal_access_tokens).
Ensure it has the `read_registry` scope.

### Step 3: Access SMI

- **Gain admin access to SMI in local Development**:
  Follow the instructions for creating a super user in the
  [SMI Readme](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/pools/smi/-/blob/master/README.md?ref_type=heads#developer-quickstart)
- **Gain admin access to SMI on Dev/Staging/Prod**:
    - Email **[Hopper team](mailto:uis-devops-hopper@lists.cam.ac.uk)**
  asking for permissions to the SMI django site for the dev, staging and prod environments.
    - Hopper team will create an account (`yourCRSid`-admin) in the prod, staging, dev environments.
    - Log into the relevant Django Admin below with the crsid-admin account.
- **Prod Django Admin Link**:
  [Prod django site](https://alpha.subjectmoderationinterface.apps.cam.ac.uk/admin/)
- **Staging Django Admin Link**:
  [Staging django site](https://staging.subjectmoderationinterface.apps.cam.ac.uk/admin/)
- **Dev Django Admin Link**:
  [Dev django site](https://webapp.devel.uga.gcp.uis.cam.ac.uk/admin/)

- **Gain applicant access to MyCApp through SSO**:
  Access through the UI should be granted to your CRSid through the University's SSO for each environment.
  This will be the same for your development instance.
  Note: while you may have access,
  you'll need to be granted view permissions in the admin site to see any data.
- **Gain applicant access to MyCApp with CRSid-admin account**:
  Log into the admin site.
  In the top right of the page, select `view site` to view MyCApp as an applicant.
- **When It's Needed**: When confirming a feature from an end-user's perspective.
  Generally, this would be conducted on staging.
- **Prod Link**: [Prod site](https://alpha.subjectmoderationinterface.apps.cam.ac.uk/)
- **Staging Link**: [Staging site](https://staging.subjectmoderationinterface.apps.cam.ac.uk/)
- **Dev Link**: [Dev site](https://webapp.devel.uga.gcp.uis.cam.ac.uk/)

### Step 4: Access AP-Frontend

#### Firebase admin on GCP

By having set up gcloudadmin and assigning it to DA, you will have access to Firebase.
From the console, you'll be able to view the firebase documents for debugging purposes.

To view the MyCApp UI as an user, you'll need to have one of two things.
A record in the Applicants table or Impersation permission on your admin-user account.
Impersation will allow you to see the state of a given application.
An applicant account will let you see "your" application.
Below is instructions on how to gain access to both.

#### Gain admin access to MyCApp (UI)

- **When It's Needed**: Performing administrative actions for MyCApp within the UI.
  This is generally relevant only for prod.
- **Moderation Levels**:
    - **impersonation** - Impersonate an Applicant in the UI for debugging purposes.
    - **manageCycleData** - Delete previous cycle's data as we enter a new digital pooling cycle.
    - **retrySubmission** - Retry submitting an Application on behalf of an user.
- **Instructions**:
    - Decide upon which moderation priviledge you require
    - Request admin access from **[Hopper team](mailto:uis-devops-hopper@lists.cam.ac.uk)**.
    Include your phone number (Used for MFA) and the required priveledge in the email.
    - They'll add you to the admin-users within Firebase.

##### MFA breakdown by environment

MFA codes are necessary across all environments.
However, staging and local aren't set up for real MFA. Below is how to find the code for each environment.

- **Prod Access**: You'll need MFA set up with the University log in. You will get a code texted to you.
- **Staging Access**: A dummy code in used in log in. This can be found in 1password for Admissions Portal.
- **Local Access**: Your MFA code can be found in the console logs.

#### Gain applicant access to MyCApp (UI)

- **When It's Needed**: Verifying a feature within an environment, generally staging.
- **Instructions**:
  Request MyCApp applicant access from **[Hopper team](mailto:uis-devops-hopper@lists.cam.ac.uk)**
  for the desired environment.
  Include your crsid+[[anything you like]]@cam.ac.uk and environment within the email.
  They'll create an applicant account on your behalf.

##### Navigating to Firebase Database

In the left sidebar, select 'Build' and then scroll to Firestore Database.
On this page you'll be able to see the contents of the database, likely admin-users in the first instance.

##### Accessing File Uploads on Firebase

In the left sidebar, select Storage under Project Shortcuts.
Under user-uploads, you'll be able to find all the user uploads for a given environment.

- **AP Frontend Repo**:
  [AP Frontend](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-frontend)
- **Firebase Rules Repo Link**: The firebase rules are a subsection of the AP Frontend repo.
[AP Frontend - Firebase](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-frontend/-/tree/master/firebase?ref_type=heads)
- **Prod Firebase Link**: [Prod](https://console.firebase.google.com/u/0/project/ap-prod-43267ef1/overview)

### Step 5: Access Albatross

- **When It's Needed**: Attaching a service
- **Instructions**: Request access from the Wilson team.
  Email **[Wilson team](mailto:uis-devops-wilson@lists.cam.ac.uk)**
  asking for a log in for the API gateway. Subsequently, email **[Hopper team](mailto:uis-devops-hopper@lists.cam.ac.uk)**
  asking for permissions to be granted to your new API gateway login for Albatross.
- **Repository Link**:
  [Albatross Backend](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/albatross/albatross-backend)

Direct access to the database is not granted.

### Step 6: Create Google Drives from Synchronisation Service

- **Permission Needed**: `uis-google-drive-creator`
- **When It's Needed**:
  When you need to manually creating/editing Google drives.
  This is typically the case for some operational process at the moment that will soon be automated.
- **Instructions**: Request access from the Wilson team.
  Email **[Wilson team](mailto:uis-devops-wilson@lists.cam.ac.uk)**
  asking to be added to the Lookup group.
- **Repository Link**: [uis-google-drive-creator](https://www.lookup.cam.ac.uk/group/uis-google-drive-creator)

---

## Development Workflow

Follow these steps to start contributing to the project effectively:

### Step 1: Pick a Ticket

- Go to the *Sprint Ready* column in the project management tool.
- Choose a ticket, assign it to yourself, and move it to *In Progress*.

### Step 2: Create a Branch

Use the following format for your branch name to keep names consistent and descriptive:

ssueNumber-short-description
Example: `454-add-readme`

### Step 3: Commit Changes

- Follow **conventional commit** style for your commit messages
(e.g., `feat(readme): add initial README file`).
This is necessary for the auto release pipeline using Release-it.

### Step 4: Run Tests and Pre-Commit Checks

1. **Run Tests**:
  Execute the test suite before pushing your changes to catch any issues early.
2. **Pre-Commit Checks**:
  Complete all linting, formatting, and other pre-commit checks to maintain code quality.

### Step 5: Update Documentation (If Needed)

- If your changes affect usage, setup, or project documentation, update the relevant sections
 to keep them current.

### Step 6: Submit a Merge Request (MR)

- **Request a Review**:
  When your work is ready, submit a Merge Request (MR) and move the ticket to *Review Required*.
- **Notify the Team**:
  Post in the dev channel to inform others that your MR is ready for review.

### Step 7: Coordinate with QA (If Required)

- For tickets that require QA, reach out to the QA team with any key information or
  context they may need.

---

## Project Testing and Linting Guide

This tutorial provides an overview of the development setup for various components of the project,
including backend and frontend tools, testing commands, and guidelines for linters, auto-formatters,
and pre-commit hooks.

### Albatross Backend

- **Pre-commit setup**: No
- **Linter**: No
- **Auto-formatter**: Black
- **Import sorter**: isort

#### Albatross Backend Commands

- **Run tests**:

  ```bash
  ./run_test.sh
  ```

- **Rebuild images**:

  ```bash
  ./run_test.sh -r
  ```

### AP Backend

- **Pre-commit setup**: Yes
- **Linter**: No
- **Auto-formatter**: Black
- **Import sorter**: isort
- **Type checker**: mypy

#### AP Backend Commands

- **Run tests**:

  ```bash
  ./test.sh
  ```

- **Rebuild images**:

  ```bash
  ./test.sh -r
  ```

### Malware Scanner

- **Tools inherited from AP Backend**

#### Malware Scanner Commands

- **Run tests**:

  ```bash
  cd ./malware-scanner
  ./run_test.sh
  ```

- **Rebuild images**:

  ```bash
  ./run_test.sh -r
  ```

### AP Frontend

- **Pre-commit setup**: Yes
- **Linter**: eslint
- **Auto-formatter**: prettier
- **Type checker**: Typescript*

#### AP Frontend Commands

- **Run unit tests**:

  ```bash
  pnpm run --filter @ap-frontend/webapp... test:unit
  ```

- **Run Firebase tests (requires emulators)**:

  ```bash
  docker compose --profile emulators build firebase-emulators
  docker compose --profile emulators run firebase-emulators
  cd firebase/rules && pnpm run test:rules
  ```

- **Run e2e tests**:

  ```bash
  docker compose --profile test-e2e up --build
  ```

- **Run linter**:

  ```bash
  pnpm run test:lint
  ```

### SMI

- **Pre-commit setup**: Yes
- **Linter**: flake8
- **Auto-formatter**: Black
- **Import sorter**: No
- **Type checker**: No

#### SMI Commands

- **Run backend tests** (includes Flake8, unit tests, and documentation update):

  ```bash
  ./tox.sh
  ```

- **Rebuild images**:

  ```bash
  ./tox.sh -r
  ```

- **Run frontend tests**:

  ```bash
  ./compose.sh jest run --run jest
  ```

- **Run End to End tests**:

  ```bash
  ./e2e.sh
  ```

### Synchronisation Service

- **Pre-commit setup**: No
- **Linter**: flake8
- **Auto-formatter**: Black
- **Import sorter**: isort

#### Synchronisation Service Commands

- **Run tests**:

  ```bash
  tox
  ```

- **Rebuild images**:

  ```bash
  tox -r
  ```
