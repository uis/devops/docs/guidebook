---
title: Set-up pre-commit hooks on a project
---

# How to set-up pre-commit hooks on a project

We add pre-commit hooks in order to run various linting and auto-formatting tools over the source
before they are committed and pushed to GitLab. This how-to guide covers how to add pre-commit
hooks to an existing project.

## Standard hooks to add to all projects

!!! info
    If you are using one of our boilerplates, an appropriate set of hooks will already be present.

Add the following hooks to all projects, by placing them in `.pre-commit-config.yaml` at the root
of the project:

```yaml title=".pre-commit-config.yaml"
# See https://pre-commit.com/hooks.html for more hooks
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v5.0.0
    hooks:
      - id: trailing-whitespace
      - id: end-of-file-fixer
      - id: check-yaml
        args:
          - --unsafe
      - id: check-json
      - id: check-toml
      - id: check-xml
      - id: check-added-large-files
      - id: check-executables-have-shebangs
      - id: check-merge-conflict
      - id: check-symlinks
      - id: detect-private-key
      - id: mixed-line-ending
      - id: pretty-format-json
        args:
          - --autofix
          - --no-sort-keys
      - id: debug-statements
  - repo: https://github.com/DavidAnson/markdownlint-cli2
    rev: v0.14.0
    hooks:
      - id: markdownlint-cli2
        args: ["--fix"]
        language_version: 22.10.0
```

!!! warning
    As the `rev` parameter above is hard-coded, you should also
    [set-up renovatebot](site:explanations/renovatebot) to keep this and your project's other
    dependencies up-to-date.

## Installing pre-commit hooks

After adding a `.pre-commit-config.yaml` or modifying it, the following should be run in order to
set-up the hooks:

```sh
pre-commit-install
```

## Testing pre-commit hooks

Normally, pre-commit will run against files which are staged to be committed. To test the
hooks separately from a git commit against all files in your repository, for
instance if to re-format all existing source code to conform to a relevant style guide,
run:

```sh
pre-commit run --all-files
```

## Language-specific hooks

Additionally, hooks should be added to process the particular source code languages contained in
the project:

### Python

The following pre-commit plugins should be added for Python projects (replacing the rev parameters
with latest releases of the plugins):

```yaml
  - repo: https://github.com/python-poetry/poetry
    rev: 1.5.1
    hooks:
      - id: poetry-check

  - repo: https://github.com/editorconfig-checker/editorconfig-checker.python
    rev: 2.7.2
    hooks:
      - id: editorconfig-checker
        args: ["-disable-indent-size"]

  - repo: https://github.com/psf/black
    rev: 23.3.0
    hooks:
      - id: black

  - repo: https://github.com/timothycrosley/isort
    rev: 5.12.0
    hooks:
      - id: isort

  - repo: https://github.com/pycqa/flake8
    rev: 6.0.0
    hooks:
      - id: flake8

  - repo: https://github.com/pre-commit/mirrors-mypy
    rev: v1.4.1
    hooks:
      - id: mypy
        additional_dependencies: [types-requests, types-oauthlib]
        args: [--explicit-package-bases]
```

### Java

To ensure your Java source is formatted consistently, use the
[google-java-format](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/precommit-google-java-format)
pre-commit plugin by adding the following to your
`.pre-commit-config.yaml` in the root of your project (a):

```yaml
    - repo: https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/precommit-google-java-format
      rev: v1.2.0
      hooks:
        - id: google-java-format
          files: "\\.java$"
          args: ["-i"]
```

This plugin formats your code to match the
[Google Java Style Guide](https://google.github.io/styleguide/javaguide.html) on `git commit`.
If your code already meets the required code format the plugin will pass and allow your commit to
continue. If your code requires formatting it will format the code automatically for you but fail
pre-commit. This allows you to review the changes and commit any updates to continue.

To run locally, the current `v1.2.0` version of the pre-commit formatter requires Java 11 in your
path.

## Summary

In this how-to you learned how to set-up a standard set of pre-commit hooks on a project.

Further information on using pre-commit:

- [Webapp developer environment](site:explanations/development/webapp-boilerplate#pre-commit)
- [Creating a Python package](site:tutorials/creating-a-python-package#catching-and-fixing-style-issues)
