---
title: Create a new application
---

# How to create a new application

We have a standard template for our web applications. Our template includes support for a UI being
provided alongside the application and for "pure" API applications. This guide covers how to create
a new application from our boilerplate template, starting it and running the tests.

Read our [explainer on the webapp boilerplate](site:explanations/development/webapp-boilerplate)
if you're unsure whether the boilerplate is right for you.

## Prepare your system

If you have not already done so, make sure you have followed our [how-to guide for preparing your
system](site:howtos/development/prepare-your-system) and installed the software which it recommends.

We use the [copier](https://copier.readthedocs.io/) to apply our webapp templates.
Make sure that it is installed on your system via:

```sh
python3 -m pip install --user copier
```

## API-only or UI-included?

Our boilerplate supports two flavours of application: "pure API" or "included UI". For standalone
webapps, you will want to include a UI. For components of a wider ecosystem you will want a "pure
API" application. Usually this choice is fairly easy given the requirements of the application. It
is easier to not use or delete a UI after the fact so, if unsure, include the UI when creating the
application.

## Create the application

!!! Info
    More information on working with copier templates is available
    on a [dedicated page](site:reference/deployment/templates).

Start the template process via:

```sh
copier copy --trust --vcs-ref <TAG> \
https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate.git <OUTPUT_PATH>
```

Where:

- `<TAG>` is a version of the template (e.g. 2.0.0. List of tags is available
[here](https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate/-/tags))
- `<OUTPUT_PATH>` is a path where generated content will be stored. Can be relative or absolute.

Answer each question or press <kbd>Enter</kbd> to accept the default.

Your application is created in a directory you specify in `<OUTPUT_PATH>`. Change to that
directory and check that the tests pass:

```console
$ cd {name-of-generated-directory}
$ poetry install
Installing dependencies from lock file
...
$ poetry poe pytest:local
Poe => pytest
============================= test session starts ==============================
platform darwin -- Python 3.11.4, pytest-7.4.0, pluggy-1.2.0
django: settings: boilerplate_webapp_project.settings.testing (from env)
rootdir: /Users/rjw57/projects/boilerplate_webapp
configfile: pyproject.toml
plugins: docker-tools-3.1.3, Faker-18.13.0, django-4.5.2, env-0.8.2
collected 14 items

api/tests/test_versions_view.py .                                        [  7%]
boilerplate_webapp/tests/test_checks.py ..                               [ 21%]
boilerplate_webapp_project/test/test_admin.py ..                         [ 35%]
boilerplate_webapp_project/test/test_pipelines.py ....                   [ 64%]
boilerplate_webapp_project/test/test_systemchecks.py ...                 [ 85%]
boilerplate_webapp_project/test/test_urls.py .                           [ 92%]
ui/tests/test_views.py .                                                 [100%]

============================== 14 passed in 2.61s ==============================
```

## Summary

In this guide you learned how to create a new boilerplate application using our template.

## Next steps

- Read the [developer reference](site:reference/misc/webapp-developer-environment) for the developer
  environment.
