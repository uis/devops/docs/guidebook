---
title: Check product configuration
---

# How to check that a product is configured correctly

This how-to guide will take you through the process of checking that all parts of a product have
been created and are correctly configured for our most up-to-date processes.

There is a [dedicated how-to](./bootstrap-a-product.md) covering how to start a new product from
scratch.

The word "product" can mean many things in the DevOps division. In particular what a "product" means
from a GitLab perspective may be different than from a "Google Cloud" perspective. We have a
dedicated explainer covering [products and service
infrastructure](site:explanations/cloud-platform/service-infrastructure).

The following sections cover the checks you should make.

## Teams

Check that:

* There is an appropriate delivery team present in
  [team_data.json](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json).
* The user accounts in the team are of the form `{crsid}@gcloudadmin.g.apps.cam.ac.uk`.

For further steps, you need to know the name of the Google Groups which have been created
representing the team members and roles.

## Google Cloud resources

Check that:

* There is configuration in
  [gcp-product-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/)
  for the product.
    * If there is no configuration but there are already Google Cloud resources, see the
      [guide to migrating existing Google Cloud
    resources](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/docs/migrating-manually-created-resources.md?ref_type=heads).
* The `use_deprecated_owner_sa` variable is not set.
    * If it is set, see [the guide for migrating older
      applications](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/docs/migrating-boilerplate.md).
* The `view_users` variable is set to a list containing the `view` and `deploy` users for the
  appropriate team.
    * If the variable is not set, set it as in the [how-to guide for new
      products](./bootstrap-a-product.md).

For further steps, you need know the "meta project" id and "configuration bucket" id for the
product. Ask {{ gitlab_label("team::Cloud") }} if you do not know these.

## GitLab projects

Check that:

* There is configuration in
  [gitlab-project-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/)
  for the product.
    * If there is no configuration but there are already GitLab projects, see the [migration
      guide](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/-/blob/main/docs/migrating-gitlab-product.md).
      {{ gitlab_label("team::Cloud") }} can help with tricky migrations.
* The configuration specifies the meta project in the `product_meta_project` variable.
* The configuration uses `parent_group_id` rather than `parent_full_path`.
* Projects which build container images have `container_expiration_policy` set. See the
  [configuration for the Regent House ballots
  product](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/-/blob/main/product-vars/regent-house-ballots/regent-house-ballots.tfvars)
  for a real-life example of container expiration policies.

For further steps, you need the numeric id of the GitLab product's group. This can be found on the
group's page in GitLab.

## GitLab CI runners

Check that:

* There is configuration in
  [gitlab-runner-infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure/)
  for your product.
    * If there is no configuration, follow the steps in the [how-to guide for new
      products](./bootstrap-a-product.md).
* The configuration specifies the Google Cloud configuration bucket.
* The configuration specifies the numeric id of the GitLab product's group.

## Summary

In this how-to you learned what configuration must be present for a product in order to use our
current development automation processes.
