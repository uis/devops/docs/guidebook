---
title: Delete an existing product
---

# How to delete all resources for the product

This page provides instructions on how to delete resources for a product. Resources
related to the product are stored across several Git repositories with Terraform code in our
[GitLab](https://gitlab.developers.cam.ac.uk):

* Product infrastructure repositories
* [GitLab GKE CI Runner Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
* [Gitlab Project Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory)
* [Google Cloud Product Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)

As our cloud platform deployment [comprises multiple stages](
site:reference/cloud-platform), it must be deleted in a strict order. The following
information illustrates the right order.

!!! Info
    Not every stage in this guide is required. For example, a product can have no related GitLab CI
    runner infrastructure.

## Stage 1 - Delete Resources in Infrastructure Project

First, it is needed to delete the top-level infrastructure, i.e. resources that are used
by the product's deployment (Cloud SQL instances, Cloud Run instances, so on).

Almost all infrastructure projects were created from [the template](site:reference/deployment/templates).
However, the set of resources may vary from product to product (e.g. some products use the MySQL
database and others use the PostgreSQL database), so there is no universal solution.
The general steps are as follows:

* Clone the Git project.
* Get a list of the workspaces for the infrastructure project by running `logan terraform workspace list`
  inside the infrastructure project directory. The output will be similar to the one shown below:

    ```terraform
     default
     * development
     production
     staging
     test
    ```

    !!! Info
        Alternatively, workspaces can be found in product's `.tfvars` file in
        [Google Cloud Product Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory).

* Check your project for resources protected from deletion (Storage buckets, SQL instances, etc).
  For example, if the infrastructure project was created from [Boilerplate Google Cloud Deployment project](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate)
  and it uses SQL, then most probably the `deletion_protection` option for the SQL instance
  is set to `true`. Set it to `false` and apply the configuration before the next step,
  otherwise `terraform destroy` command will fail.
  If the infrastructure project has storage bucket(s), ensure that it can be actually deleted.
  This can be achieved by adding `force_destroy = true` to the bucket resource(s) and applying
  the Terraform configuration.
* Destroy resources in every workspace. The example below shows how to do it for a `development` workspace:

    ```shell
     logan --workspace=development terraform destroy
    ```

   During this process, Terraform will display all resources that it will destroy. Carefully
   review and type `yes`.

## Stage 2 - Delete GitLab GKE CI Runner

Next, if your product uses the GKE based CI runner, delete it from the Terraform code in
[GitLab GKE CI Runner Infrastructure](
https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure). To do so,
perform the following:

* Clone the [GitLab GKE CI Runner Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
   repository.

    ```shell
     git clone git@gitlab.developers.cam.ac.uk:uis/devops/devhub/gitlab-runner-infrastructure.git
     cd gitlab-runner-infrastructure
    ```

* Create a new branch with a relevant name, for example `delete-ci-runner-for-my-product-name`.

    ```shell
     git checkout -b delete-ci-runner-for-my-product-name
    ```

* Find the information about the product in [locals.tf](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure/-/blob/master/locals.tf)
* Remove the corresponding item from `product_runners.production` map.
* Commit changes, push, and submit new merge request for review:

    ```shell
     git add locals.tf
     git commit -m "Delete CI runner for my-product-name"
     git push --set-upstream origin delete-ci-runner-for-my-product-name
    ```

* After merging it, apply the production configuration from the `master` branch by running
   the following:

    ```shell
     git checkout master
     git pull
     logan --workspace=production terraform apply
    ```

## Stage 3 - Delete Resources in Gitlab Project Factory

Next, when the top-level infrastructure and CI runner are destroyed, it is possible to delete
projects/groups in GitLab. Here's a simple steps to do so:

* Clone the [Gitlab Project Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory)
   repository:

    ```shell
     git clone git@gitlab.developers.cam.ac.uk:uis/devops/infra/gitlab-project-factory.git
     cd gitlab-project-factory
    ```

* Create a new branch with a relevant name, for example `delete-gitlab-resources-for-my-product-name`:

    ```shell
     git checkout -b delete-gitlab-resources-for-my-product-name
    ```

* Identify the product's directory under the [product-vars](
   https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/-/tree/main/product-vars)
   directory.
* Destroy resources:

    ```shell
     ./run-project-factory.sh -p ./product-vars/my-product-name destroy
    ```

    Make sure `my-product-name` is replaced by actual product name.

    !!! warning
        This is irreversible operation! On this step all GitLab resources, described in related
        directroy under `./product-vars/` will be deleted!

* Delete folder `./product-vars/my-product-name`.
* Commit changes, push and submit a new merge request for post factum review:

    ```shell
     git add ./product-vars/my-product-name
     git commit -m "Delete GitLab resources for my-product-name"
     git push --set-upstream origin delete-gitlab-resources-for-my-product-name
    ```

## Stage 4 - Delete Resources in GCP Product Factory

Finally, destroy the low-level GCP resources (projects, IAM, DNS, so on).

* Clone the [Google Cloud Product Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
   repository:

    ```shell
     git clone git@gitlab.developers.cam.ac.uk:uis/devops/infra/gcp-product-factory.git
     cd gcp-product-factory
    ```

* Create a new branch with a relevant name, for example `delete-gcp-resources-for-my-product-name`:

    ```shell
     git checkout -b delete-gcp-resources-for-my-product-name
    ```

* Identify the product's directory in the [product-vars](
   https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/tree/master/product-vars)
   directory.
* Override the product's deletion protection by setting `enable_product_deletion_protection = false`
  in the product's `.tfvars` file and apply the change:

    ```shell
     ./run-product-factory.sh -p ./product-vars/my-product-name apply
    ```

    Make sure `my-product-name` is replaced by actual product name.

* Destroy resources:

    ```shell
     ./run-product-factory.sh -p ./product-vars/my-product-name destroy
    ```

    Make sure `my-product-name` is replaced by actual product name.

    !!! tip

        You may see an error similar to the following:

        ```
        ╷
        │ Error: Error when reading or editing ManagedZone: googleapi: Error 400: The resource named
        │ 'project' cannot be deleted because it contains one or more 'resource records'.,
        │ containerNotEmpty
        ╵
        ```

        This can happen when DNS records not managed by the product factory have been created, for
        example by partial deployments of the product or manual operations. This can be rectified by
        ensuring that `terraform destroy` has been run in the product's deployment configuration or
        by manually inspecting the Cloud DNS configuration for the product in the Google Cloud
        Console and removing any dangling records.

    !!! warning
        This is irreversible operation! On this step all GCP resources, described in related
        directroy under `./product-vars/` (including IAM, DNS, and projects) will be deleted!

* Delete folder `./product-vars/my-product-name`.
* Commit changes, push and submit a new merge request for post factum review:

    ```shell
     git add ./product-vars/my-product-name
     git commit -m "Delete GCP resources for my-product-name"
     git push --set-upstream origin elete-gitlab-resources-for-my-product-name
    ```
