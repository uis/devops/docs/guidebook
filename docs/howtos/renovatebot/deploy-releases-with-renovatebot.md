---
title: Update releses in deployment projects
---
# How to have renovatebot update deployment projects with new web app releases

A common pattern in our deployments is to have something like the following in `locals.tf` to
specify which releases of a web app should be present in a given environment.

```tf title="locals.tf"
locals {
  container_images = merge(
    local.default_container_images,
    lookup({
      production = {
        webapp_tag = "1.2.3"
      }
      staging = {
        webapp_tag = "1.2.3"
      }
      # ... etc ...
    }, terraform.workspace, {})
  )
}
```

This how-to guide covers how to have renovatebot automatically open Merge Requests in your
deployment projects when new releases of your web app are made in GitLab.

## Prerequisites

Your upstream web app projects must have [release automation
enabled](site:howtos/git-gitlab/enable-automated-gitlab-releases)
and use [semver](https://semver.org/) versioning.

You must have [enabled renovatebot](site:howtos/renovatebot/start-using-renovatebot)
in the deployment project holding your terraform.

## Enable the terraformWebappRelease preset

Add the appropriate line to the `renovate.json` file in your *deployment* project's repository:

```json title="renovate.json" hl_lines="4"
{
  extends: [
    // ... other presets ...
    "local>uis/devops/renovate-config:terraformWebappRelease"
  ]
}
```

## Annotate terraform locals representing web app releases

Use the `# renovate-gitlab-release` annotation to mark terraform locals which refer to web app
releases. Include full path to the web app project on GitLab. For example, if your web app could be
found at `https://gitlab.developers.cam.ac.uk/uis/devops/path/to/webapp` you would update
`locals.tf` in the following way:

```tf title="locals.tf" hl_lines="6 10"
locals {
  container_images = merge(
    local.default_container_images,
    lookup({
      production = {
        # renovate-gitlab-release: project=uis/devops/path/to/webapp
        webapp_tag = "1.2.3"
      }
      staging = {
        # renovate-gitlab-release: project=uis/devops/path/to/webapp
        webapp_tag = "1.2.3"
      }
      # ... etc ...
    }, terraform.workspace, {})
  )
}
```

## Optionally trigger renovatebot once a release is made

Renovatebot runs twice per day and so it can, in some cases, take around half a day between a
release being made in your upstream web app and it triggering a MR being opened on the deployment
project.

If you want to speed this up, you have two choices:

1. [Manually trigger a run of renovatebot](./trigger-renovatebot-from-gitlab-ui.md) in the
   deployment project *after* the GitLab release has been made.
2. Add a CI job to your *deployment* project which [triggers a renovatebot
   run](./trigger-renovatebot-from-ci.md) and add jobs to your upstream web app projects which
   trigger a CI pipeline run in your deployment projects.

!!! warning

    Renovatebot caches GitLab releases and so if you trigger renovatebot *before* the GitLab release
    is actually made, you will have to wait before triggering again. Currently the cache timeout is
    set to 15 minutes.

## Summary

In this how-to guide, you learned how to configure deployment projects so that renovatebot will
automatically open Merge Requests when new versions of the deployed web apps are released.

## Next steps

* [How to configure release automation](site:howtos/git-gitlab/enable-automated-gitlab-releases)
* [Manually triggering renovatebot](./trigger-renovatebot-from-gitlab-ui.md)
* [Triggering renovatebot from CI](./trigger-renovatebot-from-ci.md)
