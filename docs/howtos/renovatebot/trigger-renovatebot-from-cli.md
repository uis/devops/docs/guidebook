---
title: Triggering from the command line
---
# How to trigger renovatebot runs using the command line

Ordinarily there are many hours between runs of renovatebot on a project. This is frustrating if,
for example, you need renovatebot to rebase or retry a Merge Request or if you can see MRs pending
in the [Dependency Dashboard](https://docs.renovatebot.com/key-concepts/dashboard/) and want them
opened.

This guide covers how you can trigger a renovatebot run yourself from the command line.

## Prerequisites

* The `gcloud` CLI tool must be installed as per the guide on [preparing your
  system](site:howtos/development/prepare-your-system).
* You must be authenticated via `gcloud auth login` with a [gcloudadmin
  account](site:howtos/cloud-platform/create-gcloudadmin-account).
* You must have the [httpie command line tool](https://httpie.io/) installed.

## Triggering a run

Make sure you are authenticated as a `gcloudadmin` user:

```sh
gcloud auth login
```

??? important "Do not use `gcloud auth application-default login`."

    We *do not* use `gcloud auth application-default login` as we would when, for example,
    running `logan`. This is because we want to use `gcloud auth print-identity-token` to generate
    an authentication token for the triggering API and that requires that we authenticate ourselves
    to the `gcloud` tool itself.

When authenticated as a `gcloudadmin` user, you are able to trigger a run against any project under
`uis/devops`. For example, to trigger a run against `uis/devops/experiments/example`:

```sh
http POST \
    "https://europe-west2-gitlab-bots-prod-364942b0.cloudfunctions.net/renovatebot-trigger-8d99" \
    "Authorization:bearer $(gcloud auth print-identity-token)" \
    "projects[0]=uis/devops/experiments/example"
```

The command should output something similar to:

```txt
HTTP/1.1 200 OK
Alt-Svc: h3=":443"; ma=2592000,h3-29=":443"; ma=2592000
Content-Length: 134
Content-Type: application/json
Date: Thu, 21 Nov 2024 11:04:51 GMT
Function-Execution-Id: 3uu6cjyk4f8s
Server: Google Frontend
X-Cloud-Trace-Context: 589f7775ce0664740b175b9446173e2a;o=1

{
    "jobs": "trigger-api-cfdbca5f08d50cceec3a24f61ee71f47",
    "projects": [
        "uis/devops/experiments/example"
    ],
    "status": "triggered"
}
```

A manually triggered run of renovatebot involves the creation and provisioning of a one-off VM for
the run and so it can take up to 10 minutes for a run to complete.
The job id can be reported to {{ gitlab_label("Developer Experience") }} if a run of renovatebot
doesn't seem to have happened within a reasonable amount of time.

!!! important

    To guard against inadvertently keeping the renovatebot VM running it will be automatically
    destroyed after a few minutes. To ensure that renovatebot can finish before the VM is destroyed,
    you are currently limited to triggering a run against one project at a time.

## Summary

In this guide you learned how to trigger a run of renovatebot for a GitLab project from the command
line.

## Next steps

* How to [trigger renovatebot runs from the GitLab UI](./trigger-renovatebot-from-gitlab-ui.md).
* How to [trigger renovatebot runs from GitLab CI pipelines](./trigger-renovatebot-from-ci.md).
