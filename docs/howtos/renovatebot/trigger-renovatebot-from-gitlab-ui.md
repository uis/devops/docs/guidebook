---
title: Triggering using GitLab's UI
---
# How to trigger renovatebot runs using the GitLab UI

Ordinarily there are many hours between runs of renovatebot on a project. This is frustrating if,
for example, you need renovatebot to rebase or retry a Merge Request or if you can see MRs pending
in the [Dependency Dashboard](https://docs.renovatebot.com/key-concepts/dashboard/) and want them
opened.

This guide covers how you can trigger a renovatebot run yourself from the Dependency Dashboard
issue.

## The dependency dashboard

Each project using renovatebot has a "Dependency Dashboard" issue. At the bottom of the issue is a
checkbox which can be used to trigger a run of renovatebot:

<figure markdown>
![](./dep-dashboard-pre-trigger.png)
<figcaption markdown>Dependency dashboard showing the manual trigger checkbox.</figcaption>
</figure>

Ticking the box will result in the checkbox being replaced with a message indicating that a run has
been scheduled.

<figure markdown>
![](./dep-dashboard-post-trigger.png)
<figcaption markdown>Dependency dashboard showing a manually triggered run.</figcaption>
</figure>

The checkbox will be restored when renovatebot has completed its run.

!!! important

    A manually triggered run of renovatebot involves the creation and provisioning of a one-off VM
    for the run and so it can take up to 10 minutes for a run to complete.

The job id can be reported to {{ gitlab_label("Developer Experience") }} if a run of renovatebot
doesn't seem to have happened within a reasonable amount of time.

## Summary

In this guide you learned how to trigger a run of renovatebot for a GitLab project using UI exposed
in the Dependency Dashboard issue for the project.

## Next steps

* How to [trigger renovatebot runs from the command line](./trigger-renovatebot-from-cli.md).
* How to [trigger renovatebot runs from GitLab CI pipelines](./trigger-renovatebot-from-ci.md).
