---
title: Test changes to configuration
---
# How to run renovatebot locally against your repository

When adding custom configuration to `renovatebot.json`, it is frustrating to have to a) merge
untested changes into `main` and b) wait for renovatebot to run.

This guide covers how you can test renovatebot yourself against a branch in your project before
merging to `master`.

## Create a GitLab personal access token

You firstly need to [create a personal access
token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) for GitLab. The token
should have the following *read-only* scopes for safety:

* `read_api`
* `read_repository`
* `read_registry`

If you want, you could also create a [project access
token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) which is further
scoped to the project you are testing.

Once you have the token, read it into the `RENOVATE_TOKEN` environment variable:

```console
$ read RENOVATE_TOKEN
{ paste GitLab access token here }
$ export RENOVATE_TOKEN
```

## Run renovatebot in dry-run mode

Set the `GITLAB_PROJECT` and `GITLAB_BRANCH` environment variables to the project you want to test
and the branch containing the `renovate.json` changes.

```console
GITLAB_PROJECT=uis/devops/replace/with/your/project
GITLAB_BRANCH=replace-with-your-branch
```

!!! warning

    The changes on your branch will not *replace* the default branch configuration but will be be
    *merged* with any existing configuration in the default branch.

Run renovatebot locally via `docker`:

```sh
docker run --rm -e RENOVATE_TOKEN -e LOG_LEVEL=debug \
  -e "RENOVATE_REPOSITORIES=[\"${GITLAB_PROJECT}\"]" \
  -e "RENOVATE_BASE_BRANCHES=[\"${GITLAB_BRANCH}\"]" \
  renovate/renovate \
  --platform=gitlab --endpoint=https://gitlab.developers.cam.ac.uk/api/v4/ --ignore-pr-author \
  --use-base-branch-config=merge \
  --dry-run
```

## Summary

In this guide you learned how to run renovatebot locally in order to test and debug changes to local
repository configuration.
