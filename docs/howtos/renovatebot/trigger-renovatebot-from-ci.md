---
title: Triggering from CI pipelines
---
# How to trigger a renovatebot runs from a GitLab CI pipeline

Teams using release automation may wish to arrange for renovatebot to run against deployment
projects when new releases are made from dependency projects. This guide covers how to trigger
renovatebot runs from CI pipelines.

## Prerequisites

In order for projects to have renovatebot runs triggered they need to have a renovatebot trigger job
configured in their CI pipelines.

It is **recommended** that the CI configuration use the [common
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/common-pipeline.yml?ref_type=heads)
which brings in the required CI job automatically.

Alternatively, you can include the renovatebot trigger job manually:

```yaml
include:
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "/trigger-renovatebot.gitlab-ci.yml"
    ref: "{replace this with the latest ci-templates repository tag}"
```

A new CI job is added to the pipeline called `trigger-renovatebot`. **The job will only be run if
the `TRIGGER_RENOVATEBOT_ENABLED` variable is set**. The job will trigger a renovatebot run against
the project containing the CI pipeline.

## Triggering runs in other projects

Suppose you have two projects `uis/devops/puntbooking/infra` and `uis/devops/puntbooking/webapp`.
You have [release automation](site:howtos/git-gitlab/enable-automated-gitlab-releases)
enabled for the `webapp`.
You have [added configuration](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config#keeping-terraform-configurations-up-to-date-with-latest-release-of-a-web-app)
in `uis/devops/puntbooking/infra` to open Merge Requests when new releases of `webapp` are made.

When releases are made in the `webapp` project you want to trigger renovatebot runs for
`uis/devops/puntbooking/infra` so that the Merge Requests are opened in a timely fashion.

Firstly, ensure that `uis/devops/puntbooking/infra` either uses the common pipeline or includes the
`trigger-renovatebot.gitlab-ci.yml` template. Then add the following job to `.gitlab-ci.yml` in
`uis/devops/puntbooking/webapp`:

```yaml
# .gitlab-ci.yml in uis/devops/puntbooking/webapp

trigger-renovatebot-downstream:
  # Trigger a pipeline in our infrastructure project.
  trigger:
    project: uis/devops/puntbooking/infra

  # Set the TRIGGER_RENOVATEBOT_ENABLED variable so that the "trigger-renovatebot" job runs.
  variables:
    TRIGGER_RENOVATEBOT_ENABLED: "1"
  inherit:
    variables: false

  # The "stage" and "needs" configuration is up to you. In this example we use values which make the
  # job depend on release-it having successfully made a release.
  stage: production
  needs: [release]

  rules:
    # Only run this job on the default branch.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

!!! important

    The `trigger-renovatebot-downstream` job will trigger a *full* pipeline downstream. You may find
    that you need to add some `..._DISABLED` variables to the `variables:` section in order to disable
    jobs you don't want to run.

    Alternatively you could use `include:rules:` in the `.gitlab-ci.yml` file within
    `uis/devops/puntbooking/infra` to selectively include templates depending on the value of
    `TRIGGER_RENOVATEBOT_ENABLED`.

## Permissions

The permissions model for triggering renovatebot runs follows [GitLab's own permissions
model](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines).
Specifically the `trigger-renovatebot-downstream` job will only succeed if the user triggering the
run has permissions to trigger CI pipelines in the downstream project.

## Summary

In this guide you learned how to ensure that CI jobs for triggering renovatebot runs are included
and how to cause renovatebot to be run against downstream projects when new releases are made via
release automation.

## Next steps

* How to [trigger renovatebot runs from the GitLab UI](./trigger-renovatebot-from-gitlab-ui.md).
* How to [trigger renovatebot runs from the command line](./trigger-renovatebot-from-cli.md).
