---
title: Temporarily disable renovatebot
---
# How to temporarily disable renovatebot

You may want to temporarily disable renovatebot if you do not want to be distracted with "business
as usual" Merge Requests during some critical period or if a project is being mothballed.

## Set the `enabled` flag to `false`

To disable renovatebot, add the following to `renovate.json`:

```js title="renovate.json"
{
  // ... other config ...
  "enabled": false
}
```

## Permanently disabling renovatebot

You can permanently disable renovatebot by removing the `renovate.json` file.

## Summary

In this guide you learned how to temporarily and permanently disable renovatebot for your project.
