---
title: Keep docker image versions fresh
---
# How to keep docker image versions fresh in your terraform deployments

If you are deploying a version of a docker image from your terraform configuration, renovatebot can
keep this version fresh for you and auto-open Merge Requests when a new image is available.

## Make sure the dependency is listed in `locals.tf`

We'll take the example of renovatebot's own deployment. Within that deployment, `locals.tf` contains
the following:

```tf title="locals.tf"
locals {
  # ... other config ...
  renovatebot_image = "renovate/renovate:38.40.0"
}
```

## Add a custom manager to `renovate.json`

When the image name and version are together like this on one line, we can use a `regex` custom
manager to teach renovate about the dependency.

Add the following to `renovate.json`:

```js title="renovate.json"
{
  // ... other config ...
  "customManagers": [
    {
      "customType": "regex",
      "fileMatch": [
        "^locals\\.tf$"
      ],
      "matchStrings": [
        "renovatebot_image\\s*=\\s*\"(?<depName>.*?):(?<currentValue>.*?)\""
      ],
      "datasourceTemplate": "docker"
    }
  ]
}
```

!!! important

    You will need to tweak this config to include the name of your local. You may find the
    instruction for [running renovatebot locally to test your
    configuration](./run-renovatebot-locally.md) useful. It is also sensible to [add pre-commit
    hooks](./use-pre-commit-hooks-with-renovatebot.md) to validate your renovatebot configuration
    when committing.

Renovatebot will now open Merge Requests to update this dependency when there is a new image
available
([example](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/-/merge_requests/19)).

## Summary

In this guide you learned how to add a custom manager to renovatebot in order to keep a docker image
referenced in a terrform configuration fresh.

## Next steps

* Learn [how to test your renovatebot configuration](./run-renovatebot-locally.md).
* Learn how to add [pre-commit hooks to validate your renovatebot
  configuration](./use-pre-commit-hooks-with-renovatebot.md).
