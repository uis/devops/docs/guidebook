---
title: Add the MIT licence to a project
---

# How to add the MIT licence to a project

We try to be as open as possible when releasing projects to the wider University
or to the public as a whole. We usually release all of our code under the [MIT
licence](https://opensource.org/license/mit/).

To add the MIT licence to a project and have GitLab detect the licence, add a
`LICENSE.txt` file[^1] to the root of a repository with the following content:

[^1]: Yes, this should be spelled `LICENCE.txt`. No, we can't teach GitLab to
    understand that.

```text title="LICENSE.txt"
MIT License

Copyright (c) University of Cambridge Information Services

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
