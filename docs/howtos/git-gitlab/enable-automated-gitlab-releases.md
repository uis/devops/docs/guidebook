---
title: Enable automated GitLab releases
---

# How to enable automated GitLab releases

This guide shows you how to enable automated releases of your GitLab code using
the [release-it](https://github.com/release-it/release-it) tool via our
[`release-it.yml`](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/release-it.yml)
GitLab template. For detailed information on how this process works, see the
[GitLab Release Automation](site:explanations/git-gitlab/gitlab-release-automation) page
in the [Learn](site:tutorials) section.

## Prerequisites

This template is designed to work with our standard products. This means that:

- The GitLab project that you wish to use the process with must have been
deployed via the
[gitlab-project-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory)
Terraform configuration.
- A product-specific Google Kubernetes Engine (GKE) runner must have been
deployed via the
[gitlab-runner-infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
Terraform configuration.

## Configuration

To configure the release-it tool and enable the `release-it.yml` GitLab template ensure that your
repository contains a `.release-it.json` config file. See
the [release-it docs](https://github.com/release-it/release-it/blob/main/docs/configuration.md)
for information on the options available in this file. The following is a good starting point for
most of our projects.

```json title=".release-it.json"
{
  "git": {
    "commitMessage": "chore(release): ${version}"
  },
  "gitlab": {
    "release": true,
    "releaseName": "${version}"
  },
  "plugins": {
    "@release-it/conventional-changelog": {
      "infile": "CHANGELOG.md",
      "header": "# Changelog",
      "preset": {
        "name": "conventionalcommits"
      }
    },
    "@release-it/bumper": {
      "out": {
        "file": "pyproject.toml",
        "type": "text/toml",
        "path": "tool.poetry.version"
      }
    }
  }
}
```

!!! warning
    The release it CI jobs are added as part of the [common
    pipeline](site:howtos/python/add-common-pipeline-to-python) but can be added to
    projects not using the common pipeline by adding an appropriate `include` statement
    to your CI config which references [the
    template](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/release-it.yml?ref_type=heads):

    ```yaml
    include:
      - project: "uis/devops/continuous-delivery/ci-templates"
        file: "/auto-devops/release-it.yml"
        ref: {REPLACE WITH MOST RECENT VERSION TAG}
    ```

## See also

For further information on how this process works see the following.

- [GitLab release automation](site:explanations/git-gitlab/gitlab-release-automation)
explanation.
- [Automating GitLab releases](site:tutorials/automating-gitlab-releases)
  tutorial.
