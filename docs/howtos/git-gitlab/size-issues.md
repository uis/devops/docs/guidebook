---
title: Size issues
---

# How to size a GitLab issue

We use issues to track individual units of work. We use the University's GitLab instance at
<https://gitlab.developers.cam.ac.uk/> to manage them. In sprint refinement meetings we
add time estimates to issues to aid with sprint planning.

This guide covers how to size an issue in GitLab.

## Estimating work required

Start by estimating the time it would take someone knowledgable to address the issue and perform any
required review. A knowledgable person is very familiar with:

- the product,
- the task specified in the issue, and
- the technology stack used.

Estimate the time taken in days. Use whole-numbers of days for estimates with the exception of
allowing a half-day for straightforward tasks.

!!! tip

    **Do not** try to estimate how long it would take _you specifically_ to do the task. Focus on
    how long you would expect a knowledgable person on your team to take.

Estimating this time can be hard to do if you are unfamiliar with some aspects of the issue. In
sprint refinement you should be able to get a feel for the work required for an issue as the team
discusses the steps involved. You do not need to fully understand all of the technical details to be
able to estimate work but you should feel empowered to ask for clarification or explanation during
sprint refinement.

**It is always OK to say "I don't know" when sizing if you feel you do not have the knowledge
required to judge the work required.**

## Estimating complexity

Now you need to estimate _complexity_. We measure complexity in dimensionless units. Our rule of
thumb is as follows:

- "I know how to do this" is a complexity of **1**.
- "Someone else on the team knows how to do this" is a complexity of **2**.
- "Someone in the world who I can ask knows how to do this" is a complexity of **3**.
- "No-one knows how to do this" is a complexity of **4**.

Unlike estimating the work involved you _should_ consider your own experience and knowledge when
estimating complexity. It is common for well-specified issues to have a complexity of 1 or 2.
Occasionally issues will have a complexity of 3. Issues with a complexity of 4 usually need to be
broken down into smaller tasks.

## The final size

Report your final size as the product of work required and complexity.

Some examples:

- If you think that a particular issue will take 1 day to complete by someone
  knowledgable but someone else on your team is the knowledgable person, the final size is 1 &times;
  2 = 2.
- Suppose you know that the work required for an issue is editing a line in a
  configuration file. This would be a small amount of work, perhaps 0.5 days. However, no-one on
  your team knows where this file is or how to edit it but they _do_ know that someone from a
  different team _does_ know. In this case you would estimate a complexity of 3 and the final size is
  0.5 &times; 3 = 1.5. Since, aside from 0.5, we use whole-number sizes this is rounded up to a size
  of 2.

Agree a final size with your team and record it in the issue as part of sprint refinement.

## Open-ended issues

Some issues represent work which could be open-ended. Examples of these include "investigate
technology X" or "tidy-up code Y". For these issues, the team may decide a number of days work in
advance and stop work on the issue after that time has passed. These issues are known as "spikes"
and usually get the {{ gitlab_label("spike") }} label.

## Summary

In this guide, you learned how we size issues.

## Next steps

- Learn about [writing good issues](site:explanations/git-gitlab/good-issues).
- See how we use [GitLab labels on issues](site:reference/git-gitlab/gitlab-labels).
