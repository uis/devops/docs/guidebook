# Sign in

GitLab is the tool we use to host our code, automate processes, plan work and
track its completion. It is designed to offer tooling for the entire DevOps
lifecycle.

This how-to covers signing in to GitLab for the first time.

## Getting an account

Any University member with a Raven account can sign in to GitLab. You should
have been given a Raven account when you joined the University. University
Information Services has a [dedicated help page for
new-starters](https://help.uis.cam.ac.uk/new-starters) which covers this.

## Signing in

To sign in to GitLab:

1. Visit [the GitLab sign in
   page](https://gitlab.developers.cam.ac.uk/users/sign_in) in your browser.
2. Click the **Raven** button, highlighted below in red.
3. Sign in using your Raven credentials.
4. Fill out your [user profile](https://gitlab.developers.cam.ac.uk/-/profile).

<figure markdown>
   ![Highlighted Raven sign in button](./gitlab-sign-in.png)
   <figcaption>Raven button used to sign in to GitLab.</figcaption>
</figure>

## Summary

In this how-to, you learned how to sign in to GitLab using GitLab.

## Next steps

* Try [opening an issue](site:howtos/git-gitlab/open-an-issue).
