---
title: Create a feature branch
---

# How to create a branch for a new feature

This page documents how to create a new branch for when you are working on a feature which closes an
issue.

Feature branches are branches being used to develop an individual story. They should be named
"{issue-number}-{summary}" where "{issue-number}" is the issue number corresponding to the feature
you are implementing and "{summary}" is a brief summary of the feature `formatted-like-this`.

To create a new branch, make sure that you are on the most recent `main` revision:

```console
git checkout main
git pull
```

!!! warning "error: pathspec 'main' did not match any file(s) known to git"

    Recent software forges like GitLab have changed the default branch name to "main". Some of our
    projects use the old default: "master". If you get an error like the above, try using
    `git checkout master` instead.

Now supposing that you are, for example, working on issue 123 which adds a copyright banner:

```sh
git checkout -b 123-add-copyright-banner
git push -u origin 123-add-copyright-banner
```

The `git push ...` command will create an branch on GitLab for you to work in and will mark the
local branch as being a local version of that branch. Subsequently one can just use `git push` to
push changes.

## Next steps

* Learn more about [git concepts](site:explanations/git-gitlab/git).
* See a list of [recommended external git documentation](site:reference/git-gitlab/git-docs).
