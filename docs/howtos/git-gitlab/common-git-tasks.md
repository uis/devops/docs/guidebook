---
title: Common git actions
---

# How to perform some common git actions

This page tells you how to perform some common git actions.

The following actions may frequently be required while working with git. Simple cases are provided
here for convenience but the [git documentation](https://git-scm.com/doc) or knowledgeable
co-workers should be consulted in complex situations.

If you're worried about losing changes while performing any of the following then you can make a
backup of a branch beforehand.

If you are on the branch to be backed up then use, for example, to create **my-backup** branch:

```bash
git branch my-backup
```

Or, to copy another branch, for example, **my-feature** branch to **my-backup**:

```bash
git branch -c my-feature my-backup
```

In both cases, you'll remain on your current branch.

## Forgot something for last commit?

You've discovered that you have a file or files that should have been added to the last commit you
made. Simply stage the forgotten file(s) then recommit using the `--amend` argument.

```bash
git add path/forgotten_file.py
git commit --amend
```

_You'll also have opportunity to change the commit message, if so desired._

## Undo last commit

You didn't mean to make that last commit and want to undo the commit but keep the changes you made.

```bash
git reset --soft HEAD~1
```

_Note: `HEAD~1` (equivalent to `HEAD~`) references 1 commit before current HEAD. By specify another
number you can undo more than one commit._

Alternatively, if you really want to undo the last commit forgetting any changes made, as if it
never happened, then switch the `--soft` for `--hard`. **Warning: files and/or changes to files will
be lost.**

```bash
git reset --hard HEAD~1
```

## Edit previous commit message

You've a typo or another mistake in the message of a previous (not necessarily the latest) commit?

Firstly, you will need to work out using `git log` how many commits you need to go back and use that
number as part a `HEAD~n` reference.

For example, three commits back:

```bash
git rebase -i HEAD~3
```

This will show a list of the previous commits, each tagged with `pick`.

```txt
pick f4b35d1 Initial Implementation
pick 130564e Created READEM (typo)
pick f4a5a0f Added RESTful API

# Rebase 64fa23f..f4a5a0f onto 64fa23f
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
...
```

For the commit(s) that you want to change the message for, change the `pick` to `reword` (or just
`r`) and save. You will then be prompted to change the commit message(s).

## Combining commits in to one

You've got some commits that you want to combine together, typically to fix up later changes into
the relevant earlier commit.

As above, use `git log` to work out how many commits to reference with `HEAD~n` then issue the
rebase command. For example:

```bash
git rebase -i HEAD~4
```

Firstly, if the fix up commit is not immediately following the commit it is to be combined with then
reorder the commits.

> The following step can be done at the same time as reordering but it is often easier to finish
> this rebase, check the ordering and then rebase again to do the actual combining. There is a
> potential for merge conflicts during the rebase, see "Merge conflicts during rebase" below.

Now change the `pick` before the fix up commit to either `fixup` (or `f`) to have it combine with
the previous commit and discard the fix up commit's message, or `squash` (or `s`) to be prompted for
a revised commit message for the resulting combined commit.

Use `git log` to check that all went well. If you previously pushed to a remote repository then
you'll need to `git push --force` to upload these changes.

## Merge conflicts during rebase

Sometimes during a rebase (especially if reordering commits) you may get merge conflicts. You will
need to resolve these conflicts, stage the conflicting files and then continue the rebase.

```bash
edit path/conflicting_file.py
git add path/conflicting_file.py
git rebase --continue
```

But don't worry! If you get in a mess you can always reset to the state before the rebase by
aborting it.

```bash
git rebase --abort
```

## Merge Request has merge conflicts with master

You've finished your changes on your branch and created a merge request (MR) but the `master` branch
has changed since you originally created your branch, and Gitlab is reporting that merge conflicts
need resolving before your MR can be merged.

You could `git merge origin/master` in to your branch but this would lead to an extra merge commit
being added to the history. An arguably cleaner way is to rebase your branch on to the current
master.

For example, rebase feature branch `my-new-feature` on to `master`:

```bash
git fetch
git checkout my-new-feature
git rebase master
git push --force
```

_There is a potential for merge conflicts during the rebase, see "Merge conflicts during rebase"
above._

## Next steps

* Learn more about [git concepts](site:explanations/git-gitlab/git).
* See a list of [recommended external git documentation](site:reference/git-gitlab/git-docs).
