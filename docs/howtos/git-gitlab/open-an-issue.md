---
title: Open an issue
---

# How to open an issue

We use issues to track individual units of work. We use the University's GitLab
instance at <https://gitlab.developers.cam.ac.uk/> to manage them. If you've not
yet signed in to GitLab, see the [signing in to GitLab
how-to](site:howtos/git-gitlab/sign-in-to-gitlab).

This how-to covers creating issues and their structure.

## Finding the correct project

The first step is finding the correct project in GitLab in which to open an
issue. These *should* be linked from the appropriate [service
page](site:services). If not, the top-level view of [DevOps GitLab
groups](https://gitlab.developers.cam.ac.uk/uis/devops) can be used to find an
appropriate project.

!!! warning
    Some projects are visible to DevOps members only. If you can't find a
    project which you think should exist and you're not a member of DevOps
    please follow the instructions on the [contact us
    page](site:home/contact).

## Search for existing issues

It's good practice to search for issues first if you are reporting a bug or
requesting a feature as an issue may already have been opened. The **Search
GitLab** box at the top-left of all GitLab pages can be used for this.

## Create a new issue

To create a new issue:

1. Click on **Issues** in the project sidebar.
2. Click on the **New Issue** button at the top-right of the issue list.
3. Use the **Title** box to give the issue a useful title. This should
   summarise the work which needs to be completed or the bug which needs to be
   addressed. Try to make it concise and search friendly.
4. Choose an appropriate template from the **Description** drop-down.
5. Fill in the template in the **Description** text box. Please try to be as
   complete as possible.
6. Click **Create issue**.
7. Add a `team::...` label to an issue if you know which DevOps team should
   pick it up.
8. Add an `issueType::...` label if the template did not do so already.
9. Consider adding special-use labels such as {{ gitlab_label("chore") }} or {{
   gitlab_label("good first issue") }}. See the [labels
   reference](site:reference/git-gitlab/gitlab-labels) for possible labels.

## Summary

In this how-to, you learned how to locate a project in GitLab and create an
issue within it.

## Next steps

* Learn about [writing good issues](site:explanations/git-gitlab/good-issues).
* See how we use [GitLab labels on issues](site:reference/git-gitlab/gitlab-labels).
