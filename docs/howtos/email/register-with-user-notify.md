---
title: Register your service
---

In DevOps we have an internal [User Notify service](site:libraries-tools/user-notify)
which can be used to send email to users of your service. This guide covers how to register
your service to send email.

The service currently supports as a "paved path":

- Sending email from an email address of the form `{service name}@notify.apps.cam.ac.uk` or "plus
  addressed" variants.
- Getting per-service feeds of send, delivery, bounce and complaint events via Google BigQuery, a
  connected Google Sheet or as a data source in a Looker Studio report.
- Low configuration integration with Django and other Python codebases via an [opinionated client
  library](https://uis.uniofcam.dev/devops/user-notify/ucam-user-notify/).
- [SPF][], [DKIM][] and [DMARC][] verification of all outbound emails.

[SPF]: https://en.wikipedia.org/wiki/Sender_Policy_Framework
[DKIM]: https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail
[DMARC]: https://en.wikipedia.org/wiki/DMARC

In addition to the above, you can opt to restrict your service to send to a fixed set of recipients
or to use a dedicated "testing" domain. These features are useful for non-production services which
still wish to send real email.

For advanced use cases we _can_ support sending via SMTP, custom domains, dedicated IP address pools
with automated warm-up, custom Reply-To addresses and forwarding any bounce emails to a service
management team but you will bear a greater responsibility for correctly configuring your service.
Please [open an
issue](https://gitlab.developers.cam.ac.uk/uis/devops/user-notify/infrastructure/-/issues/new) to
discuss your use case.

If the Python client library doesn't match your needs, you can create your own integration but no
guarantees are given as to the stability of the internal API.

## What you will need

For each service you wish to register, you will need:

- **A service "id"** which is a short identifier for your service which uses only lower case
  letters, numbers and/or "-"s.
- **A display name** which will be visible in the "From" address for emails sent from your service.
- **A local part** which will be the part before the `@` in the "From" address of emails sent from
  your service.
- **At least one technical contact** which is an email address for contact with technical
  information. At a later date the User Notify service may also start using these email addresses to
  send alerts about, for example, elevated bounce rates.
- **At least one Google Service Account** which is permitted to send email. Usually this would be
  the service account corresponding to, for example, the Cloud Run service hosting your service.

If your service corresponds to a non-production environment you may find it useful to obtain:

- **An allow list of recipient email addresses**. Your service will be restricted to
  sending email only to these addresses.
- **A decision on whether you want to use the "testing" domain.** Along with
  `notify.apps.cam.ac.uk`, there is a "testing" domain
  `email-testing.prod.user-notify.gcp.uis.cam.ac.uk` which may be useful to clearly differentiate
  email sent from a non-production environment.

To use advanced features, you will need:

- **Reporting IAM principals** which are principals such as Lookup groups, Google groups or, as a
  last resort, individual `@cam.ac.uk` accounts which will be able to create Google Sheets or Looker
  Studio reports containing send, delivery, bounce and complaint events. (Note that, depending on
  how you share them, users may not need to be in this list to _view_ Google Sheets or Looker Studio
  reports.)
- **Technical IAM principals** which are principals such as Lookup groups or Google groups which
  will be able to read technical configuration information. These should ideally be Google groups
  containing [gcloudadmin accounts](site:howtos/cloud-platform/create-gcloudadmin-account)
  and are intended for complex use cases where automated configuration provision does not suffice.

## Requesting registration

Once you have the required information you can request that your service be registered.

!!! question "Should I register separate services for each environment?"

    You may be wondering if you should register separate services for development, staging and
    production environments. That is up to you since the "best" answer likely depends on the
    intended roles of your different environments.

    *Very* broadly speaking, only your production and, perhaps, staging environments should be able
    to send "real" email and for other environments you should instead log the emails which _would_
    be sent.  We have a [mailtrap account](site:reference/best-practice/ourtools#mailtrap)
    which can be used for an "inbox-like" view of email messages which would be sent.
    The opinionated Python client library also includes a `MockSession` class
    if you don't need mailtrap's features and Django includes an in-memory email backend.

    The recommended setup if you are unsure what to do is to register a single service corresponding
    to your production environment and, if necessary, register a second service using the "testing"
    domain with a restricted recipient list. This second service can then be used for integration
    testing with User Notify in your staging and/or development environments. Once you are happy
    with the integration, you should keep development and staging workloads using mailtrap to ensure
    that they can never send "real" email.

If you feel confident the fastest way to register a service is to open a MR on the [User Notify
infrastructure project](https://gitlab.developers.cam.ac.uk/uis/devops/user-notify/infrastructure/)
to add a service description file which matches the [service
schema](https://gitlab.developers.cam.ac.uk/uis/devops/user-notify/infrastructure/).

If you prefer that the User Notify team write the description for you, [open an issue with the "new
service"
template](https://gitlab.developers.cam.ac.uk/uis/devops/user-notify/infrastructure/-/issues/new?issuable_template=new-service)
and fill in the blanks.

## Next steps

Once your service is registered, you are ready to send email. The following pages provide further
guidance.

- [Sending email from Django projects](./send-email-from-django.md)
- [Sending email from non-Django Python projects](./send-email-from-python.md)
- [Configuring event reporting](./receive-email-events.md)
