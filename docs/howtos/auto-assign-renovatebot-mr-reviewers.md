---
title: Automatically assign reviewers
---
# How to automatically assign reviewers to renovatebot MRs

Renovatebot can auto-assign reviewers to Merge Requests that it opens. It does this using the
[CODEOWNERS file](https://docs.gitlab.com/ee/user/project/codeowners/) in the repository root.

!!! important

    Renovatebot only has limited support for `CODEOWNERS` syntax. In particular it doesn't support
    GitLab groups or sections.

## Create the `CODEOWNERS` file

If you want the users `abc123` and `xyz789` to be auto-assigned to MR reviews, add the following
line to your `CODEOWNERS` file:

```txt title="CODEOWNERS"
* @abc123 @xyz789
```

## Summary

In this guide you learned how to cause renovatebot to auto-assign reviewers and the limitations in
renovatebot's current support for `CODEOWNERS`.
