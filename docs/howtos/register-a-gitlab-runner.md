---
title: Registering a Gitlab Runner
---

In GitLab 16.0, a new runner creation workflow was introduced. The new
workflow uses runner authentication tokens to register runners. The
legacy workflow that uses registration tokens is deprecated and will be
removed in GitLab 18.0. The new runner registration workflow has the
following benefits:

- Preserved ownership records for runners and minimized impact on
  users.

- The addition of a unique system ID ensures that you can reuse the
  same authentication token across multiple runners. For more
  information, see [Reusing a GitLab Runner
  configuration](https://docs.gitlab.com/runner/fleet_scaling/#reusing-a-gitlab-runner-configuration).

For our [GitLab instance](https://gitlab.developers.cam.ac.uk/), which
is now on GitLab 17.x, the legacy workflow is enabled for compatibility
reasons, but we will be forced to use a new workflow starting with
GitLab 18. Gitlab 18.0 is due to be [released](https://about.gitlab.com/upcoming-releases/) in May
2025. Runners registering after this date must use the new registration workflow.

## Migrating to the new workflow

The main difference between legacy and new workflows is that the legacy
one uses **runner registration token**, and the new workflow uses
**runner authentication token.** Besides that, certain configuration
arguments were
[deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/380872) in
GitLab 15.6 and will be removed in GitLab 18.0.

According to the [official migration
guide](https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html),
the new flow is as shown:

- [Create a
  runner](https://docs.gitlab.com/ee/ci/runners/runners_scope.html)
  directly in the GitLab UI or
  [programmatically](https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#creating-runners-programmatically).

![New Project Runner](new-gitlab-runner.png)

Runner registration dialogue (example for project runner)

- Receive a runner authentication token.

- Use the runner authentication token instead of the registration token when
  you register a runner with this configuration.

Another important change was made in gitlab-runner register [command
syntax](https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#changes-to-the-gitlab-runner-register-command-syntax).
The gitlab-runner register command will stop accepting registration
tokens and instead accept new runner authentication tokens generated in
the GitLab runners administration page. The runner authentication tokens
are recognizable by their **glrt-** prefix.

When you create a runner in the GitLab UI or over API, you specify
configuration values that were previously command-line options. These
command-line options have been
[deprecated](https://docs.gitlab.com/ee/update/deprecations.html#registration-tokens-and-server-side-runner-arguments-in-post-apiv4runners-endpoint).
As a side effect of this, there might be a situation where runner is
registered but the behaviour is changed (for example, runner starts to
pick up the untagged jobs or tags are missing). That is why it is
important to remember that **now the configuration must be done before
the registration, CLI options (as well as Helm values) will be
ignored.**

Example of an old syntax:

```console
gitlab-runner register \
    --non-interactive \
    --executor "shell" \
    --url "https://gitlab.developers.cam.ac.uk/" \
    --tag-list "shell,mac,gdk,test" \
    --run-untagged "false" \
    --locked "false" \
    --access-level "not_protected" \
    --registration-token "REDACTED"
```

Example of a new syntax:

```console
gitlab-runner register \
    --non-interactive \
    --executor "shell" \
    --url " https://gitlab.developers.cam.ac.uk/" \
    --token "REDACTED"
```

## Changes in GitLab Runner Helm Chart

The chart has also been updated to reflect changes in the runner
registration workflow.

If you use Kubernetes-based runners, deployed with the official [helm
chart](https://docs.gitlab.com/runner/install/kubernetes.html), it is
necessary to update the options in the
[values.yaml](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml)
file. For example, options "`runUntagged`", "`tags`", "`locked`" and more are
deprecated and will be deleted soon. All related configurations are now
being made on GitLab side (manually or programmatically) during the
runner creation process. Once runner is created and a token is acquired,
it must be passed to the helm as a value for option "`runnerToken`".
