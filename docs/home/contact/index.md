# Contacts

Primary contact should **almost always** be via the [UIS Service Desk][mailto-servicedesk].

- You can send us GPG encrypted files or email by using our [GPG public keys](./public-keys.md).

<!-- markdownlint-disable MD013 -->
| **Action**                | What it achieves                                      | **How to do it**                                                                                   |
|---------------------------|-------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| **Log an Incident**    | Report a bug, issue or outage.                        | - Email [servicedesk@uis.cam.ac.uk][mailto-servicedesk]<br>- Specify the affected service & issue. |
| **Escalate**           | Increase severity of an existing incident.            | - Send follow-up to email thread created above<br>- Otherwise, email that same address.            |
| **Notify ServiceDesk** | Advise ServiceDesk of ongoing or planned disruptions. | - Email [servicedeskmanager@uis.cam.ac.uk][mailto-servicedesk-manager]                             |
<!-- markdownlint-enable MD013 -->

## Per Service Escalation

!!! warning "Section for UIS ServiceDesk Only"

    - This section is intended for use by UIS ServiceDesk for escalations.
    - Please do not bypass the standard process by emailing teams directly.

<!-- markdownlint-disable MD013 -->
| **Service**                   | **Support Route** | **Contact**                                                  |
|-------------------------------|-------------------|--------------------------------------------------------------|
| **Cloud Services**            | Support Requests  | [cloudsupport@uis.cam.ac.uk][mailto-cloud-support]           |
| _(Amazon, Google, Azure etc)_ | Notify CC Address | [cloud@uis.cam.ac.uk][mailto-cloud]                          |
| **Git Legacy Service**        | Contact           | [gitmaster@uis.cam.ac.uk][mailto-git-legacy]                 |
| **GitLab**                    | Support Requests  | [Create an Issue][gitlab-issues]                             |
|                               | Operations Team   | [gitlab-team@developers.cam.ac.uk][mailto-gitlab-operations] |
| **GSuite@Cambridge**          | Support Requests  | [servicedesk@uis.cam.ac.uk][mailto-servicedesk]              |
|                               | Notify CC Address | [gapps-admin@uis.cam.ac.uk][mailto-gapps-admin]              |
| **Lookup**                    | Support Requests  | [servicedesk@uis.cam.ac.uk][mailto-servicedesk]              |
|                               | Technical Issues  | [servicedesk@uis.cam.ac.uk][mailto-servicedesk]              |
| **Raven**                     | Support Requests  | [raven-support@uis.cam.ac.uk][mailto-raven-support]          |
|                               | Emergency Contact | [raven-admin@uis.cam.ac.uk][mailto-raven-admin]               |
<!-- markdownlint-enable MD013 -->

<!-- markdownlint-disable MD013 -->
[gitlab-issues]: https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/issues
[mailto-cloud]: mailto:cloud@uis.cam.ac.uk
[mailto-cloud-support]: mailto:cloudsupport@uis.cam.ac.uk
[mailto-gapps-admin]: mailto:gapps-admin@uis.cam.ac.uk
[mailto-gitlab-operations]: mailto:gitlab-team@developers.cam.ac.uk
[mailto-git-legacy]: mailto:gitmaster@uis.cam.ac.uk
[mailto-raven-support]: mailto:raven-support@uis.cam.ac.uk
[mailto-raven-admin]: mailto:raven-admin@uis.cam.ac.uk
[mailto-servicedesk]: mailto:servicedesk@uis.cam.ac.uk
[mailto-servicedesk-manager]: mailto:servicedeskmanager@uis.cam.ac.uk
<!-- markdownlint-enable MD013 -->
