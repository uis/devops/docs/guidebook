# Knowledge Base

Some of the challenges we face as Software & Tech experts within DevOps include:

- Work of a highly complex & technical nature.
- Large volumes of project-specific information.
- A rapidly evolving technological landscape.

To help counter these,
we curate & maintain a well-organised, readily accessible shared knowledge base.

This helps to lower complexity, improve accessibility, and alleviate cognitive load
across numerous processes & procedures.

Following the [Diataxis compass](https://diataxis.fr/),
this Guidebook defines site areas for **4 Knowledge Quadrants (KQs).**

Click a KQ title below, or the equivalent navigation tab above,
to explore that quadrant and the guides it contains.

---

<h2 style="text-align: center;">Knowledge Quadrants</h2>

[cards cols=2 smaller(docs/cards/diataxis/cards.yml)]
