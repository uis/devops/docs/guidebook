# GCP & Terraform

Our adopted & recommended tools & services for Google Cloud Platform (GCP) and Terraform.

## GCP Tools & Services

<!-- markdownlint-disable MD013 -->
| Title                                                           | Summary                                                  | Our Docs                                                                                                          |
|-----------------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| [Cloud Run](https://cloud.google.com/run)                       | Serverless container platform for stateless applications | [Reference](site:reference/cloud-platform), [Migration](site:explanations/cloud-platform/gcp-cloud-run-app-module) |
| [Cloud SQL](https://cloud.google.com/sql)                       | Fully managed relational database service                | [Backups](site:reference/cloud-platform/backups)                                                              |
| [Cloud Storage](https://cloud.google.com/storage)               | Object storage service                                   | [Reference](site:reference/cloud-platform)                                                              |
| [Cloud Load Balancing](https://cloud.google.com/load-balancing) | Scalable load distribution service                       | [Architecture](site:standards/standard-architecture-diagrams)                                  |
| [Cloud DNS](https://cloud.google.com/dns)                       | Managed DNS service                                      | [DNS Guide](site:reference/cloud-platform/dns)                                                                |
| [Secret Manager](https://cloud.google.com/secret-manager)       | Secure secret storage service                            | [CI Access](site:howtos/gke-gitlab-runners/access-secrets-in-ci-jobs-using-impersonation)                     |
| [Cloud Monitoring](https://cloud.google.com/monitoring)         | Infrastructure and application monitoring service        | [Reference](site:reference/cloud-platform)                                                              |
| [Cloud Logging](https://cloud.google.com/logging)               | Centralized log management service                       | [Reference](site:reference/cloud-platform)                                                              |
| [gcloud CLI](https://cloud.google.com/sdk/gcloud)               | Command-line interface for GCP services                  | [System Setup](site:howtos/development/prepare-your-system)                                                               |
<!-- markdownlint-enable MD013 -->

## Terraform & Infrastructure Tools

<!-- markdownlint-disable MD013 -->
| Title                                                                                          | Summary                                      | Our Docs                                                     |
|------------------------------------------------------------------------------------------------|----------------------------------------------|--------------------------------------------------------------|
| [Terraform](https://www.terraform.io/)                                                         | Infrastructure as Code tool                  | [Reference](site:reference/cloud-platform)         |
| [terraform-provider-google](https://registry.terraform.io/providers/hashicorp/google/latest)   | Google Cloud Platform provider for Terraform | [Reference](site:reference/cloud-platform)         |
| [terraform-provider-aws](https://registry.terraform.io/providers/hashicorp/aws/latest)         | Amazon Web Services provider for Terraform   | [AWS Guide](site:explanations/cloud-platform/aws)                       |
| [terraform-provider-azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest) | Microsoft Azure provider for Terraform       | [Azure Guide](site:explanations/cloud-platform/azure-and-entra-id-apps) |
| [tflint](https://github.com/terraform-linters/tflint)                                          | Terraform linter                             | [Reference](site:reference/cloud-platform)         |
<!-- markdownlint-enable MD013 -->
