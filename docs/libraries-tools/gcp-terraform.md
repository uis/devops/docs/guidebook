# GCP & Terraform

Our adopted & recommended tools & services for Google Cloud Platform (GCP) and Terraform.

## GCP Tools & Services

<!-- markdownlint-disable MD013 -->
| Title                                                           | Summary                                                  | Our Docs                                                                                                          |
|-----------------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| [Cloud Run](https://cloud.google.com/run)                       | Serverless container platform for stateless applications | [Reference](../reference/cloud-platform/index.md), [Migration](../explanations/gcp-cloud-run-app-module.md) |
| [Cloud SQL](https://cloud.google.com/sql)                       | Fully managed relational database service                | [Backups](../reference/cloud-platform/backups.md)                                                              |
| [Cloud Storage](https://cloud.google.com/storage)               | Object storage service                                   | [Reference](../reference/cloud-platform/index.md)                                                              |
| [Cloud Load Balancing](https://cloud.google.com/load-balancing) | Scalable load distribution service                       | [Architecture](../standards-and-compliance/standard-architecture-diagrams.md)                                  |
| [Cloud DNS](https://cloud.google.com/dns)                       | Managed DNS service                                      | [DNS Guide](../reference/cloud-platform/dns.md)                                                                |
| [Secret Manager](https://cloud.google.com/secret-manager)       | Secure secret storage service                            | [CI Access](../howtos/gke-gitlab-runners/access-secrets-in-ci-jobs-using-impersonation.md)                     |
| [Cloud Monitoring](https://cloud.google.com/monitoring)         | Infrastructure and application monitoring service        | [Reference](../reference/cloud-platform/index.md)                                                              |
| [Cloud Logging](https://cloud.google.com/logging)               | Centralized log management service                       | [Reference](../reference/cloud-platform/index.md)                                                              |
| [gcloud CLI](https://cloud.google.com/sdk/gcloud)               | Command-line interface for GCP services                  | [System Setup](../howtos/prepare-your-system.md)                                                               |
<!-- markdownlint-enable MD013 -->

## Terraform & Infrastructure Tools

<!-- markdownlint-disable MD013 -->
| Title                                                                                          | Summary                                      | Our Docs                                                     |
|------------------------------------------------------------------------------------------------|----------------------------------------------|--------------------------------------------------------------|
| [Terraform](https://www.terraform.io/)                                                         | Infrastructure as Code tool                  | [Reference](../reference/cloud-platform/index.md)         |
| [terraform-provider-google](https://registry.terraform.io/providers/hashicorp/google/latest)   | Google Cloud Platform provider for Terraform | [Reference](../reference/cloud-platform/index.md)         |
| [terraform-provider-aws](https://registry.terraform.io/providers/hashicorp/aws/latest)         | Amazon Web Services provider for Terraform   | [AWS Guide](../explanations/aws.md)                       |
| [terraform-provider-azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest) | Microsoft Azure provider for Terraform       | [Azure Guide](../explanations/azure-and-entra-id-apps.md) |
| [tflint](https://github.com/terraform-linters/tflint)                                          | Terraform linter                             | [Reference](../reference/cloud-platform/index.md)         |
<!-- markdownlint-enable MD013 -->
