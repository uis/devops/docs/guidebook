# Python

Our adopted & recommended Python libraries, snippets & tools.

## Libraries

<!-- markdownlint-disable MD013 -->
| Title                                                                           | Summary                                                 | Our Docs                                                                                                                  |
|---------------------------------------------------------------------------------|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| [black](https://github.com/psf/black)                                           | Code formatter that enforces a consistent style         | [Setup](../howtos/setup-pre-commit-hooks.md), [Tutorial](../tutorials/creating-a-python-package.md)                 |
| [coverage](https://coverage.readthedocs.io/)                                    | Tool for measuring code coverage of Python programs     | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [docopt](http://docopt.org/)                                                    | Command-line interface description language and parser  | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [faker](https://faker.readthedocs.io/)                                          | Library for generating fake data for testing            | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [flake8](https://flake8.pycqa.org/)                                             | Tool for style guide enforcement and linting            | [Setup](../howtos/setup-pre-commit-hooks.md), [Tutorial](../tutorials/creating-a-python-package.md)                 |
| [furo](https://pradyunsg.me/furo/)                                              | Clean customizable Sphinx documentation theme           | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [geddit](https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit)             | UIS library for fetching data from URLs                 | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [isort](https://pycqa.github.io/isort/)                                         | Library for sorting Python imports                      | [Setup](../howtos/setup-pre-commit-hooks.md), [Tutorial](../tutorials/creating-a-python-package.md)                 |
| [jsonpath-ng](https://github.com/h2non/jsonpath-ng)                             | Library for extracting data using JSONPath expressions  | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [mypy](https://mypy.readthedocs.io/)                                            | Static type checker for Python                          | [Setup](../howtos/setup-pre-commit-hooks.md), [Tutorial](../tutorials/creating-a-python-package.md)                 |
| [pre-commit](https://pre-commit.com/)                                           | Framework for managing git pre-commit hooks             | [Setup](../howtos/setup-pre-commit-hooks.md), [Tutorial](../tutorials/creating-a-python-package.md)                 |
| [PyYAML](https://pyyaml.org/)                                                   | YAML parser and emitter for Python                      | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [pytest](https://docs.pytest.org/)                                              | Testing framework                                       | [Tutorial](../tutorials/creating-a-python-package.md), [Boilerplate](../explanations/webapp-boilerplate.md#testing) |
| [pytest-cov](https://pytest-cov.readthedocs.io/)                                | Coverage plugin for pytest                              | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [pytest-mock](https://pytest-mock.readthedocs.io/)                              | Thin-wrapper around mock package for pytest             | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [python-social-auth](https://python-social-auth.readthedocs.io/)                | Authentication framework for Python web frameworks      | [SSO Guide](../howtos/configure-django-for-sso.md)                                                                     |
| [sphinx](https://www.sphinx-doc.org/)                                           | Documentation generator                                 | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [sphinx-autodoc-typehints](https://github.com/tox-dev/sphinx-autodoc-typehints) | Type hints support for Sphinx                           | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [structlog](https://www.structlog.org/)                                         | Structured logging for Python                           | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
| [tox](https://tox.wiki/)                                                        | Test automation and virtual environment management tool | [Tutorial](../tutorials/creating-a-python-package.md)                                                                  |
<!-- markdownlint-enable MD013 -->

## Snippets

<!-- markdownlint-disable MD013 -->
| Type       | Snippet                                                              | Summary                                   |
|------------|----------------------------------------------------------------------|-------------------------------------------|
| decorator  | [decorators](https://gitlab.developers.cam.ac.uk/-/snippets/340)     | neat multiple decorators in a single line |
| decorator  | [threadsafe](https://gitlab.developers.cam.ac.uk/-/snippets/341)     | make a method or property thread-safe     |
| datastruct | [dotdict](https://gitlab.developers.cam.ac.uk/-/snippets/343)        | dot-accessible dict                       |
| datastruct | [threadsafedict](https://gitlab.developers.cam.ac.uk/-/snippets/342) | exemplifies how to use Generic typing     |
<!-- markdownlint-enable MD013 -->

## Tools

<!-- markdownlint-disable MD013 -->
| Title                                    | Summary                                                              | Our Docs                                                                                                                    |
|------------------------------------------|----------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| [copier](https://copier.readthedocs.io/) | Tool for rendering project templates                                 | [Create](../howtos/copier-create.md), [Update](../howtos/copier-update.md), [Migrate](../howtos/copier-migrate.md) |
| [pip](https://pip.pypa.io/)              | Package installer for Python                                         | [System Setup](../howtos/prepare-your-system.md)                                                                         |
| [pipx](https://pypa.github.io/pipx/)     | Tool to install and run Python applications in isolated environments | [System Setup](../howtos/prepare-your-system.md)                                                                         |
| [poetry](https://python-poetry.org/)     | Python dependency management and packaging tool                      | [Tutorial](../tutorials/creating-a-python-package.md), [Boilerplate](../explanations/webapp-boilerplate.md#poetry)    |
| [poe](https://poethepoet.natn.io/)       | Task runner that works well with poetry                              | [Tutorial](../tutorials/creating-a-python-package.md), [Boilerplate](../explanations/webapp-boilerplate.md#poe)       |
<!-- markdownlint-enable MD013 -->
