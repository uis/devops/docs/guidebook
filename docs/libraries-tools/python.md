# Python

Our adopted & recommended Python libraries, snippets & tools.

## Libraries

<!-- markdownlint-disable MD013 -->
| Title                                                                           | Summary                                                 | Our Docs                                                                                                                  |
|---------------------------------------------------------------------------------|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| [black](https://github.com/psf/black)                                           | Code formatter that enforces a consistent style         | [Setup](site:howtos/development/setup-pre-commit-hooks), [Tutorial](site:tutorials/creating-a-python-package)                 |
| [coverage](https://coverage.readthedocs.io/)                                    | Tool for measuring code coverage of Python programs     | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [docopt](http://docopt.org/)                                                    | Command-line interface description language and parser  | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [faker](https://faker.readthedocs.io/)                                          | Library for generating fake data for testing            | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [flake8](https://flake8.pycqa.org/)                                             | Tool for style guide enforcement and linting            | [Setup](site:howtos/development/setup-pre-commit-hooks), [Tutorial](site:tutorials/creating-a-python-package)                 |
| [furo](https://pradyunsg.me/furo/)                                              | Clean customizable Sphinx documentation theme           | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [geddit](https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit)             | UIS library for fetching data from URLs                 | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [isort](https://pycqa.github.io/isort/)                                         | Library for sorting Python imports                      | [Setup](site:howtos/development/setup-pre-commit-hooks), [Tutorial](site:tutorials/creating-a-python-package)                 |
| [jsonpath-ng](https://github.com/h2non/jsonpath-ng)                             | Library for extracting data using JSONPath expressions  | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [mypy](https://mypy.readthedocs.io/)                                            | Static type checker for Python                          | [Setup](site:howtos/development/setup-pre-commit-hooks), [Tutorial](site:tutorials/creating-a-python-package)                 |
| [pre-commit](https://pre-commit.com/)                                           | Framework for managing git pre-commit hooks             | [Setup](site:howtos/development/setup-pre-commit-hooks), [Tutorial](site:tutorials/creating-a-python-package)                 |
| [PyYAML](https://pyyaml.org/)                                                   | YAML parser and emitter for Python                      | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [pytest](https://docs.pytest.org/)                                              | Testing framework                                       | [Tutorial](site:tutorials/creating-a-python-package), [Boilerplate](site:explanations/development/webapp-boilerplate#testing) |
| [pytest-cov](https://pytest-cov.readthedocs.io/)                                | Coverage plugin for pytest                              | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [pytest-mock](https://pytest-mock.readthedocs.io/)                              | Thin-wrapper around mock package for pytest             | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [python-social-auth](https://python-social-auth.readthedocs.io/)                | Authentication framework for Python web frameworks      | [SSO Guide](site:howtos/entra-azure/configure-django-for-sso)                                                                     |
| [sphinx](https://www.sphinx-doc.org/)                                           | Documentation generator                                 | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [sphinx-autodoc-typehints](https://github.com/tox-dev/sphinx-autodoc-typehints) | Type hints support for Sphinx                           | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [structlog](https://www.structlog.org/)                                         | Structured logging for Python                           | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
| [tox](https://tox.wiki/)                                                        | Test automation and virtual environment management tool | [Tutorial](site:tutorials/creating-a-python-package)                                                                  |
<!-- markdownlint-enable MD013 -->

## Snippets

<!-- markdownlint-disable MD013 -->
| Type       | Snippet                                                              | Summary                                   |
|------------|----------------------------------------------------------------------|-------------------------------------------|
| decorator  | [decorators](https://gitlab.developers.cam.ac.uk/-/snippets/340)     | neat multiple decorators in a single line |
| decorator  | [threadsafe](https://gitlab.developers.cam.ac.uk/-/snippets/341)     | make a method or property thread-safe     |
| datastruct | [dotdict](https://gitlab.developers.cam.ac.uk/-/snippets/343)        | dot-accessible dict                       |
| datastruct | [threadsafedict](https://gitlab.developers.cam.ac.uk/-/snippets/342) | exemplifies how to use Generic typing     |
<!-- markdownlint-enable MD013 -->

## Tools

<!-- markdownlint-disable MD013 -->
| Title                                    | Summary                                                              | Our Docs                                                                                                                    |
|------------------------------------------|----------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| [copier](https://copier.readthedocs.io/) | Tool for rendering project templates                                 | [Create](site:howtos/development/copier/create), [Update](site:howtos/development/copier/update), [Migrate](site:howtos/development/copier/migrate) |
| [pip](https://pip.pypa.io/)              | Package installer for Python                                         | [System Setup](site:howtos/development/prepare-your-system)                                                                         |
| [pipx](https://pypa.github.io/pipx/)     | Tool to install and run Python applications in isolated environments | [System Setup](site:howtos/development/prepare-your-system)                                                                         |
| [poetry](https://python-poetry.org/)     | Python dependency management and packaging tool                      | [Tutorial](site:tutorials/creating-a-python-package), [Boilerplate](site:explanations/development/webapp-boilerplate#poetry)    |
| [poe](https://poethepoet.natn.io/)       | Task runner that works well with poetry                              | [Tutorial](site:tutorials/creating-a-python-package), [Boilerplate](site:explanations/development/webapp-boilerplate#poe)       |
<!-- markdownlint-enable MD013 -->
