# RenovateBot

!!! important

    The renovatebot service is **internal to DevOps** and is not offered to the wider University.

This page gives an overview of the renovatebot service. A user-focused description is available on a
separate [explainer page](site:explanations/renovatebot).

## Service Status

The renovatebot service is internal to DevOps and is in an early stage.

## Contact

Technical queries and support should be directed to the Cloud Team or Developer Experience.

## Deployment

The deployment is configured in the following repositories

* [Terraform
  configuration](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/)
* [Shared configuration](https://gitlab.developers.cam.ac.uk/uis/devops/renovate-config)
* [GitLab project
  factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/-/blob/main/product-vars/shared-infrastructure/shared-infrastructure.tfvars?ref_type=heads)
  for GitLab webhooks and access tokens.

## Operational documentation

Detailed [operational documentation](https://uis.uniofcam.dev/devops/infra/gitlab-bots-deployment/)
is available for members of the `uis/devops` group in GitLab.
