# Git, CI & Docker

Our adopted & recommended development tools & services.

<!-- markdownlint-disable MD013 -->
| Title                                                         | Summary                                        | Our Docs                                                 |
|---------------------------------------------------------------|------------------------------------------------|----------------------------------------------------------|
| [Docker](https://www.docker.com/)                             | Container platform                             | [System Setup](../howtos/prepare-your-system.md)      |
| [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)                | Continuous Integration and Deployment platform | [Tutorial](../tutorials/creating-a-python-package.md) |
| [GitLab CI Local](https://github.com/firecow/gitlab-ci-local) | Local debugging of GitLab pipelines.           |                                                          |
| [GitLab Runner](https://docs.gitlab.com/runner/)              | CI/CD job execution agent                      | [GKE Guide](../explanations/gke-gitlab-runners.md)    |
| [renovatebot](https://www.mend.io/renovate/)                  | Automated dependency updates                   | [Guide](../explanations/renovatebot.md)               |
| [pre-commit](https://pre-commit.com/)                         | Git hook framework                             | [Setup](../howtos/setup-pre-commit-hooks.md)          |
<!-- markdownlint-enable MD013 -->
