# Git, CI & Docker

Our adopted & recommended development tools & services.

<!-- markdownlint-disable MD013 -->
| Title                                                         | Summary                                        | Our Docs                                                 |
|---------------------------------------------------------------|------------------------------------------------|----------------------------------------------------------|
| [Docker](https://www.docker.com/)                             | Container platform                             | [System Setup](site:howtos/development/prepare-your-system)      |
| [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)                | Continuous Integration and Deployment platform | [Tutorial](site:tutorials/creating-a-python-package) |
| [GitLab CI Local](https://github.com/firecow/gitlab-ci-local) | Local debugging of GitLab pipelines.           |                                                          |
| [GitLab Runner](https://docs.gitlab.com/runner/)              | CI/CD job execution agent                      | [GKE Guide](site:explanations/cloud-platform/gke-gitlab-runners)    |
| [renovatebot](https://www.mend.io/renovate/)                  | Automated dependency updates                   | [Guide](site:explanations/renovatebot)               |
| [pre-commit](https://pre-commit.com/)                         | Git hook framework                             | [Setup](site:howtos/development/setup-pre-commit-hooks)          |
<!-- markdownlint-enable MD013 -->
