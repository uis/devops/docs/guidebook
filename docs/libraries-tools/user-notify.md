# Shared User Notification Service

!!! important

    The shared user notification service is **internal to DevOps** and is not offered to the wider
    University.

This page gives an overview of the shared user notification service. This service can be used to
send email to users of your services.

## Service Status

The user notification service is internal to DevOps and currently consists of a manually curated
SendGrid account. We aim to migrate to AWS SES over the course of 2025.

## Contact

Technical queries and support should be directed to the Cloud Team or Developer Experience.

## Deployment

The deployment is configured in the following repositories

* [Terraform
  configuration](https://gitlab.developers.cam.ac.uk/uis/devops/user-notify)
* [Google Cloud project
  factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/tree/master/product-vars/user-notify?ref_type=heads)
* [GitLab project
  factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/-/tree/main/product-vars/user-notify?ref_type=heads)
* [GKE-hosted GitLab CI
  runners](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure/-/blob/master/locals.tf?ref_type=heads)

## Dashboards

Reputation metrics for SES-originated email can be seen in a [dedicated
dashboard](https://console.cloud.google.com/monitoring/dashboards/builder/9ad42d9f-f591-4495-a035-830c3f6ba13c;duration=P14D?project=user-notify-prod-68cf8181).

## Operational documentation

[Operational documentation](https://uis.uniofcam.dev/devops/user-notify/infrastructure) is currently
hosted on GitLab pages.
