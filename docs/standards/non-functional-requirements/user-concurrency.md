# User Concurrency

## Status

**Status:** Limited peer review

## TDA Precedence

| TDA Submission | Relationship |
| - | - |
| [67 Digital Admissions](https://universityofcambridgecloud.sharepoint.com/sites/UIS_TechnicalDesignAuthority/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FUIS%5FTechnicalDesignAuthority%2FShared%20Documents%2FGeneral%2FGating%20Requests%2F67%2E%20Digital%20Admissions&viewid=d87dfd15%2D4853%2D4f83%2Da546%2D893c8ec17476) | Influenced by |
| [75 Activate Account](https://universityofcambridgecloud.sharepoint.com/:x:/r/sites/UIS_TechnicalDesignAuthority/_layouts/15/Doc.aspx?sourcedoc=%7B9B78B8AA-E418-4DF4-A498-BB456EDB1DCF%7D&file=6.2b%20Activate%20Account%20Non-Functional%20Requirements.xlsx&action=default&mobileredirect=true) | Used in answers and approved (version 0.1.0) |

## NFRs

| NFRs | Relationship |
| - | - |
| NFR.003 | Potentially Answers |
| NFR.004 | Potentially Answers |
| NFR.005 | Potentially Answers |
| NFR.021 | Potentially Answers |
| NFR.022 | Potentially Answers |

## Means to achieve

### 1 - UIS Terraform Modules and Boilerplate

Does your system support concurrent users accessing the same source data? YES

The platform used to serve requests horizontally auto-scales and is mainly only
limited by cost protection measures. Some resources require user intervention to
scale to higher orders, e.g. cost based limits.

The default configuration for APIs can actively process 80 simultaneous API
requests at any moment until cost protection measures kick in.

Asynchronous requests can be accepted via pub/sub at 4 GB/s which equates to
millions of messages per second. Processing speed is limited again largely by
cost protection measures.

Basic load testing will be performed where one time peak events are expected
(e.g. student enrollment on a mass scale). Otherwise observability tooling and
alerts will be used to tune the deployment to meet observed demand. Billing
alerts are sent when costs are predicted to rise above limits.

We are charged for compute services used, with no licensing up front.

Alerts, including billing are routed to multiple team members.

#### Compliance Requirements

- Project is provisioned using the [Google Cloud Product
  Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
- For HTTP APIs use [Cloud Run Terraform
  Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
    - Currently **application specific alerts must be implemented per service**
      and alerts are not enforced
- For data storage use Cloud SQL via [Boilerplate Google Cloud
  Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate)
- For asynchronous tasks use the ucam-faas [library, docker base
  image](https://gitlab.developers.cam.ac.uk/uis/devops/lib/ucam-faas-python)
  and [terraform
  module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas)
    - ensure defaults alerts are enabled or adapted for application
