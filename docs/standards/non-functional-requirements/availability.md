# Availability

## Status

**Status:** Limited peer review

## TDA Precedence

| TDA Submission | Relationship |
| - | - |
| [67 Digital Admissions](https://universityofcambridgecloud.sharepoint.com/sites/UIS_TechnicalDesignAuthority/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FUIS%5FTechnicalDesignAuthority%2FShared%20Documents%2FGeneral%2FGating%20Requests%2F67%2E%20Digital%20Admissions&viewid=d87dfd15%2D4853%2D4f83%2Da546%2D893c8ec17476) | Influenced by |
| [75 Activate Account](https://universityofcambridgecloud.sharepoint.com/:x:/r/sites/UIS_TechnicalDesignAuthority/_layouts/15/Doc.aspx?sourcedoc=%7B9B78B8AA-E418-4DF4-A498-BB456EDB1DCF%7D&file=6.2b%20Activate%20Account%20Non-Functional%20Requirements.xlsx&action=default&mobileredirect=true) | Used in answers and approved (version 0.1.0) |

## NFRs

| NFRs | Relationship |
| - | - |
| NFR.001 | Potentially Answers |
| NFR.002 | Potentially Answers |
| NFR.027 | Potentially Answers |
| NFR.053 | Partially Answers |
| NFR.055 | Potentially Answers |
| NFR.081 | Potentially Answers |
| NFR.071 | Potentially Answers |
| NFR.057 | Partially Answers |

## Means to achieve

### 1 - UIS Terraform Modules and Boilerplate

The web service is expected to be available 24/7 under normal circumstances.

Exceptions to this include:

- automatic managed service updates outside of normal working hours
- major database version changes that may require manual updates during work
  hours approximately every 2-3 years.

The service is self-healing. The service is tolerant to network failures and
uses components that are HA, unless they do not effect direct delivery of the
service. The service is tolerant to cloud zone failures (e.g. switch, power
system, etc.) and is multi-zonal. See
<https://cloud.google.com/compute/docs/regions-zones>.

Backups are performed daily. Backups can be used to recover state to datastores
in the event of ransomware attack from before the point off attack. See
[Data Backup Service](site:services/data-backup).

DevOps change practices are followed:
<https://docs.google.com/document/d/1tGIYU-11l7G7byz1IZw-mhDHjJMPRnS_rIrAX5esyTM/edit?usp=sharing>.

For public domains a service uptime check is included.

#### Compliance Requirements

- For HTTP use, [Cloud Run Terraform
  Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
- Cloud SQL via [Boilerplate Google Cloud
  Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplat)
    - Cloud SQL settings should be unchanged.
- For asynchronous tasks use the ucam-faas [library, docker base
  image](https://gitlab.developers.cam.ac.uk/uis/devops/lib/ucam-faas-python)
  and [terraform
  module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas)
- Other Google managed services with maintenance windows should match
  Boilerplate SQL windows and use a HA option
