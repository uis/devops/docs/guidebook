---
title: The Tech Lead Forum
---

# Technical Lead Forum

Our division grew into a mighty oak from a small acorn: a few Engineers working in a common office
on the same problems. In such an environment discussions around technical practice were frequent and
standards emerged over time. Since everyone was working on the same things, everyone worked in the
same way and there was great commonality between services. Since then, our division has grown to
many tens of colleagues covering many roles and arranged over multiple teams.

The division has not only grown in membership but also in the number of services it is responsible
for. Some of these services are legacy services inherited from other parts of the organisation that
no longer exists. This also has an impact on our technical standards; those legacy services may need
modification to follow our technical practices and standards.

The Tech Lead Forum is designed to be a dedicated place where we as a division can collaborate on
shared technical practice. We have always aimed at tech excellence in all aspects of technology
delivery. The forum is then born with the goal of continue to follow and promote this across the
division when more coordination is required due of its size and complexity.

The Tech Lead Forum is defined in formal language by its [terms of
reference](site:reference/misc/tech-lead-forum).

## Standards and recommendations

The Tech Lead Forum provides a place where technologies and working practices may be proposed for
standardisation. A core principal of forum is to balance the monitoring of emerging new technologies
and standards in our industry with making sure that we only standarise after direct experience with
these. Where good practice exists and is known to the Tech Lead Forum they can decide to document
and recommend it. At that point we actively encourage teams within DevOps to implement that
recommendation and report their experiences back to the Tech Lead forum.

When a recommendation has been implemented by one team, and it is thought useful by the Tech Lead
forum, the recommendation may become a standard. At that point teams in DevOps are expected to align
their work with it unless there is a well reasoned technical justification not to.

## A problem shared is a problem halved

An engineer working on a project may well think "surely, _this_ has been solved already?" The Tech
Lead Forum is a place where engineers can share their problems and see if other teams have
implemented solutions. If no existing solution exists, the Tech Lead Forum is a place to decide if
the problem should be considered at the divisional level or delegated to a single team.

## A common way of working

This guidebook lists a number of common ways of working and common technologies. As time goes on we
want to refine our ways of doing things so that they remain relevant, useful and allow people to
work freely. The Tech Lead Forum provides a place to discuss common ways of working and common
tooling and technology choices.

## Documentation and guidance

The Tech Lead Forum provides a group of experienced and technically minded people. They can offer
advice and guidance. If a question is commonly asked, they can provide written guidance in this
guidebook.

## Read more

* [Tech Lead Forum Terms of Reference](site:reference/misc/tech-lead-forum).
