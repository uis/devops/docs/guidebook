---
title: Custom DNS for Cloud Platform Service
---

# Custom DNS for Cloud Platform Service

We have a [Cloud Platform DNS](site:reference/cloud-platform/dns) guide, which gives an overview of
the different aspects of DNS that can be used as part of the Cloud Platform. This doesn't go into
the specifics of setting up a custom domain for a GCP service, however.

In this tutorial, we'll walk through a recent implementation of a custom domain for the
[Lab Allocator](site:services/lab-allocator) product.

These were the requirements for the custom domain:

* Project: `Laboratory Allocator - prod` / `laballoc-prod-11cb940c`
* Web App DNS: `webapp.prod.laballoc.gcp.uis.cam.ac.uk`
* Custom DNS: `laballocator.apps.cam.ac.uk`
    * The `apps.cam.ac.uk` domain is already managed by the UIS DNS team.

Therefore, the custom domain would be `laballocator.apps.cam.ac.uk`, which needs to be pointed to
`webapp.prod.laballoc.gcp.uis.cam.ac.uk`.

## Step 1: Domain Verification

In order to use a custom DNS name using the standard UIS [GCP Deploy Boilerplate](https://gitlab.developers.cam.ac.uk/uis/devops/gcp-deploy-boilerplate),
the web app DNS must be verified.

Following [Domain verification instructions](site:reference/cloud-platform/dns#domain-verification):
e.g.:

```console
gcloud domains verify prod.laballoc.gcp.uis.cam.ac.uk
```

You will see an initial screen like this. Use `Domain` and click continue:

<figure markdown>
  ![](./gcp-custom-dns-search-console.png)
  <figcaption markdown>Initial domain verification screen</figcaption>
</figure>

You will then be asked to verify the domain. In `1. Select record type` choose `CNAME`. You should
then see the following screen:

<figure markdown>
  ![](./gcp-custom-dns-verify.png)
  <figcaption markdown>CNAME Verification Screen</figcaption>
</figure>

## Step 2: Add `gcp-product-factory` Domain Verification

The next step is to add the domain verification to the products configuration in
`gcp-product-factory`.

In `./product-vars/laballoc/laballoc.tfvars` for the example above, add the following
(the actual real values are different):

```hcl
workspace_domain_verification = {
  production = {
    cname_host   = "5uhara6v7bxe"
    cname_target = "gv-w6vqraye5fqgpp.dv.googlehosted.com"
    verified     = true
  }
}
```

Apply the configuration:

```console
./run-product-factory.sh -p ./product-vars/laballoc apply
```

## Step 3: Click Verify in the GCP Console

Go back to the GCP console and click `Verify`. You may have to wait a few minutes for the DNS to
propagate.

## Step 4: IPReg VBox Entry

Navigate to the [vbox_ops](https://jackdaw.cam.ac.uk/ipreg/vbox_ops) page and add an entry, filling
in the following fields:

* `vbox name`: webapp.prod.laballoc.gcp.uis.cam.ac.uk
* `purpose`: `NST Lab Allocator (Production)`

Click the `create` button.

The screen should look like this:
<figure markdown>
  ![](./gcp-custom-dns-vbox.png)
  <figcaption markdown>Vbox Management</figcaption>
</figure>

## Step 5: IPReg CName Entry

Navigate to the [cname_ops](https://jackdaw.cam.ac.uk/ipreg/cname_ops) page and add an entry, filling
in the following fields:

* `name`: laballocator.apps.cam.ac.uk
* `target_name`: webapp.prod.laballoc.gcp.uis.cam.ac.uk
* `purpose`: `NST Lab Allocator (Production)`

Click the `create` button.

The screen should look like this:
<figure markdown>
  ![](./gcp-custom-dns-cname.png)
  <figcaption markdown>Cname Management</figcaption>
</figure>

## Step 6: OAuth Redirects

If you are using OAuth, you will need to add the new domain to the list of allowed domains in the
OAuth configuration.

## Step 7: Add the Custom Domain to the Product Terraform Configuration

The final step is to add the custom domain to the product Terraform configuration. This is done by
adding an entry to the `workspace_webapp_custom_dns_name` in `locals.tf`.

```hcl
  workspace_webapp_custom_dns_name = {
    # These should be of the form:
    #
    #   <workspace name> = "<custom DNS>"
    production = "laballocator.apps.cam.ac.uk"
  }
```

Apply the configuration:

```console
terraform --workspace=production apply
```

If this product is already created, you may receive an error like this:

```console
Error: Error creating ManagedSslCertificate: googleapi: Error 409: The resource
'projects/laballoc-prod-11cb940c/global/sslCertificates/webapp-cert' already exists, alreadyExists
```

If this does happen, add the following to `module "webapp_http_load_balancer"` in `webapp_load_balancer.tf`:

```hcl
module "webapp_http_load_balancer" {
  # ...

  # This is needed to enable *changing* the certificate over time since certificate can't be
  # deleted/re-created in a single run.
  random_certificate_suffix = true
}
```

Then apply the configuration again.

## Step 8: Confirm the Custom Domain

Verify the A record for the custom domain:

```console
dig a laballocator.apps.cam.ac.uk
```

Browse to the custom domain in a browser to confirm that it is working.

## Complete

That's it! The custom domain should now be pointing to the GCP service.
