# Automating releases on GitLab

In this tutorial you'll create multiple automated releases for a GitLab project.
You'll use the [release-it](https://github.com/release-it/release-it) tool with
our [GitLab pipeline
template](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/release-it.yml)
to configure a release pipeline and trigger automated released using one of two
available workflows.

## Prerequisites

- The GitLab project that you wish to use must have been deployed via the
  [gitlab-project-factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory)
  Terraform configuration and the code should've been created via the [WebApp
  Boilerplate](https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate).
- A product-specific Google Kubernetes Engine (GKE) runner must have been
  deployed via the
  [gitlab-runner-infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
  Terraform configuration.
- You must have followed the steps in [How to enable automated GitLab
  releases](site:howtos/git-gitlab/enable-automated-gitlab-releases) to enable the release
  pipeline and create a `.release-it.json` config file in your repository.

## Default workflow

The default workflow for the `release-it.yml` GitLab template is based on the
[Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/)
specification, as recommended in the [Git
Commits](site:explanations/git-gitlab/git#commits) section of this guidebook. Each time
commits are merged to the default branch the `release` job analyses each new
commit message and determines what the next version of the code should be.

The following steps show you how to use this workflow to create a new minor
release for your project.

1. Checkout a new branch for your feature.

    ```bash
    git checkout -b my-new-feature
    ```

2. Make the required changes to your source code and create a git commit using
   the Conventional Commit `feat` prefix in the commit message.

    ```bash
    git add --all
    git commit -m "feat: my awesome feature"
    ```

3. Push the new branch to the GitLab remote.

    ```bash
    git push -u origin my-new-feature
    ```

4. In the GitLab UI, open a merge request to merge the `my-new-feature` branch
   into the default branch.
5. Once the merge request has been approved and merged you'll see a `release`
   job in the triggered pipeline. This job will invoke the `release-it` command
   to perform the tasks configured in your project's `.release-it.json` config
   file. If you used the example `.release-it.json` file from the [Enable
   automated GitLab releases](site:howtos/git-gitlab/enable-automated-gitlab-releases)
   how-to, the following tasks will be performed.

      - Calculate the next version of your project based on the commit messages
        since the previous git tag.
      - Generate the required updates to the `CHANGELOG.md` and `pyproject.toml`
        files and commit them back to the default branch.
      - Push a new git tag to the repository for the new version.
      - Create a GitLab Release corresponding to the new git tag and containing
        a copy of the generated changelog notes.

## Alternate workflow

The `release-it.yml` GitLab template has an [alternate
workflow](site:explanations/git-gitlab/gitlab-release-automation#alternate-workflow) mode
which queues unreleased changes in a draft release merge request.

The following steps show you how to use this workflow to create a new minor
release for your project.

1. Checkout a new branch for your feature.

    ```bash
    git checkout -b my-new-feature1
    ```

2. Make the required changes to your source code and create a git commit using
   the Conventional Commit `feat` prefix in the commit message.

    ```bash
    git add --all
    git commit -m "feat: my awesome feature"
    ```

3. Push the new branch to the GitLab remote.

    ```bash
    git push -u origin my-new-feature1
    ```

4. In the GitLab UI, open a merge request to merge the `my-new-feature1` branch
   into the default branch.
5. Once the merge request has been approved and merged you'll see an
   `update-release-merge-request` job in the triggered pipeline. This job will
   create/update a draft merge request containing the proposed changes to the
   `CHANGELOG.md` and `pyproject.toml` files for the next version of your code.
   The following is an example of this merge request.

    ![release-mr](./example-release-merge-request.png)

6. Once you're ready to release this version simply approve and merge the
   release merge request. Merging this MR will trigger a pipeline containing the
   `release` job which will perform the git tag and create the corresponding
   GitLab Release.

## Creating a hotfix for a previously released version

Creating a patch for a previously released version can also be automated using
either of the two workflows above. The main requirement is that you create a
branch named with the prefix `release/fix-`. For example, assuming you had previously
released versions `1.0.0` and `2.0.0` of your project but you now need to fix a
bug in the original version `1.0.0` code you could do the following.

1. Checkout a new hotfix branch from the `1.0.0` tag.

    !!! note
        Using an `x` in the branch name is not mandatory but can be useful
        as you may need to fix other bugs in the `1.0.0` version of your code in
        the future. However, the process would work the same if you wanted to
        name your branch `release/fix-1.0.1` instead.

    ```bash
    git checkout -b release/fix-1.0.x 1.0.0
    ```

2. Push the hotfix branch to the remote repository.

    ```bash
    git push -u origin release/fix-1.0.x
    ```

3. Create a new branch to hold your bug fix commit.

    ```bash
    git checkout -b fix-that-annoying-bug
    ```

4. Make the required changes to your source code and create a git commit using
   the Conventional Commit `fix` prefix in the commit message to denote that
   this is a bug fix.

    ```bash
    git add --all
    git commit -m "fix: fix that annoying bug"
    ```

5. Push the new branch to the GitLab remote.

    ```bash
    git push -u origin fix-that-annoying-big
    ```

6. In the GitLab UI, open a merge request to merge the `fix-that-annoying-bug` branch
   into the `release/fix-1.0.x` hotfix branch.

7. Once the merge request has been approved and merged you'll see either the
   `release` job or the `update-release-merge-request` job in the triggered
   pipeline depending on which of the above workflows you are using.

    If you're using the default workflow, the `release` job will perform the
    full `1.0.1` release at this stage. If you're using the alternate workflow,
    the draft merge request will be waiting to be merged into the `release/fix-1.0.x`
    branch when you're ready to release the `1.0.1` version.

## See also

For more information on how this process works see the [GitLab release
automation](site:explanations/git-gitlab/gitlab-release-automation) page in the
Explanations section.
