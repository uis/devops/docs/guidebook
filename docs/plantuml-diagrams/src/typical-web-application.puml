@startuml typical-web-application
!include ../include/c4-container.puml
!include ../include/standard-arch-diagram.puml

title C4 container diagram of a typical DevOps web application

LAYOUT_LANDSCAPE()

Person_Ext(extUser, "Application user")
System_Ext(extSystem, "Automated process", $sprite="robot")
System_Ext(extAPI, "External service", $sprite="cloud")
System_Ext(dbBackup, "Centralised DevOps Backup service")

System_Boundary(service, "Service") {
  Boundary(envProject, "Environment", "Google Cloud Project", $descr="One for each of production, staging, etc.") {
    Container(loadBalancer, "Public endpoint", "Cloud Load Balancing", $sprite="Cloud_Load_Balancing")
    ContainerDb(sqlInstance, "Relational Database", "Cloud SQL", $sprite="Cloud_SQL")
    Boundary(appNetwork, "Application private network", "VPC Network") {
      Container(application, "Web application", "Cloud Run", $sprite="Cloud_Run")
      Container(egressRouter, "NAT Router", "Cloud NAT", $tags="optional", $sprite="Cloud_NAT")
    }
    Container(dataBucket, "Binary object storage", "Cloud Storage", $tags="optional", $sprite="Cloud_Storage")

    Rel(loadBalancer, application, "Forwards requests", "Google-mediated HTTP connection")
    Rel(application, dataBucket, "Persists binary content (rare)", "HTTPS")
    Rel(application, sqlInstance, "Persists state", "Google-mediated connection")
    Rel_D(application, egressRouter, "Requests to external services", "TCP/IP")
    Rel(egressRouter, extAPI, "Forwarded requests to external services", "TCP/IP")
  }

}

Rel(extUser, loadBalancer, "Requests resources", "HTTPS")
Rel(extSystem, loadBalancer, "Requests resources", "HTTPS")
Rel(sqlInstance, dbBackup, "Nightly SQL dumps")

SHOW_LEGEND()
@enduml
