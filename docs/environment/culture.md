---
title: Culture and Values
---

# Shared culture and values

We, as a division, regularly meet to try and reflect on what things about our
culture, values and ways of working are good and which need improvement. These
meetings can be as a group or as one-to-one sessions with line managers. This
page documents some specific observations from these retrospective sessions.

## Good documentation is paramount

The moment you stop working on something, details of its operation start leaking
out of your head. Documenting processes and procedures helps guard against this.
On multiple occasions we have had difficulties arising from the loss of a
single-point of failure or simply that something had been forgotten over time.

We discussed and developed some strategies for good documentation:

1. Write the documentation you would want for yourself in six months. It is hard
   sometimes to know who the audience of documentation is. Unless there's a
   clear audience, write assuming that it is you who'll be re-visiting this
   topic in six months and write the documentation you'd want to find.
2. Merge requests can and should be moved back to rework if they do not include
   documentation. This documentation should include good commit messages. In
   six months time you may be staring at a line of code wishing that you'd
   written down your thought process in a commit message at the time.
3. When extended design work is being performed prior to implementation work, it
   is useful if that design work can be captured and discussed at the earliest
   opportunity. Team members should feel empowered to ask someone performing
   high-level design to keep notes and to be willing to talk through the design
   as they are working on it.

## Keep working state in GitLab

We are increasingly finding ourselves needing to be more work-from-home and
asynchronous-friendly. To that end it is everyone's responsibility to keep state
in a shared place. This place is usually GitLab.

1. Make sure that GitLab issues contain enough information for someone to start
   work on it. Single-line issues should be extremely rare. The issue
   description or the comments below should reflect the discussion which
   happened in sprint refinement.
2. GitLab issues should be short-lived once picked up. If an issue unpicks to
   have a number of sub-issues which weren't anticipated initially, split the
   issue into sub-issues, close the parent issue and start work on the first
   sub-issue.

   Sub-issues should be grouped together using GitLab's "related issue"
   mechanism.

## It is OK not to know something

We are a cross-product team with many different specialisms. Do not be afraid
in a sprint refinement session to ask for clarification on an issue if you do
not understand the problem it is describing.

Attempt to make time to explain concepts to other team members when asked.
Explaining things is a useful transferable skill and one which we all can
benefit from developing.

If you feel that something is being designed or developed without enough context
being recorded, feel free to open an issue and request that it be scheduled.

## It is OK to claim an issue if you are less knowledgeable on it

Our issue estimation process attempts to capture differing familiarity within
the team about specific technologies. If an issue deals with a technology
unfamiliar to you it is OK to pick it up regardless and ask for help. Please try
to document your learning when tackling the issue as "from nothing"
documentation is very hard to write.

If you pick up an issue on a topic you are less familiar with, it is your
responsibility to identify quickly if you have a knowledge issue which is
blocking process and to ask someone for help. If no one can help you, place the
issue back into the backlog and choose another.
