---
title: Home
---

# Welcome to UIS DevOps

DevOps is a division of [University Information Services (UIS)](https://www.uis.cam.ac.uk/)
within the University of Cambridge.

- We design, create & maintain numerous IT [Services](services/index.md).

- Related groups of services are managed by particular [Teams](home/teams.md).

- Members of our division can be viewed in
[Lookup](https://www.lookup.cam.ac.uk/group/uis-devops-division/members).

Each [service we provide](services/index.md) has its own page in the Guidebook.
For specific information on a service, look there.

--8<-- "snippets/log-incident.md"

??? question "What is the Guidebook?"

    The UIS DevOps Division **Guidebook** is
    a central location where we capture & share
    our practices, processes, services & standards.

??? info "Updating the Guidebook"

    Anyone with a Raven account may propose an edit to the guide by opening a merge request on the
    [Guidebook project](https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/)
    in the University [Developers' Hub](https://gitlab.developers.cam.ac.uk/).

    Each page in the Guidebook has an edit link which takes you directly to the corresponding
    page in the Guidebook project.

    Within the division we view this Guidebook as an "external wiki"; it should be
    quick to update and anyone should feel empowered to do so.
