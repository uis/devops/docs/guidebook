---
title: Standard architecture diagrams
---
# Standard architecture diagrams

This page contains standard architecture diagrams which can be shared as part of documenting our
services to external users. The diagrams follow the nomenclature and style used by the [C4
model](https://c4model.com/) which is widely used in the industry. Each diagram can be downloaded in
SVG format for embedding in other documentation although ideally some link back to this page should
be preserved.

There is a [30 minute video](https://www.youtube.com/watch?v=x2-rSnhpw0g) which provides a good
overview of the C4 model if you are unfamiliar with it.

Our standard architecture patterns evolve over time to align with industry best practice. Not all of
our services follow these pattern. For greenfield projects, these diagrams reflect our current
"paved path" of development.

!!! warning

    The C4 style uses the word "container" to mean, roughly, a collection of interacting components
    which forms part of a wider system. This is different to the normal use of "container" within
    UIS DevOps to refer to a namespaced runtime environment used to execute a packaged application.

## A typical web application

This is a container diagram showing a typical web application. Monitoring, alerting, metrics
gathering and logging has not been shown. Some parts of the diagram are optional depending on the
operational needs of the service.

Typically we only use a Cloud SQL instance to persist state. Some applications which deal with
generated or user-uploaded binary content may also persist some state to a Cloud Storage bucket.

!!! note

    Using Cloud Storage in this manner to persist *state* as opposed to *configuration* is uncommon
    but not unknown. Cloud Storage is usually used in cases where:

    - a significant amount of generated or user-uploaded binary content needs to be stored, or
    - static binary content needs to be served to users of the application by means of HTTP
      redirects to [signed URLs](https://cloud.google.com/storage/docs/access-control/signed-urls).

    Use of Cloud Storage tends to be service-specific as will be any backup and data resilience
    methodology.

{{ downloadable_figure(
  "../plantuml-diagrams/out/typical-web-application.svg",
  "Simplified C4 container diagram of a typical web application deployment."
) }}

## Traffic ingress

This is a component diagram showing the traffic ingress for a typical web application in more
detail. Monitoring, alerting, metrics gathering and logging has not been shown. Some parts of the
diagram are optional depending on the operational needs of the service.

We use a Cloud Load Balancer frontend, forwarding rules and a serverless network endpoint group to
handle incoming traffic. The service has a static ingress IP which can be resolved, indirectly, via
a `{service}.apps.cam.ac.uk` DNS record. TLS termination and HTTP to HTTPS redirect is handled at
the load balancer frontend with traffic being forwarded to the backend over a dedicated
Google-mediated HTTP connection.

Optionally, static and dynamic Cloud Armor rules may be included for DDoS and other abuse
protection.

{{ downloadable_figure(
"../plantuml-diagrams/out/typical-web-application-ingress.svg",
  "C4 component diagram of the ingress for a typical web application."
) }}

## Traffic egress for external service wishing to use IP allow listing

This is a component diagram showing the traffic egress for a typical web application in more detail
for the case where an external service wishes to use IP allow listing. Monitoring, alerting, metrics
gathering and logging has not been shown. Some parts of the diagram are optional depending on the
operational needs of the service. **We strongly prefer active per-request authentication as the
primary means of service-to-service authentication** but accept the use of IP allow listing as an
*additional* layer of protection.

{{ downloadable_figure(
  "../plantuml-diagrams/out/typical-web-application-egress.svg",
  "C4 component diagram of traffic egress of a web application using a static egress IP."
) }}
