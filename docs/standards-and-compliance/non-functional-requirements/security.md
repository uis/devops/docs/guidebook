# Security

## Status

**Status:** Limited peer review

## TDA Precedence

| TDA Submission | Relationship |
| - | - |
| [67 Digital Admissions](https://universityofcambridgecloud.sharepoint.com/sites/UIS_TechnicalDesignAuthority/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FUIS%5FTechnicalDesignAuthority%2FShared%20Documents%2FGeneral%2FGating%20Requests%2F67%2E%20Digital%20Admissions&viewid=d87dfd15%2D4853%2D4f83%2Da546%2D893c8ec17476) | Influenced by |
| [75 Activate Account](https://universityofcambridgecloud.sharepoint.com/:x:/r/sites/UIS_TechnicalDesignAuthority/_layouts/15/Doc.aspx?sourcedoc=%7B9B78B8AA-E418-4DF4-A498-BB456EDB1DCF%7D&file=6.2b%20Activate%20Account%20Non-Functional%20Requirements.xlsx&action=default&mobileredirect=true) | Used in answers and approved (version 0.1.0) |

## NFRs

| NFRs | Relationship |
| - | - |
| NFR.054 | Potentially Answers |
| NFR.056 | Potentially Answers |

## Means to achieve

### 1 - Use small single responsibility services

The service is hosted in it's on Virtual Private Network. Each service uses
dedicated service accounts for each component following least privileged
principles.

System users have no access to the underlying cloud admin controls.

CI pipelines have been configured for rapid deployment of specific docker images
when a patch has been prepared. Further details here:
<https://docs.google.com/document/d/14Ob5uZVJ-l9Tb-JlU2ouxcTfSOqPA_gKeoCCZAyyPVE/edit?usp=sharing>

Where files are uploaded by users the [Malware and Virus Scanner](https://developer.api.apps.cam.ac.uk/docs/malware-virus-scanner/1/overview)
API will be used.

Pen testing occurs on most services periodically. All services benefit from
common tooling that receives indirect pen testing.

Software dependency updates are performed by the service team and follow DevOps
change practices:
<https://docs.google.com/document/d/1tGIYU-11l7G7byz1IZw-mhDHjJMPRnS_rIrAX5esyTM/edit?usp=sharing>.

#### Compliance Requirements

- Use the
  [team-data](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data)
  to apply cloud admin permissions to each project
- Use `{crsid}@gcloudadmin.g.apps.cam.ac.uk` accounts for privileged access
  operations:
  <https://guidebook.devops.uis.cam.ac.uk/explanations/gcloudadmin-accounts/#more-information>
- Project is provisioned using the [Google Cloud Product
  Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
- For HTTP APIs use [Cloud Run Terraform
  Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
- For asynchronous tasks use the ucam-faas [library, docker base
  image](https://gitlab.developers.cam.ac.uk/uis/devops/lib/ucam-faas-python)
  and [terraform
  module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas)
- If users can upload files, use the [Malware and Virus Scanner](https://developer.api.apps.cam.ac.uk/docs/malware-virus-scanner/1/overview)
service.
