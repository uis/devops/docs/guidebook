# APIs

## Status

**Status:** Limited peer review

## TDA Precedence

| TDA Submission | Relationship |
| - | - |
| [67 Digital Admissions](https://universityofcambridgecloud.sharepoint.com/sites/UIS_TechnicalDesignAuthority/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FUIS%5FTechnicalDesignAuthority%2FShared%20Documents%2FGeneral%2FGating%20Requests%2F67%2E%20Digital%20Admissions&viewid=d87dfd15%2D4853%2D4f83%2Da546%2D893c8ec17476) | Influenced by |
| [75 Activate Account](https://universityofcambridgecloud.sharepoint.com/:x:/r/sites/UIS_TechnicalDesignAuthority/_layouts/15/Doc.aspx?sourcedoc=%7B9B78B8AA-E418-4DF4-A498-BB456EDB1DCF%7D&file=6.2b%20Activate%20Account%20Non-Functional%20Requirements.xlsx&action=default&mobileredirect=true) | Used in answers and approved (version 0.1.0) |

## NFRs

| NFRs | Relationship |
| - | - |
| NFR.019 | Potentially Answers |
| NFR.020 | Potentially Answers |
| NFR.052 | Partially Answers |
| NFR.053 | Partially Answers |
| NFR.057 | Partially Answer |
| NFR.058 | Potentially Answers |

## Means to achieve

### 1 - Boilerplate Web Application

API schemas are generated and stored in the services repo via third party libraries.

The API uses:

- JSON
- Is RESTful and supports HTTPS for clients
- ISO 8601 standard used for time and date
- Unicode encoding in UTF-8 is to be used for textual representations of data
- Endpoints can be used for mass data retrievable with suitable permissions
- All API requests will use OAuth2 or GCP based Bearer machine-to-machine
  authentication when possible
- 2 non-production environments will be provisioned

No log of requests for personal data will be made but wider formal subject
access request practices will be followed.

#### Logging

All API requests will be logged to a log aggregator and retained for 1 month.

No one is specifically tasked with "watching" the API but any observed unusual
activity will be reported. Unauthorised requests are logged to the log
aggregator.

#### Compliance Requirements

- Use the Django based [Boilerplate Web
  Application](https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate)
- For HTTP use, [Cloud Run Terraform
  Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
- Publish via the [API gateway](https://developer.api.apps.cam.ac.uk/apis) for
  wider interfacing
- Project is provisioned using the [Google Cloud Product
  Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
- For HTTP APIs use [Cloud Run Terraform
  Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
- For asynchronous tasks use the ucam-faas [library, docker base
  image](https://gitlab.developers.cam.ac.uk/uis/devops/lib/ucam-faas-python)
  and [terraform
  module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas)
