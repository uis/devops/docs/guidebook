# GCP

## Status

**Status:** Limited peer review

## TDA Precedence

| TDA Submission | Relationship |
| - | - |
| [67 Digital Admissions](https://universityofcambridgecloud.sharepoint.com/sites/UIS_TechnicalDesignAuthority/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FUIS%5FTechnicalDesignAuthority%2FShared%20Documents%2FGeneral%2FGating%20Requests%2F67%2E%20Digital%20Admissions&viewid=d87dfd15%2D4853%2D4f83%2Da546%2D893c8ec17476) | Influenced by |
| [75 Activate Account](https://universityofcambridgecloud.sharepoint.com/:x:/r/sites/UIS_TechnicalDesignAuthority/_layouts/15/Doc.aspx?sourcedoc=%7B9B78B8AA-E418-4DF4-A498-BB456EDB1DCF%7D&file=6.2b%20Activate%20Account%20Non-Functional%20Requirements.xlsx&action=default&mobileredirect=true) | Used in answers and approved (version 0.1.0) |

## NFRs

| NFRs | Relationship |
| - | - |
| NFR.023 | Potentially Answers |
| NFR.024 | Potentially Answers |
| NFR.025 | Potentially Answers |
| NFR.079 | Potentially Answers |
| NFR.082 | Potentially Answers |
| NFR.049 | Partially Answers |
| NFR.050 | Partially Answers |
| NFR.053 | Partially Answers |

## Means to achieve

### 1 - GCP services

There are no licensing restrictions for most parts of the service. Adding
environments does incur additional costs.

There are no limitations on how data is moved or transformed between
environments. Moving production data to non-production environments only occurs
under careful consideration and is generally avoided for UK GDPR and general
security best practices.

GCP is one of the big 3 cloud providers. All providers are multi-tenancy with
strong, mature and well trusted isolation mechanisms.

All GCP services used are covered by their ISO 27017 certification
<https://cloud.google.com/security/compliance/iso-27017>.

All GCP services used are covered by their ISO 27001 certification
<https://cloud.google.com/security/compliance/iso-27001>.

Services are a predominately deployed to `europe-west2` (some managed services are
multi-region in nature) which is a ["Low CO2"
region](https://cloud.google.com/sustainability/region-carbon#region-picker).

Estimated CO2 emission data is available per project.

Details of responsibility split between GCP and UIS is documented here:
<https://cloud.google.com/terms/secops>

Privileged access to cloud resources (e.g. production database read access)
is only available through a dedicated admin account. See
<https://guidebook.devops.uis.cam.ac.uk/explanations/gcloudadmin-accounts/#more-information>.

#### Compliance Requirements

- Use the
  [team-data](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data)
  to apply cloud admin permissions to each project
- Use `{crsid}@gcloudadmin.g.apps.cam.ac.uk` accounts for privileged access
  operations:
  <https://guidebook.devops.uis.cam.ac.uk/explanations/gcloudadmin-accounts/#more-information>
- Project is provisioned using the [Google Cloud Product
  Factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory)
- For HTTP APIs use [Cloud Run Terraform
  Module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-app)
- For asynchronous tasks use the ucam-faas [library, docker base
  image](https://gitlab.developers.cam.ac.uk/uis/devops/lib/ucam-faas-python)
  and [terraform
  module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas)
