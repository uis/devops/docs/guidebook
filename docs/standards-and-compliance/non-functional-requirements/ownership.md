# Ownership

## Status

**Status:** Limited peer review

## TDA Precedence

| TDA Submission | Relationship |
| - | - |
| [67 Digital Admissions](https://universityofcambridgecloud.sharepoint.com/sites/UIS_TechnicalDesignAuthority/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FUIS%5FTechnicalDesignAuthority%2FShared%20Documents%2FGeneral%2FGating%20Requests%2F67%2E%20Digital%20Admissions&viewid=d87dfd15%2D4853%2D4f83%2Da546%2D893c8ec17476) | Influenced by |
| [75 Activate Account](https://universityofcambridgecloud.sharepoint.com/:x:/r/sites/UIS_TechnicalDesignAuthority/_layouts/15/Doc.aspx?sourcedoc=%7B9B78B8AA-E418-4DF4-A498-BB456EDB1DCF%7D&file=6.2b%20Activate%20Account%20Non-Functional%20Requirements.xlsx&action=default&mobileredirect=true) | Used in answers and approved (version 0.1.0) |

## NFRs

| NFRs | Relationship |
| - | - |
| NFR.049 | Partially Answers |

## Means to achieve

### 1 - Team Ownership

No individual owns a service, but a team. A team lead and tech lead exist for
each service. They may be the same person. Either can be referenced when the
team cannot, but this is discouraged.

#### Compliance Requirements

- use lookup groups and/or mailing lists where possible (see
  <https://universityofcambridgecloud.sharepoint.com/:w:/r/sites/UIS_UIS_DevOps/_layouts/15/Doc.aspx?sourcedoc=%7BF2583933-DFD6-42CB-8F27-908E269C91B8%7D&file=DevOps%20structure.docx&action=default&mobileredirect=true&wdsle=0>
  for each teams email alias)
- follow DevOps guidebook team workflow and code contribution best practices
