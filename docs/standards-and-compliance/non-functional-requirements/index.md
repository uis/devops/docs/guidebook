# Non-functional requirements (NFRs)

This sections includes information that relates to UIS published Non-functional requirements
(NFRs) and documents potential means to complete some of the NFRs.

It is hoped this effort will improve the degree of NFR compliance in a scalable
manner, including helping to drive decisions on tooling and practices to
support this.

## Completing NFRs

These documents are not intended as a means to skip the NFR process. Those
completing NFRs need to read each NFR question and all the information in each
page to ensure any referenced pages answer the NFR for that service.

Typically the further the service strays from DevOps paved paths, the less these
pages can be referenced or supplementary details are needed. E.g. if using
anything other than containers and the centrally managed Cloud Run modules.

- The TDA website includes a template NFR spreadsheet and a copy of this should
  be made
- Entries exist for a lot of the NFRs in the spreadsheet and you can use the
  guidebook search functionality with the NFR ID to find matching entries, as well as
  following the section headings below.
- You must read, understand and make an independent determination if the answers
  are suitable for the NFR and the target service.

## Index

- [Availability](./availability.md)
- [User Concurrency](./user-concurrency.md)
- [GCP](./gcp.md)
- [APIs](./apis.md)
- [Ownership](./ownership.md)
- [Security](./security.md)
