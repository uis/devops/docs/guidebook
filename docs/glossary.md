---
title: Glossary
---

Artifact
: A by-product of the software development process, including documentation and packages.

Cambridge Human Resources Information System (CHRIS)
: The University's Human Resources (HR) system.

Cambridge Student Information System (CamSIS)
: The University's comprehensive system for handling student information.

CI/CD (Continuous Integration/Continuous Delivery)
: A method for frequent app delivery through automation.

Cloud Computing
: Computing services delivered over the internet, including storage and processing.

Containerization
: Encapsulating an application and its dependencies in a single container package.

Docker
: An open platform for developing, shipping, and running applications.

Git
: A version-control system for tracking changes in source code.

HEAT
: The University Information Services (UIS) support management system.

Infrastructure as Code (IaC)
: Managing data centers through machine-readable definition files.

Jackdaw
: The University Information Services (UIS) administrative database.

Lookup
: A directory with info about people known to Cambridge University and its affiliates.

Open Source Software (OSS)
: Software that anyone can inspect, modify, and enhance.

Pipeline
: Automated processes for compiling, building, and deploying code.

Raven
: The University of Cambridge's central web authentication service.

Secure Sockets Layer (SSL)/Transport Layer Security (TLS)
: Protocols for communications security over a network.

Single Sign-On (SSO)
: Authentication that allows logging in with a single ID across multiple systems.

Terraform
: A tool for defining and provisioning infrastructure as code.

The University Computing Service (UCS)
: The UCS development group wrote services, some still in use. As the UCS and its
dev group no longer exist, running these has been transferred to DevOps.

Universities and Colleges Admissions Service (UCAS)
: A charity connecting people to higher education.

University Information Services (UIS)
: The central IT department of the University of Cambridge.

Version Control System (VCS)
: A tool for managing changes to source code and documents.
