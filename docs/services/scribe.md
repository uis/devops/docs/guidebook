<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of Scribe, the case management system for the Legal Office,
describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Scribe is an application for managing case loads for the University's Legal Office.

## Service Status

Scribe is currently **live**.

Currently there is no planned roadmap for the future of this service; however, the service is
integral to the administration of the University's legal case loads and there are occasionally
some tweaks made to the system.

## Contact

Technical queries and support should be directed to <mailto:uis-cloud@lists.cam.ac.uk> and will be
picked up by a member of the team working on the service.

<!-- Issues discovered in the service or new feature requests should be opened as
`[GitLab issues in the application repository](<link to gitlab issues page>)`.-->

## Environments and Client

Scribe is a Windows Forms app so is currently deployed to and run from a file server.
The application is run over the network on the UIS File Server. This is secured by file permissions,
by the need to be run on a University machine belonging to a University Windows Domain, and by pass
through Windows login to the database server. Membership of the following group will give access.

- uis-sec-microsoft-.net-drive-access

Access to environment is determined by which folder
the executable is run from. The application is started by running the **ScribeApp.exe**
executable.

Scribe is currently deployed to the following environments (this assumes the Z
drive is mapped to `\\blue.cam.ac.uk\group` as per standard university build):

| Name        | File Folder |
| ----------- | ------------------ |
| Production  | Z:\J Share\Applications\DotNet\Legal Group\Scribe\Scribe Live |
| Pending*     | Z:\J Share\Applications\DotNet\Legal Group\Scribe\Scribe Pending |
| Development | Z:\J Share\Applications\DotNet\Legal Group\Scribe\Scribe Dev |

*_Pending_ is a version of the system where the front end application is updated but
on the back end points to the real live database.

<!-- markdownlint-disable MD013 -->

## Source code

The source code for Scribe is spread over the following repository folders:

| Repository  | Description |
| ----------- | ------------------ |
| [Components](https://https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/Components?ref_type=heads) | Key custom DLLs required for the build  |
| [SQL](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/SQL?ref_type=heads) | Holds a few MS SQL scripts for objects in the database |
| [ScribeApp](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeApp?ref_type=heads) | The Windows client application  |
| [ScribeBEL](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeBEL?ref_type=heads) | Holds business objects |
| [ScribeDAL](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeDAL?ref_type=heads) |  Data access layer to communicate with database |
| [ScribeDB](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeDB?ref_type=heads) |  Deprecated Redgate folder |
| [ScribeExport](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeExport?ref_type=heads) | Library to generate PDF files for Scribe  |
| [ScribeTestApp](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeTestApp?ref_type=heads) | Deprecated old app to test logic  |
| [ScribeUnitTests](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/ScribeUnitTests?ref_type=heads) | Unit tests (needs revision) |
| [WpfControlApp](https://gitlab.developers.cam.ac.uk/uis/devops/scribe/scribe/-/tree/main/WpfControlApp?ref_type=heads) | An app not proceeded with so consider deprecated  |

<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the technologies Scribe is built on.

| Category        | Language | Framework(s) |
| --------------- | -------- | ------------ |
| Database Server | MS SQL   | SQL Server 2019 |
| Client Code     | VB.NET   | 4.6.1        |

Nuget package manager is also used to control versions of integrated code such as
EPPlus (used for Excel interoperability).
Full details are outside the scope of this document but can be seen through Visual Studio.

### Database

Scribe's data resides on a Microsoft SQL Server (uas-sql1.blue.cam.ac.uk)

| Environment        | Instance |
| --------------- | -------- |
| Live | Live |
| All development  | UAT |

Access is done through Windows Active Directory, meaning there is no need to store passwords,
it's all integrated.
To have access you need to either be in the SQL Administrators group or be in the following group:

- BLUE\legal-legalservicesoffice
- BLUE\legal-legaladmin

### IDE

Scribe is developed using Microsoft's Visual Studio (minimum version recommended 2019).
There is a solution file that contains projects; those projects are reflected in the
different folders in stored in the repository and listed above.

## Operational documentation

The following gives an overview of how Scribe is deployed and maintained.

### How and where Scribe is deployed

Deployment for Scribe is extremely simple - files are copied over from your build folder
in Visual Studio
to the folder holding the environment you are deploying to.

### Deploying a new release

Make sure no files are open in the destination document -
that is that the target version of Scribe is not open,
then copy files over.

### Monitoring

Currently no monitoring is carried out; the userbase is small and there are excellent relations
between the users and the developer.

### Debugging

Debugging can only be done through Visual Studio, running the app natively in it.

<!-- ### `<other operation issues>`

`<documentation or links to external docs about other operation issues>`
-->

{{ service_management() }}
