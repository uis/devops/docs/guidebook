<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Lookup service, describing its current status, where and how it's
developed and deployed, and who is responsible for maintaining it.

## Service Description

Lookup provides an [directory of University staff, institutions and
groups](https://www.lookup.cam.ac.uk/) available via
[LDAP](https://help.uis.cam.ac.uk/service/collaboration/lookup/ldapqueries/ldapqueries) (Uni only)
or a [web API](https://www.lookup.cam.ac.uk/doc/ws-doc/).

## Service Status

Lookup is currently live.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Queries regarding correctness or completeness of data should be sent to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk).

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis/-/issues).

## Environments

Lookup is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://www.lookup.cam.ac.uk](https://www.lookup.cam.ac.uk)                                    | lookup-live1.srv.uis.cam.ac.uk<br/> lookup-live2.srv.uis.cam.ac.uk |
|             | [https://ldap.lookup.cam.ac.uk](https://ldap.lookup.cam.ac.uk) (live LDAP endpoint)<br/> ldap{1,2}.lookup.cam.ac.uk (HA LDAP endpoints) | lookup-live1.srv.uis.cam.ac.uk<br/> lookup-live2.srv.uis.cam.ac.uk |
|             |  Database                                                                                       | lookup-live-db.srv.uis.cam.ac.uk   |
| Staging     | [https://lookup-test.srv.uis.cam.ac.uk](https://lookup-test.srv.uis.cam.ac.uk)  | lookup-test1.srv.uis.cam.ac.uk<br/> lookup-test2.srv.uis.cam.ac.uk |
|             | lookup-test.srv.uis.cam.ac.uk (test LDAP endpoint)                                      | lookup-test1.srv.uis.cam.ac.uk<br/> lookup-test2.srv.uis.cam.ac.uk |
|             | Database                                                                                        | devgroup-test-db.srv.uis.cam.ac.uk |
<!-- markdownlint-enable MD013 -->

## Source code

The source code for Lookup is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis) | The source code for the main application server |
| [Lookup API Client](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis-client) | API Client |
<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the technologies Lookup is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Java + Groovy| Grails 1.3.10 |
| DB     | Postgres      | [Moa](https://confluence.uis.cam.ac.uk/display/SYS/Moa%3A+managed+database+service) |

## Operational documentation

The following gives an overview of how Lookup is deployed and maintained.

### How and where Lookup is deployed

Lookup is deployed to a pair of servers which use keepalived to manage a VIP.

![Lookup block diagram](lookup-block.jpg)

### Deploying a new release

New releases are deployed via
[Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment/).
The `package_version` group_var determines the tagged version of the app to deploy.

Each server should be set to maintenance mode then the `run-ansible-playbook.sh` should be run
limited to the node in maintenance mode. Deployments can be monitored using `watch-status.sh` script.

The following example is for production service, this should be done using the test service first.

```bash
# In separate window on client machine
./watch-status.sh ibis-production

# On lookup-live1 (as root)
touch /maintenance_mode

# On client machine
eval $(op signin)

# If your local user is not your CRSid, set APP_USER var to your CRSid
# If not using VPN, use aperture by setting APERTURE_JUMP_HOST var to non-empty
./run-ansible-playbook.sh -i ibis-production ibis-playbook.yml --diff --limit lookup-live1

# When complete, confirm that the server is running before removing it from maintenance mode

# On lookup-live1
rm /maintenance_mode

# Then repeat for the other server in the pair

# On lookup-live2 (as root)
touch /maintenance_mode

# On client machine
./run-ansible-playbook.sh -i ibis-production ibis-playbook.yml --diff --limit lookup-live2

# Again, confirm server is running

# On lookup-live2
rm /maintenance_mode
```

### Other documentation

- [UIS help site documentation](https://help.uis.cam.ac.uk/service/collaboration/lookup)
- [Web service API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws)

### Monitoring

Lookup is monitored by [nagios](https://nagios.uis.cam.ac.uk):

- [Overview](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup&style=overview)
- [Disc Space](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-disc-space&style=overview)
- [SSH Server](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-ssh-server&style=overview)
- [SSL Certs](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-ssl-certs&style=overview)
- [Lookup Web Application](https://nagios.uis.cam.ac.uk/nagios/cgi-bin/status.cgi?hostgroup=lookup-webapp&style=overview)

### Backups

See [legacy application backups](legacy-application-backups.md)

### Debugging

The [Lookup server readme](https://gitlab.developers.cam.ac.uk/uis/devops/ibis/ibis) contains
details on running a local copy for debugging.

#### Resetting the LDAP databases

A well known error state of lookup is for the LDAP database not to be synchronized with the
Postgresql database correctly. To resolve this
the following commands should be run:

```bash
# Take node out of service
touch /maintenance_mode

# Run ibis-ldap-sync
cd /usr/share/ibis/bin
./ibis-ldap-import

# Put node back in to service
rm /maintenance_mode
```

{{ service_management() }}
