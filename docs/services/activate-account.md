<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Activate Account service, describing its current status, where
and how it's developed and deployed, and who is responsible for maintaining it.

!!! warning
    This service is currently in active development.

## Service Description

The Activate Account service is designed to replace the signup/account activation step with
a new webapp independent of the Password App, which is being decommissioned in favor
of moving password resets. This transition is pending the deprecation
of the Raven Legacy Login system.

## Service Status

The Activate Account service is currently in active development.
Details of the project can be found within the
[project page on the UIS site](https://universityofcambridgecloud.sharepoint.com/sites/portfolios/Lists/Portfolio%20Register/DispForm.aspx?ID=300&e=ADZTeh).

## Contact

Technical queries and support should be directed to the Wilson DevOps team
via [devops-wilson@uis.cam.ac.uk](mailto:devops-wilson@uis.cam.ac.uk),
which will be handled by the team working on the service.

## Architecture

The Activate Account service consists of a frontend and a backend:

* [The Activate Account Frontend](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend)
    * a Next.js-based frontend application used by university members to activate their accounts.
* [The Activate Account API](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api)
    * a Django-based backend that handles all server-side logic and API requests
for the account activation processes.

## Deployment

The service is deployed across multiple environments, detailed as follows:

| Environment | Component     | URL                                                               |
|-------------|---------------|-------------------------------------------------------------------|
| Development | API           | <https://api.devel.activate-ac.gcp.uis.cam.ac.uk/>                |
| Development | Frontend      | <https://devel.myaccount.apps.cam.ac.uk/get-your-account/>        |
| Staging     | API           | <https://api.test.activate-ac.gcp.uis.cam.ac.uk/>                 |
| Staging     | Frontend      | <https://test.myaccount.apps.cam.ac.uk/get-your-account/>         |
| Production  | API           | <https://api.prod.activate-ac.gcp.uis.cam.ac.uk/>                 |
| Production  | Frontend      | <https://myaccount.apps.cam.ac.uk/get-your-account/>              |

## Technologies Used

The Activate Account service utilizes a range of technologies, listed below:

| Category | Technologies                            |
|----------|-----------------------------------------|
| Frontend | Next.js, React, TypeScript, Cypress, MUI|
| Backend  | Django, Python, Gunicorn, Docker, PostgreSQL|

{{ service_management() }}

## Additional Documentation

Further documentation related to the architecture and deployment of the Activate Account service
can be accessed through the
[GitLab repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account).
