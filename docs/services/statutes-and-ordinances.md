<!-- markdownlint-disable MD041 -->
{{ service_title() }}

## Service Description

Statutes and Ordinances is a key document in the governance of the University. It is revised
annually and its print publication arranged by the University Draftsman.

The [on-line version](https://www.admin.cam.ac.uk/univ/so/) is now becoming the more important
presentation of the document.

## Service Status

The Statutes and Ordinances site is currently live.

## Contact

Technical queries and support should be directed to
[uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk) and will be picked
up by a member of the team working on the service.

Issues discovered in the service or new feature requests should be opened as [GitLab
issues](https://gitlab.developers.cam.ac.uk/uis/devops/governance-systems/reporter-and-statutes-and-ordinances).

Non-technical and end-user queries relating to pages and content should be directed to one of the
following:

For content queries, you can contact the **University Draftsman**:

- [Ceri Benton](mailto:ceri.benton@admin.cam.ac.uk)

All the pages are processed and maintained by the **Reporter Editors**:

- [Tara Grant](mailto:tara.grant@admin.cam.ac.uk)
- [Sadie Byrne](mailto:Sadie.Byrne@admin.cam.ac.uk)

## Environments

Statutes and Ordinances is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | <http://www.admin.cam.ac.uk/univ/so> | web-rso-live1.srv.uis.private.cam.ac.uk<br/>web-rso-live2.srv.uis.private.cam.ac.uk |
| Staging / Development    | <http://dev.admin.internal.admin.cam.ac.uk/univ/so> | web-rso-dev1.srv.uis.private.cam.ac.uk |

**Note:** XML source files need to be manually added to development so they can be processed using
the xml converter to produce the static html files which are then edited, verified, and then copied
to production using various `bash` scripts located on the development server.

## Source code

The source code for the Statutes and Ordinances XML converter is on [GitLab](https://gitlab.developers.cam.ac.uk/uis/webcms/statutes-and-ordinances).

The Statutes and Ordinances website source code can be found on
[GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/governance-systems/reporter-and-statutes-and-ordinances)
(under construction).

### Notable folders on development and production servers

The XML Converter is located on the development server:

- `web-rso-dev1:/home/webadmin/statutes-and-ordinances`

The website is located on the development and production servers:

- `web-rso-*:/home/webadmin/web/global`
- `web-rso-*:/home/webadmin/web/univ/so`

The scripts are located on the development server:

- `web-rso-dev1:/home/webadmin/scripts`

## Technologies used

The following gives an overview of the technologies Statutes and Ordinances is built on.

XML Converter

- Java
- XML
- XSLT

Website

- Apache with ucam_webauth module
- CGI / Perl
- HTML, CSS, JS

## Operational documentation

Every year, around September, a 3rd party supplier provides the source XML for the print document,
which is used to produce the on-line version, usually by the first week of term (early October.).

The tech lead has been responsible for producing the on-line version using scripts located on the
development server.

<!--
### How and where is Statutes and Ordinances deployed

`<documentation or links to external docs describing how and where the application is deployed>`

### Deploying a new release

`<documentation or links to external docs about how a new release is made and deployed>`

### Monitoring

`<documentation or links to external docs about monitoring and how alerts should be responded to>`

### Debugging

`<documentation or links to external docs describing how to debug both a deployed instance and locally>`
-->

### Other operation issues

The online version has not been produced since 2018 with only pdf versions available.

{{ service_management() }}
