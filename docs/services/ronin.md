<!-- markdownlint-disable MD041 -->
{{ service_title() }}

Simplified AWS resources management for research groups.

## Service Description

Ronin is UIS' response to the need of flexible cloud infrastructure with simplified setup and
management. It uses a straightforward web application (Ronin) which hides AWS' technical details
from the end users.

Its basic operation consists in creating research projects containing a group of users and a budget,
and allowing those users to consume (create, reshape, monitor, stop, destroy) cloud resources from a
predefined catalogue in a self-service way.

The application provides instant visual feedback about the existing resources, their statuses and
associated costs as well as forecasts and notifications according to budget thresholds. From a
management perspective, it allows a detailed expenditure tracking including breakdowns by cost
centre, project, cost type and source of funds.

## Service Status

Ronin is currently live.

## Contact

End-user support is provided through Ronin channels:

- [blog](https://blog.ronin.cloud/)
- [support](https://ronincloud.atlassian.net/servicedesk/customer/portals) (user id
  <cloud@uis.cam.ac.uk> in 1password)
- [slack](https://blog.ronin.cloud/slack/)

For escalations and incidents, please use [cloudsupport@uis.cam.ac.uk](mailto:cloudsupport@uis.cam.ac.uk)

## Environments

We currently have two instances of RONIN running  at:

- [https://trial.uniofcam.cloud](https://trial.uniofcam.cloud)
- [https://research.uniofcam.cloud](https://research.uniofcam.cloud)

The service is co-managed with RONIN (company) which is responsible for the upgrades and monitoring
of the service.

## Operational documentation

Please visit [Operational Documentation](https://gitlab.developers.cam.ac.uk/groups/uis/devops/cloud/ronin/-/wikis/home)
internal wiki.

{{ service_management() }}
