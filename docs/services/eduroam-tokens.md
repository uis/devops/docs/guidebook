---
title: Network Tokens
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Network Access Tokens service, describing its current status,
where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The Network Access Tokens site provides a site for Raven-authenticated users to obtain and manage
per-device tokens allowing them to use eduroam and the University’s wireless network, and the VPN
service.

## Service Status

The Network Access Tokens service is currently live.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/network-access/network-access/-/issues).

End user support is provided by the [Service Desk](mailto:servicedesk@uis.cam.ac.uk).

## Environments

The Network Access Tokens service is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://tokens.uis.cam.ac.uk](https://tokens.uis.cam.ac.uk) | tokens-live1.srv.uis.private.cam.ac.uk|
|             | [https://uis-tm-live.tokens.uis.cam.ac.uk/](https://uis-tm-live.tokens.uis.cam.ac.uk/)| tokens-live2.srv.uis.private.cam.ac.uk|
|             | Database                                                                              | network-tokens-live-db.srv.uis.private.cam.ac.uk |
| Staging     | [https://uis-tm-test.tokens.uis.cam.ac.uk/](https://uis-tm-test.tokens.uis.cam.ac.uk/) | tokens-test1.srv.uis.private.cam.ac.uk |
|             |                    | tokens-test2.srv.uis.private.cam.ac.uk |
|             | Database                                                                              | devgroup-test-db.srv.uis.private.cam.ac.uk |
<!-- markdownlint-enable MD013 -->

## Source code

The source code for the Network Access Tokens service is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/network-access/network-access) | The source code for the main application server |
| [Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment) | The Ansible used to deploy all the Dev Group Grails Apps |
<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the technologies the Network Access Tokens service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | [Moa](https://confluence.uis.cam.ac.uk/display/SYS/Moa%3A+managed+database+service) |

## Operational documentation

The following gives an overview of how the Network Access Tokens service is deployed and maintained.

### How and where the Network Access Tokens service is deployed

The Network Access Tokens service application is deployed using WAR packages. These are built by
Gitlab CI in the [Application Server
repository](https://gitlab.developers.cam.ac.uk/uis/devops/network-access/network-access). These are
deployed using the Ansible above

### Deploying a new release

Once the WAR packages are built they can be deployed using the Ansible. See the Ansible docs for
information on how to.

### Monitoring

The Network Access Tokens service is monitored by [nagios](https://nagios.uis.cam.ac.uk/nagios/).
There is also a [status page](https://tokens.uis.cam.ac.uk/adm/status) that checks the various
components of the service and returns 200 if everything is functioning normally. There is a
[liveness](https://tokens.uis.cam.ac.uk/adm/liveness) page used by the traffic manager to determine
live nodes.

## Other documentation

* [End user documentation](https://help.uis.cam.ac.uk/service/network-services/tokens)
* [Eduraom WiFi](https://help.uis.cam.ac.uk/service/wi-fi)

{{ service_management() }}
