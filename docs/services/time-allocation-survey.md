<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of Time Allocation Survey, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The University undertakes an annual Time Allocation Survey (TAS) which involves asking University
academic staff whose duties include Teaching and Research to estimate the time they spend on these
and other activities. The purpose of the TAS is to attribute costs to these activities.

The results are then used (in conjunction with other cost drivers) to ascertain the costs of
Teaching, Research and Other activity by the University and to calculate the University’s Research
full economic cost (FEC) rates.

## Service Status

Time Allocation Survey is currently live.

## Contact

Issues discovered in the service should be opened as a support ticket in [HEAT][HEAT],
either raised there directly or via email to [UIS ServiceDesk][email-servicedesk],
specifying Time Allocation Survey as the service and providing all relevant details
for further investigation by an engineer listed below.

New feature requests should be discussed with the Service Manager listed below,
who can liaise with the listed engineers in the DevOps team who owns this service.

## Source Code

Source code for Time Allocation Survey is located in a common [Gitlab Group][gitlab-tas-group].

Time Allocation Survey consists of 3 webservices: **TAS**, **TAS CMS** and **TAS Questionnaire**.

<!-- markdownlint-disable MD013 -->
| Repository                                    | Description                                               |
|-----------------------------------------------|-----------------------------------------------------------|
| [tas][gitlab-tas]                             | Time Allocation Survey Management System (TasMS)          |
| [tas-cms][gitlab-tas-cms]                     | Time Allocation Survey Content Management System (TasCMS) |
| [tas-questionnaire][gitlab-tas-questionnaire] | Time Allocation Survey Questionnaire (TasQ)               |
<!-- markdownlint-enable MD013 -->

## Technologies Used

The service is built on the following technologies:

<!-- markdownlint-disable MD013 -->
| Category                  | Technologies                                                             |
|---------------------------|--------------------------------------------------------------------------|
| Programming Language      | Java/JEE                                                                 |
| Templates & Configuration | HTML & XML                                                               |
| Frameworks                | Spring, Spring MVC, Quartz, Jersey, Jackson, JPA and Hibernate           |
| Application Servers       | Apache Tomcat mounted on Apache HTTP Server                              |
| Databases                 | MongoDB (dedicated), MySQL (dedicated) & Oracle (CHRIS HRDB integration) |
<!-- markdownlint-enable MD013 -->

## Environments

The Time Allocation Survey is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name       | Webservice Links                                                                                        | Supporting VMs                        |
|------------|---------------------------------------------------------------------------------------------------------|---------------------------------------|
| Production | [TAS][tas-production], [TAS CMS][tas-cms-production], [TAS Questionnaire][tas-questionnaire-production] | sanders (primary), timmons (failover) |
| Staging    | [TAS][tas-staging], [TAS CMS][tas-cms-staging], [TAS Questionnaire][tas-questionnaire-staging]          | jackson.internal.admin.cam.ac.uk      |
<!-- markdownlint-enable MD013 -->

### Databases

#### CHRIS Integration (HR Database)

Projects TAS & TAS Questionnaire communicate with CHRIS (HR database) using the Oracle JDBC driver
to perform data import of staff member information such as CRSID & Email address.

* **HRREPT** is a nightly copy of CHRIS HRLIVE db and is the live reporting database.
* **HRREPD** is the same nightly copy and a development reporting environment.
* Custom view **TAS_POSITIONS** is available in both HRREPT & HRREPD to expose select data.
* That custom view is used by TAS & TAS Questionnaire.

#### Dedicated Databases

Projects TAS & TAS Questionnaire use a dedicated MySQL database to store user data, survey data &
more besides.

Project TAS CMS relies on a dedicated MongoDB database to store user-created dynamic content.

The Production databases run on dedicated servers, while the Staging databases are hosted
on the same server as the webservices.

<!-- markdownlint-disable MD013 -->
| Environment | Database       | VMs                                                                               |
|-------------|----------------|-----------------------------------------------------------------------------------|
| Production  | MySQL, MongoDB | tatum + wright (primary + failover) <br>MySQL DB: webdb4.internal.admin.cam.ac.uk |
| Staging     | MySQL, MongoDB | jackson                                                                           |
<!-- markdownlint-enable MD013 -->

## Operational Documentation

The following gives an overview of how the Time Allocation Survey is deployed and maintained.

### How and where the Time Allocation Survey is deployed

TAS is deployed in both the Staging & Production environments listed above.

Deployment is done via Ansible. See the [TAS Admin][gitlab-tas-admin] project README for details.

Using the Ansible `deploy` role provided, a one-line change can specify a new version,
then the rest of deployment is fully automated to staging and/or production.

Under the hood, that process reinstalls Tomcat management scripts,
then runs the relevant "cold deploy" Tomcat management script with the fresh WAR file,
for each of the 3 WAR files copied by Ansible to the target servers
(one for each TAS webservice):

```sh
/home/tas/tomcat-scripts/<service-name>-cold-deploy.sh /path/to/the-tas-webservice.war"
```

{{ service_management() }}

[email-servicedesk]: mailto:servicedesk@uis.cam.ac.uk
[HEAT]: https://uniofcam.saasiteu.com/Account/SelectRole?NoDefaultProvider=True
[gitlab-tas-group]: https://gitlab.developers.cam.ac.uk/uis/devops/tas
[gitlab-tas]: https://gitlab.developers.cam.ac.uk/uis/devops/tas/tas
[gitlab-tas-cms]: https://gitlab.developers.cam.ac.uk/uis/devops/tas/tas-cms
[gitlab-tas-questionnaire]: https://gitlab.developers.cam.ac.uk/uis/devops/tas/tas-questionnaire
[gitlab-tas-admin]: https://gitlab.developers.cam.ac.uk/uis/devops/tas/tas-admin
[tas-production]: https://tas.admin.cam.ac.uk/tas-web
[tas-cms-production]: https://tas.admin.cam.ac.uk/tas-cms-web/login/home
[tas-questionnaire-production]: https://tas.admin.cam.ac.uk/tas-web-questionnaire/login/home
[tas-staging]: https://staging.app3.admin.cam.ac.uk/tas-web
[tas-cms-staging]: http://staging.app3.admin.cam.ac.uk/tas-cms-web/login/home
[tas-questionnaire-staging]: https://staging.app3.admin.cam.ac.uk/tas-web-questionnaire/login/home
