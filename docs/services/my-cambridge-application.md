<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the My Cambridge Application<sup>[1]</sup> - the undergraduate
applicant portal - describing its current status, where and how it's developed and deployed, and who
is responsible for maintaining it.

<small>[1] My Cambridge Application is often internally abbreviated to `MyCApp`, and has been
historically been referred to as "Admissions Portal (AP)" or "Cambridge Admissions Portal (CAP)".
You will find references to these names throughout historic documentation and the
codebase.</small>

!!! note
    This service **only** applies to undergraduate applicants. The
    [Applicant Portal for postgraduates](https://apply.postgraduate.study.cam.ac.uk/) is an
    unrelated web application.

## Service Description

My Cambridge Application provides a web application for undergraduate applicants to supply
supplementary information as part of their university application, and to track the progress of
their university application.

## Service Status

My Cambridge Application is currently live.

!!! note
    The production environment's **Data Service API URL** contains "alpha" because the project
    started out following the [GDS agile phases](https://www.gov.uk/service-manual/agile-delivery),
    but the current phase hasn't been advanced from alpha.

## Contact

Technical queries and support should be directed to [the DevOps Hopper
team](mailto:uis-devops-hopper@lists.cam.ac.uk) and will be picked up by a member of the team
working on the service.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/operational-support-and-helpdesk).

## Environments

My Cambridge Application is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name         | <div style="width:290px">Usage</div>                                                    | Public Application URL                                                                     | Data Service API URL                                                                                                                   | Payment Callback API URL                                                                 |
| ------------ | --------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------- |
| Production   | Live service                                                                            | [https://apply.undergraduate.study.cam.ac.uk](https://apply.undergraduate.study.cam.ac.uk) | [https://api.apps.cam.ac.uk/undergraduate-admissions/v1alpha1](https://api.apps.cam.ac.uk/undergraduate-admissions/v1alpha1)           | [https://wpmapp.prod.ap.gcp.uis.cam.ac.uk/](https://webapp.int.ap.gcp.uis.cam.ac.uk/)    |
| Integration  | End-to-end testing getting as close to production-like as possible                      | [https://webapp.int.ap.gcp.uis.cam.ac.uk](https://webapp.int.ap.gcp.uis.cam.ac.uk)         | [https://api.apps.cam.ac.uk/undergraduate-admissions-int/v1alpha1/](https://api.apps.cam.ac.uk/undergraduate-admissions-int/v1alpha1/) | [https://wpmapp.int.ap.gcp.uis.cam.ac.uk/](https://wpmapp.int.ap.gcp.uis.cam.ac.uk/)     |
| Regression   | Automated testing                                                                       | [https://webapp.reg.ap.gcp.uis.cam.ac.uk](https://webapp.reg.ap.gcp.uis.cam.ac.uk)         | [https://api.apps.cam.ac.uk/undergraduate-admissions-reg/v1alpha1/](https://api.apps.cam.ac.uk/undergraduate-admissions-reg/v1alpha1/) | [https://wpmapp.reg.ap.gcp.uis.cam.ac.uk/](https://wpmapp.reg.ap.gcp.uis.cam.ac.uk/)     |
| UAT          | Content and design and user acceptance testing                                          | [https://webapp.uat.ap.gcp.uis.cam.ac.uk/](https://webapp.uat.ap.gcp.uis.cam.ac.uk/)       | n/a                                                                                                                                    | [https://wpmapp.uat.ap.gcp.uis.cam.ac.uk/](https://wpmapp.uat.ap.gcp.uis.cam.ac.uk/)     |
| Test         | Manual testing before deployment to production and production-like environments         | [https://webapp.test.ap.gcp.uis.cam.ac.uk/](https://webapp.test.ap.gcp.uis.cam.ac.uk/)     | [https://api.apps.cam.ac.uk/undergraduate-admissions-test/v1alpha1](https://api.apps.cam.ac.uk/undergraduate-admissions-test/v1alpha1) | [https://wpmapp.test.ap.gcp.uis.cam.ac.uk/](https://wpmapp.test.ap.gcp.uis.cam.ac.uk/)   |
| Development  | Development playground                                                                  | [https://webapp.devel.ap.gcp.uis.cam.ac.uk/](https://webapp.devel.ap.gcp.uis.cam.ac.uk/)   | [https://api.apps.cam.ac.uk/undergraduate-admissions-dev/v1alpha1](https://api.apps.cam.ac.uk/undergraduate-admissions-dev/v1alpha1)   | [https://wpmapp.devel.ap.gcp.uis.cam.ac.uk/](https://wpmapp.devel.ap.gcp.uis.cam.ac.uk/) |
<!-- markdownlint-enable MD013 -->

### Environment details

<!-- markdownlint-disable MD013 -->
| Environment | Deployment Frequency       | User Access | Purpose                                                                                                                                        |
|-------------|----------------------------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| Production  | End of each sprint         |             | An environment where code changes are tested on live user traffic.                                                                             |
| Integration | End of each sprint         |             | An environment to test various interfaces and the interactions between integrated components or systems.                                       |
| Regression  | On merge to default branch |             | An environment to test that defects have not been introduced or uncovered in unchanged areas of the software, as a result of the changes made. |
| UAT         |                            |             | An environment to determine if intended users, customers or other authorised entities accept the system.                                       |
| Test        | On merge to default branch |             | An environment to evaluate if a component or system satisfies functional requirements.                                                         |
| Development | On demand via pipeline job |             | An environment to check any development code changes without affecting end-users.                                                              |
<!-- markdownlint-enable MD013 -->

### Integrations

<!-- markdownlint-disable MD013 -->
| MyCApp env  | CamSIS env (applicant feed)           | Photo API env                            |
|-------------|---------------------------------------|------------------------------------------|
| Production  | <https://camsis.cam.ac.uk/intb_prod/> | <https://api.apps.cam.ac.uk/photo/>      |
| Integration | <https://camsis.cam.ac.uk/intb_reg/>  | <https://api.apps.cam.ac.uk/photo-test/> |
| UAT         | gs://ap-testdata-uat-76f5195d         | none                                     |
| Regression  | gs://ap-testdata-regression-052d7463  | <https://api.apps.cam.ac.uk/photo-test/> |
| Test        | gs://ap-testdata-staging-26edd96d     | <https://api.apps.cam.ac.uk/photo-test/> |
| Development | gs://ap-testdata-development-f1130a5d | none                                     |
<!-- markdownlint-enable MD013 -->

The mapping between MyCApp environments the Photo API is defined in [ap-deployment/workspace-specific-settings.yml](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment/-/blob/master/workspace-specific-settings.yml).

`ap-backend` receives the applicant feed URL as follows:

1. An [EXTRA_SETTINGS_URL](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment/-/blob/master/apiapp.tf#L95)
   points to two environment-specific artefacts:

   - an `apiapp-secret-settings` secret in GCP Secret Manager
   - an `apiapp-settings` YAML config file in GCP Cloud Storage

1. Both are fetched by `ap-backend` on startup and their contents read to populate the
   [load_settings](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-backend/-/blob/master/src/app/dependencies.py#L69)
   FastAPI dependable.
1. The location of the applicant feed is now available as either

   - `load_settings.applicants_source.camsis.url` for a CamSIS feed.
   - `load_settings.applicants_source.test_source.url` for a feed mocked by a JSON file.

### Google Cloud Platform

The GCP console pages for managing the infrastructure of each component of the deployment are:

<!-- markdownlint-disable MD013 -->
| Name         | Public Application Static Assets                                                            | Public Application Backend                                                       | Internal Portal Backend and Payment Callback API                                | Frontend Cloud Functions                                                                     | Data service backend                                                                   |
| ------------ | ------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- | ------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| Production   | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-prod-43267ef1)  | [Firebase](https://console.firebase.google.com/project/ap-prod-43267ef1)         | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-prod-43267ef1)  | [Cloud Functions](https://console.cloud.google.com/functions/list?project=ap-prod-43267ef1)  | n/a                                                                                    |
| Integration  | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-int-84c75e1b)   | [Firebase](https://console.firebase.google.com/project/ap-int-84c75e1b)          | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-int-84c75e1b)   | [Cloud Functions](https://console.cloud.google.com/functions/list?project=ap-int-84c75e1b)   | [GCP Cloud Run](https://console.cloud.google.com/run?project=albatross-int-9d74dc7d)   |
| Regression   | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-reg-0720d5d8)   | [Firebase](https://console.firebase.google.com/project/ap-reg-0720d5d8/overview) | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-reg-0720d5d8)   | [Cloud Functions](https://console.cloud.google.com/functions/list?project=ap-reg-0720d5d8)   | [GCP Cloud Run](https://console.cloud.google.com/run?project=albatross-reg-a12f71f6)   |
| UAT          | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-uat-8c323712)   | [Firebase](https://console.firebase.google.com/project/ap-uat-8c323712)          | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-uat-8c323712)   | [Cloud Functions](https://console.cloud.google.com/functions/list?project=ap-uat-8c323712)   | n/a                                                                                    |
| Test         | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-test-3d41aff5)  | [Firebase](https://console.firebase.google.com/project/ap-test-3d41aff5)         | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-test-3d41aff5)  | [Cloud Functions](https://console.cloud.google.com/functions/list?project=ap-test-3d41aff5)  | [GCP Cloud Run](https://console.cloud.google.com/run?project=albatross-test-1a115fe1)  |
| Development  | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-devel-dff67399) | [Firebase](https://console.firebase.google.com/project/ap-devel-dff67399)        | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-devel-dff67399) | [Cloud Functions](https://console.cloud.google.com/functions/list?project=ap-devel-dff67399) | [GCP Cloud Run](https://console.cloud.google.com/run?project=albatross-devel-7a2c4f75) |
<!-- markdownlint-enable MD013 -->

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?project=ap-meta-296bac5a).

## Source code

The source code for My Cambridge Application is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository                                                                                                                            | Description                                                                                       |
| ------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| [Portal Frontend](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-frontend)                    | The source code for the application frontend and supporting cloud functions.                      |
| [Internal Portal Backend](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-backend)             | The source code for the internal backend service and WPM callback API                             |
| [Portal Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment) | The Terraform infrastructure code for deploying the portal to GCP                                 |
| [Data service](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/albatross/albatross-backend)                        | The source code for the data service which provides an API on the [API Gateway](./api-gateway.md) |
| [Data service Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/albatross/albatross-deployment)           | The Terraform infrastructure code for deploying the data service to GCP                           |
<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the technologies My Cambridge Application is built on.

| Category             | Language   | Framework(s)                |
| -------------------- | ---------- | --------------------------- |
| Frontend             | JavaScript | React 17, Gatsby, Firestore |
| Cloud Functions      | JavaScript | Nodejs 16                   |
| Internal Backend API | Python 3.8 | FastAPI                     |
| WPM Callback API     | Python 3.8 | FastAPI                     |
| Data Service API     | Python 3.9 | FastAPI, PostgreSQL 14      |

## Operational documentation

The following gives an overview of how My Cambridge Application is deployed and maintained.

### How and where My Cambridge Application is deployed

My Cambridge Application infrastucture is deployed using Terraform, with releases of the frontend
and service-to-service backend API deployed by the GitLab CD pipelines associated with the
[infrastructure deployment
repository](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to deploy the Admissions
Portal.

### Deployment schedule

All environments are deployed to at sprint end.
However, there are some exceptions:

- Development is only deployed to at sprint end, to keep it up to date, if there is no one currently
  using it for feature testing. This manual feature testing deployments are only done when a larger
  piece of functionality needs further testing, after manual development before or during a review.
- Staging is deployed to on merge to master.

### User access management

| Environment  | User access management                                          |
| ------------ | --------------------------------------------------------------- |
| Production   | Firebase Auth - Accounts created Live CamSIS feed sync          |
| Integration  | Firebase Auth - Fake CamSIS feed sync into Albatross and MyCApp |
| Regression   | Firebase Auth - API calls to create and tear down               |
| User testing | Firebase Auth - Excel spreadsheet sync to Albatross and MyCApp  |
| Staging      | Firebase Auth - JSON file import to Albatross and MyCApp        |
| Development  | Firebase Auth - JSON file import to Albatross and MyCApp        |

### Monitoring

[GCP Cloud Monitoring](https://console.cloud.google.com/monitoring?project=ap-meta-296bac5a)
is used for tracking the health of applications in the environments and sending alert emails when
problems are detected.

### Debugging

TBD

### Displaying service alert messages

My Cambridge Application supports the display of service alert messages, to target one or more pages
and/or user groups with information relevant to any ongoing live service issues.

For more information on how to configure these alerts, see
[alerts](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-frontend/-/blob/master/docs/alerts.md)
in the `ap-frontend` repository.

### Specifications

- [Admissions Portal: High-Level Specification](https://docs.google.com/document/d/1UJFCdnS06PpmILPfj75T0TTqnLQ7ZNEm-iIZxRVGXKU)
- [Admissions Portal: Backend Specification](https://docs.google.com/document/d/10aHuF78vrxITQkCLsUUUOmiodZOu7mvCI2_dBYbYgpk)
- [Admissions Portal: Frontend Specification](https://docs.google.com/document/d/1TKutBiVlW076sjX-EkM_nOTdOlZvSgssHDb4Y6czKPU)
- [Digital Admissions Project Index](https://docs.google.com/document/d/19j0wYtijlYHv3AYGKnSFSNASCSJT4saFEWFhQc3chgs)

{{ service_management() }}
