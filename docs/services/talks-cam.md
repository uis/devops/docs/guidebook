<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of talks.cam, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

talks.cam is the University's platform for managing, disseminating, and advertising talks organised
by departments, research groups, societies, and individuals (although *not* lectures associated with
taught courses). Features that set it apart from being a conventional University talks-listing
service include:

* The content is entirely user-generated, content-management is deliberately de-centralised
* The content can be consumed in numerous ways beyond directly browsing the website (e.g. feeds,
  embedding, email reminders)
* Talks can be aggregated into numerous Lists, and Lists themselves can be aggregated into Lists
* The target audience includes the wider local community beyond the University
* Public archiving: old talks are deliberately preserved as a snapshot of the University's
  intellectual activity

Also see:

* [Aims of talks.cam](https://talks.cam.ac.uk/document/Aims%20of%20talks.cam) as written by the
  original founders.
* [Internal UIS Business Case](https://confluence.uis.cam.ac.uk/display/TAL/Business+Case)

## Service Status

talks.cam is currently live.

The current talks.cam system is reaching the end of its life, and in the medium-term, UIS is
investigating how best to continue providing a largely equivalent service via a more modern and
sustainable platform, before decommissioning the current system. In the meantime, the current system
is not being actively developed.

## Contact

Queries about the **content** on the site itself (*i.e. queries about Talks, Lists of Talks, and
anything to do with the actual events that they relate to*) should usually be sent to the **content
owners directly, NOT to UIS** in the first instance - any logged-in user can view the contact
details for a Talk or List by clicking the contact person's name, as displayed on Talk and List
pages (look for ***If you have a question about this [talk|list], please contact `organiser-name`***
on the page). Anybody with a Raven account can login to Talks.cam (and thus view such contact
details).

In the case of content that is **obviously not legitimate**, please contact
[webmaster@talks.cam.ac.uk](mailto:webmaster@talks.cam.ac.uk).

Technical queries and support should be directed to
[webmaster@talks.cam.ac.uk](mailto:webmaster@talks.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[webmaster@talks.cam.ac.uk](mailto:webmaster@talks.cam.ac.uk) rather than reaching out to team
members directly.

With regards issues discovered in the service or new feature requests, due to the current status of
the service, only the most critical issues will be acted upon. All other requests will either be
parked or noted for inclusion in a future replacement system. Bearing this in mind, please send
critical issues to [webmaster@talks.cam.ac.uk](mailto:webmaster@talks.cam.ac.uk).

## Environments

talks.cam is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://talks.cam.ac.uk](https://talks.cam.ac.uk) | talks-web1.internal.admin.cam.ac.uk<br/> talks-web2.internal.admin.cam.ac.uk |
|             | Database | talks-sql.internal.admin.cam.ac.uk<br/> The [UIS Servers and Storage Team (infra-sas)](https://confluence.uis.cam.ac.uk/display/ISC/Infrastructure+Support+Contacts) have details of the two DB servers backing this single hostname |
<!-- markdownlint-enable MD013 -->

Note that access to the supporting VMs is restricted to a small subset of the ACN.

## Source code

Please see further information on
[GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/talks.cam/legacy-talks.cam/documentation/-/blob/main/README.md)
visible to UIS Staff.

## Technologies used

The following gives an overview of the technologies talks.cam is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Ruby  | Rails |
| DB     | MySQL | n/a   |

Also see further information on
[GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/talks.cam/legacy-talks.cam/documentation/-/blob/main/README.md)
visible to UIS Staff.

## Operational documentation

Please see further information on
[GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/talks.cam/legacy-talks.cam/documentation/-/blob/main/README.md)
visible to UIS Staff.

{{ service_management() }}

* Please see further information on
  [GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/talks.cam/legacy-talks.cam/documentation/-/blob/main/README.md)
  visible to UIS Staff.
* The [UIS Servers and Storage Team
  (infra-sas)](https://confluence.uis.cam.ac.uk/display/ISC/Infrastructure+Support+Contacts) are
  also able to help with infrastructure-level incidents.
