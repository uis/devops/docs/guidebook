<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of PostBox, describing its current status, where and how it's developed
and deployed, and who is responsible for maintaining it.

!!! danger
    PostBox uses technology, coding standards, and deployment methodologies which
    are not endorsed by the TDA and not standard practice for the DevOps team.

## Service Description

PostBox provides an API to allow users to subscribe and unsubscribe from weekly job notifications based
on configurable job criteria. It is only invoked by the [Job Opportunities](./job-opportunities.md)
application which implements the UI to allow users to create and manage subscriptions.

## Service Status

PostBox is currently `live`.

## Contact

Technical queries and support should be directed to
[hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be reported to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)

## Environments

PostBox is currently deployed to the following environments:

| Name        | URL
| ----------- | ------------------
| Production  | [https://webapp.prod.postbox.gcp.uis.cam.ac.uk/](https://webapp.prod.postbox.gcp.uis.cam.ac.uk/)
| Staging     | [https://webapp.test.postbox.gcp.uis.cam.ac.uk/](https://webapp.test.postbox.gcp.uis.cam.ac.uk/)
| Development | [https://webapp.devel.postbox.gcp.uis.cam.ac.uk/](https://webapp.devel.postbox.gcp.uis.cam.ac.uk/)

## Source Code

The source code for PostBox is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/hr/postbox/postbox-webapp) | The source code for the main application server
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/postbox/postbox-infrastructure) | The Terraform infrastructure code for deploying the application server to GCP

The source code for PostBox has not been fully audited and does not follow existing DevOps practices,
therefore it is only available to DevOps team members actively working on the project.

## Technologies Used

The following gives an overview of the technologies PostBox is built on.

| Category | Language | Framework(s)
| -------- | -------- | ---------
| Server | Java | Spring / Jersey
| DB | MySQL | Hibernate

## How and where Postbox is deployed

Deployment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Deploying a new release

Making a new release of the application is done via [release
automation](site:explanations/git-gitlab/gitlab-release-automation).
In short: each commit to the web app's main branch builds and pushes a Docker image to GCR.
An MR can then be raised within the infrastructure repository to deploy a given image by SHA.
Once merged, the main GitLab pipeline will allow deployment to production via the "play" buttons
in the CI pipeline. An automated deployment to staging will be completed as part of this pipeline.

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

### Access Management

Access management is managed internally within the application, with the application holding a
list of clients who are able to access the API alongside their credentials.

{{ service_management() }}
