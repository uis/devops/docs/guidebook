<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Web Recruitment System, describing its current
status, where and how it's developed and deployed, and who is responsible for maintaining it.

!!! danger
    Web Recruitment uses technology, coding standards, and deployment methodologies which
    are not endorsed by the TDA and not standard practice for the DevOps team.

## Service Description

The Web Recruitment System allows applicants to apply for vacancies created by the
[Recruitment Administration System](./recruitment-administration-system.md), and allows
recruiters to manage and respond to applications. Data is synchronized to the central
HR system (CHRIS) and from the
[Recruitment Administration System](./recruitment-administration-system.md) via the
[HR Integration App](./hr-integration-app.md).

## Service Status

The Web Recruitment System is currently `live`.

## Contact

Technical queries and support should be directed to
[hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be reported to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)

Issues related to the Oracle Database which serves as the primary data store for the application should
be raised with the [DBA Team](mailto:uisdba@admin.cam.ac.uk).

## Environments

The Web Recruitment System is currently deployed to the following environments:

| Name                                               | URL                  | Supporting VMs  |
| -------------------------------------------------  | -------------------- | --------------- |
| Production                                         | [https://hrsystems.admin.cam.ac.uk/recruit-ui/](https://hrsystems.admin.cam.ac.uk/recruit-ui/) | hr-live1.internal.admin.cam.ac.uk, hr-live2.internal.admin.cam.ac.uk |
| Training (only available within the admin network) | [https://training.hrsystems.admin.cam.ac.uk/recruit-ui/](https://training.hrsystems.admin.cam.ac.uk/recruit-ui/) | hr-train1.internal.admin.cam.ac.uk, hr-train2.internal.admin.cam.ac.uk |
| Staging (only available within the admin network)  | [https://testing.hrsystems.admin.cam.ac.uk/recruit-ui/](https://testing.hrsystems.admin.cam.ac.uk/recruit-ui/) | hr-test1.internal.admin.cam.ac.uk, hr-test2.internal.admin.cam.ac.uk |
| Development                                        | [https://development.hrsystems.admin.cam.ac.uk/recruit-ui/](https://development.hrsystems.admin.cam.ac.uk/recruit-ui/) | hr-dev1.internal.admin.cam.ac.uk |

## Source Code

The source code for the Web Recruitment System is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/hr/wr/wrs-webapp) | The source code for the main application server |
| [On-Prem Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure) | The Ansible configuration for the existing on-prem deployment. |
| [Cloud Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/wr/wrs-infrastructure) | The WIP Terraform infrastructure code for deploying the application server to GCP |

The source code for the Web Recruitment System has not been fully audited and does not follow
existing DevOps practices, therefore it is only available to DevOps team members actively working
on the project.

## Technologies Used

The following gives an overview of the technologies the service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web App | Java 8 | JSF2, PrimeFaces 6, Spring 4 |
| Server | Tomcat 9 | |
| Database | Oracle 19c  | Hibernate 4 |

## Operational documentation

!!! note

    Information about the operation of the existing on-prem deployment is contained within
    the [README.md](https://gitlab.developers.cam.ac.uk/uis/devops/hr/wr/wrs-webapp/-/blob/main/README.md?ref_type=heads)
    of the web application.

The following gives an overview of how the Web Recruitment System will be deployed and maintained within
GCP:

### Deployment

The on-premise deployment is managed by the [HR Apps Ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure#deployment)
repository. Traffic to the web-application is load balanced via the UIS Load Balancer, which additionally
provides SSL termination.

Deployment to the GCP environment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

### Access Management

Access management is managed internally within the application, with the application holding a
list of users who are able to access the admin UI and their permissions within the Oracle database.
Therefore access needs to be granted by an existing super-user of the application.

{{ service_management() }}
