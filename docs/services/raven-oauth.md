---
title: OAuth2
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Raven OAuth2 service, describing its current
status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Service Description

The Raven service provides a self-service, web-based interactive sign in service
for the University. It has several parts. Raven OAuth2 provides a standard [OAuth
2.0](https://oauth.net/2/) interface for sites around the University.

This is provided by Google and conforms to the [OpenID Connect](https://openid.net/connect/)
specification. This also provides improved security using Google's
[2-step verification](https://www.google.com/landing/2step/).

Any application supporting sign in with Google can make use of Raven OAuth2.
The `cam.ac.uk` Google Workspace is configured to authenticate users via the
[Raven Core Authenticator](https://core-idp.raven.cam.ac.uk/) (a SAML2 IdP).

There is a [dedicated documentation site](https://docs.raven.cam.ac.uk/) for
Raven including its OAuth2 interface.

## Service Status

The Raven OAuth2 service is currently live. There are no plans to decommission
the service.

## Contact

Technical queries and support should be directed to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) and will be
picked up by a member of the team working on the service. To ensure that you
receive a response, always direct requests to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues in the
[Raven Core Authenticator](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/g-suite-authenticator)
or [Raven Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure)
projects (both DevOps only).

## Environments

Raven OAuth2 is currently deployed to the following environments:

| Name        | Main Application URL | GCP Project      |
| ----------- | -------------------- | ---------------- |
| Production  | [https://core-idp.raven.cam.ac.uk/](https://core-idp.raven.cam.ac.uk/) | [Raven Core IdP - production](https://console.cloud.google.com/home/dashboard?project=raven-core-prod-3bdf4baa) |
|             | [https://ravencore.prod.raven-core.gcp.uis.cam.ac.uk/](https://ravencore.prod.raven-core.gcp.uis.cam.ac.uk/) | |
| Staging     | [https://test.core-idp.raven.cam.ac.uk/](https://test.core-idp.raven.cam.ac.uk/) | [Raven Core IdP - staging](https://console.cloud.google.com/home/dashboard?project=raven-core-test-7167dedf) |
|             | [https://ravencore.test.raven-core.gcp.uis.cam.ac.uk/](https://ravencore.test.raven-core.gcp.uis.cam.ac.uk/) | |
| Development | [https://ravencore.devel.raven-core.gcp.uis.cam.ac.uk/](https://ravencore.devel.raven-core.gcp.uis.cam.ac.uk/) | [Raven Core IdP - development](https://console.cloud.google.com/home/dashboard?project=raven-core-devel-0d19e404) |

All environments access a meta project
([Raven Core Idp meta](https://console.cloud.google.com/home/dashboard?project=raven-core-meta-71b30c4c))
for shared secrets and monitoring.

## Notification channel(s) for environments

| Environment | Display name | Email |
| ----------- | ------------ | ----- |
| Production  | Raven core IdP - Wilson DevOps team email channel | **<devops-wilson@uis.cam.ac.uk>** |
| Staging     | Raven core IdP - Wilson DevOps team email channel | **<devops-wilson@uis.cam.ac.uk>** |

## Source code

Source code for Raven OAuth2 is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository | Description |
| ---------- | ----------- |
| [Raven Core Authenticator](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/g-suite-authenticator)<sup>1</sup> | Containerised Apache2 frontend which handles interactive authentication |
| [Raven Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/)<sup>1</sup> | Terraform configuration for infrastructure and deployment |
<!-- markdownlint-enable MD013 -->

<sup>1</sup> DevOps only

## Technologies used

The following gives an overview of the technologies that Raven OAuth2 is built
on.

| Category       | Language   | Framework(s) |
| -------------- | ---------- | ------------ |
| Authenticator  | Python 3.7 | Django 2.2   |
|                |            | [djangosaml2idp](https://djangosaml2idp.readthedocs.io/en/latest/) |
| GCP deployment | Terraform  |              |

## Operational documentation

There is a dedicated [operational
documentation folder](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/tree/master/doc)
in the [infrastructure Gitlab project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/)
(DevOps only).

### How and where the service is deployed

The Raven Core infrastructure is deployed using Terraform, with releases
of the authenticator application deployed by the GitLab CD pipelines associated
with the [infrastructure Gitlab project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/)
(DevOps only).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to
deploy the Authenticator App.

### Monitoring

The monitoring and alerting system is based on [Cloud Monitoring](https://cloud.google.com/monitoring).
Alert policies and metrics can be views in the
[Raven Core IdP meta project](https://console.cloud.google.com/monitoring/alerting?project=raven-core-meta-71b30c4c)
(DevOps only).

Our standard 'webapp' alerts have been configured:

* Service uptime check from various geographic regions
* SSL expiry checks

### Debugging

See the [Raven Core Authenticator](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/g-suite-authenticator/)
project (DevOps only) for details on how to deploy a local development instance.

{{ service_management() }}
