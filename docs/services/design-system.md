<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the `Design System`, describing its current
status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Description

The Design System (name unconfirmed) is one strand of the DPP (Digital Presence
Programme). It is a collection of resources enabling developers and content
editors to more easily create web publications, web apps, and websites that are
consistent with the university's branding and style guidelines.

It consists of:

- A ReactJS component library.
- An API and example documentation website.

Expected future extensions:

- A website for content editors, with guidelines on content style.
- An assets download section, containing images and other content.
- A Gatsby website template.

## Status

The Design System is currently `pre alpha`.

There is not, currently, a fixed timeline for releases as the development of the
Design System is being done alongside the Undergraduate Admissions and
University Card System projects. However it is hoped that an alpha release will
be available around Autumn 2021.

The Design System is likely to factor into future changes to the internal
SharePoint and external facing Drupal websites.

Various documents and websites are used to track the progress of the Design
System and DPP as a whole, these are available via the [DPP Sharepoint Site](https://universityofcambridgecloud.sharepoint.com/sites/ContentCommunity).

Some other documents are also used:

| Document  | Description          |
| ----------- | ------------------ |
| [Working Document](https://universityofcambridgecloud-my.sharepoint.com/:w:/r/personal/cm896_cam_ac_uk/_layouts/15/guestaccess.aspx?e=Oq25DO&share=EbGomPeO0qNPuEpt4GnpVr0BkYZt4qRTt-F1_53w8snIGg) | Goals, Scope, Progress on the Design System |
| [Miro Board](https://miro.com/app/board/o9J_knzrgcc=/) | Used for the weekly team meetings |

## Contact

Issues discovered in the Design System or new feature requests should be opened
as [GitLab issues](https://gitlab.developers.cam.ac.uk/digital/design-system/design-system/-/issues)

Urgent technical queries and support should be directed to
`devops+design-system@uis.cam.ac.uk` and will be picked up by a member of the
team working on the service. To ensure that you receive a response, always
direct requests to `devops+design-system@uis.cam.ac.uk` rather than reaching out
to team members directly.

## Environments

The `Design System` is currently deployed to the following environments:

| Name           | URL                | Supporting VMs  |
| -------------- | ------------------ | --------------- |
| NPM Package    | [@ucam/design-system](https://www.npmjs.com/package/@ucam/design-system) |  |
| Developer Docs | [Not yet deployed](https://gitlab.developers.cam.ac.uk/digital/design-system/design-system/-/issues/21) | GitLab Pages |

## Source code

Projects relating to the `Design System` are in a
[GitLab Group](https://gitlab.developers.cam.ac.uk/digital/design-system).

| Repository  | Description        |
| ----------- | ------------------ |
| [Design System](https://gitlab.developers.cam.ac.uk/digital/design-system/design-system) | A mono-repo containing the Design System and its Documentation Website |

## Technologies used

The following gives an overview of the technologies `Design System` is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Design System | [Typescript](https://www.typescriptlang.org/) | [Material-UI](https://material-ui.com/) |
| API Documentation | [Typescript](https://www.typescriptlang.org/) | [Gatsby](https://www.gatsbyjs.com/) |

## Operational documentation

The following gives an overview of how the `Design System` is deployed and
maintained

### How and where the `Design System` is deployed

The Design System is published to [NPM](https://www.npmjs.com/package/@ucam/design-system),
by the [ucam-devops-bot](https://www.npmjs.com/~ucam-devops-bot) (Credentials in
1Password Team Vault).

GitLab pipelines attempt to publish a release whenever there is a change on the
master branch. This only succeed if the package.json version has been updated.
Upon successful publish, the commit is tagged with the version number.

### Deploying a new release

To publish a new release, merge a commit to master with a new package.json
`version` number.

### Debugging

Debugging and general development instructions are available in the [README](https://gitlab.developers.cam.ac.uk/digital/design-system/design-system/-/tree/master#university-of-cambridge-design-system).

{{ service_management() }}
