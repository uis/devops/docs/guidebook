<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Equipment Sharing, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

This is the University of Cambridge research facilities and equipment searchable database.
This allows University members to search entries in the Research Facilities and Equipment Database

## Service Status

The Equipment Sharing is currently live.

## Contact

Technical queries and support should be directed to development team and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to development team rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/equipshare/equipshare).

## Environments

The Equipment Sharing is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | admin app: [https://www.equipment.admin.cam.ac.uk/esd-admin/](https://www.equipment.admin.cam.ac.uk/esd-admin/)<br>web front-end : [https://www.equipment.admin.cam.ac.uk/](https://www.equipment.admin.cam.ac.uk/)  | equipment-sharing-live1.internal.admin.cam.ac.uk<br>equipment-sharing-live2.internal.admin.cam.ac.uk     |
| Internal     | [http://equipment-sharing-live1.internal.admin.cam.ac.uk:8080/esd-admin/](http://equipment-sharing-live1.internal.admin.cam.ac.uk:8080/esd-admin/)   | equipment-sharing-staging1.internal.admin.cam.ac.uk |
<!-- markdownlint-enable MD013 -->

## Source code

The source code for the Equipment sharing is can be found on  Gitlab repository:

| Repository  | Description |
| ----------- | ------------------ |
| Application  | [https://gitlab.developers.cam.ac.uk/uis/devops/equipshare/equipshare](https://gitlab.developers.cam.ac.uk/uis/devops/equipshare/equipshare) |

## Technologies used

The following gives an overview of the technologies the Equipment Sharing is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web       | Java    | Spring    |
| Front end    |PHP   | Symfony,  Guzzle, Capifony Composer |

## Operational documentation

The following gives an overview of how the Equipment Sharing is deployed and maintained.

### How and where the Equipment Sharing is deployed

Deploying web service, copy  equipshare .war file to tomcat installation location: `/opt/apache-tomcat-8.5.73/webapps`
restart tomcat : `systemctl restart tomcat.service`

## Deploying a php front end

Through Capifony on saw. (For example, inside /home/httpd/capifony/equipment execute “cap prod deploy”.)

### Monitoring

Logging - locations, levels
Apps logs (Monolog) to /home/httpd/www.equipment.admin.cam.ac.uk/shared/app/logs. Probably should be
log-rotated as it will fill up over time.

### Other operation issues

As the Research Facilities and Equipment Database web service is a bit broken, there are some
workarounds in the code:

* When something doesn’t exist a 200 response is returned with an empty body, BrokenResponseListener
  converts this in a 404.
* A load of data in the database is broken (eg phone numbers like 00000 000000, email addresses like
  <unknown@cam.ac.uk>), so the `MisdEquipmentSharingDataCleaningBundle` tries to tidy it up.

See also
[http://jira.devtools.admin.cam.ac.uk/browse/ESWEB](http://jira.devtools.admin.cam.ac.uk/browse/ESWEB).

{{ service_management() }}

Contact [UIS Servers and Storage](https://www.lookup.cam.ac.uk/group/uis-infra-sas)
about any server issues.
