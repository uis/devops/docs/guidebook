<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the University Payment System, describing its current status, where
and how it's developed and deployed, and who is responsible for maintaining it.

!!! danger
    The University Payment System uses technology, coding standards, and deployment methodologies
    which are not endorsed by the TDA and not standard practice for the DevOps division. Support is
    provided on a "best effort" basis and there is no expectation that new features will be developed
    within the existing system.

## Service Description

UPS allows individuals within the University to make a one off payment to people. Once approved by
finance, payments are loaded into CHRIS and paid via the payroll module.

## Service Status

The University Payment System is **live** but users are being migrated to other services, with the
aim to decommission UPS in **February 2026**.

## Contact

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/university-payment-system-ups/ups).

## Environments

The University Payment System is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  |[https://hrsystems.admin.cam.ac.uk/ups](https://hrsystems.admin.cam.ac.uk/ups) | ups-live1.internal.admin.cam.ac.uk |
| Staging     |[https://testing.hrsystems.admin.cam.ac.uk/ups](https://testing.hrsystems.admin.cam.ac.uk/ups) | ups-test1.internal.admin.cam.ac.uk |

## Source code

The source code for the University Payment System is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/sysdev/hr-systems/university-payments-system) |  The source code for the main application server |
| [Ansible Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/ups-ansible-infrastructure) | The ansible which manages the on-premise deployment |

## Technologies used

The following gives an overview of the technologies the University Payment System is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
|Application  | Java 8 |Play Framework 1.5, Hibernate 5.2.10, Lucene 5.3, Apache Ivy 2.4.0  |

## Operational documentation

The following gives an overview of how the University Payment System is deployed and maintained.

### How and where the University Payment System is deployed

Play! Framework can automatically generate a WAR file for deployment. Batch files (see below) have
been created to automate this process.

#### Development

- Execute `{Project Working Copy}/build/create_war_dev.bat`
- Output is to `{Project Working Copy}/build/dist/ups_dev.war`

### Deployment

The on-premise deployment is managed by the [UPS Ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/ups-ansible-infrastructure)
repository.

#### Load Balancer

Round-robin load balancing of HTTP traffic with sticky sessions (stickiness controlled by load
balancer). SSL offloading is also performed by the load balancer.

HTTP Headers set (for use by application). See Wikipedia for a full list of HTTP Headers.

`X-Forwarded-Proto` - a de facto standard for identifying the originating protocol of an HTTP
request, since a reverse proxy (load balancer) may communicate with a web server using HTTP even if
the request to the reverse proxy is HTTPS.

`X-Forwarded-For` - a de facto standard for identifying the originating IP address of a client
connecting to a web server through an HTTP proxy or load balancer.

{{ service_management() }}
