<!-- markdownlint-disable MD041 -->
{{ service_title() }}

!!! note
    Previously this application was refereed to as the Professorial Pay Review (PPR)
    submission portal. The name of this application changed in 2024 to match the new
    name of the reward scheme which it caters for.

This page gives an overview of the ACP-12 Submission Portal, describing its current status,
where and how it is developed and deployed, and who is responsible for maintaining it.

!!! danger
    The ACP-12 Submission Portal uses technology, coding standards, and deployment methodologies
    which are not endorsed by the TDA and not standard practice for the DevOps Division.

## Service Description

The Academic Career Pathways Grade 12 (ACP-12) reward scheme is a biannual process to allow
University professors to apply for a pay review. The submission portal allows a professor to
submit their initial application, with the application details being held within the
[Orchestra component of IRS](./integrated-reward-system.md) and references collected and
reviewed within the [Referee component of IRS](./integrated-reward-system.md).

## Service Status

The ACP-12 Submission Portal is currently live, although is planned to be re-implemented
prior to its re-use within the next iteration of the ACP-12 scheme, in 2026.

## Contact

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/ppr/ppr2022).

## Environments

The PPR service is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| [Production](https://ppr-submission.admin.cam.ac.uk)  |  |  |
|             | Web Server         | spt-live-web1.internal.admin.cam.ac.uk |
|             | Application Server | spt-live-app1.internal.admin.cam.ac.uk |
|             | Database           | spt-live-db1.internal.admin.cam.ac.uk |
| [Staging](https://uat-ppr-submission.admin.cam.ac.uk)     | |  |
|             | Web Server         | spt-test-web1.internal.admin.cam.ac.uk |
|             | Application Server | spt-test-app1.internal.admin.cam.ac.uk |
|             | Database           | spt-test-db1.internal.admin.cam.ac.uk |

## Source code

Source code for both the Application Submission and SAP Submission applications
can be found in [GitLab PPR 2022](https://gitlab.developers.cam.ac.uk/uis/devops/ppr/ppr2022).

## Technologies used

The following gives an overview of the technologies which PPR is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Application Submission | C# 6 | ASP.NET 4 |
| SAP Submission | C# 6 | SharePoint Server-side Object Model |

## Operational documentation

Documentation for development and deployment is available
at [within the ACP12 2024 GitLab repository](https://gitlab.developers.cam.ac.uk/uis/devops/ppr/ppr2024)

{{ service_management() }}
