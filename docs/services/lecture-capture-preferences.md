<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page documents key information about the Lecture Capture Preferences App
service.

!!! danger
    This service has been retired.

The Lecture Capture Preferences app allows any member of the University with a
Raven account to express a preference surrounding the recording of any lectures
they may give.

It is unclear if this application is being used with the current lecture capture
service.

## Environments

- Production at <https://preferences.lecturecapture.uis.cam.ac.uk/>
    - API at <https://preferences.lecturecapture.uis.cam.ac.uk/api/preferences/?user=CRSID>
- Staging at <https://webapp.test.lc.gcp.uis.cam.ac.uk/>
- Development at <https://webapp.devel.lc.gcp.uis.cam.ac.uk/>

## Application repositories

- [Web
    application](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/preferences-webapp/)
- [Terraform
    deployment](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/deploy)

## Deployment

Deployed via terraform using our usual deployment boilerplate.

## Current Status

The service is currently **decommissioned**.

{{ service_management() }}
