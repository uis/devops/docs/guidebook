<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the  service, describing its current status, where and how it's
developed and deployed, and who is responsible for maintaining it.

## Service Description

Jackdaw provides a database that holds consolidated user administration records for staff and
students in the University. Jackdaw get daily feeds from CamSIS, CHRIS, the Card Office and
daily feeds out to a variety of services/systems.

## Service Status

Jackdaw is currently **live**.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/jackdaw-projects/jackdaw/-/issues) (DevOps
and Service Manager only).

## Environments

### Current infrastructure

Jackdaw is being re-development. During this time, the application has been moved
to more stable hardware, and the database has been split away from the application.

<!-- markdownlint-disable MD013 -->
| Name        | URL                                          | Supporting VMs  |
| ----------- | -------------------------------------------  | --------------- |
| Production  | <https://jackdaw.cam.ac.uk>                  | jackdaw-live1.srv.uis.cam.ac.uk |
|             |                                              | jackdaw-live2.srv.uis.cam.ac.uk |
| Test        | <https://jackdaw-test.srv.uis.cam.ac.uk/>    | jackdaw-test1.srv.uis.cam.ac.uk |
|             |                                              | jackdaw-test2.srv.uis.cam.ac.uk |
| Database    | Live                                         | jdbc:oracle:thin:@//ocm.admin.cam.ac.uk:1545/jackdaw.srv.uis.private.cam.ac.uk |
|             | Test                                         | jdbc:oracle:thin:@//ocm.admin.cam.ac.uk:1545/jackdawt.srv.uis.private.cam.ac.uk |
<!-- markdownlint-enable MD013 -->

## Source code

The source code for future Jackdaw development is under the
<https://gitlab.developers.cam.ac.uk/uis/devops/iam/jackdaw-sunset> group.

## Technologies used

The following gives an overview of the technologies Jackdaw is built on.

| Category | Language | Framework(s) |
| -------- | -------- | ------------ |
| Server   | Perl PerlToolKit PLSQL | Apache mod_perl  |
| Client | Jquery | |
| Database | Oracle | |

## Operational and other documentation

The following gives and overview of how Jackdaw is deployed and maintained.

### How and where Jackdaw is deployed

Jackdaw is deployed to a pair of servers and uses Keepalived to manage a VIP:

```mermaid
graph LR
    A[Client] ---> B[Keepalived<br/>131.111.8.21]
    subgraph Application
        B -- "http(s)" --> C["jackdaw-live1<br/>131.111.8.18"]
        B -- ssh --> C["jackdaw-live1<br/>131.111.8.18"]
        B -- "http(s)" --> D[jackdaw-live2<br />131.111.8.19]
        B -- ssh --> D[jackdaw-live2<br />131.111.8.19]
    end
    subgraph Database
        C --> E[(Oracle)]
        D --> E[(Oracle)]
    end
```

### Deploying a new release

New releases are deployed via
[Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/iam/jackdaw-sunset/infrastructure). Each
server should be set to maintenance mode then the `run-ansible-playbook.sh` should be run limited to
the node in maintenance mode. e.g.

```bash
# On jackdaw-live1
touch /maintenance_mode

# On client machine
eval $(op signin)
source ./jackdaw-live-setup
./run-ansible-playbook.sh -i jackdaw-live jackdaw-useradmin-playbook.yml --diff --limit jackdaw-live1

# On jackdaw-live1
rm /maintenance_mode

# On jackdaw-live2
touch /maintenance_mode

# On client machine
./run-ansible-playbook.sh -i jackdaw-live jackdaw-useradmin-playbook.yml --diff --limit jackdaw-live2

# On jackdaw-live2
rm /maintenance_mode
```

## Other documnetation

- [Jackdaw on Confluence](https://confluence.uis.cam.ac.uk/display/JAC/Jackdaw+Home)
- [Jackdaw - service definition](https://confluence.uis.cam.ac.uk/display/IAM/Jackdaw+-+service+definition)
- [Jackdaw UIS JIRA](https://jira.uis.cam.ac.uk/secure/Dashboard.jspa?selectPageId=11002)
- [Jackdaw HPC JIRA](https://jira.hpc.cam.ac.uk/secure/Dashboard.jspa?selectPageId=10500)
- [UIS help site documentation](https://help.uis.cam.ac.uk/service/collaboration/lookup)
- [Web service API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws)

{{ service_management() }}
