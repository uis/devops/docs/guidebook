<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Recruitment Administration System (RAS), describing its current
status, where and how it's developed and deployed, and who is responsible for maintaining it.

!!! danger
    RAS uses technology, coding standards, and deployment methodologies which
    are not endorsed by the TDA and not standard practice for the DevOps team.

## Service Description

RAS allows recruiters and departmental administrators to create and gain sign-off for vacancies.
RAS provides a web-service for active vacancies to be listed on the
[Job Opportunities](./job-opportunities.md) frontend, as well as synchronizing vacancy data to the
central HR system (CHRIS) and [Web Recruitment System](./web-recruitment-system.md) via the
[HR Integration App](./hr-integration-app.md).

![RAS high-level architecture](ras-arch.png)

## Service Status

RAS is currently `live`.

## Contact

Technical queries and support should be directed to
[hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be reported to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)

Issues related to the Oracle Database which serves as the primary data store for the application should
be raised with the [DBA Team](mailto:uisdba@admin.cam.ac.uk).

## Environments

RAS is currently deployed to the following environments:

| Name                                               | URL                  | Supporting VMs  |
| -------------------------------------------------  | -------------------- | --------------- |
| Production                                         | [https://hrsystems.admin.cam.ac.uk/ras-webapp/](https://hrsystems.admin.cam.ac.uk/ras-webapp/) | hr-live1.internal.admin.cam.ac.uk, hr-live2.internal.admin.cam.ac.uk |
| Training (only available within the admin network) | [https://training.hrsystems.admin.cam.ac.uk/ras-webapp/](https://training.hrsystems.admin.cam.ac.uk/ras-webapp/) | hr-train1.internal.admin.cam.ac.uk, hr-train2.internal.admin.cam.ac.uk |
| Staging (only available within the admin network)  | [https://testing.hrsystems.admin.cam.ac.uk/ras-webapp/](https://testing.hrsystems.admin.cam.ac.uk/ras-webapp/) | hr-test1.internal.admin.cam.ac.uk, hr-test2.internal.admin.cam.ac.uk |
| Development                                        | [https://development.hrsystems.admin.cam.ac.uk/ras-webapp/](https://development.hrsystems.admin.cam.ac.uk/ras-webapp/) | hr-dev1.internal.admin.cam.ac.uk |

A development instance will be created as part of the deployment to GCP.

## Source Code

The source code for RAS is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/hr/ras/ras-webapp) | The source code for the main application server |
| [On-Prem Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure) | The Ansible configuration for the existing on-prem deployment. |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/ras/ras-infrastructure) | The WIP Terraform infrastructure code for deploying the application server to GCP |

The source code for RAS has not been fully audited and does not follow existing DevOps practices,
therefore it is only available to DevOps team members actively working on the project.

## Technologies Used

The following gives an overview of the technologies the service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web App | Java 8 | JSP, Struts 2, Spring 4 |
| Server | Tomcat 9 | |
| Database | Oracle 19c  | Hibernate 4 |

## Operational Documentation

!!! note

    Information about the operation of the existing on-prem deployment is contained within
    the [README.md](https://gitlab.developers.cam.ac.uk/uis/devops/hr/ras/ras-webapp/-/blob/main/README.md?ref_type=heads)
    of the web application.

The following gives an overview of how RAS will be deployed and maintained within
GCP:

### Deployment

The on-premise deployment is managed by the [HR Apps Ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure#deployment)
repository. Traffic to the web-application is load balanced via the UIS Load Balancer, which additionally
provides SSL termination.

Deployment to the GCP environment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

### Access Management

Access management is managed internally within the application, with the application holding a
list of users who are able to access the admin UI and their permissions within the Oracle database.
Therefore access needs to be granted by an existing super-user of the application.

{{ service_management() }}
