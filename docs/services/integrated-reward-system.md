---
title: Integrated Reward System
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Integrated Reward System (IRS), describing
its current status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Service Description

The Integrated Reward System allows staff to apply for rewards, and allows HR and departmental
staff to administrate the reward process, including collecting references and scoring applications.

The only reward scheme implemented so far is Academic Career Pathways: Research and Teaching
(ACP R&T, formerly just ACP, formerly Senior Academic Promotions / SAP), which allows senior
academics to apply for promotion.

The Referee Portal and Referee Admin Console components of the system have been used in the past to
support another reward scheme, Professorial Pay Review (PPR), but PPR has never been fully
supported by IRS.

While the original vision was to have all reward schemes implemented in IRS, the current thinking
is that others may or may not be a good fit, depending on their similarity to ACP.  This will need
to be analysed before adding any additional schemes.

## Service Status

IRS is a live service, and is used each year for that ACP round.  ACP (as in the business process)
runs every year. Demand on the system fluctuates throughout the year according to where we are in
the cycle.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/groups/uis/devops/hr/irs/-/issues).

## Environments

IRS is currently deployed to the following environments:

| Name        | Main Application URL | Admin URL |
| ----------- | -------------------- | ---------------- |
| Production  | [https://irs.hr.apps.cam.ac.uk/apply](https://irs.hr.apps.cam.ac.uk/apply) | [https://irs.hr.apps.cam.ac.uk/admin](https://irs.hr.apps.cam.ac.uk/admin) |
| Staging | [https://staging.irs.hr.apps.cam.ac.uk/apply](https://staging.irs.hr.apps.cam.ac.uk/apply) | [https://staging.irs.hr.apps.cam.ac.uk/admin](https://staging.irs.hr.apps.cam.ac.uk/admin) |
| Development | [https://development.irs.hr.apps.cam.ac.uk/apply](https://development.irs.hr.apps.cam.ac.uk/apply) | [https://development.irs.hr.apps.cam.ac.uk/admin](https://development.irs.hr.apps.cam.ac.uk/admin) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database |
| ----------- | ------------------------ | -------- |
| Production | [GCP Cloud Run](https://console.cloud.google.com/run?project=irs-prod-f81fe5ab) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=irs-prod-f81fe5ab) |
| Staging | [GCP Cloud Run](https://console.cloud.google.com/run?project=irs-test-57d87e66) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=irs-test-57d87e66) |
| Development | [GCP Cloud Run](https://console.cloud.google.com/run?project=irs-devel-6f50d5bf) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=irs-devel-6f50d5bf) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?referrer=search&project=irs-meta-c8a3156c).

## Source code

Source code for Integrated Reward System is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository | Description |
| ---------- | ----------- |
| [Integrated Reward System](https://gitlab.developers.cam.ac.uk/uis/devops/hr/irs/irs-webapp)¹ | The Applicant Portal and Admin Console web-apps.  Most of the business logic is here. A separate `panel` API service is included within this repo which manages details of the committees who score applications, including members of the committees, are maintained    |
| [Referee](https://gitlab.developers.cam.ac.uk/uis/devops/hr/irs/irs-referee)¹ | The Referee Portal (and Referee Admin Portal, which is only used for PPR) |
| [Orchestra](https://gitlab.developers.cam.ac.uk/uis/devops/hr/irs/irs-orchestra)¹ | Core backend component.  Data repository and API.  Implements some business logic. |
| [Cloud Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/hr/irs/irs-infrastructure)¹ | Terraform configuration for infrastructure and deployment |
<!-- markdownlint-enable MD013 -->

¹ University GitLab users only

## Technologies used

The following gives an overview of the technologies that Integrated Reward System is built on.

| Category        | Language          | Framework(s) |
| --------------- | ----------------- | ------------ |
| Web Apps and Web Services | Java 17 | TomEE Plume, CDI, JAX/RS, JSF, Primefaces |
| DB              | PostgreSQL 16, Oracle 19c to connect to `CHRIS_EXTRACT` schema |      |
| GCP deployment  | Terraform         |              |

## Operational documentation

The following gives an overview of how Integrated Reward System is deployed and maintained.

### How and where Integrated Reward System is deployed

Deployment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Deploying a new release

Making a new release of the application is done via [release
automation](site:explanations/git-gitlab/gitlab-release-automation).

Deployment is done by:

1. Updating the deployment project's repository with any changes, including bumping the deployed web
   application release.
2. Using the "play" buttons in the CI pipeline to deploy to production when happy. (Deployments to
   **staging** happen automatically on merges to `main`.)

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module, with uptime checks
configured for the `referee` and `irs-webapp` components, with the `irs-webapp` monitoring path
configured as `/admin/pages/system-status` which validates the health of downstream services.

### Debugging

The README.md files in each of the source code repositories provide information about debugging
both local and deployed instances of the applications.

{{ service_management() }}
