<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Temporary Employment Service, describing its current status, where
and how it's developed and deployed, and who is responsible for maintaining it.

!!! danger
    The Temporary Employment Service uses technology, coding standards, and deployment methodologies
    which are not endorsed by the TDA and not standard practice for the DevOps team.

## Service Description

The Temporary Employment Service manages the recruitment, administration and submission of timesheets
for Temporary Employees, managed by the [TES HR Team](https://www.hr.admin.cam.ac.uk/hr-services/tes).
It allows Temporary Employees to be assigned positions across the University, and integrates with
CHRIS to ensure that details of temporary workers are tracked in CHRIS and temporary workers can be
paid by CHRIS's payroll module.

## Service Status

The Temporary Employment Service is currently `live`.

## Contact

Technical queries and support should be directed to
[hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [hr-systems-development@uis.cam.ac.uk](mailto:hr-systems-development@uis.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be reported to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)

Issues related to the Oracle Database which serves as the primary data store for the application should
be raised with the [DBA Team](mailto:uisdba@admin.cam.ac.uk).

## Environments

TES is currently deployed to the following environments:

| Name                                               | URL                  | Supporting VMs  |
| -------------------------------------------------  | -------------------- | --------------- |
| Production                                         | [https://hrsystems.admin.cam.ac.uk/tes-webapp](https://hrsystems.admin.cam.ac.uk/tes-webapp) | hr-live1.internal.admin.cam.ac.uk, hr-live2.internal.admin.cam.ac.uk |
| Staging (only available within the admin network)  | [https://testing.hrsystems.admin.cam.ac.uk/tes-webapp](https://testing.hrsystems.admin.cam.ac.uk/tes-webapp) | hr-test1.internal.admin.cam.ac.uk, hr-test2.internal.admin.cam.ac.uk |
| Development                                        | N/A | N/A |

## Source Code

The source code for TES is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Admin Application](https://gitlab.developers.cam.ac.uk/uis/devops/hr/tes/tes-webapp) | The source code for the administrative application |
| [Timesheet Application](https://gitlab.developers.cam.ac.uk/uis/devops/hr/tes/tes-timesheet) | The source code for application which manages timesheets |
| [On-Prem Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure) | The Ansible configuration for the existing on-prem deployment. |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-int/hr-int-infrastructure) | The Terraform infrastructure code for deploying the application server to GCP |

The source code for TES has not been fully audited and does not follow existing
DevOps practices, therefore it is only available to DevOps team members actively working on the project.

## Technologies Used

The following gives an overview of the technologies the service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web App | Java 8 | JSF2, PrimeFaces 6, Spring 4 |
| Server | Tomcat 9 | |
| Database | Oracle 19c  | Hibernate 4 |

## Operational documentation

### Deployment

The on-premise deployment is managed by the [HR Apps Ansible deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/hr-ansible/hr-ansible-infrastructure#deployment)
repository. Traffic to the web-application is load balanced via the UIS Load Balancer, which additionally
provides SSL termination.

Deployment to the GCP environment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

### Access Management

Access management is managed internally within the application, with the application holding a
list of users who are able to access the admin UI within the Oracle database.

{{ service_management() }}
