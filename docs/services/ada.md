<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of Ada, the financial management app for Disability Office,
describing its current status, where and how it's developed and deployed, and who is responsible
for maintaining it.

## Service Description

Ada is an application for managing income and expenditure for students with disabilities.

## Service Status

Ada is currently **live**.

Currently there is no planned roadmap for the future of this service; however, the service is
integral to the administration of students with disabilities and each year there are normally
some tweaks made to the system.

## Contact

Technical queries and support should be directed to <mailto:uis-cloud@lists.cam.ac.uk> and will be
picked up by a member of the team working on the service.

<!-- Issues discovered in the service or new feature requests should be opened as
`GitLab issues in the application repository`.-->

## Environments and Client

Ada is a Windows Forms app so is currently deployed to and run from a file server. The application
is run over the network on the UIS File Server. This is secured by file permissions, by the need to
be run on a University machine belonging to a University Windows Domain, and by pass through
Windows login to the database server. Membership of the following group will give access.

- uis-sec-microsoft-.net-drive-access

Access to environment is determined by which folder the executable is run from. The application is
started by running the **UIS.Ada.Ada.exe** executable.

Ada is currently deployed to the following environments (this assumes the Z drive is mapped to
`\\blue.cam.ac.uk\group` as per standard university build):

| Name        | File Folder |
| ----------- | ------------------ |
| Production  | Z:\J Share\Applications\DotNet\Academic\Disability\Ada\Ada Live |
| Pending     | Z:\J Share\Applications\DotNet\Academic\Disability\Ada\Ada Pending |
| Development | Z:\J Share\Applications\DotNet\Academic\Disability\Ada\Ada Dev |

<!-- markdownlint-disable MD013 -->

## Source code

The source code for Ada is spread over the following repository folders:

| Repository  | Description |
| ----------- | ------------------ |
| [Ada](https://gitlab.developers.cam.ac.uk/uis/devops/childcare-services/ada/Ada/-/tree/master/Ada?ref_type=heads) | The Windows Forms application  |
| [BEL](https://gitlab.developers.cam.ac.uk/uis/devops/childcare-services/ada/BEL/-/tree/master/Ada?ref_type=heads) | Holds business objects |
| [BPL](https://gitlab.developers.cam.ac.uk/uis/devops/childcare-services/ada/BPL/-/tree/master/Ada?ref_type=heads) |  Holds business processes; knits together BEL and DAL|
| [AdaUnitTests](https://gitlab.developers.cam.ac.uk/uis/devops/childcare-services/ada/AdaUnitTests/-/tree/master/Ada?ref_type=heads) | Unit tests (needs revision) |

<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the technologies Ada is built on.

| Category        | Language | Framework(s) |
| --------------- | -------- | ------------ |
| Database Server | MS SQL   | SQL Server 2019 |
| Client Code     | VB.NET   | 4.6.1        |

Nuget package manager is also used to control versions of integrated code such as EPPlus (used for
Excel interoperability). Full details are outside the scope of this document but can be seen
through Visual Studio.

### Database

Ada's data resides on a Microsoft SQL Server (stu-sql1.blue.cam.ac.uk)

| Environment        | Instance |
| --------------- | -------- |
| Live | Live |
| All development  | UAT |

Access is done through Windows Active Directory, meaning there is no need to store passwords, it's
all integrated. To have access you need to either be in the SQL Administrators group or be in the
following group:

- BLUE\disab-disability-access

### IDE

Ada is developed using Microsoft's Visual Studio (minimum version recommended 2019). There is a
solution file that contains projects; those projects are reflected in the different folders in
stored in the repository and listed above.

## Operational documentation

The following gives an overview of how Ada is deployed and maintained.

### How and where Ada is deployed

Deployment for Ada is extremely simple - files are copied over from your build folder in Visual
Studio to the folder holding the environment you are deploying to.

### Deploying a new release

Make sure no files are open in the destination document - that is that the target version of Ada is
not open, then copy files over.

### Monitoring

Currently no monitoring is carried out; the userbase is small and there are excellent relations
between the users and the developer.

### Debugging

Debugging can only be done through Visual Studio, running the app natively in it.

<!-- ### `<other operation issues>`

`<documentation or links to external docs about other operation issues>` -->

{{ service_management() }}
