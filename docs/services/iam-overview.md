<!-- markdownlint-disable MD041 -->
{{ service_title() }}

The landscape of Identity and Access Management (IAM) within UIS, and the University in general,
is ever-changing. Trying to document the overall architecture of identity systems within UIS alone
would create a document needing constant maintenance and which would very easily become defunct.
Therefore, this page links to external sources of information related to IAM which are more likely
to remain accurate.

In-depth information about IAM systems maintained by our division can be found under sibling pages
of this page. The DevOps Division does not own all IAM systems maintained by UIS, and therefore
information about IAM systems falling outside our division is very high-level and for context only.

## Sources of IAM documentation

Documentation about IAM systems and their high-level architecture can be found within:

* [March 2022 IAM discovery report](https://docs.google.com/document/d/1LxesJLrWIx47fSjf-lGK_o1VqczbB2Eqag3nJj52iEk/edit)
* [High-level architecture documentation](https://docs.google.com/document/d/1txrC7NekNn-ff6XDee39BUhB-Z_w2FC9ou7wU08N5gU/edit#)
* [High-level overview of the re-architecture of the University Card system](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation/-/wikis/card/Architecture)
* [DevOps IAM project Wiki](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation)
