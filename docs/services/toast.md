<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of TOAST, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

_Please note that TOAST stands for **T**rusts **O**nline **A**pplication **S**ys**T**em_

## Service Description

TOAST provides a scoring and application system for graduate funding, allowing departments to
identify candidates and funders to assess their potential and select those more suitable.

Secondly it provides a portal for Gates Trust to manage scholars; please note this is not intended
as a long term function.

## Service Status

TOAST is currently **live**.

Currently there is no planned roadmap for the future of this service; however, the service is
integral to the graduate funding process and each year there are normally some tweaks made to the
business logic.

## Contact

Technical queries and support should be directed to <mailto:uis-cloud@lists.cam.ac.uk> and will be
directed to [Martin Hunt](https://www.lookup.cam.ac.uk/person/crsid/meh37), who is the member of the
team who works on the service.

<!-- Issues discovered in the service or new feature requests should be opened as
`[GitLab issues in the application repository](<link to gitlab issues page>)`.
-->
## Environments and Clients

TOAST has two components:

- A web client used by departments to score and assess applicants in the system
- A windows forms application used by admin staff to set the system up and to run various reports

### Web

Authentication is global and partially governed through _Raven_. Raven protected sections are under
the URL Raven/. TOAST has a database with approved users. Environment is decided by URL as listed
below.

The TOAST web app is currently deployed to the following web environments:

| Name        | URL                                    | IIS Servers          |
| ----------- | -------------------------------------- | -------------------- |
| Production  | <https://toast.admin.cam.ac.uk/>         | Dreamer/Executive    |
| Staging     | <https://uattoast.admin.cam.ac.uk/>      | stu-live-web1    |
| Funding Management Project Development | <https://uatfmptoast.admin.cam.ac.uk/>  | stu-live-web1    |
| Development | localhost                              | localhost            |

### Windows Forms App

The application is run over the network on the US File Server. This is secured by file permissions,
by the need to be run on a University machine belonging to a University Windows Domain, and by pass
through Windows login to the database server. Access to environment is determined by which folder
the excutable is run from. The application is started by running the **MISD.Toast.FormsApp.exe**
executable.

TOAST windows forms apps are currently deployed to the following environments (this assumes the Z
drive is mapped to `\\blue.cam.ac.uk\group` as per standard university build):

| Name        | Folder                                 |
| ----------- | -------------------------------------- |
| Production  | Z:\J Share\Applications\DotNet\Trusts\Live |
| Staging     | Z:\J Share\Applications\DotNet\Trusts\Dev |
| Funding Management Project Development | Z:\J Share\Applications\DotNet\Trusts\UAT_FMP |

## Source code

The source code for the TOAST can be found at the following URL:

<https://dev.azure.com/meh37/TOAST>

The code is spread over the following repository folders:

<!-- markdownlint-disable MD013 -->

| Repository  | Description |
| ----------- | ------------------ |
| [BEL](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/BEL) | Holds business objects |
| [BPL](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/BPL) |  Holds business processes; knits together BEL and DAL|
| [Components](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/Components) | Holds any bespoke components; mostly deprecated and superseded by NuGet |
| [DAL](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/DAL) | Data access routines, for example a library to communicate by SQL server |
| [Documentation](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/Documentation) | A library that can hold documentation |
| [EDMSTestingApp](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/EDMSTestingApp) | A testing application for EDMS (Electronic Document Management System) - this is a bespoke library that accesses data related to applicants |
| [FormsApp](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/FormsApp) | Holds code for the windows forms application |
| [packages](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/packages) | Holds the NuGet packages for the project |
| [SQL](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/SQL) | Holds SQL change scripts |
| [SQLDatabase (Deprecated)](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/SQLDatabase) | Deprecated library that holds SQL change scripts - use  [SQL](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/SQL) instead |
| [Temp](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/Temp) | Some temporary data used in testing |
| [Toast](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/Toast) | Holds source code for the website |
| [ToastDatabase (Deprecated)](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/ToastDatabase) | Deprecated - was for Redgate SQL product that was dropped |
| [UnitTests](https://meh37.visualstudio.com/TOAST/_versionControl?path=%24/TOAST/UnitTests) | Unit tests around TOAST |

<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the technologies TOAST is built on.

| Category        | Language | Framework(s) |
| --------------- | -------- | ------------ |
| Database Server | MS SQL   | SQL Server 2019 |
| Web Code        | VB.NET   | 4.6.1        |
| Client Code     | VB.NET   | 4.6.1        |

### Database

TOAST's data resides on a Microsoft SQL Server (stu-sql1.blue.cam.ac.uk)

| Environment        | Instance |
| --------------- | -------- |
| Live | Live |
| All development  | UAT |

Access is done through Windows Active Directory, meaning there is no need to store passwords,
it's all integrated.
To have access through the Windows app you need to either be in the SQL Administrators group
or be in the following group:

- BLUE\ad-feesandfundingteam

For IIS server access this has been set up to use integrated Windows authentication
to circumvent the issue of generating/storing passwords,
rather the server account itself managed by Windows Active Directory is used.

### IDE

TOAST is developed using Microsoft's Visual Studio (minimum version recommended 2019).
There is a solution file that contains projects; those projects are reflected in the
different folders in stored in the repository and listed above.

## Operations

### How and where TOAST is deployed

<!-- markdownlint-disable-next-line MD024 -->
### Web

It is important to note that this application does **not** use docker, rather it is directly
installed on the server.

| Server        | OS | IIS version |
| --------------- | -------- | ------------ |
| Dreamer | Windows Server 2012 R2   | 8.0 |
| Executive | Windows Server 2012 R2   | 8.0 |
| stu-live-web1 | Windows Server 2012 R2   | 8.0 |

The websites are set up locally on the E drive on those machines. On Dreamer/Executive,
to keep files in sync a DFS share has been set up between the two servers that can be accessed here:

- `\\internal\support\IIS_Data`

On the stu-live-web1 awecwe files are merely local,
stored here:

- `C:\inetpub\custom\toast`

### Deploying a new release

Web deployment is done through Visual Studio. Profiles have been set up to copy the web files and
can be deployed through a right click publish. UIS Windows Server Team are responsible for
monitoring the userlying machines.

Admin app deployment is done through copying files to the file server location.

<!-- ## Operational documentation

The following gives an overview of how the TOAST is deployed and maintained.

### How and where the TOAST is deployed

`<documentation or links to external docs describing how and where the application is deployed>`

### Deploying a new release

`<documentation or links to external docs about how a new release is made and deployed>`

### Monitoring

`<documentation or links to external docs about monitoring and how alerts should be responded to>`

### Debugging

`<documentation or links to external docs describing how to debug both a deployed instance and locally>`

### `<other operation issues>`

`<documentation or links to external docs about other operation issues>`
-->

{{ service_management() }}
