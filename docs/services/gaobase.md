---
title: GAOBase
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Graduate Admissions Course Database (GAOBase) system, describing
its current status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Service Description

GAOBase maintains the course data used for the [Postgraduate
Study](https://www.postgraduate.study.cam.ac.uk/) website. Once data has been through a review
process, static site data is published by GAOBase into the Postgraduate Study system which is
powered by the Drupal web content management system. The Drupal system interacts with GAOBase via
API's for searches and course / department listings.

Also see:

* [Postgraduate Study](https://www.postgraduate.study.cam.ac.uk/)
* [GAOBase Help Guide](https://www.postgraduate.study.cam.ac.uk/files/help_guide_2018-19.pdf)

## Service Status

GAOBase is currently under development as a GCP Cloud based product.

The current GAOBase is live as an on-prem service, but is being migrated by the DevOps team to run
in GCP.

## Contact

Technical queries and support should be directed to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/gaobase/gaobase/-/issues).

## Environments

GAOBASE is currently deployed to the following environments:

| Name        | Main Application URL | Django Admin URL |
| ----------- | -------------------- | ---------------- |
| Production  | [https://gaobase.admin.cam.ac.uk/](https://gaobase.admin.cam.ac.uk/) | [https://gaobase.admin.cam.ac.uk/admin](https://gaobase.admin.cam.ac.uk/admin) |
| Staging | [https://webapp.test.gaobase.gcp.uis.cam.ac.uk/](hhttps://webapp.test.gaobase.gcp.uis.cam.ac.uk/) | [https://webapp.test.gaobase.gcp.uis.cam.ac.uk/admin](https://webapp.test.gaobase.gcp.uis.cam.ac.uk/admin) |
| Development | [https://webapp.devel.gaobase.gcp.uis.cam.ac.uk/](https://webapp.devel.gaobase.gcp.uis.cam.ac.uk/) | [https://webapp.devel.gaobase.gcp.uis.cam.ac.uk/admin](https://webapp.devel.gaobase.gcp.uis.cam.ac.uk/admin) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database |
| ----------- | ------------------------ | -------- |
| Production | [GCP Cloud Run](https://console.cloud.google.com/home/dashboard?project=gaobase-prod-ddc6ae38) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=gaobase-prod-ddc6ae38) |
| Staging | [GCP Cloud Run](https://console.cloud.google.com/home/dashboard?project=gaobase-test-7ad72cdb) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=gaobase-test-7ad72cdb) |
| Development | [GCP Cloud Run](https://console.cloud.google.com/home/dashboard?project=gaobase-devel-e662dd2b) | [GCP Cloud SQL (Postgres)](https://console.cloud.google.com/sql/instances?project=gaobase-devel-e662dd2b) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/sql/instances?project=gaobase-meta-5c82d605).

## Notification channel(s) for environments

| Environment | Display name | Email |
| ----------- | ------------ | ----- |
| Production  | Graduate Admissions - Lovelace DevOps team email channel | **<devops-lovelace@uis.cam.ac.uk>** |

## Source code

Source code for GAOBase is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository | Description |
| ---------- | ----------- |
| [GAOBase](https://gitlab.developers.cam.ac.uk/uis/devops/gaobase/gaobase)¹ | The source code for the application |
| [Cloud Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/gaobase/cloud-infrastructure)¹ | Terraform configuration for infrastructure and deployment |
<!-- markdownlint-enable MD013 -->

¹ DevOps only

## Technologies used

The following gives an overview of the technologies that GAOBase is built on.

| Category        | Language          | Framework(s) |
| --------------- | ----------------- | ------------ |
| Server          | Python 3.8        | Django 3.2   |
| Client          | Javascript/jQuery |              |
| DB              | PostgreSQL 13     |              |
| GCP deployment  | Terraform         |              |

## Operational documentation

The following gives an overview of how GAOBase is deployed and maintained.

### How and where GAOBase is deployed

* The Database for Postgraduate course data is a PostgreSQL database hosted by GCP Cloud SQL.
* The main web application is a Django application hosted by GCP Cloud Run.
* There are a number of asynchronous processes which rely on a GCP Cloud Tasks queue.
  The application submits jobs to the queue that callback the application's application's
  `cloud_tasks/` endpoint.

### Deploying a new release

The README.md file in the [Infrastructure
Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/gaobase/cloud-infrastructure) repository
explains how to deploy GAOBase.

### Monitoring

* [GCP Cloud Monitoring](https://console.cloud.google.com/monitoring?project=gaobase-meta-5c82d605)
  for tracking the health of applications in the environments and sending alert emails when
  problems are detected.
* Cloud Logs for tracking individual requests/responses to/from the web application and the
  synchronisation job application.
    * [production](https://console.cloud.google.com/logs?project=gaobase-prod-ddc6ae38),
    * [staging](https://console.cloud.google.com/logs?project=gaobase-test-7ad72cdb),
    * [development](https://console.cloud.google.com/logs?project=gaobase-devel-e662dd2b)

### Debugging

The README.md files in each of the source code repositories provide information about debugging
both local and deployed instances of the applications.

{{ service_management() }}
