---
title: Covid-19 Pools
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

!!! danger
    This service has been retired.

This page gives an overview of the Covid-19 Testing Pools Management service, describing its
current status, where and how it's developed and deployed, and who is responsible for maintaining
it.

## Service Description

A way for College administrators to be able to rapidly assign students to pools and households was
required for the [Asymptomatic COVID-19 screening programme](https://www.cam.ac.uk/coronavirus/stay-safe-cambridge-uni/asymptomatic-covid-19-screening-programme).
The quickest solution was to create Google Sheets for each College, each with a `Pools` and a
`Households` worksheet. Additionally, a `Contacts` worksheet was added for notification of failures
when processing the Sheet. College administrators were given permission to access these Google
Sheets and fill in rows for each pool and household, and continue to make adjustments throughout
the year.

This service is a combination of a GitLab Schedule calling endpoints in a webapp to cause the
Google Sheet for a College to be read, parsed and stored in a database.

The webapp also has an endpoint exposing the current state of pools and households in the database.
This is pulled from periodically and imported in to the Azure DB system managed by the
[Collaboration Team](mailto:collab.management@uis.cam.ac.uk).

## Service Status

The service is currently **decommissioned**.

> The Asymptomatic COVID-19 Screening Programme will continue to operate as normal until the end of
Easter term, with the last days of pooled screening on Monday-Wednesday 28-30 June. It will NOT
operate over the summer vacation.

It yet to be confirmed whether this service will be required for the next and academic year. If
not, this it will be decommission.

## Contact

Technical queries and support should be directed to
[uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the administration repository](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/administration/-/issues)
(DevOps team only).

## Environments

The `Covid-19 Testing Pools Management Service` is currently deployed to the following environments:

| Name        | URL                                                |
| ----------- | -------------------------------------------------- |
| Production  | <https://webapp.prod.covid-pool.gcp.uis.cam.ac.uk/>  |
| Staging     | <https://webapp.test.covid-pool.gcp.uis.cam.ac.uk/>  |
| Development | <https://webapp.devel.covid-pool.gcp.uis.cam.ac.uk/> |

## Source code

The source code for the Covid-19 Testing Pools Management service is spread over the following
repositories:

| Repository  | Description        |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/webapp) | The source code for the main webapp server |
| [Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/infrastructure) | The Terraform code for the GCP infrastructure |
| [Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/deploy) | The GitLab CI for deploying the webapp onto the infrastructure |
| [Scheduled Tasks](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/sched-tasks) | The GitLab CI and python scripts to call the webapp |

## Technologies used

The following gives an overview of the technologies the `<service_name>` is built on.

| Category           | Language   | Framework(s) |
| ------------------ | ---------- | ------------ |
| Application Server | Python 3.7 | Django 2.2   |
| Infrastructure     | Terraform  | |
| Deployment         | GitLab CI  | |
| Scheduled Tasks    | Python 3.7 | Requests     |
|                    | GitLab CI  | |

## Operational documentation

### How and where the service is deployed

See [Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/deploy) Gitlab project
that deploys on to infrastructure created by [Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/infrastructure).

### Deploying a new release

Updates to the master branch in the [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/webapp)
repository will automatically by deployed to the `staging` instance. After testing this can be
pushed to `production` via GitLab CI manual trigger.

### Monitoring

Failures of the [Scheduled Tasks](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/sched-tasks)
Gitlab CI pipeline will cause automatic notification to the schedule owner (UIS DevOps Division Robot).

Additionally, failures to process an individual College's sheet will notify the members of the
`Contacts` worksheet with details of the failure (such as more than 10 students in a pool).

### Debugging

Standard boilerplate `compose.sh` scripts existing the [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/covid-pool/webapp)
in order to bring up a local development instance.

"Dry-run" attempts to parse a College sheet may be made using the `?dry_run=1` query string
parameter.

### Further Documentation

Little other documentation exists outside the `README.md` files in each of the repositories
listed [above](#source-code).

{{ service_management() }}
