<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Raven Password Management service, describing
its current status, where and how it's developed and deployed, and who is
responsible for maintaining it.

## Service Description

This services allows Raven account holders to manage their password. Changing of
passwords can be performed via 'normal' login, self-service recovery or password
reset tokens. These tokens may be issued by departmental or college
administrators, or by University Information Services.

Self-service password recovery allows users to reset their password by
configuring a recovery email address and/or mobile phone number and set up some
security questions that will be used to prove their identity during
password recovery.

Changed passwords are synchronised with a wide range of University systems,
including Raven based websites, Exchange Online email, Hermes email and Desktop
Services including the Managed Cluster Service (MCS).

A history of changes to passwords and recovery details is also visible to an
authenticated user.

## Service Status

The Raven Password Management service is currently **live**.

The intention is to move this service from UIS infrastructure VMs to a Google
Cloud deployment but no timeline for this is currently set.

## Contact

End-user support is provided by the [Service Desk](mailto:servicedesk@uis.cam.ac.uk)
and [User Admin](mailto:user-admin@uis.cam.ac.uk).

Technical queries and support should be directed to the
[Service Desk](mailto:servicedesk@uis.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a
response, always direct requests to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues [here](https://gitlab.developers.cam.ac.uk/uis/devops/passwords/passwords/-/issues).

## Environments

The Password app is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                                           | Supporting VMs                                 |
| ----------- | --------------------------------------------- | ---------------------------------------------- |
| Production  | <https://password.raven.cam.ac.uk/>             | raven-password-live1.srv.uis.private.cam.ac.uk |
|             |                                               | raven-password-live2.srv.uis.private.cam.ac.uk |
|             |                                               | passwordapp-live-db.srv.uis.private.cam.ac.uk  |
| Test        | <https://test.password.raven.cam.ac.uk/>        | raven-password-test1.srv.uis.private.cam.ac.uk |
|             | <https://uis-tm-test.password.raven.cam.ac.uk/> | raven-password-test2.srv.uis.private.cam.ac.uk |
|             |                                               | devgroup-test-db.srv.uis.private.cam.ac.uk     |
<!-- markdownlint-enable MD013 -->

> The Traffic Manager load-balances web traffic to the supporting VMs, making use of
> sticky sessions to ensure that the relevant in-memory password-change queue is
> displayed to the user.
>
> The test VMs have their own fake local password clients that act as black
> holes for all password changes. Any actions that require a password to be
> quoted on a test VM (not including the initial Raven logon) should
> quote the fake password stored in 1password.

## Source code

The source code for the Password Management Application is stored in
[this Gitlab repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/passwords/passwords)

## Technologies used

The following gives an overview of the technologies the Password app is built on.

| Category | Language    | Framework(s) |
| -------- | ----------- | ------------ |
| Server   | Java+Groovy | Grails       |
| DB       | Postgres    | [Moa](https://confluence.uis.cam.ac.uk/display/SYS/Moa%3A+managed+database+service) |

## Operational documentation

The following gives an overview of how the Password app is deployed and maintained.

## Taking a host out of service

Hosts can be taken out of service by creating a file `/maintenance_mode` which causes the monitor
script to return an http 500 response code, which in turn causes the Traffic Manager to direct
traffic elsewhere.

### Deploying a new release

New releases are deployed via
[Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment/).
The `package_version` group_var determines the tagged version of the app to deploy.

Each server should be set to maintenance mode then the `run-ansible-playbook.sh` should be run
limited to the node in maintenance mode. Deployments can be monitored using `watch-status.sh` script.

The following example is for production service, this should be done using the test service first.

```bash
# In separate window on client machine
./watch-status.sh passwords-production

# On raven-password-live1 (as root)
touch /maintenance_mode

# Wait for UIS traffic manager to switch to consistently using live2 server for requests and
# allow some extra time for users that may be in the middle of setting their password.
# e.g. wait a couple of minutes before proceeding

# On client machine
eval $(op signin)

# If your local user is not your CRSid, set APP_USER var to your CRSid
# If not using VPN, use aperture by setting APERTURE_JUMP_HOST var to non-empty
./run-ansible-playbook.sh -i passwords-production passwords-playbook.yml --diff --limit raven-password-live1

# When complete, confirm that the server is running before removing it from maintenance mode

# On raven-password-live1
rm /maintenance_mode

# Then repeat for the other server in the pair, again allowing time for the switch over

# On raven-password-live2 (as root)
touch /maintenance_mode

# On client machine
./run-ansible-playbook.sh -i passwords-production passwords-playbook.yml --diff --limit raven-password-live2

# Again, confirm server is running

# On raven-password-live2
rm /maintenance_mode
```

### Monitoring

The Password app is monitored by the UIS infra-sas
[nagios service](https://nagios.uis.cam.ac.uk/nagios/).

Sevices currently monitored:

* ping - standard nagios ping check.
* SSL - checks for a valid TLS certificate on port 8443.
* https_devgroup - checks for a 200 response from the /adm/status page on port 8443.
* disc-space - checks for at least 15% free disk space.

There is also a check for a vaild TLS certificate being served by the traffic manager for <https://uis-tm.password.raven.cam.ac.uk>

### Backups

See [legacy application backups](legacy-application-backups.md)

### Debugging

The Password app cannot be run locally, and therefore the test instance should
be used to trial changes and fixes.

### Other operational documentation

End-user documentation: <https://help.uis.cam.ac.uk/service/accounts-passwords>

Description of the password strength checker: <https://wiki.cam.ac.uk/uis/UIS_Password_Strength_Checker>

The self-service password recovery flow: <https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/passwords/passwords/-/blob/master/doc/password-recovery.pdf>

{{ service_management() }}
