<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Human Tissue Tracking application (HTA), describing its current
status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The Human Tissue Tracking application records information about Human Tissue stored within the
University of Cambridge, and assists compliance with the Human Tissue Act.

## Service Status

The HTA is currently live.

## Contact

Technical queries and support should be directed to
[uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/hta/hta/-/issues)

## Environments

The HTA is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://tissue.apps.cam.ac.uk/](https://tissue.apps.cam.ac.uk/)                 | hta-live1.srv.uis.private.cam.ac.uk<br/>hta-live2.srv.uis.private.cam.ac.uk        |
|             | Database                                                                         | hta-live-db.srv.uis.private.cam.ac.uk      |
| Production (Poole Group) | [https://poolegroup.tissue.apps.cam.ac.uk/](https://poolegroup.tissue.apps.cam.ac.uk/)                 | poole-hta-live1.srv.uis.private.cam.ac.uk<br/>poole-hta-live2.srv.uis.private.cam.ac.uk        |
|             | Database                                                                         | poole-hta-live-db.srv.uis.private.cam.ac.uk      |
| Staging     | [https://hta-test.apps.cam.ac.uk/](https://hta-test.apps.cam.ac.uk/)  | hta-test1.srv.uis.private.cam.ac.uk<br/>hta-test2.srv.uis.private.cam.ac.uk |
|             | Database                                                                                        | devgroup-test-db.srv.uis.private.cam.ac.uk |
<!-- markdownlint-enable MD013 -->

## Source code

The source code for the HTA is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/hta/hta) | The source code for the main application server |

## Technologies used

The following gives an overview of the technologies the HTA is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web application | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | [Moa](https://confluence.uis.cam.ac.uk/display/SYS/Moa%3A+managed+database+service) |

## Operational documentation

The following gives an overview of how the HTA is deployed and maintained.

### Deploying a new release

New releases are deployed via
[Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment/).
Each server should be set to maintenance mode then the `run-ansible-playbook.sh` should be run
limited to the node in maintenance mode. e.g.

```bash
# Setup environment
source hta-production-setup

# On hta-live1
touch /maintenance_mode

# On client machine
./run-ansible-playbook.sh hta-production ihta-playbook.yml --diff --limit hta-live1

# On hta-live1
rm /maintenance_mode

# On hta-live2
touch /maintenance_mode

# On client machine
./run-ansible-playbook.sh -i hta-production hta-playbook.yml --diff --limit hta-live2

# On hta-live2
rm /maintenance_mode
```

### Monitoring

The HTA database is monitored by [nagios](https://nagios.uis.cam.ac.uk/nagios/).

There is also an [application status page](https://tissue.csx.cam.ac.uk/adm/status).

### Backups

See [legacy application backups](legacy-application-backups.md)

{{ service_management() }}

## System Dependencies

* [Raven](raven-ucamwebauth.md)
* [Moa](https://wiki.cam.ac.uk/ucs/Migrating_dev-group_databases_to_Moa)
