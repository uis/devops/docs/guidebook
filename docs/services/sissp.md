<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the HESA applications, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The SIS/SP provides repositories for extra staff information which can't be handled or stored in the
university HR system. The applications consist two separate repositories for college staff and
university staff.

## Service Status

The SIS/SP is currently live.

## Contact

Technical queries and support requests should be emailed to [UIS Helpdesk](mailto:servicedesk@uis.cam.ac.uk)
and will be picked up by a member of the team working on the service.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/hesa2021).

## Environments

The SIS/SP is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | <https://refsystems.admin.cam.ac.uk/staff> | |
|             | <https://refsystems.admin.cam.ac.uk/college> | |
|             | Web Server         | sp19-live-web1.blue.cam.ac.uk |
|             | Application Server | sp19-live-app1.blue.cam.ac.uk |
|             | Database           | sp19-live-db1.blue.cam.ac.uk |
| Staging     | <https://uat-refsystems.admin.cam.ac.uk/staff>    | |
|             | <https://uat-refsystems.admin.cam.ac.uk/college>    | |
|             | Web Server         | sp19-uat-web1.blue.cam.ac.uk |
|             | Application Server | sp19-uat-app1.blue.cam.ac.uk |
|             | Database           | sp19-uat-db1.blue.cam.ac.uk |
| Migration   |     | |
|             | SharePoint 2016    | sp19-dev2.blue.cam.ac.uk |
|             | SharePoint 2019    | sp19-dev1.blue.cam.ac.uk |

## Source code

The source code for the SIS/SP is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [SIS University](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/hesa2021/staff) | The source code for the SIS university staff |
| [SIS College](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/hesa2021/college) | The source code for the SIS college staff |

## Technologies used

The following gives an overview of the technologies the SIS/SP is built on.

<!-- markdownlint-disable MD013 -->
| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server and Client side code | SharePoint 2019, C#, InfoPath, JavaScript | Server-side Object Model |
<!-- markdownlint-enable MD013 -->

## Operational documentation

The following gives an overview of how the SIS/SP portals are deployed and maintained.

### How and where the SIS/SP are deployed

The portals are created with SharePoint sites and the server-side codes are deployed as
SharePoint features. The client-side HTML/JavaScript files are uploaded to portal asset library.

### Deploying a new release

The server-side codes are deployed using PowerShell script running on SharePoint servers. The
client-side files are uploaded to portal asset library manually.

{{ service_management() }}
