<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the RGEA, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The RGEA application is a Grails web application that provides PIs (Principal
Investigators) with an online "bank account" view of their grants. It shows,
using CUFS data:

* How much money has been spent
* What it was spent on
* How much is left
* How much would be left if the spending profile was modified via future commitments

## Service Status

The RGEA is currently **live**.

It is expected that the functionality provided by RGEA will be migrated as part
of the [Transforming Research Programme](https://universityofcambridgecloud.sharepoint.com/sites/RGA/SitePages/TRS-Transforming-Research-Support.aspx).

## Contact

Technical queries and support should be directed to <uis-devops-division@lists.cam.ac.uk> and will
be picked up by a member of the team working on the service. To ensure that you receive a response,
always direct requests to <uis-devops-division@lists.cam.ac.uk> rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/research/rgea/grant-expenditure/-/issues).

## Environments

The RGEA is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | <http://www.expenditure.admin.cam.ac.uk> | rgea-live1.srv.uis.private.cam.ac.uk |
| Staging     | <https://rgea-test.srv.uis.cam.ac.uk>    | rgea-test1.srv.uis.private.cam.ac.uk |

Unlike most systems, RGEA working on a single node and is not load balanced. Downtime should be planned
around [vulnerable periods](https://help.uis.cam.ac.uk/vulnerable-periods) and users should be informed.

## Source code

The source code for the RGEA is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/research/rgea/grant-expenditure) | The source code for the main application server |
| [GREB](https://gitlab.developers.cam.ac.uk/uis/devops/research/rgea/grant-expenditure-batch) | ETL Batch processor |
| [Ansible Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/research/rgea/ansible-infrastructure) | The Ansible infrastructure code for deploying the application server to on-prem hosts |
| [Cloud Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/research/rgea/cloud-infrastructure) | The Terraform infrastructure code for deploying the application server to GCP (not currently in use) |

## Technologies used

The following gives an overview of the technologies the RGEA is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Java 8 | Grails, Spring, Spring Batch |
| ETL (Greb) | Java 8 | Groovy |

## Operational documentation

The following gives an overview of how RGEA is deployed and maintained.

### How and where the RGEA is deployed

RGEA is deployed on a single node per environment and uses a on-host MySQL
instance for data caching.

### Deploying a new release

After tagging, new releases are deployed via
[Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/research/rgea/ansible-infrastructure). The
`run-ansible-playbook.sh` should be run to deploy new version. e.g.

```bash
# On client machine
eval $(op signin)
source ./rgea-live-setup
./run-ansible-playbook.sh -i rgea-live rgea-playbook.yml --diff
```

### Monitoring

RGEA is monitored by [nagios](https://nagios.uis.cam.ac.uk/nagios/). This checks that the status of
the home page returns a 200 result.

### Debugging

Further debugging instructions can be found on the [RGEA Wiki](https://uniofcam.atlassian.net/wiki/spaces/DEVS/pages/13993341/Research+Grant+Expenditure+Application+RGEA).

{{ service_management() }}
