---
title: Webauth to SAML2
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the legacy Raven webauth to SAML2 shim, describing its current
status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

This service provides a shim, allowing requests made to authenticate using the [legacy raven webauth
protocol](raven-ucamwebauth.md) to be authenticated using SAML2 instead. Sites not yet implementing
the SAML2 protocol can continue to use the existing webauth protocol, but the authentication request
will be passed to the [Raven Shibboleth](raven-shibboleth.md) instance to perform the actual
authentication.

Sites requiring raven-4-life (R4L) support must be registered with the [shibboleth metadata
app](raven-metadata.md) in order to keep that functionality.

## Service Status

The Webauth to SAML2 Shim is currently under active development.

## Contact

Technical queries and support should be directed to <servicedesk@uis.cam.ac.uk> and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to <servicedesk@uis.cam.ac.uk> rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues [here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/ucamwebauth-2-saml2/-/issues).

## Environments

The Webauth to SAML2 Shim is currently deployed to the following environments:

| Name        | Main Application URL | GCP Deployment |
| ----------- | -------------------- | -------------- |
| Production  | [https://webauth-shim.prod.raven-legacy.gcp.uis.cam.ac.uk/](https://webauth-shim.prod.raven-legacy.gcp.uis.cam.ac.uk/) | [Production Deployment](https://console.cloud.google.com/run/detail/europe-west2/webauth-shim/metrics?project=raven-legacy-prod-d23f54e6) |
| Staging | [https://webauth-shim.test.raven-legacy.gcp.uis.cam.ac.uk/](https://webauth-shim.test.raven-legacy.gcp.uis.cam.ac.uk/) | [Staging Deployment](https://console.cloud.google.com/run/detail/europe-west2/webauth-shim/metrics?project=raven-legacy-test-9cc6166d) |
| Development | [https://webauth-shim.devel.raven-legacy.gcp.uis.cam.ac.uk/](https://webauth-shim.devel.raven-legacy.gcp.uis.cam.ac.uk/) | [Development Deployment](https://console.cloud.google.com/run/detail/europe-west2/webauth-shim/metrics?project=raven-legacy-devel-27608283) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?project=raven-legacy-meta-f3c3e713)

## Notification channel(s) for environments

| Environment | Display name | Email |
| ----------- | ------------ | ----- |
| Production  | Raven Legacy - Wilson DevOps team email channel | **<devops-wilson@uis.cam.ac.uk>** |
| Staging     | Raven Legacy - Wilson DevOps team email channel | **<devops-wilson@uis.cam.ac.uk>** |

## Source code

The source code for the Webauth to SAML2 Shim is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/ucamwebauth-2-saml2) | The source code for the main application server |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/infrastructure) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies the Webauth to SAML2 Shim is built on.

| Category | Language | Framework(s) |
| -------- | -------- | ------------ |
| Web Application | Python 3.10 | Django 3.2 |

## Operational documentation

The following gives an overview of how the Webauth to SAML2 Shim is deployed and maintained.

### How and where the Webauth to SAML2 Shim is deployed

The main web application is a lightweight Django application, hosted by GCP Cloud Run. There
is an associated PostgreSQL database hosted by GCP Cloud SQL, which is used for storing cache
items only, and contains no critical persistent data.

The shim is deployed as part of the legacy raven infrastructure using Terraform (see [Source
Code](#source-code) above).

### Deploying a New Release

The `README.md` files in each of the source code repositories explain how to deploy the
Webauth to SAML2 Shim.

### Monitoring

The logs for the running shim instances can be found in the GCP console:

- [production](https://console.cloud.google.com/run/detail/europe-west2/webauth-shim/logs?project=raven-legacy-prod-d23f54e6)
- [staging](https://console.cloud.google.com/run/detail/europe-west2/webauth-shim/logs?project=raven-legacy-test-9cc6166d)
- [development](https://console.cloud.google.com/run/detail/europe-west2/webauth-shim/logs?project=raven-legacy-devel-27608283)

### Debugging

For debugging the deployed app see "Monitoring" above. For debugging locally the
[application `README.md`](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/ucamwebauth-2-saml2)
describes how the containerised app can be run.

### Other operational documentation

- [Webauth protocol](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/website/-/blob/master/waa2wls-protocol-4.1.txt)

{{ service_management() }}
