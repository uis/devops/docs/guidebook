# Our Services

--8<-- "snippets/log-incident.md"

The DevOps Division offers a number of services to the University which are listed in this site area.

- Browse **all services** or **services by team** below _(click a column header to reorder tables)_.
- Navigate by **service areas** in the sidebar _(to the left)_.
- Each service page documents key aspects of the service in a standard form.
- Services are grouped into [IT Portfolios](https://www.uis.cam.ac.uk/it-portfolios).

---

{{ tabbed_services_view() }}
