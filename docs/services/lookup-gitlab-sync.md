<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Lookup To GitLab Sync service, describing its current status, where
and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The Lookup To GitLab Sync service blocks and unblocks users in the GitLab University
Developers' Hub.

University users present in Lookup will be blocked in GitLab if they are cancelled in Lookup.
By the same token, they will be unblocked in GitLab if they were previously blocked by this
service, but are now no longer cancelled in Lookup.

Also, external users who's accounts have been inactive for 24 months will by blocked in GitLab.

## Service Status

The Lookup To GitLab Sync service is currently beta.

## Contact

Technical queries and support should be directed to [cloud@uis.cam.ac.uk](mailto:cloud@uis.cam.ac.uk)
and will be picked up by a member of the team working on the service. To ensure that you receive a
response, always direct requests to [cloud@uis.cam.ac.uk](mailto:cloud@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
`[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/lookup-gitlab-sync/faas)`.

## Environments

The `Lookup To GitLab Sync` is currently deployed to the following environments:

| Name        | Cloud Scheduler | URL                |
| ----------- | --------------- | ------------------ |
| Production  | [lookup-gitlab-sync-0](https://console.cloud.google.com/cloudscheduler?project=lgsync-prod-fcc24789) | [lookup-gitlab-sync](https://lookup-gitlab-sync-685393389588.europe-west2.run.app) |
| Staging     | [lookup-gitlab-sync-0](https://console.cloud.google.com/cloudscheduler?project=lgsync-test-b2c4b48b)| [lookup-gitlab-sync](https://lookup-gitlab-sync-272453387498.europe-west2.run.app) |
| Development | [lookup-gitlab-sync-0](https://console.cloud.google.com/cloudscheduler?project=lgsync-devel-1492c1e2) | [lookup-gitlab-sync](https://lookup-gitlab-sync-685393389588.europe-west2.run.app) |

## Source code

The source code for the Lookup To GitLab Sync is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/lookup-gitlab-sync/faas) | The source code for the main application server |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/lookup-gitlab-sync/infrastructure) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies the Lookup To GitLab Sync service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Python 3.11 | [ucam-faas](https://gitlab.developers.cam.ac.uk/uis/devops/lib/ucam-faas-python) |
| GCP deployment  | Terraform  1.7 | Google Cloud Platform |

## Operational documentation

The following gives an overview of how the Lookup To GitLab Sync is deployed and maintained.

### How and where the Lookup to Gitlab Sync service is deployed

Deployment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Deploying a new release

Making a new release of the application is done via [release
automation](site:explanations/git-gitlab/gitlab-release-automation).
In short: merged commits are collected together into a draft "next release" Merge Request.
When merged, a new release tag is pushed to the repository along with a Docker image
being pushed to Google's Artefact registry.

Deployment is done by:

1. Updating the deployment project's repository with any changes,
   including bumping the deployed web application release.
2. Using the "play" buttons in the CI pipeline to deploy to production when happy. (Deployments to
   **staging** happen automatically on merges to `main`.)

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

{{ service_management() }}
