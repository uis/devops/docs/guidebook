<!-- markdownlint-disable MD041 -->
{{ service_title() }}

## Service Description

The Cambridge University Reporter is the University's journal of official business and the primary
means through which official information and governance-related matters are conveyed by the
University to its members and the wider community.

_Ordinary_ issues of the Reporter are published weekly in term time with _Special_ issues published
at set times throughout the year.

## Service Status

The Reporter site is currently live.

## Contact

Technical queries and support should be directed to
[uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk) and will be picked
up by a member of the team working on the service.

Issues discovered in the service or new feature requests should be opened as [GitLab
issues](https://gitlab.developers.cam.ac.uk/uis/devops/governance-systems/reporter-and-statutes-and-ordinances).

Non-technical and end-user queries relating to pages and content should be directed to one of the
following:

The **University Draftsman** can comment on content issues:

- [Ceri Benton](mailto:ceri.benton@admin.cam.ac.uk)

All the pages are processed and maintained by the **Reporter Editors**:

- [Tara Grant](mailto:tara.grant@admin.cam.ac.uk)
- [Sadie Byrne](mailto:Sadie.Byrne@admin.cam.ac.uk)

## Environments

The Reporter is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | <https://www.admin.cam.ac.uk/reporter/2022-23/weekly/><br/><https://www.admin.cam.ac.uk/reporter/2022-23/special/> | web-rso-live1.srv.uis.private.cam.ac.uk<br/>web-rso-live2.srv.uis.private.cam.ac.uk |
| Staging / Development    | <http://dev.admin.internal.admin.cam.ac.uk/reporter/2022-23/weekly><br/><http://dev.admin.internal.admin.cam.ac.uk/reporter/2022-23/special> | web-rso-dev1.srv.uis.private.cam.ac.uk |
<!-- markdownlint-enable MD013 -->

**Note:** XML source files need to be manually added to development so they can be processed using
the xml converter to produce the static html files which are then edited, verified, and then copied
to production using various `bash` scripts located on the development server.

## Source code

The Reporter development server website source code can be found on
[GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/governance-systems/reporter-and-statutes-and-ordinances)
(under construction).

### Notable folders on development and production servers

The XML Converter is located on the development server:

- `web-rso-dev1:/home/webadmin/web/reporter/xml-converter`

The website is located on the development and production servers:

- `web-rso-*:/home/webadmin/web/global`
- `web-rso-*:/home/webadmin/web/reporter`

The scripts are located on the development server:

- `web-rso-dev1:/home/webadmin/scripts`

## Technologies used

XML Converter

- Java
- XML
- XSLT

Website

- Apache with ucam_webauth module
- CGI / Perl
- HTML, CSS, JS

## Operational documentation

The [original documentation](https://foswiki.admin.cam.ac.uk/bin/view/WebTeam/ReporterXML) is no
longer available. New documentation will be added here.

<!--
### How and where the `<service_name>` is deployed

`<documentation or links to external docs describing how and where the application is deployed>`

### Deploying a new release

`<documentation or links to external docs about how a new release is made and deployed>`

### Monitoring

`<documentation or links to external docs about monitoring and how alerts should be responded to>`

### Debugging

`<documentation or links to external docs describing how to debug both a deployed instance and
locally>`

-->
### Other operational documentation

The Reporter is laid out as a print publication using Adobe InDesign. XML is exported, which this
software then processes to produce the corresponding web pages automatically
([https://www.admin.cam.ac.uk/reporter/2022-23/weekly/](https://www.admin.cam.ac.uk/reporter/2022-23/weekly/)
etc.)

There is also an overarching Drupal element
([https://www.reporter.admin.cam.ac.uk](https://www.reporter.admin.cam.ac.uk)) which is maintained
separately.

All the pages are processed and maintained by the Reporter Editors.

{{ service_management() }}
