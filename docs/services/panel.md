<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of Panel, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Panel provides a discussion system for graduate funding, allowing departments to identify candidates
for admissions, funding and supervision to assess their potential and select those more suitable.

## Service Status

Panel is currently **live**.

Currently there is no planned roadmap for the future of this service;
however, the service is widely used to support departments with the graduate Applications
and there are regular updates made to the business logic.

## Contact

Technical queries and support should be directed to <mailto:uis-cloud@lists.cam.ac.uk> and will be
directed to [Martin Hunt](https://www.lookup.cam.ac.uk/person/crsid/meh37), who is the member of the
team who works on the service.

## Environments and Client

Panel has the following component:

- A web client used by departments to score and assess applicants in the system and to administer
  their users/applicants etc.

### Web

Authentication is global and partially governed through _Raven_. Raven protected sections are under
the URL Raven/. Panel has a database with approved users. Environment is decided by URL as listed
below.

The Panel web app is currently deployed to the following web environments:

| Name        | URL                                    | IIS Servers          |
| ----------- | -------------------------------------- | -------------------- |
| Production  | <https://panel.admin.cam.ac.uk/>       | Dreamer/Executive    |
| Staging     | <https://uatPanel.admin.cam.ac.uk/>    | Dreamer/Executive    |
| Development | localhost                              | localhost            |

## Source code

The source code for the Panel can be found at the following URL:

<https://dev.azure.com/meh37/panel>

The code is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [BEL](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/BEL) | Holds business objects |
| [BPL](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/BPL) |  Holds business processes; knits together BEL and DAL|
| [BuildProcessTemplates (Deprecated)](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/BuildProcessTemplates) | Not currently used |
| [ConsolePullDocuments](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/ConsolePullDocuments) | Used by Computer Lab, this pulls the CamSIS documents into one downloadable zip file they can source |
| [DAL](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/DAL) | Data access routines, for example a library to communicate by SQL server |
| [Database (Deprecated)](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/Database) | No longer used as requires a subscription from Redgate - use  [SQL](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/SQL) instead |
| [Documents](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/Documents) | A library that can hold documentation |
| [EDMSTestingApp](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/EDMSTestingApp) | A testing application for EDMS (Electronic Document Management System) - this is a bespoke library that accesses data related to applicants |
| [FormsTestApp (Deprecated)](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/FormsTestApp) | Deprecated project for an unused windows forms application |
| [packages](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/packages) | Holds the NuGet packages for the project |
| [SQL](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/SQL) | Holds SQL change scripts |
| [TestConsole (Deprecated)](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/TestConsole) | Deprecated library that holds console app for testing |
| [UnitTests](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/UnitTests) | Unit tests around Panel |
| [Website](https://meh37.visualstudio.com/Panel/_versionControl?path=%24/Panel/Website) | Holds source code for the website |

## Technologies used

The following gives an overview of the technologies Panel is built on.

| Category        | Language | Framework(s) |
| --------------- | -------- | ------------ |
| Database Server | MS SQL   | SQL Server 2019 |
| Web Code | VB.NET   | 4.6.1 |

## Operations

### How and where Panel is deployed

<!-- markdownlint-disable-next-line MD024 -->
### Web

It is important to note that this application does **not** use docker, rather it is directly
installed on the server.

| Server        | OS | IIS version |
| --------------- | -------- | ------------ |
| Dreamer | Windows Server 2012 R2   | 8.0 |
| Executive | Windows Server 2012 R2   | 8.0 |

The websites are set up locally on the E drive on those machines. To keep files in sync a DFS share
has been set up between the two servers that can be accessed here:

- `\\internal\support\IIS_Data`

### Deploying a new release

Web deployment is done through Visual Studio. Profiles have been set up to copy the web files and
can be deployed through a right click publish. UIS Windows Server Team are responsible for
monitoring the underlying machines.

Admin app deployment is done through copying files to the file server location.

{{ service_management() }}
