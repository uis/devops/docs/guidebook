<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page documents key information about the Google Workspace preferences app.

The preferences app allows any member of the University with a Raven account to
opt in to additional Google services not covered by the core Terms and
Conditions associated with our Google Workspace license.

Behind the scenes it manages membership of multiple "opt in" Google Groups which
each have one or more additional services enabled for them.

## Environments

- Production at <https://preferences.g.apps.cam.ac.uk/>
- Staging at <https://preferences.test.gworkspace.gcp.uis.cam.ac.uk/>
- Development at <https://preferences.devel.gworkspace.gcp.uis.cam.ac.uk/>

## Application repositories

- [Web
    application](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/preferences-webapp)
- [Terraform
    deployment](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/deploy)

## Deployment

Deployed via terraform using our usual deployment boilerplate.

## Current Status

Live

{{ service_management() }}
