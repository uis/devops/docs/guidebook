<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the University Student API, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The University Student API is a read-only REST API which allows access to identity information
related to students at the University. It exposes an easy-to-consume interface, allowing clients
to query students by student number (USN) or by affiliation (institution or academic plan). The API
exposes a view of data from the
[University's Student Management System (CamSIS)](https://www.camsis.cam.ac.uk/).

The University Student API queries data from CamSIS using an HTTP endpoint which exposes a stream of
[student entities in XML
form](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api/uploads/48ec5f320052d35d4eea8bf012511314/CamTech-CardFeedofStudents-260321-0836-4.pdf),
and caches data within GCP buckets to allow for effective querying by
[affiliation](https://gitlab.developers.cam.ac.uk/uis/devops/iam/student-api#aggregate-storage-population).

## Service Status

The University Student API is currently `alpha` and not under active development. New use-cases
and improvements for the service need to be well-understood and scoped alongside the considerations
below.

The API was initially developed as a means to allow the new
[University Card System](./card-management-system.md) to query data from CamSIS, with a view to
provide an API which can be re-used by other client applications. Going forward, it would be
preferable for such APIs to be developed and maintained as part of
[CamSIS](https://www.camsis.cam.ac.uk/) itself rather than as separate services outside of CamSIS.
Therefore, before significant improvements / changes are made to the University Student API, the
ability to expose this information directly from CamSIS should be assessed.

## Contact

Technical queries and support should be directed to
[uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/student-api/-/issues/new).

## Environments

The University Student API is currently deployed behind the [API Gateway](api-gateway.md):

| Name        | URL |
| ----------- | ------------------ |
| Production  | `https://api.apps.cam.ac.uk/university-student` |
| Staging     | `https://api.apps.cam.ac.uk/university-student-staging` |
| Development | `https://api.apps.cam.ac.uk/university-student-devel` |

The OpenAPI documentation for the production environment can be found on the
[API Gateway's developer portal](https://developer.api.apps.cam.ac.uk/docs/university-student/1/overview).

## Source code

The source code for the University Student API is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [API project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/student-api) | The source code for the API project |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/student_identity.tf) | The Terraform infrastructure code for deploying the API to GCP, part of the identity platform deployment |

## Technologies used

The following gives an overview of the technologies the University Student API is built on:

| Language   | Framework(s) |
| ---------- | ------------ |
| Python 3.9 | FastAPI      |

## Operational documentation

The following gives an overview of how the University Student API is deployed and maintained.

### How and where the University Student API is deployed

The API is deployed to [GCP Cloud Run](https://cloud.google.com/run) as part of the
[Identity Platform Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/)
project.

Secrets for each environment are held within
[1Password](https://uis-devops.1password.eu/vaults/vu6ddj3urnbbvduik3gy2zg6lu/allitems/awrqubijrzfjbpenep27e7og7a)
and bootstrapped using the
[`root-bootstrap` sub-project of the identity deployment project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/root-bootstrap/main.tf).

### Deploying a new release

A new release to staging and production can be triggered by running the
[pipeline of the default branch within GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/iam/student-api/-/pipelines/153913).

Deployments to staging and development can be triggered manually by triggering a
[pipeline within the deploy identity project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/pipelines/new)
with the following variables set:

```sh
DEPLOY_ENABLED=<development|staging>
STUDENT_IDENTITY_DOCKER_IMAGE=<gitlab registry url of image to deploy>
```

### Monitoring

The University Student API makes use of our
[GCP site monitoring module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring)
for monitoring of the production environment. This will send an alert to
<devops-alarms@uis.cam.ac.uk> if any of the deployments have non-transient uptime check failures.

Additionally, an alert is configured to fire if the API has been unable to refresh its cache of
data from CamSIS within the
[expected timeframe](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/student_identity_aggregate_population.tf).

Alerting policies and their status can be viewed within the
[GCP Identity Meta project](https://console.cloud.google.com/monitoring/alerting?project=identity-meta-12c86b54).

### Debugging

The [API's README](https://gitlab.developers.cam.ac.uk/uis/devops/iam/student-api#developer-quickstart)
includes information about running the service locally for debugging. Additionally, logs for the
production instance can be viewed through
[GCP's Cloud Run console](https://console.cloud.google.com/run/detail/europe-west1/student-identity/logs?project=identity-prod-60422764).

### Operational issues with CamSIS

The University Student API relies on a feed of data from CamSIS, and therefore it's uptime is
dependent on the uptime of CamSIS. Unexpected failure to query CamSIS should be raised with the
[CamSIS team using camsishelp@admin.cam.ac.uk](mailto:camsishelp@admin.cam.ac.uk).

{{ service_management() }}
