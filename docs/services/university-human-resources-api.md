<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the University Human Resources API, describing its current status,
where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The University Human Resources API is a read-only REST API which allows access to identity
information related to members of staff at the University. It exposes an easy-to-consume interface,
allowing clients to query students by staff number, CRSid, or by affiliation (institution). The
API exposes a view of data from
[University HR system (CHRIS)](https://www.hrsystems.admin.cam.ac.uk/systems/systems-overview/chris-hr-system).

The University Human Resources API exposes data from a CHRIS datamart view, details of how data
is transformed can be found on the
[project's readme](https://gitlab.developers.cam.ac.uk/uis/devops/iam/staff-identity-api#data-abstraction).

## Service Status

The University Human Resources API is currently `alpha` and not under active development. New use-cases
and improvements for the service need to be well-understood and scoped alongside the considerations
below.

The API was initially developed as a means to allow the new
[University Card System](./card-management-system.md) to query data from CHRIS, with a view to
provide an API which can be re-used by other client applications.

## Contact

Technical queries and support should be directed to
[uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk) and will be picked
up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [uis-devops-division@lists.cam.ac.uk](mailto:uis-devops-division@lists.cam.ac.uk)
rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/staff-identity-api/-/issues/new).

## Environments

The University Human Resources API is currently deployed behind the [API Gateway](api-gateway.md):

| Name        | URL |
| ----------- | ------------------ |
| Production  | `https://api.apps.cam.ac.uk/university-human-resources` |
| Staging     | `https://api.apps.cam.ac.uk/university-human-resources-staging` |
| Development | `https://api.apps.cam.ac.uk/university-human-resources-devel` |

The OpenAPI documentation for the production environment can be found on the
[API Gateway's developer portal](https://developer.api.apps.cam.ac.uk/docs/university-human-resources/1/overview).

## Source code

The source code for the University Human Resources API is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [API project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/staff-identity-api) | The source code for the API project |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/staff_identity.tf) | The Terraform infrastructure code for deploying the API to GCP, part of the identity platform deployment |

## Technologies used

The following gives an overview of the technologies the University Human Resources API is built on:

| Language   | Framework(s)                                            |
| ---------- | ------------------------------------------------------- |
| Python 3.9 | FastAPI, cx_Oracle to allow querying the Oracle DB view |

## Operational documentation

The following gives an overview of how the University Human Resources API is deployed and maintained.

### How and where the University Human Resources API is deployed

The API is deployed to [GCP Cloud Run](https://cloud.google.com/run) as part of the
[Identity Platform Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/)
project.

Secrets for each environment are held within
[1Password](https://uis-devops.1password.eu/vaults/vu6ddj3urnbbvduik3gy2zg6lu/allitems/g6mql5rqpojg6wxrjulbhutane)
and bootstrapped using the
[`root-bootstrap` sub-project of the identity deployment project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/root-bootstrap/main.tf).

### Deploying a new release

A new release to staging and production can be triggered by running the
[pipeline of the default branch within GitLab](https://gitlab.developers.cam.ac.uk/uis/devops/iam/staff-identity-api/-/pipelines/153883).

Deployments to staging and development can be triggered manually by triggering a
[pipeline within the deploy identity project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/pipelines/new)
with the following variables set:

```sh
DEPLOY_ENABLED=<development|staging>
STAFF_IDENTITY_DOCKER_IMAGE=<gitlab registry url of image to deploy>
```

### Monitoring

The University Human Resources API makes use of our
[GCP site monitoring module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring)
for monitoring of the production environment. This will send an alert to
<devops-alarms@uis.cam.ac.uk> if any of the deployments have non-transient uptime check failures.

Alerting policies and their status can be viewed within the
[GCP Identity Meta project](https://console.cloud.google.com/monitoring/alerting?project=identity-meta-12c86b54).

### Debugging

The [API's README](https://gitlab.developers.cam.ac.uk/uis/devops/iam/staff-identity-api#developer-quickstart)
includes information about running the service locally for debugging. Additionally, logs for the
production instance can be viewed through
[GCP's Cloud Run console](https://console.cloud.google.com/run/detail/europe-west1/staff-identity/logs?project=identity-prod-60422764).

### Operational issues with Datamart view

The University Human Resources API an Oracle Datamart view which exposes data from CHRIS.
Unexpected failure to query this view should be raised with the
[CHRIS team using chrishelpdesk@admin.cam.ac.uk](mailto:chrishelpdesk@admin.cam.ac.uk).

{{ service_management() }}
