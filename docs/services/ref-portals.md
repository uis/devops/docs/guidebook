<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the REF portals, describing their current status, where and
how they are developed and deployed, and who is responsible for maintaining them.

## Service Description

The REF portals contain documents and data for REF 2021 submissions.

## Service Status

The users currently have read-only access to the REF portals.

## Contact

Issues discovered in the portals should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/ref2021).

## Environments

The REF portals are currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| [Production](https://refsystems.admin.cam.ac.uk/)  |  |  |
|             | Web Server         | sp19-live-web1.blue.cam.ac.uk |
|             | Application Server | sp19-live-app1.blue.cam.ac.uk |
|             | Database           | sp19-live-db1.blue.cam.ac.uk |

## Source code

Source code for the REF portals can be found in the Gitlab group [REF 2021](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/ref2021):

| Gitlab Project | Summary |
|---------------------------------------------------------------------------------------|-----------------------------------------------------|
| [Impact](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/ref2021/impact)           | Impact Submission                                   |
| [Environment](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/ref2021/env)         | Environment Templates Submission                              |
| [Legacy Files](https://gitlab.developers.cam.ac.uk/uis/dotnet/ref/ref2021/legacyfiles) | Legacy files Submission |

## Technologies used

The REF portals are built on the following technologies:

- C#
- JavaScript
- SharePoint server-side object model

## Operational documentation

The SharePoint solution for each portal is deployed using a PowerShell script
 found in its `SiteAssets` folder.

To deploy one of the solutions:

- Copy the solution file and installer script to the target Server, e.g. `SP19-Live-Web1`
- Start PowerShell with admin privilege
- Run the installer script

{{ service_management() }}
