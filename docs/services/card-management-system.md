<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Card Management System, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

!!! warning
    This service as a whole is currently in active development and at **beta stability**.

## Service Description

The Card Management System is a replacement for the existing
[legacy card database](./legacy-card-db.md). The Card Management System will allow the management
of university cards - from the creation and printing of university cards, through to the export
of data for use by downstream systems.

## Service Status

The Card Management System as a whole is currently in `beta`, with many of the components of the
service under active development. Details of the project can be found within the
[project page on the UIS site](https://www.uis.cam.ac.uk/it-portfolios/projects/card-system-replacement).

## Contact

Technical queries and support should be directed to
[universitycard-dev@uis.cam.ac.uk](mailto:universitycard-dev@uis.cam.ac.uk) and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [universitycard-dev@uis.cam.ac.uk](mailto:universitycard-dev@uis.cam.ac.uk)
rather than reaching out to team members directly.

Queries about the project or functionality of the Card Management System should be raised as
[GitLab issues in the feedback repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/feedback).

Technical questions or problems with the Card API should be raised as
[GitLab issues in the Card API repository](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api).

Queries about data or operational issues such as card replacements should be directed to the card
office at [universitycard@admin.cam.ac.uk](mailto:universitycard@admin.cam.ac.uk).

## Architecture

The Card Management system is made up of the following components:

* [The Card Frontend](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-frontend)
    * a React-based frontend application used by card representatives and card office staff to
    view card details, print cards, and in future order new cards and revoke existing cards.
* [The Printer Client](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/printer-client)
    * a Python and Java CLI application which interfaces with the Card API to allow cards to be
    printed by the card office, making use of a wrapper around the SDK for the Zebra Series 7
    printers currently in use for card printing.
* [The Card Client](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-client) -
    a Python CLI to allow export of card details for institutions across the University, mainly
    used to populate access systems which require a CSV feed of active card details.
* [The Card API](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api) -
    a Django application which acts as our single source of truth for card details, allowing
    querying of card data, printing of cards, raising card requests and revoking cards.
* [The Photo API](https://gitlab.developers.cam.ac.uk/uis/devops/iam/identity-photos-service/photo-api)
    * a Django application which acts as our single source of truth for 'approved' photos of members
    of the University, used to fetch photos which appear on University cards.
* [Card Sync Tool](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/sync-tool) -
    a Python CLI tool which is used to sync card and photo data from the
    [legacy card database](./legacy-card-db.md) to the Card and Photo APIs.

All components are deployed to GCP within the
[Identity Platform Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity)
project. The [Card Sync Tool](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/sync-tool)
is run on a
[GitLab Scheduled pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/pipeline_schedules)
defined under the
[`legacy_data_sync`](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/tree/master/legacy_data_sync)
directory.

## Deployment

| Environment Name | Component     | URL                                                     |
| ---------------- | ------------- | ------------------------------------------------------- |
| Production       | Card Frontend | <https://card.apps.cam.ac.uk>                             |
| Production       | Card API      | <https://api.apps.cam.ac.uk/card/>                        |
| Production       | Photo API     | <https://api.apps.cam.ac.uk/photo/>                       |
| Staging          | Card Frontend | <https://card-frontend.test.identity.gcp.uis.cam.ac.uk/>  |
| Staging          | Card API      | <https://api.apps.cam.ac.uk/card-test/>                   |
| Staging          | Photo API     | <https://api.apps.cam.ac.uk/photo-test/>                  |
| Development      | Card Frontend | <https://card-frontend.devel.identity.gcp.uis.cam.ac.uk/> |
| Development      | Card API      | <https://api.apps.cam.ac.uk/card-dev/>                    |
| Development      | Photo API     | <https://api.apps.cam.ac.uk/photo-dev/>                   |

API documentation for the production version of the Card API and Photo API can be found on the API
Gateway's developer portal:

* [Card API documentation](https://developer.api.apps.cam.ac.uk/docs/card/1/overview),
* [Photo API documentation](https://developer.api.apps.cam.ac.uk/docs/photo/1/overview),

Currently the `Production` instances of the Photo and Card API sync data from the `CARD`
[legacy card database](./legacy-card-db.md). The `Staging` instances of the Photo and Card API
sync data from the development `CARD_T` Oracle database, and the `Development` instances sync
a limited set of data from the `CARD_T` Oracle database. The frequency of data sync is defined
by the sync configuration held within the
[deployment project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/legacy_data_sync.tf#L3).

## Technologies used

The following gives an overview of the technologies the Card Management System is built on.

| Category | Language | Primary framework |
| -------- | -------- | --------- |
| Card Frontend | TypeScript | React |
| Printer Client | Python / Java | pyscard / Zebra Printer SDK |
| Card Client | Python | Requests |
| Card API | Python | Django |
| Photo API | Python | Django |
| Sync Tool | Python | Oracle_cx |

## Operational documentation

The following gives an overview of how the Card Management System is deployed and maintained.

### How and where the Card Management System is deployed

The infrastructure for the Card Management System is deployed through the Terraform defined
within the [Deploy Identity](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity)
project. The Card API, Photo API and Card Frontend are deployed via GitLab pipelines, and will be
automatically deployed to staging on the success of each master pipeline
([see pipeline configuration here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api/-/blob/master/.gitlab-ci.yml#L26.)),
with production deployment a manual step in the pipeline once staging has succeeded.

### Deploying a new release

Deploying any infrastructure changes can be completed using
[Logan](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan)
as described within the
[Deploy Identity project's readme](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/README.md).

Deploying a new release of the Card API, Photo API or card frontend to staging and then production
can be completed by running the
[master pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-api/-/pipelines)
within the project's repo. Changes must be tested in staging before deploying to production.

### Monitoring

The Card API, Photo API and Card Frontend make use of our
[GCP site monitoring module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-site-monitoring)
for monitoring of the production environment. This will send an alert to
<devops-alarms@uis.cam.ac.uk> if any of the deployments have non-transient uptime check failures.

### Authentication and Authorization

The card management system enforces authentication and authorization using a number of different
methods.

#### Service-to-service Card and Photo API authorization

The [Card](https://developer.api.apps.cam.ac.uk/docs/card/1/overview) and
[Photo](https://developer.api.apps.cam.ac.uk/docs/photo/1/overview) allow users of the API Gateway
to request access to the API backends. Granting access to an application within the API Gateway
will allow an application to query the APIs, but will only allow them to see entities that they
own - i.e. photos or cards which are attached to the identifier given to that application within
the API Gateway. This will likely mean that they will not be able to see any cards or photos. See
`Card and Photo API permission model` below to determine how to grant wider access to applications.

!!! important
    The API Gateway is only used for authentication for the Card and Photo APIs and therefore
    allowing an application to access either API does not imply that this application can access
    or make changes to any card records.

#### End-user authentication in the Card Frontend

The [Card Frontend](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-frontend)
authenticates uses against the API Gateway using the
[Auth Code with PKCE flow](https://auth0.com/docs/get-started/authentication-and-authorization-flow/authorization-code-flow-with-proof-key-for-code-exchange-pkce).
This allows the API Gateway to identify the individual who is using the Card Frontend and pass
this information to the Card and Photo APIs. This also allows the Card Frontend to use the identity
token provided by the API Gateway to display basic information about the user. The code which
handles the authentication flow can be found
[here](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-frontend/-/blob/main/src/api-gateway/api-gateway-service.ts).

#### Card and Photo API permissions model

The Card and Photo API's permissions models rely on a specification which documents the
applications, people or groups of people who are granted certain levels of access. Both
permissions specifications can be found in the deploy identity project:

* [Card permissions spec](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/card_permissions.tf),
* [Photo permissions spec](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity/-/blob/master/photo_permissions.tf),

The most commonly granted permission are `CARD_DATA_READER` for the Card API and `PHOTO_VIEWER`
for the Photo API, which allow a person or application to view all cards and all photos. These
permissions should only be granted to applications registered by institutional staff, computer
officers or other IT staff.

The permissions specifications are updated using terraform in the same way as any other part of the
infrastructure, as documented in the
[deployment project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/deploy-identity#bootstrapping)

#### Card Frontend permissions model

The Card Frontend honours the permissions model enforced by the Card and Photo APIs by only
displaying relevant actions to users with a given permission
(e.g. [hiding the 'queue for printing' button if the user is not able to create cards](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-frontend/blob/main/src/card-requests/CardRequestPage.tsx#L673).
Additionally, the Card Frontend currently requires that all users have the
[`CARD_DATA_READER` permission](https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/card-frontend/blob/main/src/admin-layout/CardAdminLayout.tsx#L27),
without this permission a user is shown an error screen informing them that do not have access.

## Additional documentation

Information about the overall architecture of the new Card Management System can be found
[within the wiki page for the identity project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/documentation/-/wikis/card/Architecture).

{{ service_management() }}
