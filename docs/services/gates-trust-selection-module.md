<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the GCT/SM service, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The GCT/SM service is a collection of web applications for GCT to manage
the annual scholarship selection process.

The GCT/SM service includes:

* An admin portal
* A scholarship interview survey
* A scholarship acceptance form.

## Service Status

The GCT/SM service is currently live.

## Contact

Issues discovered in the service or new feature requests should be opened as issues in the
[GitLab GCT/SM repository](https://gitlab.developers.cam.ac.uk/uis/devops/gct).

## Environments

The GCT/SM is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  |  | |
|             | Web Server         | spt-live-web1.internal.cam.ac.uk |
|             | Database Server    | STU-SQL1.blue.cam.ac.uk\Live |
| Staging     |  | |
|             | Web Server         | spt-test-web1.internal.cam.ac.uk |
|             | Database Server    | STU-SQL1.blue.cam.ac.uk\UAT |

## Source Code

The source code for the GCT/SM service is in the [GitLab GCT/SM group](https://gitlab.developers.cam.ac.uk/uis/devops/gct).

## Technologies Used

The GCT/SM is built on the following technologies:

* ASP.NET 4
* C# 5
* SQL Server

{{ service_management() }}
