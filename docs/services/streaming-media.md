<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Streaming Media Service, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

!!! warning
    This service is being decommissioned. Please use alternative services for publishing new content
    (see the [SMS closure news article](https://help.uis.cam.ac.uk/news/sms-future-feb2021) for more
    information).

## Service Description

The Streaming Media Service (SMS) provides a video and audio transcoding and hosting service.

## Service Status

The Streaming Media Service is currently live/decommissioning.

- [Webpage with details of closure](https://help.uis.cam.ac.uk/news/sms-future-feb2021)

## Contact

Technical queries and support should be directed to
[sms-support@uis.cam.ac.uk](mailto:sms-support@uis.cam.ac.uk) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[sms-support@uis.cam.ac.uk](mailto:sms-support@uis.cam.ac.uk) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/groups/uis/devops/sms/-/issues).

## Environments

The Streaming Media Service is currently deployed to the following environments:

<!-- markdownlint-disable MD013 -->
| Name        | URL                                  | Supporting VMs                                     |
| ----------- | ------------------------------------ | -------------------------------------------------- |
| Production  | [https://sms.cam.ac.uk](https://sms.cam.ac.uk) | sms-node1.sms.cam.ac.uk (web application)          |
|             |                                      | sms-node2.sms.cam.ac.uk (web application)          |
|             |                                      | smew.sms.csx.cam.ac.uk (downloads)                 |
|             |                                      | goldeneye.sms.csx.cam.ac.uk (downloads)            |
|             |                                      | sms-nfs1.sms.private.cam.ac.uk (ceph->nfs gateway) |
|             |                                      | sms-nfs2.sms.private.cam.ac.uk (ceph->nfs gateway) |
|             |                                      | ruddy.sms.cam.ac.uk (nfs server)               |
|             |                                      | pintail.sms.cam.ac.uk (nfs server)             |
|             |                                      | pochard.sms.cam.ac.uk (nfs server)             |
|             |                                      | transcode1.sms.private.cam.ac.uk (transcoding)     |
|             |                                      | transcode2.sms.private.cam.ac.uk (transcoding)     |
|             |                                      | transcode3.sms.private.cam.ac.uk (transcoding)     |
|             |                                      | transcode4.sms.private.cam.ac.uk (transcoding)     |
|             |                                      | sms-live-db.srv.uis.private.cam.ac.uk (DB) provided by [Moa](https://wiki.cam.ac.uk/ucs/Migrating_dev-group_databases_to_Moa)     |
<!-- markdownlint-enable MD013 -->

## Source code

The source code for the Streaming Media Service is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/sms/sms)     | The source code for the main application server       |
| [Transcode worker](https://gitlab.developers.cam.ac.uk/uis/devops/sms/smsworker) | The source code for the transcode worker              |
| [Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/sms/ansible)              | Ansible infrastructure code for some management tasks |
<!-- markdownlint-enable MD013 -->

## Technologies used

The following gives an overview of the Streaming Media Service is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web application | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | 10.5 |

## Operational documentation

The following gives an overview of how the Streaming Media Service is deployed and maintained.

### How and where the Streaming Media Service is deployed

The Streaming Media Service application is deployed using RPM packages. These are built by Gitlab CI
in the [Application Server repository](https://gitlab.developers.cam.ac.uk/uis/devops/sms/sms).
These can be copied to the [Bes RPM package repository](https://bes.csi.cam.ac.uk) run by
infra-sas@uis by following these
[instructions](https://wiki.cam.ac.uk/ucs/Bes_-_Managed_Installation_Service#How_to_add_SLES_10.2F11_or_RHEL_.28S.29RPMS_to_Bes).
As a last resort RPMs can be manually copied and installed.

### Deploying a new release

Once the RPM packages are added to the [Bes RPM package repository](https://bes.csi.cam.ac.uk)
installation is simply a matter of running `zypper up` as root.

### Monitoring

The streaming Media Service is monitored by [nagios](https://nagios.uis.cam.ac.uk/nagios/)
There is also a [status page](https://sms.cam.ac.uk/adm/status) that checks the various components
of the service and returns 200 if everything is functioning normally.

### Backups

See [legacy application backups](legacy-application-backups.md)

### Other Documentation

- [UCS Wiki](https://wiki.cam.ac.uk/ucs/SMS_-_Streaming_Media_Service)
- [Gitlab wiki](https://gitlab.developers.cam.ac.uk/uis/devops/sms/docs/wikis/Legacy-SMS-Docs)

{{ service_management() }}

## System Dependencies

- [Lookup](lookup.md)
- [Raven](raven-ucamwebauth.md)
- [Moa](https://wiki.cam.ac.uk/ucs/Migrating_dev-group_databases_to_Moa)

## Contact Addresses

- <sms-support@uis.cam.ac.uk>
