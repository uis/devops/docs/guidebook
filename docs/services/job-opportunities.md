<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Job Opportunities, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Welcome to the University of Cambridge's Job Opportunities pages. Here you can view our current
vacancies and those of many Cambridge colleges and affiliated institutions. You can also find all of
the information you need about working for one of the world's oldest and most successful
universities.

## Service Status

The Job Opportunities is currently live.

## Contact

The primary contacts for the Job Opportunities are [Simon
Virr](https://www.lookup.cam.ac.uk/person/crsid/sav25) and [Andrew
Rowland](https://www.lookup.cam.ac.uk/person/crsid/ar972).

The service manager is [Andrew Crook](https://www.lookup.cam.ac.uk/person/crsid/ajc322)

Issues discovered in the service or new feature requests should be opened as [GitLab issues in the
application
repository](https://gitlab.developers.cam.ac.uk/uis/devops/hr/job-opp/job-opp-webapp).

## Environments

The Job Opportunities is currently deployed to the following environments:

| Name        | URL                |
| ----------- | ------------------ |
| Production  | [https://www.jobs.cam.ac.uk/](https://www.jobs.cam.ac.uk/) |
| Staging     | [https://webapp.test.job-opp.gcp.uis.cam.ac.uk/](https://webapp.test.job-opp.gcp.uis.cam.ac.uk/)   |
| Development | [https://webapp.devel.job-opp.gcp.uis.cam.ac.uk/](https://webapp.devel.job-opp.gcp.uis.cam.ac.uk/) |

## Source Code

The source code for the Job Opportunities is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/hr/job-opp/job-opp-webapp) | The source code for the main application server |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/hr/job-opp/job-opp-infrastructure) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies Used

The following gives an overview of the technologies the Job Opportunities is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Application | PHP | N/A |

### How and where the Job Opportunities application is deployed

Deployment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Deploying a new release

Making a new release of the application is done via [release
automation](site:explanations/git-gitlab/gitlab-release-automation).
In short: each commit to the web app's main branch builds and pushes a Docker image to GCR.
An MR can then be raised within the infrastructure repository to deploy a given image by SHA.
Once merged, the main GitLab pipeline will allow deployment to production via the "play" buttons
in the CI pipeline. An automated deployment to staging will be completed as part of this pipeline.

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

{{ service_management() }}
