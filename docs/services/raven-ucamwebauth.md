---
title: UCamWebAuth
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Raven UCamWebAuth service, describing its current
status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Service Description

The Raven service provides a self-service, web-based interactive sign in service
for the University. It has several parts. Raven UCamWebAuth provides the most commonly
used interface for sites around the University.

## Service Status

The Raven UCamWebAuth service is currently live. There are plans to decommission
this protocol in favour of the modern industry standard protocols (SAML, OAuth2).

## Contact

Technical queries and support should be directed to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) and will be
picked up by a member of the team working on the service. To ensure that you
receive a response, always direct requests to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues in the
[Raven UcamWebauth](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/webauth)
or [Raven Legacy Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/infrastructure)
projects (both DevOps only).

## Environments

Raven UCamWebAuth is currently deployed to the following environments:

| Name        | Main Application URL | GCP Project      |
| ----------- | -------------------- | ---------------- |
| Production  | [https://raven.cam.ac.uk/](https://raven.cam.ac.uk/) | [Raven Legacy - production](https://console.cloud.google.com/home/dashboard?project=raven-legacy-prod-d23f54e6) |
|             | [https://webauth.prod.raven-legacy.gcp.uis.cam.ac.uk](https://webauth.prod.raven-legacy.gcp.uis.cam.ac.uk) | |
| Staging     | [https://test.legacy.raven.cam.ac.uk/](https://test.legacy.raven.cam.ac.uk/) | [Raven Legacy - staging](https://console.cloud.google.com/home/dashboard?project=raven-legacy-test-9cc6166d) |
|             | [https://webauth.test.raven-legacy.gcp.uis.cam.ac.uk/](https://webauth.test.raven-legacy.gcp.uis.cam.ac.uk/) | |
| Development | [https://dev.legacy.raven.cam.ac.uk/](https://dev.legacy.raven.cam.ac.uk/) | [Raven Legacy - development](https://console.cloud.google.com/home/dashboard?project=raven-legacy-devel-27608283) |
|             | [https://webauth.devel.raven-legacy.gcp.uis.cam.ac.uk/](https://webauth.devel.raven-legacy.gcp.uis.cam.ac.uk/) | |

All environments access a meta project
([Raven Legacy meta](https://console.cloud.google.com/home/dashboard?project=raven-legacy-meta-f3c3e713))
for shared secrets and monitoring.

## Notification channel(s) for environments

| Environment | Display name | Email |
| ----------- | ------------ | ----- |
| Production  | Raven Legacy - Wilson DevOps team email channel | **<devops-wilson@uis.cam.ac.uk>** |
| Staging     | Raven Legacy - Wilson DevOps team email channel | **<devops-wilson@uis.cam.ac.uk>** |

## Source code

Source code for Raven UCamWebAuth is spread over the following repositories:

<!-- markdownlint-disable MD013 -->
| Repository | Description |
| ---------- | ----------- |
| [Raven Legacy WebAuth Server](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/webauth)<sup>1</sup> | Containerised Apache2 frontend which handles interactive authentication |
| [Raven Legacy Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/infrastructure)<sup>1</sup> | Terraform configuration for infrastructure and deployment |
<!-- markdownlint-enable MD013 -->

<sup>1</sup> DevOps only

## Technologies used

The following gives an overview of the technologies that Raven UCamWebAuth is built
on.

| Category       | Language   | Framework(s) |
| -------------- | ---------- | ------------ |
| Server         | Perl       | Mason        |
| GCP deployment | Terraform  |              |
| Admin API      | Python     | FastAPI      |

## Operational documentation

There is a dedicated [operational documentation
folder](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/tree/master/doc)
in the [infrastructure Gitlab
project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/infrastructure)
(DevOps only).

### Admin scripts

An
[admin-scripts](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/webauth/-/tree/master/admin-script-wrapper)
container also provides restricted API access for management actions.

### How and where the service is deployed

The Raven Legacy infrastructure is deployed using Terraform, with releases
of the authenticator application deployed by the GitLab CD pipelines associated
with the [infrastructure Gitlab project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/infrastructure)
(DevOps only).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to
deploy the App.

### Monitoring

The monitoring and alerting system is based on [Cloud Monitoring](https://cloud.google.com/monitoring).
Alert policies and metrics can be viewed in the
[Raven Legacy meta project](https://console.cloud.google.com/monitoring/alerting?project=raven-legacy-meta-f3c3e713)
(DevOps only).

Our standard 'webapp' alerts have been configured:

* Service uptime check from various geographic regions
* SSL expiry checks
* Sign-in alerts if logins fail (or succeed when they should fail)

### Debugging

See the [Raven UcamWebauth](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/raven/webauth)
project (DevOps only) for details on how to deploy a local development instance.

{{ service_management() }}
