---
title: GitLab
---

<!-- markdownlint-disable MD041 -->
{{ service_title() }}

## Service Description

This page gives an overview of the GitLab service for the University
Developers' Hub.`, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

!!! note
    The Developers' Hub is a University-wide project to foster a sense of
    community among software developers.

## Service Status

The GitLab service is currently live.

## Contact

Issues discovered in the service or new feature requests should be opened as GitLab issues
on the [support project](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs).

## Environments

The GitLab service is currently deployed to the following environments:

The production GitLab service is available at
[gitlab.developers.cam.ac.uk](https://gitlab.developers.cam.ac.uk/).

Test environments are spun up on demand to test changes to the deployment. More
information is available in the [deployment project
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/deploy/blob/master/docs/bootstrap.md)
(DevOps only).

## Source code

The source code for the GitLab service is spread over the following repositories:

- [Deployment repository](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-deploy).

## Technologies used

The following gives an overview of the technologies the GitLab service is built on.

| Category | Language |
| -------- | -------- |
| Server | Ruby |
| Deployment | Helm |

## Operational documentation

The following gives an overview of how the GitLab service is deployed and maintained.

### Monitoring

Monitoring for GitLab is via StackDriver and is configured by the deploy terraform mostly from the
[monitoring](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-deploy/-/tree/master/environment/monitoring)
module

### Debugging

Debugging is mostly done using the development instance of GitLab

### Deploying a new release

Deploying a new release, usually for dev/testing is documented in the
[deploy](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-deploy/-/blob/master/docs/workspaces.md)
repo.

### User facing docs

- [User-facing
    documentation](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/wikis/FAQs).
- [User support
    project](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/)

{{ service_management() }}
