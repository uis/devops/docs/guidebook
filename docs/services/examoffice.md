<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the exam time monitoring application, describing its current status,
where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The exam time monitoring tool enables Exam Office to check time spent on exams against any extra
allowance given due to accessibilities.

## Service Status

The exam time monitoring is currently live.

## Contact

Technical queries and support should be directed to `<role email>` and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to `<role email>` rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/dotnet/ExamOffice).

## Environments

The exam time monitoring is a VBA app and currently deployed to run on Exam Office staff's PCs.

## Source code

The source code for the exam time monitoring is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Exam time monitoring](https://gitlab.developers.cam.ac.uk/uis/dotnet/ExamOffice/ExamPaperVBA) | The source code for exam time monitoring application |

## Technologies used

The following gives an overview of the technologies the Exam Time Monitoring is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Client App | VBA |  |

## Operational documentation

The following gives an overview of how the exam time monitoring app is deployed and maintained.

### How and where the exam time monitoring is deployed

The app is an Excel file with macro code. The deployed file is uploaded to shared repository for
users to download and run locally.

### Deploying a new release

The new release is uploaded to the shared repository and notice users to download and discard
existing version.

### Monitoring

N/A

### Debugging

N/A

{{ service_management() }}
