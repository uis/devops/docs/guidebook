<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of Project Symphony, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Project Symphony is a project to host a high performance static website. The static website will be
provided by [Chameleon Studios](https://www.chameleonstudios.co.uk/).

## Service Status

Project Symphony is currently **pre-release**. The project will become live at some point in the
future.

## Contact

Technical queries and support should be directed to <cloud@uis.cam.ac.uk> and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to <cloud@uis.cam.ac.uk> rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/project-symphony/cloud-infrastructure/-/issues).

## Environments

Project Symphony is currently deployed from Cloud Storage in the following environments:

| Name        | URL                | Supporting Bucket  |
| ----------- | ------------------ | --------------- |
| Production  | TBA | project-symphony-web_assets-production-9d56ae1c |
| Staging     | TBA | project-symphony-web_assets-staging-5b422c80 |
| Development | TBA | project-symphony-web_assets-development-0dbc9efd |

Access to the buckets is public via a Cloud CDN and Cloud Loadbalancer. Certificates are managed by
GCP.

## Source code

The source code for the `<service_name>` is spread over the following repositories:

| Repository  | Description |
| ----------- | ------------------ |
| [Application](https://gitlab.developers.cam.ac.uk/uis/devops/project-symphony/symphony) | The static website is provided by a [3rd Party](https://chameleonstudios.co.uk) |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/project-symphony/cloud-infrastructure) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies Project Symphony is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Website | HTML/JS | Wordpress/[Staatic](https://staatic.com/) |
| Deployment | Terraform | Google Cloud Platform |

## Operational documentation

The following gives an overview of how Project Symphony is deployed and maintained.

### How and where the Project Symphony is deployed

<https://gitlab.developers.cam.ac.uk/uis/devops/project-symphony/cloud-infrastructure/-/blob/main/README.md?ref_type=heads>

### Deploying a new release

New release are control by a 3rd Party by uploading to the appropriate Cloud Storage Bucket.

### Monitoring

Monitoring is configured as per our standard [Google monitoring module](https://gitlab.developers.cam.ac.uk/uis/devops/project-symphony/cloud-infrastructure/-/blob/main/monitoring.tf?ref_type=heads).

{{ service_management() }}
