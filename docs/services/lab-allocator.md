<!-- markdownlint-disable MD041 -->
{{ service_title() }}

This page gives an overview of the Lab Allocator service.

## Service Description

The Lab Allocator service allows administrators to allocate students to subject lectures
and practicals within the Natural Sciences Tripos. It potentially replaces the
[Legacy NST IA Lab Allocation](legacy-nst-ia-labs.md),
which does the same thing but takes 3 days (human tweaking and allocation runs) to
come up with a valid allocation. The existing system also has some rigidity to it, that would
make adding new features difficult.

This Lab Allocator service aims to solve the performance issue whilst introducing a more
flexible architecture.

## Service Status

This service is currently in development.

## Contact

Issues with the service should be raised on the [corresponding GitLab
project](https://gitlab.developers.cam.ac.uk/uis/devops/laboratory-allocator)
(University members only).

## Environments

The Lab Allocator is hosted on GCP.

## Source code

These are the two relevant repos:

* <https://gitlab.developers.cam.ac.uk/uis/devops/laboratory-allocator/laballoc-infrastructure>
* <https://gitlab.developers.cam.ac.uk/uis/devops/laboratory-allocator/lab-allocator>

## Technologies used

The following gives an overview of the technologies the Lab Allocator is built on.

| Category          | Language  | Framework(s)          |
|-------------------|-----------|-----------------------|
| API               | Python    | Django REST framework |
| Genetic Algorithm | Python    | PyGAD                 |
| Testing           | Python    | PyTest                |
| Database          | SQL       | PostgreSQL            |
| Deployment        | terraform | N/A                   |

## Deployment

Deployment is via Gitlab using the
[laballoc-infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/laboratory-allocator/laballoc-infrastructure)
pipelines.

## Documentation

See the following README documents:

* <https://gitlab.developers.cam.ac.uk/uis/devops/laboratory-allocator/laballoc-infrastructure/-/blob/main/README.md>
* <https://gitlab.developers.cam.ac.uk/uis/devops/laboratory-allocator/lab-allocator/-/blob/main/README.md>

{{ service_management() }}
