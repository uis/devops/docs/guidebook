# Static data

This directory contains static data used by the guidebook.

## Labels

The `labels.yml` file contains GitLab label definitions for the [uis/devops
group](https://gitlab.developers.cam.ac.uk/uis/devops) in GitLab. It was
generated using the following command line which uses
[httpie](https://httpie.io/) and [yq](https://mikefarah.gitbook.io/yq/):

<!-- markdownlint-disable MD013 -->
```sh
http "https://gitlab.developers.cam.ac.uk/api/v4/groups/uis%2Fdevops/labels?per_page=100" "PRIVATE-TOKEN:$TOKEN" | yq eval -P > labels.yml
```
<!-- markdownlint-enable MD013 -->

You should arrange for the `TOKEN` environment variable to contain a [GitLab
personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
with at least the `read_api` scope.
