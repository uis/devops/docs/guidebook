---

portfolios:
  business:
    title: Business
  education:
    title: Education
  generic:
    title: Generic Services
  research:
    title: Research

service_categories:
  business:
    title: Business Systems
  cloud:
    title: Cloud and Platforms
  education:
    title: Education Services
  finance:
    title: Finance
  generic:
    title: Generic
  hr:
    title: HR
  iam:
    title: Identity and Access Management (IAM)
  intranet:
    title: Intranet Services
  other:
    title: Other
  postgraduate:
    title: Postgraduate
  shared:
    title: Shared Web Tech
  recruitment:
    title: Recruitment
  research:
    title: Research Admin
  teaching:
    title: Teaching and Learning
  undergraduate:
    title: Undergraduate
  web:
    title: Web Services


# Each entry here is of the form:
#
# <team-key>:
#   title: <team-title>
#   shorthand: <team-shorthand>
#   email: <team-email>
#   lead: <team-lead>
#
# Only title & email are mandatory.
teams:
  cloud:
    title: Cloud Team
    email: cloud@uis.cam.ac.uk
    lead: ad2139
    lookup_id: "105531"
    portfolio: generic
  devex:
    title: Developer Experience Team
    shorthand: DevEx
    email: devops-devex@uis.cam.ac.uk
    lead: rjw57
    portfolio: generic
  drupal:
    title: Drupal Team
    email: drupal@uis.cam.ac.uk
    lead: jag212
    portfolio: generic
  hamilton:
    title: Hamilton Team
    email: devops-hamilton@uis.cam.ac.uk
    lead: bs461
    portfolio: education
  holberton:
    title: Holberton Team
    email: devops-holberton@uis.cam.ac.uk
    lead: wgd23
    portfolio: business
  hopper:
    title: Hopper Team
    email: devops-hopper@uis.cam.ac.uk
    lead: bs461
    portfolio: education
  jackson:
    title: Jackson Team
    email: devops-jackson@uis.cam.ac.uk
    lead: nb745
    portfolio: generic
  johnson:
    title: Johnson Team
    email: devops-johnson@uis.cam.ac.uk
    portfolio: research
  lovelace:
    title: Lovelace Team
    email: devops-lovelace@uis.cam.ac.uk
    lead: snr21
    portfolio: education
  wilson:
    title: Wilson Team
    email: devops-wilson@uis.cam.ac.uk
    lead: ek599
    portfolio: generic

# Each entry here is of the form:
#
# <service-key>:
#   title: <service-page-title>
#   product-manager: <crs-id>
#   service-owner: <crs-id>
#   service-manager: <crs-id>
#   tech-lead: <crs-id>
#   teams:
#     - <team-key>
#     - ...
#   engineers:
#     - <operational engineer crs-id>
#     - ...
#
# The '<service-key>' must match a filename in 'docs/services'
#   e.g. 'my-service:' implies 'docs/services/my-service.md' exists.
#
# Any referenced '<team-key>' must be defined in the 'teams' section below.
#
# The only mandatory sub-key is 'title'.
services:

  acp-12-submission-portal:
    category: hr
    title: Academic Career Pathways Grade 12 (ACP-12) Submission Portal
    service-owner: drb58
    teams:
      - holberton
    engineers:
      - jl364
      - wgd23
      - prb34
      - az330

  activate-account:
    category: iam
    title: Activate Account Service
    service-owner: nrt33
    teams:
      - wilson

  ada:
    category: business
    title: ADA for Disability Office
    service-owner: meh37
    teams:
      - jackson

  api-gateway:
    category: cloud
    title: API Gateway
    tech-lead: rjg21
    teams:
      - cloud
      - devex
      - wilson
    engineers:
      - dkh21

  budget-calculator:
    category: research
    title: Budget Calculator
    tech-lead: meh37
    teams:
      - johnson

  caias:
    category: recruitment
    title: Colleges and Affiliated Institutions Advertising System (CAIAS)
    service-manager: ajc322
    teams:
      - holberton
    engineers:
      - wgd23

  camdata:
    category: education
    title: Cambridge Data (CamData)
    teams:
      - jackson
    engineers:
      - dkh21
      - jag212
      - jl364
      - mh2215

  card-management-system:
    category: iam
    title: Card Management System
    tech-lead: av603
    teams:
      - wilson
    engineers:
      - rjg21
      - sdw37
      - dkh21

  claims-management-system:
    category: research
    title: Claims Management System (CMS)
    tech-lead: meh37
    teams:
      - johnson

  covid-pooled-testing:
    category: other
    title: Covid-19 Testing Pools Management
    service-owner: jh535
    service-manager: rjw57
    tech-lead: rjg21
    engineers:
      - sdw37
      - si202

  data-backup:
    category: cloud
    title: Data Backup Service
    service-owner: amc203
    service-manager: ad2139
    tech-lead: rh841
    teams:
      - cloud
    engineers:
      - rh841
      - si202
      - rk725
      - du228
      - sdw37

  # Outlier - page lists "project leads" and "work-stream lead"
  design-system:
    category: other
    title: Design System
    tech-lead: rp431
    engineers:
      - rp431

  digital-pooling:
    category: undergraduate
    title: Digital Pooling
    product-manager: bmd42
    service-owner: hr244
    service-manager: soj23
    team-lead: ek599
    tech-lead: amh254
    teams:
      - hopper
    engineers:
      - hwtb2
      - jb2595
      - pfk22

  web-platform:
    category: web
    title: Web Platform (Drupal)
    teams:
     - drupal

  eduroam-tokens:
    category: iam
    title: Network Access Tokens Service
    service-owner: rcf34
    tech-lead: prb34
    teams:
      - wilson
    engineers:
      - si202
      - sdw37
      - ad2139

  equipshare:
    category: research
    title: Equipment Sharing
    teams:
      - johnson
    engineers:
      - ao475

  essm-sync:
    category: teaching
    title: Education Space Scheduling and Modelling Sync
    service-manager: nik22
    tech-lead: rjg21
    teams:
      - hamilton

  events:
    category: shared
    title: Events System
    teams:
      - jackson

  examoffice:
    category: education
    title: Exam Office exam time monitoring
    engineers:
      - jl364

  gaobase:
    category: postgraduate
    title: Graduate Admissions Course Database (GAOBase)
    teams:
      - lovelace
    engineers:
      - ad2139
      - rh841
      - rk725
      - sdw37
      - si202

  gates-trust:
    category: other
    title: Gates Trust Admin Tool
    tech-lead: meh37
    teams:
      - jackson

  gates-trust-selection-module:
    category: other
    title: Gates Cambridge Trust - Selection Module (GCT/SM)
    tech-lead: jl364
    teams:
      - jackson
    engineers:
      - jl364

  gitlab:
    category: cloud
    title: GitLab service for the Developers' Hub
    service-owner: amc203
    service-manager: rjw57
    tech-lead: rjw57
    teams:
      - cloud
      - devex
    engineers:
      - ad2139
      - rh841
      - si202
      - rk725
      - du228
      - sdw37

  governance:
    category: intranet
    title: Governance
    teams:
      - jackson
    engineers:
      - dj257
      - jl364

  gsuite:
    category: cloud
    title: Google Workspace for Education @ Cambridge
    teams:
      - wilson
    engineers:
      - rjg21

  gsuite-preferences:
    category: iam
    title: Google Workspace Preferences App
    service-owner: amc203
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - rjw57
      - rjg21

  hr-integration-app:
    category: hr
    title: HR Integration App
    service-manager: ajc322
    tech-lead: az330
    teams:
      - holberton
    engineers:
      - prb34
      - du228
      - wgd23

  hta:
    category: other
    title: Human Tissue Tracker
    tech-lead: prb34
    teams:
      - holberton
    engineers:
      - si202
      - sdw37
      - ad2139
      - prb34

  iam-overview:
    category: iam
    title: Identity and Access Management
    teams:
      - wilson

  information-asset-register:
    category: business
    title: Information Asset Register
    service-owner: jak59
    teams:
      - jackson
    engineers:
      - rjg21

  integrated-reward-system:
    category: hr
    title: Integrated Reward System (IRS)
    service-manager: ajc322
    tech-lead: prb34
    teams:
      - holberton
    engineers:
      - az330
      - wgd23
      - av603

  interfacer:
    category: research
    title: Interfacer for Research Systems Team
    tech-lead: meh37
    teams:
      - johnson

  jackdaw:
    category: iam
    title: Jackdaw
    service-owner: vkhs1
    service-manager: ukd20
    teams:
      - wilson
    engineers:
      - rjw57
      - rjg21
      - ad2139
      - sdw37
      - si202

  job-opportunities:
    category: recruitment
    title: Job Opportunities
    service-owner: et305
    service-manager: ajc322
    tech-lead: wgd23
    teams:
      - holberton
    engineers:
      - av603

  lab-allocator:
    category: teaching
    title: Lab Allocator
    teams:
      - hamilton
    engineers:
      - ek599
      - db947

  lecture-capture-preferences:
    category: teaching
    title: Lecture Capture Preferences App
    teams:
      - hamilton
    service-manager: amc203
    engineers:
      - rjw57
      - rjg21

  legacy-application-backups:
    category: other
    title: Legacy application backups

  legacy-card-db:
    category: iam
    title: Legacy Card System
    tech-lead: av603
    teams:
      - wilson
    engineers:
      - rjg21
      - sdw37
      - si202

  legacy-nst-ia-labs:
    category: teaching
    title: Legacy NST IA Lab Allocation

  lookup-gitlab-sync:
    category: cloud # ..maybe iam in future
    title: Lookup To GitLab Sync
    service-owner: amc203
    service-manager: ad2139
    tech-lead: rh841
    engineers:
      - rh841
      - si202
      - rk725
      - du228
      - sdw37

  lookup:
    category: iam
    title: Lookup
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - ad2139
      - dkh21
      - sdw37
      - si202

  malware-and-virus-scanner:
    category: other
    title: Malware & Virus Scanner (MVS)
    tech-lead: mh2215
    teams:
      - jackson

  my-cambridge-application:
    category: undergraduate
    title: My Cambridge Application
    service-owner: hr244
    product-manager: bmd42
    service-manager: soj23
    team-lead: ek599
    tech-lead: amh254
    teams:
      - hopper
    engineers:
      - hwtb2
      - jb2595
      - pfk22

  nursery-waiting-list:
    category: generic
    title: Nursery Waiting List
    tech-lead: meh37
    teams:
      - jackson

  panel:
    category: postgraduate
    title: Panel
    tech-lead: meh37
    teams:
      - lovelace

  panopto-deployment:
    category: teaching
    title: Panopto Deployment

  passwords:
    category: iam
    title: Password Management
    service-owner: vkhs1
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - ad2139
      - si202
      - prb34
      - sdw37

  postbox:
    category: hr
    title: PostBox
    service-manager: ajc322
    tech-lead: wgd23
    teams:
      - holberton
    engineers:
      - av603

  raven-metadata:
    category: iam
    title: Shibboleth Metadata Administration service
    service-owner: vkhs1
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - amc203

  raven-oauth:
    category: iam
    title: Raven OAuth2
    service-owner: vkhs1
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - amc203
      - rjg21
      - sdw37

  raven-saml2-shim:
    category: iam
    title: Raven Webauth to SAML2 Shim
    service-owner: vkhs1
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - rjg21
      - mk2155

  raven-shibboleth:
    category: iam
    title: Raven SAML2
    service-owner: vkhs1
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - amc203
      - rjg21
      - sdw37
      - si202

  raven-stats:
    category: iam
    title: Raven Device Statistics API
    teams:
      - wilson

  raven-ucamwebauth:
    category: iam
    title: Raven UCamWebAuth
    service-owner: vkhs1
    tech-lead: rjg21
    teams:
      - wilson
    engineers:
      - amc203
      - rjg21
      - sdw37

  rco-database:
    category: research
    title: RCO Database
    tech-lead: meh37
    teams:
      - johnson

  recruitment-administration-system:
    category: recruitment
    title: Recruitment Administration System (RAS)
    service-manager: ajc322
    tech-lead: az330
    teams:
      - holberton
    engineers:
      - prb34
      - du228
      - wgd23

  ref-portals:
    category: research
    title: Research Excellence Framework (REF) portals
    teams:
      - johnson
    engineers:
      - jl364

  regent-house-ballots:
    category: generic
    title: Regent House Ballots application
    service-owner: cb765
    service-manager: ajc322
    teams:
      - jackson
    engineers:
      - rjw57

  reporter:
    category: intranet
    title: Reporter
    teams:
      - jackson
    engineers:
      - mh2215

  research-dashboard:
    category: research
    title: Research Dashboard
    service-owner: dgb26
    teams:
      - johnson

  rgea:
    category: research
    title: Research Grant Expenditure Application (RGEA)
    service-owner: weer2
    service-manager: weer2
    tech-lead: ad2139
    teams:
      - johnson
    engineers:
      - snr21
      - si202

  ronin:
    category: cloud
    title: RONIN
    service-owner:  amc203
    service-manager: jpk28
    teams:
      - cloud
    engineers:
      - sdw37

  roo-intranet:
    category: research
    title: ROO Intranet
    tech-lead: meh37
    teams:
      - johnson

  scribe:
    category: other
    title: SCRIBE
    tech-lead: meh37
    teams:
      - jackson

  self-service-gateway:
    category: cloud
    title: Self-Service Gateway
    teams:
      - cloud
    engineers:
      - im530

  service-template:
    category: other
    title: service-template

  sissp:
    category: research
    title: Staff Information System SharePoint portals (SIS/SP)
    tech-lead: jl364
    teams:
      - johnson
    engineers:
      - jl364
      - dj257

  software-sales: # retired
    category: generic
    title: Software Sales

  staff-information-system:
    category: research
    title: Staff Information System
    service-manager: meh37
    tech-lead: meh37
    teams:
      - johnson

  statutes-and-ordinances:
    category: intranet
    title: Statutes and Ordinances
    tech-lead: mh2215
    teams:
      - jackson
    engineers:
      - mh2215

  streaming-media:
    category: cloud
    title: Streaming Media Service
    service-owner: amc203
    service-manager: si202
    tech-lead: si202
    teams:
      - cloud

  symphony:
    category: other
    title: Project Symphony
    service-owner: amc203
    service-manager: amc203
    tech-lead: ad2139

  talks-cam:
    category: shared
    title: talks.cam
    service-manager: ad2139
    teams:
      - jackson

  temporary-employment-service:
    category: hr
    title: Temporary Employment Service (TES)
    service-owner: hmd26
    service-manager: ajc322
    tech-lead: az330
    teams:
      - holberton
    engineers:
      - prb34
      - du228
      - wgd23

  time-allocation-survey:
    category: hr
    title: Time Allocation Survey
    tech-lead: nb745
    teams:
      - jackson
    engineers:
      - nb745

  tls-certificates: # retired
    category: generic
    title: TLS Certificates
    service-owner: amc203
    service-manager: rjg21

  toast:
    category: postgraduate
    title: TOAST
    tech-lead: meh37
    teams:
      - lovelace

  university-human-resources-api:
    category: iam
    title: University Human Resources API
    teams:
      - wilson

  university-student-api:
    category: iam
    title: University Student API
    tech-lead: av603
    teams:
      - wilson

  ups-service-definition:
    category: finance
    title: University Payment System
    service-owner: sav25
    service-manager: ajc322
    teams:
      - holberton
    engineers:
      - az330
      - wgd23
      - du228

  utbs:
    category: hr
    title: University Training Booking System
    service-owner: mg356
    service-manager: mg356
    tech-lead: prb34
    teams:
      - holberton
    engineers:
      - si202
      - ad2139
      - wgd23

  web-recruitment-system:
    category: recruitment
    title: Web Recruitment System (WRS)
    service-manager: ajc322
    tech-lead: az330
    teams:
      - holberton
    engineers:
      - prb34
      - du228
      - wgd23
