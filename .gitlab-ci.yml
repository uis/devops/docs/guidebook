include:
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "/pre-commit.yml"
    ref: v6.7.2
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "/auto-devops-stages.yml"
    ref: v6.7.2

  # Disable MR pipelines
  - template: Workflows/Branch-Pipelines.gitlab-ci.yml

# Basic CI to install and run mkdocs build in strict mode with verbose output.
# We upload the built documentation as an artifact so that it can be browsed
# from the build jobs.
test:
  stage: test
  image: registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:alpine
  services:
    - name: plantuml/plantuml-server:jetty
      alias: plantuml
  before_script:
    - apk add python3-dev gcc musl-dev
    - pip install -r requirements.txt
  script:
    # We use --no-directory-urls so that the documentation is browsable from the
    # GitLab artefact browser.
    - mkdocs build --strict --verbose --site-dir public --no-directory-urls
    # Expose services.yml for consumption by other apps/services.
    - mkdir -p public/api
    - cp data/services.yml public/api/
    # This is a horrible hack to preserve legacy URLs from RTD. Unfortunately symlinks aren't
    # supported in GitLab pages which would be a nicer solution.
    - cp -r public latest
    - mkdir -p public/en
    - mv latest public/en/
    - git tag -l | while read tag; do git checkout $tag && mkdocs build --strict --verbose --site-dir public/en/$tag --no-directory-urls ; done
    - git checkout master
  artifacts:
    expose_as: "Browse the documentation for this MR by following links to public and index html"
    paths:
      - public/

markdownlint:
  stage: test
  image:
    # Keep this version number in sync with the one in .pre-commit-config.yaml
    name: davidanson/markdownlint-cli2:v0.17.2
    entrypoint: [""]
  script:
    - cp mdlint-formatter.jsonc .markdownlint-cli2.jsonc
    - markdownlint-cli2 "**/*.md" "#node_modules"
  artifacts:
    reports:
      junit: markdown-lint.xml


pages:
  # Just like the "test" build except we enable directory URLs.
  extends: test
  stage: production
  script:
    - mkdocs build --strict --verbose --site-dir public
    # Expose services.yml for consumption by other apps/services.
    - mkdir -p public/api
    - cp data/services.yml public/api/
    # This is a horrible hack to preserve legacy URLs from RTD. Unfortunately symlinks aren't
    # supported in GitLab pages which would be a nicer solution.
    - cp -r public latest
    - mkdir -p public/en
    - mv latest public/en/
    - git tag -l | while read tag; do git checkout $tag && mkdocs build --strict --verbose --site-dir public/en/$tag --no-directory-urls ; done
    - git checkout master
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG
